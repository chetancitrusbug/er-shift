@extends('layouts.frontendDashboard')

@section('title',trans('jobs.label.jobs'))
@section('content')
<style>
.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
padding: 12px 12px;}

</style>
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <h1>@lang('jobs.label.jobs')</h1>
                </div>
            </div>
            <div class="box-content bg-white">
                <!--<a href="{{ url('/jobs') }}" title=@lang('jobs.label.back')><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('jobs.label.back')</button></a>-->

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif


                @if($sample != '')
                {{-- <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal"> <i class="fa fa-download" aria-hidden="true"></i> @lang('jobs.label.sample_csv')</button> --}}
                <a href={{$sample}} title=@lang('jobs.label.sample_csv')><button class="btn btn-warning"><i class="fa fa-download" aria-hidden="true"></i>@lang('jobs.label.sample_csv')</button></a>
                <a href='{{ url('downloadcsv') }}' title=@lang( 'jobs.label.bulk_job_data')><button class="btn btn-warning"><i class="fa fa-download" aria-hidden="true"></i>@lang('jobs.label.bulk_job_data')</button></a>
                @endif

                <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Download sample csv format for multiple jobs</h4>
                        </div>
                        <div class="modal-body">
                            <div>
                                <h4>To upload multiple shifts please use the format below. We highly encourage you to have the pay rate. You will get more inquiries.</h4>
                            </div>
                            <div class="pull-left">
                                <a href={{$sample}} title=@lang('jobs.label.sample_csv')><button class="btn btn-info">@lang('jobs.label.sample_csv')</button></a>

                                <a href='{{ url('/downloadcsv') }}' title=@lang('jobs.label.bulk_job_data')><button class="btn btn-success">@lang('jobs.label.bulk_job_data')</button></a>
                            </div>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    </div>
                </div>

                {!! Form::open(['url' => '/store-jobs', 'class' => 'form-horizontal','method'=>'POST','enctype' => 'multipart/data','files'=>'true']) !!}

                <div class="form-group margin-tb">
                    {!! Form::label('file',trans('jobs.label.upload_file'), ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::file('file', null, ['class' => 'form-control file-upload', 'required' => 'required']) !!}
					</div>
                </div>

                <h5>Download sample csv format for multiple jobs</h5>
                <h5>To upload multiple shifts please use the format below. We highly encourage you to have the pay rate. You will get more inquiries.</h5>
                <br><br>

                <h3>Profession , Speciality & Degree</h3>
                <div class="table-responsive">
                    <table class="table table-borderless" id="job-table">
                        <thead>
                            <tr>
                                <th>@lang('jobs.label.profession')</th>
                                <th>@lang('jobs.label.speciality')</th>
                                <th>@lang('jobs.label.degree')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($shiftData as $item)
                            <tr>
                                <th>{{ (isset($item['profession']) ? $item['profession'] : '') }}</th>
                                <th>{{ implode(', ',$item['speciality']) }}</th>
                                <th>{{ implode(', ',$item['degree']) }}</th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <br><br>
                <b> <p style="color: red;">Max Job Rate is {{ (isset($jobMaxRate) ? $jobMaxRate : '0') }} </p></b>
                <br><br>
                <h3>CSV format</h3>
                <div class="table-responsive">
                    <table class="table table-borderless" id="job-table">
                        <thead>
                            <tr>
                                <th>@lang('jobs.label.job_title')</th>
                                <th>@lang('jobs.label.job_responsibility')</th>
                                <th>@lang('jobs.label.profession')</th>
                                <th>@lang('jobs.label.speciality')</th>
                                <th>@lang('jobs.label.degree')</th>
                                <th>@lang('jobs.label.experience')</th>
                                <th>@lang('jobs.label.address')</th>
                                <th>@lang('jobs.label.location')</th>
                                <th>@lang('jobs.label.ed_volume')</th>
                                <th>@lang('jobs.label.dates')</th>
                                <th>@lang('jobs.label.shift')</th>
                                <th>@lang('jobs.label.rate')</th>
                            </tr>
                        </thead>
                        @if(isset($job))
                        <tbody>
                            <tr>
                                <th>{{ $job->job_title }}</th>
                                <th>{{ $job->job_responsibility }}</th>
                                <th>{{ (isset($job->profession->title) ? $job->profession->title : '') }}</th>
                                <th>{{ (isset($job->speciality->title) ? $job->speciality->title : '') }}</th>
                                <th>{{ (isset($job->degree->title) ? $job->degree->title : '') }}</th>
                                <th>{{ (isset($job->experience->title) ? $job->experience->title : '') }}</th>
                                <th>{{ $job->address }}</th>
                                <th>{{ (isset($job->location->name) ? $job->location->name : '') }}</th>
                                <th>{{ (isset($job->edvolume->title) ? $job->edvolume->title : '') }}</th>
                                <th>mm/dd/yyyy|12/25/2018</th>
                                <th>{{ (isset($job->shift->title) ? $job->shift->title : '') }}</th>
                                <th>{{ $job->rate }}</th>
                            </tr>
                        </tbody>
                        @endif
                    </table>
                </div>
                <div class="form-group">
                    <div class="col-md-3">
                        {!! Form::submit(trans('jobs.label.upload'), ['class' => 'btn btn-primary btn-submit']) !!}
						<a href="{{ url('/jobs') }}" ><button type="button" class="btn btn-danger btn-cancel">Cancel</button></a>
					</div>
					<!--<div class="col-md-3">
						@if($sample != '')
						<a href={{$sample}} title=@lang('jobs.label.sample_csv')><button class="btn btn-warning btn-xs"><i class="fa fa-download" aria-hidden="true"></i>@lang('jobs.label.sample_csv')</button></a>
						@endif
					</div>-->
                </div>



            </div>
        </div>
    </div>
</div>
@endsection