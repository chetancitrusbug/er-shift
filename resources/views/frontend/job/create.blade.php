@extends('layouts.frontendDashboard')
@section('title',trans('jobs.label.add_jobs'))
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <h1>@lang('jobs.label.add_jobs')</h1>
                </div>
                <div class="actions">
                    
                </div>
            </div>
            <div class="box-content panel-body">
                <!--<a href="{{ url('/jobs') }}" title=@lang('jobs.label.back')><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('jobs.label.back')</button></a>-->
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    {!! Form::open(['url' => '/jobs', 'class' => 'form-horizontal','id'=>'formjobs']) !!}
                        @include ('frontend.job.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
