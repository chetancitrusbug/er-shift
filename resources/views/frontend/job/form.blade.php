<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="https://momentjs.com/downloads/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<style>
select#rate { display: none; }
.rate_value { float: right; color:red;font-weight: bold; margin: -20px -50px 0px 0px;}
</style>
<div class="form-group{{ $errors->has('job_title') ? ' has-error' : ''}}">
    {!! Form::label('job_title', trans('jobs.label.job_title'), ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('job_title', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('job_title', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('job_responsibility') ? ' has-error' : ''}}">
    {!! Form::label('job_responsibility',trans('jobs.label.job_responsibility'), ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('job_responsibility', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('job_responsibility', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('profession_id') ? 'has-error' : ''}}">
    {!! Form::label('profession_id', trans('jobs.label.profession'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
            {!! Form::select('profession_id',$profession,null, ['class' => 'form-control', 'required' => 'required']) !!} {!! $errors->first('profession_id','<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('speciality_id') ? 'has-error' : ''}}">
    {!! Form::label('speciality_id',trans('jobs.label.speciality'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
            {!! Form::select('speciality_id',(isset($speciality) ? $speciality : ['' =>'Select Speciality']),null, ['class' => 'form-control', 'required' => 'required'] ) !!} {!! $errors->first('speciality_id','<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('experience_id') ? 'has-error' : ''}}">
    {!! Form::label('experience_id', trans('jobs.label.experience'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
            {!! Form::select('experience_id',$experience,null, ['class' => 'form-control', 'required' => 'required']) !!} {!! $errors->first('experience_id','<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('location_id') ? 'has-error' : ''}}">
    {!! Form::label('location_id', trans('jobs.label.location'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
            {!! Form::select('location_id',$location,null, ['class' => 'form-control', 'required' => 'required']) !!} {!! $errors->first('location_id','<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('degree_id') ? 'has-error' : ''}}">
    {!! Form::label('degree_id',trans('jobs.label.degree'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
            {!! Form::select('degree_id',$degree,null, ['class' => 'form-control', 'required' => 'required']) !!} {!! $errors->first('degree_id','<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('ed_volume_id') ? 'has-error' : ''}}">
    {!! Form::label('ed_volume_id',trans('jobs.label.ed_volume'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
            {!! Form::select('ed_volume_id',$ed_volume,null, ['class' => 'form-control', 'required' => 'required']) !!} {!! $errors->first('ed_volume_id','<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('shift_id') ? 'has-error' : ''}}">
    {!! Form::label('shift_id', trans('jobs.label.shift'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
            {!! Form::select('shift_id',$shift,null, ['class' => 'form-control', 'required' => 'required']) !!} {!! $errors->first('shift_id','<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('address') ? ' has-error' : ''}}">
    {!! Form::label('address', trans('jobs.label.address'), ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('address', null, ['class' => 'form-control']) !!}
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('rate') ? 'has-error' : ''}}">
    {!! Form::label('rate', trans('jobs.label.rate'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('rate', $jobMaxRateArray ,null,['required' => 'required','class' => '','id' => 'rate']) !!}

        {!! $errors->first('rate','<p class="help-block with-errors">:message</p>') !!}

        {!! Form::label('rate_value', (isset($job->rate) ? $job->rate: 1), ['class' => 'control-label rate_value']) !!}

    </div>
</div>

<div class="form-group{{ $errors->has('dates') ? ' has-error' : ''}}">
    {!! Form::label('dates',  trans('jobs.label.dates'), ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('dates', '', ['class' => 'dates date form-control', 'required' => 'required']) !!}
        {!! $errors->first('dates', '<p class="help-block">:message</p>') !!}
    </div>
</div>

{{-- <div class="form-group{{ $errors->has('to_date') ? ' has-error' : ''}}">
    {!! Form::label('to_date', trans('jobs.label.to_date') , ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('to_date', null, ['class' => 'to_date date form-control', 'required' => 'required']) !!}
        {!! $errors->first('to_date', '<p class="help-block">:message</p>') !!}
    </div>
</div> --}}

<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status',  trans('jobs.label.status'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('status',['' =>'Select Status', '1'=>trans('jobs.label.active'),'0'=>trans('jobs.label.inactive')] ,null, ['required' => 'required','class' => 'form-control']) !!} {!! $errors->first('status','<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-2 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('jobs.label.create'), ['class' => 'btn btn-primary btn-submit']) !!}
		<a href="{{ url('/jobs') }}" >
            <button type="button" class="btn btn-danger btn-cancel">Cancel</button>
        </a>
    </div>
</div>

@push('js')
<script>

    var today = new Date(); today.setDate(today.getDate() + 1);
    $('.dates').datepicker({ multidate: true , format: 'yyyy-mm-dd', startDate: today });


    @if(isset($job->dates) && $job->dates != null)
        var arr = new Array();
        @foreach ($job->dates as $key => $item)
            arr.push(new Date("{{$item}}"));
        @endforeach
        $('.dates').datepicker('setDates', arr);
    @endif


    $( function() {
        var select = $( "#rate" );
        var slider = $( "<div id='slider'></div>" ).insertAfter( select ).slider({
        min: 1,
        max: '{{ count($jobMaxRateArray) - 1 }}',
        class:'range',
        range: "min",
        value: select[ 0 ].selectedIndex + 1,
        slide: function( event, ui ) {
            select[ 0 ].selectedIndex = ui.value;
            $('.rate_value').text(ui.value + 1);
        },
        //   change: function( event, ui ) {
        //       $('.rate_value').text(ui.value);
        //     }
        });
        $( "#rate" ).on( "change", function() {
        slider.slider( "value", this.selectedIndex + 1 );
        });
    });
$(function() {
    $(document).on('change', '#profession_id', function (e) {
        var id = $('#profession_id').val();
        if(id != '')
        {
            var url ="{{ url('/getSpeciality/') }}";
            url = url + "/" + id;
            $.ajax({
                type: "get",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                        data = JSON.parse( data );
                        var select = $('#speciality_id');
                        select.empty();
                        select.append('<option value="">Select Speciality</option>');
                        $.each(data,function(key, value)
                        {
                            select.append('<option value=' + key + '>' + value + '</option>');
                        });
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
        else
        {
            var select = $('#speciality_id');
            select.empty();
            select.append('<option value="">Select Speciality</option>');
        }

    });
});
</script>
@endpush