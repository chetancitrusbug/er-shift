@extends('layouts.frontendDashboard')
@section('title',trans('jobs.label.edit_jobs'))
@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"> <h3>@lang('jobs.label.edit_jobs')</h3> </div>
                <div class="panel-body">
                    <a href="{{ url('/jobs') }}" title=@lang('jobs.label.back')><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('jobs.label.back')</button></a>
                    <br />
                    <br />
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($job, [
                        'method' => 'PATCH',
                        'url' => ['/jobs', $job->id],
                        'class' => 'form-horizontal',
                        'id'=>'formjob',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('frontend.job.form', ['submitButtonText' => trans('jobs.label.update')])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
