@extends('layouts.frontendDashboard')
@section('title',trans('jobs.label.jobs'))
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <h1>@lang('jobs.label.jobs')</h1>
                </div>
            </div>
            <div class="box-content box-1">
                <div class="row btnbtmspace">
                    <div class="col-md-6">
                        <a href="{{ url('/jobs/create') }}" class="btn btn-success btn-sm" title=@lang('jobs.label.add_jobs')>
                        <i class="fa fa-plus" aria-hidden="true"></i> @lang('jobs.label.add_jobs') </a>
                        <a href="{{ url('/addBlukJobs') }}" class="btn btn-success btn-sm" title=@lang('jobs.label.add_bulk_jobs')>
                        <i class="fa fa-plus" aria-hidden="true"></i> @lang('jobs.label.add_bulk_jobs') </a>
                    </div>
                </div>
                <div class="table-responsive" >
                    <table class="table table-borderless" id="job-table">
                        <thead>
                            <tr>
                                <th>@lang('jobs.label.job_title')</th>
                                <th>@lang('jobs.label.job_responsibility')</th>
                                <th>@lang('jobs.label.profession')</th>
                                <th>@lang('jobs.label.speciality')</th>
                                <th>@lang('jobs.label.degree')</th>
                                <th>@lang('jobs.label.status')</th>
                                <th>@lang('jobs.label.action')</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
    $(function() { 
        var url ="{{ url('/jobs/') }}";
        var datatable = $('#job-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{!! route("jobsData") !!}',
                    type: "get", // method , by default get
                },
                columns: [
                    { data: 'job_title',name:'job_title',"searchable" : true}, 
                    { data: 'job_responsibility',name:'job_responsibility',"searchable" : true}, 
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var profession = '';
                            if(o.profession != null)
                            profession = o.profession.title;
                            else
                            profession = "";
                            return profession;
                        }
                    }, 
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var speciality = '';
                            if(o.speciality != null)
                            speciality = o.speciality.title;
                            else
                            speciality = "";
                            return speciality;
                        }
                    }, 
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var degree = '';
                            if(o.degree != null)
                            degree = o.degree.title;
                            else
                            degree = "";
                            return degree;
                        }
                    }, 
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.status == '0')
                            status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-danger btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang("jobs.label.inactive")</button></a>';
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('jobs.label.active')</button></a>";
                            return status;
                        }
                    }, 
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>@lang('jobs.label.edit')</button></a>&nbsp;";
                                d = "<a href='javascript:void(0);' ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> @lang('jobs.label.delete')</button></a>&nbsp;";                    
                                f= "<a href='javascript:void(0);' ><button class='btn btn-primary btn-xs favou-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> @lang('jobs.label.favorite')</button></a>&nbsp;";
                                return e+d+f;
                        }
                    }
                ]
        });
        
        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            
            var url ="{{ url('/jobs/') }}";
            url = url + "/" + id;
            var r = confirm("@lang('jobs.label.sure_job_delete')");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(trans('jobs.label.action_success'), data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error(trans('jobs.label.action_not_success'),erro)
                    }
                });
            }
        });

        $(document).on('click', '.favou-item', function (e) {
            var id = $(this).attr('data-id');
            
            var url ="{{ url('/favourite/') }}";
            url = url + "/" + id;
                $.ajax({
                    type: "get",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        window.location=url;
                       
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error(trans('jobs.label.action_not_success'),erro)
                    }
                });
         });
    }); 
</script>
@endpush
