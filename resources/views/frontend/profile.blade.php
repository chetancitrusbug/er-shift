@extends('layouts.frontprofile')
@section('title','')

@section('content')
    <div class="row main-content">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Profile</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('/edit/profile') }}">
                        {{ csrf_field() }}
                            <input type="hidden" name="uid" value="{{ Auth::user()->id }}"/>
                        <div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">
                            <label for="fname" class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input id="fname" type="text" class="form-control" name="fname" value="{{ Auth::user()->fname}}" required autofocus>

                                @if ($errors->has('fname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('lname') ? ' has-error' : '' }}">
                            <label for="lname" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input id="lname" type="text" class="form-control" name="lname" value="{{ Auth::user()->lname}}" required autofocus>

                                @if ($errors->has('lname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ Auth::user()->email}}" readonly>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('comp_name') ? ' has-error' : '' }}">
                            <label for="comp_name" class="col-md-4 control-label">Company Name</label>

                            <div class="col-md-6">
                                <input id="comp_name" type="text" class="form-control" name="comp_name" value="{{ @$emp_deatil->comp_name}}" required autofocus>

                                @if ($errors->has('comp_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('comp_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('comp_website') ? ' has-error' : '' }}">
                            <label for="comp_website" class="col-md-4 control-label">Company Website</label>

                            <div class="col-md-6">
                                <input id="comp_website" type="text" class="form-control" name="comp_website" value="{{ @$emp_deatil->comp_website}}" required autofocus>

                                @if ($errors->has('comp_website'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('comp_website') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('comp_information') ? ' has-error' : '' }}">
                            <label for="comp_information" class="col-md-4 control-label">Company Information</label>

                            <div class="col-md-6">
                                <input id="comp_information" type="text" class="form-control" name="comp_information" value="{{ @$emp_deatil->comp_information}}" required>

                                @if ($errors->has('comp_information'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('comp_information') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('tel_number') ? ' has-error' : '' }}">
                            <label for="tel_number" class="col-md-4 control-label">Telephone Number</label>

                            <div class="col-md-6">
                                <input id="tel_number" type="tel_number" class="form-control" name="tel_number" value="{{ @$emp_deatil->tel_number}}" 
                                onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="12" required>

                                @if ($errors->has('tel_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tel_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <textarea id="address" class="form-control" name="address">{{ @$emp_deatil->address}} </textarea>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
 

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@push('js')
 
@endpush
@endsection
