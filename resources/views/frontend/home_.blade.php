@extends('layouts.frontend')
@section('title','Home')

@section('content')
	
    <section class="header-section">
		@include('include.section.banner')
		
		<div class="banner-container clearfix">
		
			<div class="banner-div clearfix fadeInLeft animated">
			
				<h3>Lorem ipsum dolor sit amet</h3>
				<h4>Fusce hendrerit sollicitudin odio, nec iaculis mauris ornare condimentum</h4>
				
				<div class="top-email-box clearfix">
					<a href="{{ url('/register') }}"><button class="btn btn-startfree" type="button">Register Now</button>
					</a>
					<span class="btm-link">or <a href="{{ url('/auth/linkedin') }}">@lang('login.label.login_using_linkedin')</a></span>
				</div>
				
			
			</div><!-- end of banner-div -->
		
		</div><!-- end of banner-container -->
		
		<div class="dashboard-mb-img fadeInRight animated"><img src="images/design/Dashboard.png" alt=""></div>
		
		<div class="three-action-box slideInLeft animated clearfix">
			<div class="container">
			<div class="row">
			
				<div class="col-md-4 col-sm-4">
					<div class="three-action-div clearfix">
						<h3>Lorem ipsum dolor sit amet</h3>
						<p>Nulla posuere neque sed ex pellentesque ultrices. Donec iaculis felis tempor, auctor ipsum et, egestas velit. Morbi laoreet orci at blandit iaculis.</p>
					</div>
				</div><!-- end of col -->
				<div class="col-md-4 col-sm-4">
					<div class="three-action-div clearfix">
						<h3>Lorem ipsum dolor sit amet</h3>
						<p>Nulla posuere neque sed ex pellentesque ultrices. Donec iaculis felis tempor, auctor ipsum et, egestas velit. Morbi laoreet orci at blandit iaculis.</p>
					</div>
				</div><!-- end of col -->
				<div class="col-md-4 col-sm-4">
					<div class="three-action-div clearfix">
						<h3>Lorem ipsum dolor sit amet</h3>
						<p>Nulla posuere neque sed ex pellentesque ultrices. Donec iaculis felis tempor, auctor ipsum et, egestas velit. Morbi laoreet orci at blandit iaculis.</p>
					</div>
				</div><!-- end of col -->
			
			</div>
			</div>
		
		</div><!-- end of three-action-box -->
		
		<div class="short-plan-div slideInRight animated clearfix">
			<div class="short-plan-box clearfix">
				<div class="txt-box">
					<h3>Try out our 100% Free package</h3>
					<p>We increase our clients ROADs by 26%</p>
				</div>
				<div class="btn-link-div">
						<a href="{{ url('/price') }}" class="link"></a>
				</div>
			</div><!-- end of short-plan-box -->
		</div><!-- end of short-plan-div -->
		
		</section><!-- end of header-section -->
		<section class="features-section feature-top-1 light-red-bg-1">
	
			<div class="container">
			<div class="row">
			
				<div class="features-div clearfix">
				
				<div class="col-md-6 col-sm-6 pull-left slideInLeft animated">
					<div class="features-img-div clearfix">
						<span class="img1"><img src="images/design/facebook-permissions.png" alt=""></span>
						<span class="img2"><img src="images/design/dashboard-full.png" alt=""></span>
					</div>
				</div><!-- end of col -->
				<div class="col-md-6 col-sm-6  pull-right slideInRight animated">
					<div class="features-action-div clearfix">
						<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque in dui finibus</h3>
						<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla posuere neque sed ex pellentesque ultrices.</p>
						<p>Fusce hendrerit sollicitudin odio, nec iaculis mauris ornare condimentum. Donec varius lobortis magna, id vestibulum dolor tristique in.</p>
					</div>
				</div><!-- end of col -->
				
			
			
				</div><!-- end of features-div -->
			</div>
			</div>
		
		</section><!-- end of features-section -->
	
	<section class="features-section feature-2">
	
		<div class="container">
		<div class="row">
		
			<div class="features-div clearfix">
			
			<div class="col-md-6 col-sm-6 pull-right slideInLeft animated">
				<div class="features-img-div right clearfix">
					<span class="img1"><img src="images/design/facebook-permissions.png" alt=""></span>
					<span class="img2"><img src="images/design/dashboard-full.png" alt=""></span>
				</div>
			</div><!-- end of col -->
			<div class="col-md-6 col-sm-6  pull-left slideInRight animated">
				<div class="features-action-div clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque in dui finibus</h3>
					<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla posuere neque sed ex pellentesque ultrices.</p>
					<p>Fusce hendrerit sollicitudin odio, nec iaculis mauris ornare condimentum. Donec varius lobortis magna, id vestibulum dolor tristique in.</p>
				</div>
			</div><!-- end of col -->
			
		
		
			</div><!-- end of features-div -->
		</div>
		</div>
		
	</section><!-- end of features-section -->
	
	<section class="features-section light-red-bg-2 feature-top-2">
	
		<div class="container">
		<div class="row">
		
			<div class="features-div clearfix">
			
			<div class="col-md-6 col-sm-6 pull-left slideInLeft animated">
				<div class="features-img-div clearfix">
					<span class="img1"><img src="images/design/facebook-permissions.png" alt=""></span>
					<span class="img2"><img src="images/design/dashboard-full.png" alt=""></span>
				</div>
			</div><!-- end of col -->
			<div class="col-md-6 col-sm-6  pull-right slideInRight animated">
				<div class="features-action-div clearfix">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque in dui finibus</h3>
					<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla posuere neque sed ex pellentesque ultrices.</p>
					<p>Fusce hendrerit sollicitudin odio, nec iaculis mauris ornare condimentum. Donec varius lobortis magna, id vestibulum dolor tristique in.</p>
				</div>
			</div><!-- end of col -->
			
		
		
			</div><!-- end of features-div -->
		</div>
		</div>
		
	</section><!-- end of features-section -->
	
	<section class="video-section">
		<div class="video-div">
			<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
			<p>Mauris varius tristique lectus, a feugiat neque scelerisque ut. Proin eget ornare sem. Morbi nisl tortor, egestas ut urna quis, sodales consequat metus.</p>
			
			<div class="vidoe-icon slideInUp animated">
				<a href="#"><img src="images/design/video-icon.png" alt="" class="img-responsive"></a>
			</div>
			
		</div>
	
	</section><!-- end of video-section -->

	<section class="get-started-section ">
		<div class="get-started-div">
		
			<div class="get-started-img slideInDown animated">
				<img src="images/design/laptop-mobile.png" alt="" class="img-responsive">
			</div>
		
			
			
		</div>
	
	</section><!-- end of get-started-section -->
	
	<section class="download-app-section">
		<div class="download-app-div clearfix">
		
			<h3>Download the app.</h3>
			<p>Mauris varius tristique lectus, a feugiat neque scelerisque ut. Proin eget ornare sem. Morbi nisl tortor.</p>
			
			<div class="app-btn slideInUp animated">
				<a href="#"><img src="images/design/app-btn.png" alt="" class="img-responsive"></a>
			</div>
			
		</div><!-- end of get-download-app-div -->
	
	</section><!-- end of get-download-app-section -->
	
	@include('include.section.footer')

<div id="back-to-top"><span><i class="fa fa-angle-up"></i></span></div>
@endsection
