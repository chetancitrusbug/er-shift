@extends('layouts.frontendDashboard')
@section('title',trans('dashboard.home_view.view_doctor_wish'))
@section('content')
     <div class="row">
        <div class="col-md-12">
			<div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <h1>@lang('dashboard.home_view.view_doctor_wish')</h1>
                    </div>
                </div>
                <div class="box-content ">
                    <div class="table-responsive" >
                        <table class="table table-bordered">
                            <tbody>
                                <tr><th colspan="2"><h3>@lang('dashboard.home_view.doctor_wish_information')</h3></th></tr>
                                <tr><th>@lang('dashboard.home_view.doctor_name')</th><td>{{ $SearchDetail->doctorName }}</td></tr>
                                <tr><th>@lang('dashboard.home_view.title')</th><td>{{ $SearchDetail->wish_title }}</td></tr>
                                <tr><th>@lang('dashboard.home_view.description')</th><td>{{ $SearchDetail->wish_desc }}</td></tr>
                                <tr><th>@lang('dashboard.home_view.location')</th><td>{{ $SearchDetail->location }}</td></tr>
                                <tr><th>@lang('dashboard.home_view.ed_volume')</th><td>{{ $SearchDetail->edvolume_title }}</td></tr>
                                <tr><th>@lang('dashboard.home_view.rate')</th><td>{{ $SearchDetail->rate }}</td></tr>
                                
                                <tr><th colspan="2"><h3>@lang('dashboard.home_view.doctor_information')</h3></th></tr>
                                <tr><th>@lang('dashboard.home_view.doctor_name')</th><td>{{ $SearchDetail->doctorName }}</td></tr>
                                <tr><th>@lang('dashboard.home_view.image')</th><td><img src={{ $SearchDetail->doctorImage }} height='100px' width='100px'></td></tr>
                                <tr><th>@lang('dashboard.home_view.address')</th><td>{{ $SearchDetail->doctorDetail->address }}</td></tr>
                                <tr><th>@lang('dashboard.home_view.phone_no')</th><td>{{ $SearchDetail->doctorDetail->tel_number }}</td></tr>
                                <tr><th>@lang('dashboard.home_view.profession')</th><td>{{ ((($SearchDetail->doctorDetail->profession != null) && ($SearchDetail->doctorDetail->profession->title != null)) ? $SearchDetail->doctorDetail->profession->title : '' ) }}</td></tr>
                                <tr><th>@lang('dashboard.home_view.speciality')</th><td>{{ ((($SearchDetail->doctorDetail->speciality != null) && ($SearchDetail->doctorDetail->speciality->title != null)) ? $SearchDetail->doctorDetail->speciality->title : '' ) }}</td></tr>
                                <tr><th>@lang('dashboard.home_view.experience')</th><td>{{ ((($SearchDetail->doctorDetail->experience != null) && ($SearchDetail->doctorDetail->experience->title != null)) ? $SearchDetail->doctorDetail->experience->title : '' ) }}</td></tr>
                                <tr><th>@lang('dashboard.home_view.location')</th><td>{{ ((($SearchDetail->doctorDetail->location != null) && ($SearchDetail->doctorDetail->location->name != null)) ? $SearchDetail->doctorDetail->location->name : '' ) }}</td></tr>
                                <tr><th>@lang('dashboard.home_view.degree')</th><td>{{ $SearchDetail->doctorDetail->degree }}</td></tr>
                                <tr><th>@lang('dashboard.home_view.license')</th><td>{{ $SearchDetail->doctorDetail->license }}</td></tr>
                                <tr><th>@lang('dashboard.home_view.resume')</th><td>
                                    @if ($SearchDetail->doctorDetail->resume_type == 'attachment' )
                                    <a href={{ $SearchDetail->doctorDetail->resume }} target="_blank"><button class='btn btn-primary btn-xs'>Resume</button> </a>
                                    @elseif ($SearchDetail->doctorDetail->resume_type == 'link')
                                    <a href={{ '//'.$SearchDetail->doctorDetail->resume }} target="_blank"><button class='btn btn-primary btn-xs'>Resume</button> </a>
                                    @else
                                    @lang('dashboard.home_view.not_uploaded')
                                    @endif
                                </td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
@endsection
