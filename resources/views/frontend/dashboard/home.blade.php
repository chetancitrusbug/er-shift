@extends('layouts.frontendDashboard')
@section('title',trans('dashboard.home.dashboard'))

@section('content')
     <div class="row">
        <div class="col-md-12">
			<div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <h1>@lang('dashboard.home.dashboard')</h1>
                    </div>
                </div>
                <div class="box-content box-1">
                    <div class="table-responsive" >
                        <table class="table table-borderless" id="job-table">
                            <thead>
                                <tr>
                                    <th>@lang('dashboard.home.name')</th>
                                    <th>@lang('dashboard.home.profession')</th>
                                    <th>@lang('dashboard.home.wish_title')</th>
                                    <th>@lang('dashboard.home.rate')</th>
                                    <th>@lang('dashboard.home.location')</th>
                                    <th>@lang('dashboard.home.ed_volume')</th>
                                    <th>@lang('dashboard.home.actions')</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
@endsection
@push('js')
<script>
    $(function() {
        var url ="{{ url('/dashboard/') }}";
        var datatable = $('#job-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{!! route("dashboardData") !!}',
                    type: "get", // method , by default get
                },
                columns: [
                    { data: 'doctorName',name:'doctorName',"searchable" : true},
                    { data: 'profession_title',name:'profession_title',"searchable" : true},
                    { data: 'wish_title',name:'wish_title',"searchable" : true},
                    { data: 'rate',name:'rate',"searchable" : true},
                    { data: 'location',name:'location',"searchable" : true},
                    { data: 'edvolume_title',name:'edvolume_title',"searchable" : true},
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";
                                e= "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-primary btn-xs'> @lang('dashboard.home.view') </button></a>&nbsp;";
                            return e;
                        }
                    }
                ]
        });
    });
</script>
@endpush