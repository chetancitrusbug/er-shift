@extends('layouts.frontend')
@section('title','Home')

@section('content')

    <section class="header-section">
		@include('include.section.banner')

		<div class="pricing-container">

		<div class="pricing-top-container">
		<div class="container">
		<div class="row justify-content-md-center">

			<div class="col-md-10 col-sm-10">

				<h2>Pricing</h2>
				<h3>
				    ErShifts.com is a new platform and we are currently offering an introductory FREE RATE during our beta testing.
                    No monthly charges for new employers at this time.
                    Please register and load Emergency department shifts.
                    If you have multiple shifts to upload, you can accomplish this in your admin.
                    You can also send your CSV file to <a href="mailto:staff@ershifts.com">Staff@ershifts.com</a>.
                    Monthly rates will be no frills and unlimited number of shifts.
                    REGISTER NOW before monthly subscription rates are activated JAN 1 2019.
				</h3>

				<div class="plan-pricing-blk-div text-center slideInLeft animated clearfix">
					<div class="row">
						@foreach($packages as $package)
							<div class="col-md-4 col-sm-4">
								<div class="plan-pricing-row-blk clearfix">
									<div class="top-block clearfix">
										<h2>{{$package->title}}</h2>
									</div>
									<div class="middle-pricing">
										<div class="btm-block clearfix">
											<h4>{{$package->amount}} USD</h4>
											<h5>{{$package->day}} days</h5>
											<div class="block-txt clearfix">

											</div><!-- end block-txt -->
										</div><!-- end of btm bblock -->
									</div>
									<div class="btm-buy-now clearfix">
										<a href="{{ url('/confirm-order/'.$package->id) }}">Pay Now</a>
									</div>
								</div><!-- end nof plan-pricing-row-blk -->
							</div>
						@endforeach
						{{-- <div class="col-md-4 col-sm-4">
							<div class="plan-pricing-row-blk clearfix">
								<div class="top-block clearfix">
									<h2>Basic</h2>
								</div>
								<div class="middle-pricing">
									<div class="btm-block clearfix">
										<h4>10.00 USD</h4>
										<h5>Per month</h5>
										<div class="block-txt clearfix">

										</div><!-- end block-txt -->
									</div><!-- end of btm bblock -->
								</div>
								<div class="btm-buy-now clearfix">
									<a href="#">Start Free</a>
								</div>
							</div><!-- end nof plan-pricing-row-blk -->
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="plan-pricing-row-blk clearfix">
								<div class="top-block clearfix">
									<h2>Basic</h2>
								</div>
								<div class="middle-pricing">
									<div class="btm-block clearfix">
										<h4>10.00 USD</h4>
										<h5>Per month</h5>
										<div class="block-txt clearfix">

										</div><!-- end block-txt -->
									</div><!-- end of btm bblock -->
								</div>
								<div class="btm-buy-now clearfix">
									<a href="#">Start Free</a>
								</div>
							</div><!-- end nof plan-pricing-row-blk -->
						</div> --}}

					</div>
				</div>

			

			</div><!-- end of col -->
		</div>
		</div>

		</div>

	</div><!-- end of pricing-top-container -->





	</section><!-- end of header-section -->

	<section class="faq-section">
	<div class="faq-div clearfix">
		<h2 class="blk-title ">FAQ</h2>

		<div class="container">
		<div class="row">

			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>1. How much does it cost to use?</h3>
					<p>The site is free for all Employers during beta testing. Any further changes will be announced. The site is always free for all medical providers </p>

				</div>
			</div><!-- end of col -->

			<div class="col-md-6 col-sm-6 slideInLeft animated">
				<div class="faq-blk clearfix">
					<h3>2. What do I download?</h3>
					<p>Employers can download the mobile app ErShifts from the app store or google play Employers can input multiple shifts on the web login but not the mobile apps Medical Staff have access to the mobile app only</p>

				</div>
			</div><!-- end of col -->

		</div>
		</div>
		</div><!-- end of faq-div -->
	</section><!-- end of faq-section -->


	<section class="download-app-section">
		<div class="download-app-div clearfix">

			<h3>Download the app.</h3>

			<div class="app-btn slideInUp animated">
				<a href="{{ $ios_url }}"><img src="images/design/app-btn.png" alt="" class="img-responsive"></a>
			</div>

		</div><!-- end of get-download-app-div -->

	</section><!-- end of get-download-app-section -->

	@include('include.section.footer')

<div id="back-to-top"><span><i class="fa fa-angle-up"></i></span></div>

@endsection
