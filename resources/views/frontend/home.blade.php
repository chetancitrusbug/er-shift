@extends('layouts.frontend')
@section('title','Home')

@section('content')

    <section class="header-section" >
		@include('include.section.banner')

		<div class="banner-container clearfix">

			<div class="banner-div clearfix ">

				<h3>Welcome to ErShifts.com</h3>
				<h4>The first Healthcare industry's mobile app for Pay and Shift
matching for Emergency department Staff</h4>

				<div class="top-email-box clearfix">
					<a href="{{ url('/register') }}"><button class="btn btn-startfree" type="button">Register Now</button>
					</a>
					<!--<span class="btm-link">or <a href="{{ url('/auth/linkedin') }}">@lang('login.label.login_using_linkedin')</a></span>-->
				</div>

			</div><!-- end of banner-div -->

		</div><!-- end of banner-container -->

		<div class="dashboard-mb-img ">
			<!--<img src="images/design/Dashboard.png" alt="">-->
			<div class="vidoe-icon ">

				<div class="down-arrow-div">
					<h1>THIS IS HOW IT WORKS</h1>
					<!--<p>&darr;<i class="fa fa-arrow-down"></i></p>-->
					<div class="down-thumb bounce">
						<!--<a href="#content-div" class="down-arrow">
							<div class="icon-scroll"></div>
						</a>-->
						<div class="mouse_scroll">
							<div class="mouse">
								<div class="wheel"></div>
							</div>
							<div>
								<span class="m_scroll_arrows unu"></span>
								<span class="m_scroll_arrows doi"></span>
								<span class="m_scroll_arrows trei"></span>
							</div>
						</div>
					</div>

				</div>

				<a href="#" data-toggle="modal" class="video-btn" data-src="{{ $home_video }}" data-target="#video-modal"><img src="images/design/video-icon.png" alt="" class="img-responsive"></a>

				<div class="modal fade" id="video-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-top:10%;">
					<div class="modal-dialog vidoe-modal" role="document">
					  <div class="modal-content">
						<div class="modal-header">
						  <h5 class="modal-title" id="exampleModalLabel">Let us show you how it works</h5>
						  <button type="button" class="close" id="StopClickButton" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						  </button>
						</div>
						<div class="modal-body">
							<div class="embed-responsive embed-responsive-16by9">
								<iframe id="Video" class="youtube-video" width="560" height="315" src="{{ $home_video }}" frameborder="0" allowfullscreen></iframe>
							</div>
						</div>
					  </div>
					</div>
				</div>

			</div>
		</div>

		<div class="three-action-box slideInLeft animated clearfix">
			<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 text-center">
					<div class="three-action-div clearfix">
						<a href="{{ url('/login') }}" class="btn btn-startfree">Employer Login</a>
					</div>
				</div><!-- end of col -->
				<div class="col-md-12 col-sm-12 text-center mrgin">
					<button  class="btn btn-startfree">ER staff download app here</button>
					<div class="three-action-div clearfix">
						<!-- Page_section -->
						<div class="down-thumb bounce">
						<!--<a href="#content-div" class="down-arrow">
							<div class="icon-scroll"></div>
						</a>-->
						<div class="mouse_scroll">
							<div class="mouse">
								<div class="wheel"></div>
							</div>
							<div>
								<span class="m_scroll_arrows unu"></span>
								<span class="m_scroll_arrows doi"></span>
								<span class="m_scroll_arrows trei"></span>
							</div>
						</div>
					</div>
					</div>
						<!--<a href="#" class="btn btn-startfree">ER staff download app here</a>-->
						<!-- <p class="big-fonts">ED Doctors<span><i class="fa fa-angle-right"></i></span> Registered Nurses <span><i class="fa fa-angle-right"></i></span> Midlevel providers </p> -->
					</div>
				</div><!-- end of col -->
				<div class="col-md-12 col-sm-12 text-center">
					<div class="three-action-div center-margin clearfix">
						<div class="app-btn slideInUp animated">
							<a href="{{ $android_url }}">
								<img src="images/design/android.png" alt="" class="img-responsive iphone-img">
							</a>
							<a href="{{ $ios_url }}">
								<img src="images/design/apple.png" alt="" class="img-responsive android-img">
							</a>
						</div>
					</div>
				</div><!-- end of col -->
			</div>
			</div>

		</div><!-- end of three-action-box -->

		<div class="short-plan-div slideInRight animated clearfix">
			<div class="short-plan-box clearfix">
				<div class="txt-box">
					<h3>Employers try our 100% risk free packages</h3>
					<!--<p>We increase our clients ROADs by 26%</p>-->
				</div>
				<div class="btn-link-div">
					<a href="{{ url('/price') }}" class="link"></a>
				</div>
			</div><!-- end of short-plan-box -->
		</div><!-- end of short-plan-div -->

		</section><!-- end of header-section -->
		<section class="features-section feature-top-1 light-red-bg-1">

			<div class="container">
			<div class="row">

				<div class="features-div clearfix">

				<div class="col-md-6 col-sm-6 pull-left slideInLeft animated">
					<div class="features-img-div clearfix">
						<span class="img1"><img src="images/design/facebook-permissions.png" alt=""></span>
						<span class="img2"><img src="images/design/dashboard-full.png" alt=""></span>
					</div>
				</div><!-- end of col -->
				<div class="col-md-6 col-sm-6  pull-right slideInRight animated">
					<div class="features-action-div clearfix">
						<h3>For Employers</h3>
						<p><i class="fa fa-check"></i>	Access to provider’s mobile App</p>
						<p><i class="fa fa-check"></i>	Access to willing ED staff </p>
						<p><i class="fa fa-check"></i>	Access to Dr/ RN/ PA/NP availability </p>
						<p><i class="fa fa-check"></i>	Increased ability to fill open shifts</p>
						<p><i class="fa fa-check"></i>	Open access to chat with selected candidates</p>
						<p><i class="fa fa-check"></i>	1st month free & 3 mth / 6mth /1yr packages</p>
						<p><i class="fa fa-check"></i>	Upload multiple jobs </p>
						<p><i class="fa fa-check"></i>	Access to resumes </p>
					</div>
				</div><!-- end of col -->



				</div><!-- end of features-div -->
			</div>
			</div>

		</section><!-- end of features-section -->

	<section class="features-section feature-2">

		<div class="container">
		<div class="row">

			<div class="features-div clearfix">

			<div class="col-md-6 col-sm-6 pull-right slideInLeft animated">
				<div class="features-img-div right clearfix">
					<span class="img1"><img src="images/design/facebook-permissions.png" alt=""></span>
					<span class="img2"><img src="images/design/dashboard-full.png" alt=""></span>
				</div>
			</div><!-- end of col -->
			<div class="col-md-6 col-sm-6  pull-left slideInRight animated">
				<div class="features-action-div clearfix">
					<h3>For Drs, PAs, NPs, RNs</h3>
					<p><i class="fa fa-check"></i>	Free access to ErShifts.com app </p>
					<p><i class="fa fa-check"></i>	Access to Hospitals / agencies/ other employers </p>
					<p><i class="fa fa-check"></i>	Declare desired pay rate ($/hr) </p>
					<p><i class="fa fa-check"></i>	Enter dates that you want to work </p>
					<p><i class="fa fa-check"></i>	ErShifts.com matches dates with your requests </p>
					<p><i class="fa fa-check"></i>	Chat with employers privately </p>
					<p><i class="fa fa-check"></i>	Run your own 1 person company </p>
					<p><i class="fa fa-check"></i>	You can still keep your daily job </p>
					<p><i class="fa fa-check"></i>	Upload resume </p>


				</div>
			</div><!-- end of col -->



			</div><!-- end of features-div -->
		</div>
		</div>

	</section><!-- end of features-section -->

	<section class="features-section light-red-bg-2 feature-top-2">

		<div class="container">
		<div class="row">

			<div class="features-div clearfix">

			<div class="col-md-6 col-sm-6 pull-left slideInLeft animated">
				<div class="features-img-div clearfix">
					<span class="img1"><img src="images/design/facebook-permissions.png" alt=""></span>
					<span class="img2"><img src="images/design/dashboard-full.png" alt=""></span>
				</div>
			</div><!-- end of col -->
			<div class="col-md-6 col-sm-6  pull-right slideInRight animated">
				<div class="features-action-div clearfix">
					<h3>What are you waiting for...</h3>
					<p>Take a test drive and you will be pleased.</p>

				</div>
			</div><!-- end of col -->



			</div><!-- end of features-div -->
		</div>
		</div>

	</section><!-- end of features-section -->

	<!--<section class="video-section">
		<div class="video-div">
			<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
			<p>Mauris varius tristique lectus, a feugiat neque scelerisque ut. Proin eget ornare sem. Morbi nisl tortor, egestas ut urna quis, sodales consequat metus.</p>

			<div class="vidoe-icon slideInUp animated">
				<a href="#"><img src="images/design/video-icon.png" alt="" class="img-responsive"></a>
			</div>

		</div>

	</section><!-- end of video-section -->

	<!--<section class="get-started-section ">
		<div class="get-started-div">

			<div class="get-started-img slideInDown animated">
				<img src="images/design/laptop-mobile.png" alt="" class="img-responsive">
			</div>



		</div>

	</section><!-- end of get-started-section -->

	<section class="download-app-section">
		<div class="download-app-div clearfix">

			<h3>Download the app.</h3>
			<!--<p>Mauris varius tristique lectus, a feugiat neque scelerisque ut. Proin eget ornare sem. Morbi nisl tortor.</p>-->

			<!--<div class="app-btn slideInUp animated">
				<a href="#"><img src="images/design/app-btn.png" alt="" class="img-responsive"></a>
			</div>-->

			<div class="three-action-div clearfix">
				<div class="app-btn slideInUp animated">
					<a href="{{ $android_url }}">
                    						<img src="images/design/android.png" alt="" class="img-responsive iphone-img">
                    					</a>
                    <a href="{{ $ios_url }}">
                    						<img src="images/design/apple.png" alt="" class="img-responsive android-img">
                    					</a>
				</div>
			</div>

		</div><!-- end of get-download-app-div -->

	</section><!-- end of get-download-app-section -->

	@include('include.section.footer')

<div id="back-to-top"><span><i class="fa fa-angle-up"></i></span></div>
@endsection


@push('script-head')

<script>
	$(document).ready(function() {


		$('.header-section').click(function(){
			$('.youtube-video').each(function(index) {
				$(this).attr('src', $(this).attr('src'));
			});
		});

	});

</script>
@endpush