@extends('layouts.frontendDashboard')
@section('title',trans('dashboard.favourit.favourit'))

@section('content')
     <div class="row">
        <div class="col-md-12">
			<div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <h1>@lang('dashboard.favourit.favourites')</h1>
                    </div>
                </div>
                <div class="box-content ">
                    <div class="table-responsive" >
                        <table class="table table-borderless" id="favourite-table">
                            <thead>
                                <tr>
                                    <th>@lang('dashboard.favourit.user_type')</th>
                                    <th>@lang('dashboard.favourit.user')</th>
                                    <th>@lang('dashboard.favourit.job')</th>
                                    <th>@lang('dashboard.favourit.action')</th>
                                 </tr>
                            </thead>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
@endsection
@push('js')
<script>
    $(function() { 
        var url ="{{ url('/favourite/') }}";
     // var id = "{{ $id }}";
        var datatable = $('#favourite-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{!! route("favouriteData") !!}',
                    type: "get", // method , by default get
                    "data": {
                        "VarID": "{{ $id }}"
                    },
                  
                },
                columns: [
                    { data: 'user_type',name:'user_type',"searchable" : false}, 
                    { data: 'name',name:'name',"searchable" : false}, 
                    /*{ data: 'job',name:'job',"searchable" : true}, */
                    { data: 'job_title',name:'job_title',"searchable" : false},
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                                e= "<a href='"+url+"/view/"+o.job_id+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>@lang('dashboard.favourit.view')</button></a>&nbsp;";
                                 return e;
                        }
                    }
                   
                ]
        });
         
        
    }); 
</script>
@endpush