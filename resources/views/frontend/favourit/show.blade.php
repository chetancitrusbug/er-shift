@extends('layouts.frontendDashboard')
@section('title',trans('dashboard.favourit.view_favourites'))
@section('content')
     <div class="row">
        <div class="col-md-12">
			<div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <h1>@lang('dashboard.favourit.view_favourites')</h1>
                    </div>
                </div>
                <div class="box-content ">
                    <div class="table-responsive" >
                        <table class="table table-bordered">
                            <tbody>
                                <tr><th>@lang('dashboard.favourit.user_type')</th><td>{{ $SearchDetail->user_type }}</td></tr>
                                <tr><th>@lang('dashboard.favourit.user')</th><td>{{ $SearchDetail->name }}</td></tr>
                                <tr><th>@lang('dashboard.favourit.job_title')</th><td>{{ $SearchDetail->job_title }}</td></tr>
                                
                                
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
@endsection
