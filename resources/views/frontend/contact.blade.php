@extends('layouts.frontend')
@section('title','Contact')

@section('content')
	<section class="header-section other-header-section">
		@include('include.section.banner')		
		<div class="contact-container">
			<div class="contact-top-container">
				<div class="container">
					<div class="row justify-content-md-center">
						<div class="col-lg-7 col-md-9 col-sm-12">
							<div class="content_div">
								<h1>Talk to us.</h1>
								<h3>Please fillout the simple form below and we'll get back to you within 24 hours. </h3>
							</div>	
							<div class="card_div">
								<form class="form-horizontal" method="POST" action="{{ url('contact/add') }}">
									{{ csrf_field() }}
                                     <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name">@lang('register.label.uname')</label>
                                        
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
									<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="exampleInputEmail1">@lang('register.label.e_mail')</label>
                                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ old('email') }}" required autofocus placeholder="Enter your email...">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
									
									<div class="form-group">
										<label for="exampleFormControlTextarea1">Now let's hear the details</label>
										<textarea name="comment" class="form-control" id="exampleFormControlTextarea1" rows="4" placeholder="Let us know what you need"></textarea>
									</div>
									<div class="btn-div">
										<button class="btn btn-startfree" type="submit">Submit</button>
									</div>
								</form>
							</div>
						</div><!-- end of col -->
					</div>
				</div>
			</div>
		</div><!-- end of contact-top-container -->
	
	</section><!-- end of header-section -->
	
	@include('include.section.footer')

<div id="back-to-top"><span><i class="fa fa-angle-up"></i></span></div>
@endsection
