@extends('layouts.frontendDashboard')

@section('title','Subscription')
@section('content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title p-title">@lang('subscription.label.billing_subscription')</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="row">


            <div class="panel-padding">
                <div class="">
					<div class="">
                        <div class="tabbable">
                           <ul class="nav nav-tabs nav-tabs-solid nav-justified">
                                <li class="{{ request()->is('billing/subscription') ? 'active' : '' }}" ><a href="{{url('billing/subscription')}}" >Current Subscription</a></li>
                                <li class="{{ request()->is('billing/plans') ? 'active' : '' }}"><a href="{{url('billing/plans')}}" >Purchase/Change Subscription Package</a></li>

                                <li class="{{ request()->is('billing/history') ? 'active' : '' }}"><a href="{{url('billing/history')}}" >Subscription History</a></li>

                            </ul>

                            <div class="tab-content">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <div class="">
                                                <!-- start -->

                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-framed">
                                                        <thead>
                                                            <tr class="bg-blue">
                                                                <th>Package Name</th>
                                                                <th>Invoice ID</th>
                                                                <th>Issued Date</th>
                                                                <th>Expires Date</th>
                                                                <th>Price</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($Order as $key => $value)
                                                            <tr>
                                                                <td>{{ $value->package->title }}</td>
                                                                <td> #000{{ $value->id }}</td>
                                                                <td>{{ $value->start_date }}</td>
                                                                <td>{{ date('Y-m-d', strtotime($value->expire_date. ' - 1 days'))   }}</td>
                                                                <td>{{ $value->package->amount }}</td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>


                                                <!-- end -->

                                            </div>
                                        </div>
                                    </div>





                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>



    </div>
</div>
@endsection


@push('js')
<script>
</script>
@endpush


