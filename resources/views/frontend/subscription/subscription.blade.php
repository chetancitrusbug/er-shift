@extends('layouts.frontendDashboard')

@section('title',trans('subscription.label.subscription'))
@section('content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title p-title">@lang('subscription.label.billing_subscription') </h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="panel-padding">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-solid nav-justified">
                        <li class="{{ request()->is('billing/subscription') ? 'active' : '' }}" ><a href="{{url('billing/subscription')}}" >@lang('subscription.label.current_subscription')  </a></li>
                        <li class="{{ request()->is('billing/plans') ? 'active' : '' }}"><a href="{{url('billing/plans')}}" >@lang('subscription.label.change_plan')</a></li>
                        <li class="{{ request()->is('billing/history') ? 'active' : '' }}"><a href="{{url('billing/history')}}" >@lang('subscription.label.subscription_history')</a></li>
                    </ul>

                    <div class="tab-content">

                        @if($current_subscription)
                        <div class="panel invoice-grid">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h6 class="text-semibold no-margin-top">{{$current_subscription->packageDetail->title}}</h6>
                                        <ul class="list list-unstyled">
                                            <li>@lang('subscription.label.id') #: &nbsp;00{{$current_subscription->id}}</li>
                                            <li>@lang('subscription.label.issued_on') <span class="text-semibold">
                                            {{ \Carbon\Carbon::parse($current_subscription->start_date)->toFormattedDateString()}}</span></li>
                                            @if($current_subscription->packageDetail->isFree == 1)
                                            <button type="button" class="btn btn-danger btn-sm cancel-subscription" title="Cancel Subscription">
                                                @lang('subscription.label.cancel_subscription') </button>
                                            @else
                                            @endif
                                        </ul>
                                        
                                    </div>

                                        <!--<div class="col-sm-6">
                                        <ul class="list list-unstyled text-right">
                                            <li>Package Day: <span class="text-semibold">
                                            @if($current_subscription->packageDetail->day < 30)
                                            {{ $current_subscription->packageDetail->day }} Days</span></p>
                                            @else
                                                {{ round(($current_subscription->packageDetail->day)/30) }} Month</span></p>
                                            @endif
                                             {{ round(($current_subscription->packageDetail->day)/30) }} 
                                            </span></li>
                                            <li class="dropdown">
                                                Status: &nbsp;
                                                
                                                <a href="#" class="label bg-success-400 dropdown-toggle" data-toggle="dropdown">
                                                    @if($current_subscription->packageDetail->status == 1)
                                                        @lang('subscription.label.active')
                                                    @else
                                                        @lang('subscription.label.inactive')
                                                    @endif 
                                                </a>
                                            </li>
                                        </ul>
                                    </div>-->
                                </div>
                            </div>

                            <div class="panel-footer">
                                <ul>
                                    <li><span class="status-mark border-danger position-left"></span> @lang('subscription.label.expired_on') <span class="text-semibold">
                                    {{ \Carbon\Carbon::parse( date('Y-m-d', strtotime($current_subscription->end_date. ' - 1 days')) )->toFormattedDateString()}}
                                        </span></li>
                                </ul>

                                
                            </div>
                        </div>
                        @elseif($count_subscription <= 0 )
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-body border-top-primary text-center">
                                    <form class="form-horizontal" method="POST" action="{{ url('billing/subscription/trial') }}">
                                            {{ csrf_field() }}
                                    <h6 class="no-margin text-semibold">{{ \config('settings.free_trial_pack_info.name')}}  </h6>
                                    <p class="text-muted content-group-sm">{{ \config('settings.free_trial_pack_info.desc')}}  </p>
                                    <p class="text-muted content-group-sm">{{ \config('settings.free_trial_pack_info.unit')}}  @lang('subscription.label.compagnie') / {{ \config('settings.free_trial_pack_info.FREE_TRIAL_DAY')}} @lang('subscription.label.days')</p>
                                    

                                    <button type="submit" class="btn btn-primary">  @lang('subscription.label.start_free_trial') </button>
                                    </form>
                                </div>
                            </div>
                        </div>

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('js')
<script>
    $('.cancel-subscription').on('click', function (e) {
        var id = "{{ @$current_subscription->id}}";
        var url ="{{ url('/cancelSubscription') }}";
        url = url + "/" + id;
        var r = confirm("Are you sure you want to Cancel Subscription ?");
        if (r == true && id != '') {
            window.location = url;
        }
    });
</script>
@endpush


