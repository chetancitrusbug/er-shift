@extends('layouts.frontendDashboard')

@section('title',trans('subscription.label.dashboard'))
@section('content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title p-title">@lang('subscription.label.dashboard')</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="row">
			<div class="panel-padding">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-solid nav-justified">
                        <li class="{{ request()->is('billing/subscription') ? 'active' : '' }}" ><a href="{{url('billing/subscription')}}" >@lang('subscription.label.current_subscription')</a></li>
                        <li class="{{ request()->is('billing/plans') ? 'active' : '' }}"><a href="{{url('billing/plans')}}" >@lang('subscription.label.change_plan')</a></li>
                        <li class="{{ request()->is('billing/history') ? 'active' : '' }}"><a href="{{url('billing/history')}}" >@lang('subscription.label.subscription_history')</a></li>
                        
                    </ul>
                    <div class="tab-content">
                        <div class="panel invoice-grid">
                            <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-framed">
                                    <thead>
                                        <tr class="bg-blue">
                                            <th>Package Name</th>
                                            <th>Duration</th>
                                            <th>Amount</th>
                                            <th>Order</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($packages as $key=>$pack)
                                        <tr>
                                            <td>{{ $pack->title }}</td>
                                            @if($pack->day < 30)
                                            <td>{{ $pack->day }} @lang('subscription.label.days')</td>
                                            @else
                                            <td>{{ round(($pack->day)/30) }} @lang('subscription.label.month')</td>
                                            @endif
                                    
                                            <td> ${{ $pack->amount }}</td>
                                            <td><button type="button" class="btn btn-primary" onclick="order({{ $pack->id }});" style="color:#fff;text-align:center;" ><i class="fa fa-cart-arrow-down"></i>  @lang('subscription.label.order_now') </button></td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    function order(pack_id)
    {
        var url ="{{ url('/confirm-order/') }}";
        url = url + "/" + pack_id;
        var r = confirm("{{$orderChangeMsg}}");
        if (r == true && pack_id != '') {
            window.location = url;
        }
    }
</script>
@endpush


