@extends('layouts.user-backend')

@section('title',trans('subscription.label.subscription'))
@section('content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title p-title">@lang('subscription.label.billing_subscription')</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="row">


            <div class="panel-padding">
                <div class="">


                    <div class="">
                        <div class="tabbable">
                           <ul class="nav nav-tabs nav-tabs-solid nav-justified">
                                <li class="{{ request()->is('Billing/subscription') ? 'active' : '' }}" ><a href="{{url('Billing/subscription')}}" >@lang('subscription.label.current_subscription')</a></li>
                                <li class="{{ request()->is('Billing/plans') ? 'active' : '' }}"><a href="{{url('Billing/plans')}}" >@lang('subscription.label.change_plan')</a></li>
                                <li class="{{ request()->is('Billing/unit-history') ? 'active' : '' }}"><a href="{{url('Billing/unit-history')}}" >@lang('subscription.label.unit_history')</a></li>
                                <li class="{{ request()->is('Billing/history') ? 'active' : '' }}"><a href="{{url('Billing/history')}}" >@lang('subscription.label.subscription_history')</a></li>
                                
                            </ul>

                            <div class="tab-content">

                                <div class="panel invoice-grid">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h6 class="text-semibold no-margin-top">@lang('subscription.label.leonardo_fellini')</h6>
                                                <ul class="list list-unstyled">
                                                    <li>@lang('subscription.label.invoice') #: &nbsp;0028</li>
                                                    <li>@lang('subscription.label.issued_on') <span class="text-semibold">2015/01/25</span></li>
                                                </ul>
                                            </div>

                                            <div class="col-sm-6">
                                                <h6 class="text-semibold text-right no-margin-top">$8,750</h6>
                                                <ul class="list list-unstyled text-right">
                                                    <li>@lang('subscription.label.method')  <span class="text-semibold">SWIFT</span></li>
                                                    <li class="dropdown">
                                                            @lang('subscription.label.status') &nbsp;
                                                        <a href="#" class="label bg-danger-400 dropdown-toggle" data-toggle="dropdown">@lang('subscription.label.overdue') <span class="caret"></span></a>
                                                        <ul class="dropdown-menu dropdown-menu-right active">
                                                            <li class="active"><a href="#"><i class="icon-alert"></i> @lang('subscription.label.overdue')</a></li>
                                                            <li><a href="#"><i class="icon-alarm"></i>@lang('subscription.label.pending') </a></li>
                                                            <li><a href="#"><i class="icon-checkmark3"></i> @lang('subscription.label.paid')</a></li>
                                                            <li class="divider"></li>
                                                            <li><a href="#"><i class="icon-spinner2 spinner"></i> @lang('subscription.label.on_hold')</a></li>
                                                            <li><a href="#"><i class="icon-cross2"></i> @lang('subscription.label.canceled')</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-footer">
                                        <ul>
                                            <li><span class="status-mark border-danger position-left"></span> @lang('subscription.label.due') <span class="text-semibold">2015/02/25</span></li>
                                        </ul>

                                        <ul class="pull-right">
                                            <li><a href="#" data-toggle="modal" data-target="#invoice"><i class="icon-eye8"></i></a></li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i> <span class="caret"></span></a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="#"><i class="icon-printer"></i> @lang('subscription.label.print_invoice')</a></li>
                                                    <li><a href="#"><i class="icon-file-download"></i> @lang('subscription.label.download_invoice')</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#"><i class="icon-file-plus"></i> @lang('subscription.label.edit_invoice')</a></li>
                                                    <li><a href="#"><i class="icon-cross2"></i> @lang('subscription.label.remove_invoice')</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>



    </div>
</div>
@endsection


@push('js')
<script>
</script>
@endpush


