@extends('layouts.frontendDashboard')

@section('title',trans('subscription.label.confirm_order'))
@section('content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title p-title">@lang('subscription.label.confirm_order')</h5>
        <div class="heading-elements">
            
        </div>
    </div>

    <div class="panel-body">

        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['url' => '/confirm-order', 'class' => 'form-horizontal', 'files' => true,'autocomplete'=>'off']) !!}
                {!! Form::hidden('package_id',$package->id, ['class' => 'form-control','id'=>'package_id']) !!}
             
                {!! Form::hidden('step',1, ['class' => 'form-control','id'=>'step']) !!}
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="form-group">
                                        {!! Form::label('name',trans('subscription.label.name'), ['class' => 'col-lg-3 control-label']) !!}
                                        <div class="col-lg-9">
                                            {!! Form::text('name',Auth::user()->name, ['class' => 'form-control','readonly']) !!}
                                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('email',trans('subscription.label.email'), ['class' => 'col-lg-3 control-label']) !!}
                                        <div class="col-lg-9">
                                            {!! Form::text('email',Auth::user()->email, ['class' => 'form-control','readonly']) !!}
                                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                               
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary">@lang('subscription.label.next') <i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>

        </div>



    </div>
</div>
@endsection


@push('js')
<script>
</script>
@endpush


