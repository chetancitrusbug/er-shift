@extends('layouts.frontendDashboard')

@section('title',trans('subscription.label.confirm_billing_payment'))
@section('content')

@push('css')
<style>
	input.has-error {
    border: 1px solid red !important;
}

</style>
@endpush
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title p-title">@lang('subscription.label.confirm_billing_payment')</h5>
        <div class="heading-elements">
            
        </div>
    </div>

    <div class="panel-body">

		
		<div class="alert alert-danger hidden" id="carderrors"> </div> 

        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['url' => '/order/payment', 'id' =>'payment-form', 'class' => 'form-horizontal checkoutform', 'files' => true,'autocomplete'=>'off']) !!}
                {!! Form::hidden('billing_id',$billing->id, ['class' => 'form-control','id'=>'billing_id']) !!}
                {!! Form::hidden('step',2, ['class' => 'form-control','id'=>'step']) !!}
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                
								<!-- Payment Page  -->

								
								<div class="container">
									<div class="row">
										<div class="col-md-6 col-sm-12 col-xs-12">

										<!-- CREDIT CARD FORM STARTS HERE -->
											<div class="panel panel-default credit-card-box">
												<div class="panel-heading display-table col-md-12 col-sm-12 col-xs-12">
													<div class="row display-tr text-center">
														<div class="">                            
															<img class="img-responsive text-center-img" src="{{asset('assets/images/accepted.png')}}">
														</div>
													</div>                    
												</div>
												<div class="panel-body">
												
														<div class="row">
															<div class="col-md-12 col-sm-12 col-xs-12">
																<div class="form-group pt-20">
																	{!! Form::label('card_no',trans('subscription.label.card_no')) !!}
																	<div class="input-group">
																		{{-- {!! Form::number('card_no',null, ['class' => 'form-control','min'=>100,'placeholder'=>'Valid Card Number']) !!} --}}
																		
																		<input type="text" id="cardnum" class="payment-input form-control mmb10 cardnum" placeholder="XXXX-XXXX-XXXX-XXXX">

																		<span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
																		 
																		   <label class="error"></label>
																		   {!! $errors->first('card_no', '<p class="help-block">:message</p>') !!}
																		
																	</div>
																</div>                            
															</div>
														</div>

														<div class="row">
																<div class="col-md-6 col-sm-6 col-xs-6">
																	<div class="form-group pt-20">
																		<div class="input-group">
																			<label>EXP. DATE</label>
																			<input type="text" id="cardexp" class="payment-input form-control cc-card cardexp" placeholder="MM/YYYY">
																		</div>
																	</div>                            
																</div>
																<div class="col-md-6 col-sm-6 col-xs-6">
																		<div class="form-group pt-20">
																			<div class="input-group">
																				<label>Cardholder Name</label>
																				<input type="text" name="card_holder_name" class="payment-input form-control" placeholder="Cardholder name">
																			</div>
																		</div>                            
																	</div>
															</div>

													

														{{-- <div class="row">
															<div class="col-md-7 col-sm-7 col-xs-12">
																<div class="form-group">
																	{!! Form::label('ccExpiryMonth',trans('subscription.label.expiry_month'), []) !!}
																	{!! Form::select('ccExpiryMonth',['' =>'Select Month', '1'=>'01' ,'2'=>'02','3'=>'03','4'=>'04','5'=>'05','6'=>'06','7'=>'07','8'=>'08','9'=>'09','10'=>'10','11'=>'11','12'=>'12'] ,null, ['required' => 'required','class' => 'form-control','min'=>1]) !!}
																	{!! $errors->first('ccExpiryMonth', '<p class="help-block">:message</p>') !!}
																	
																</div>
															</div>
															<div class="col-md-5 col-sm-5 col-xs-12">
																<div class="form-group">
																	{!! Form::label('ccExpiryYear',trans('subscription.label.expiry_year'), ['class' => 'pl-10']) !!}
																	<div class="col-md-12 col-sm-12 col-xs-12">
																		{!! Form::number('ccExpiryYear',null, ['class' => 'form-control','min'=>100]) !!}
																		{!! $errors->first('ccExpiryYear', '<p class="help-block">:message</p>') !!}
																	</div>
																</div>
															</div>
														</div> --}}
														<div class="row">
															<div class="col-md-12 col-sm-12 col-xs-12">
																<div class="form-group">
																	{!! Form::label('cvvNumber',trans('subscription.label.cvv_no'), ['class' => 'col-lg-3 control-label']) !!}
																	{{-- {!! Form::number('cvvNumber',null, ['class' => 'form-control','min'=>3,'placeholder'=>'CVV']) !!} --}}
																	<input type="text" id="cardcvv" class="payment-input form-control  cardcvv" placeholder="XXX">
            														{!! $errors->first('cvvNumber', '<p class="help-block">:message</p>') !!}
																</div>
															</div>                        
														</div>

														<input type="hidden" name="token" id="stripetoken">

														<div class="col-md-12 col-sm-12 col-xs-12">
															<!--<button class="btn btn-success btn-lg btn-block" type="submit">Start Subscription</button>-->
															<div class="row">
																<button name="btnSubmit" type="submit" id="card_form_submit_btn" class="btn btn-primary btn-block checkButton">@lang('subscription.label.start_subscription')<i class="icon-arrow-right14 position-right"></i></button>
															</div>	
														</div>
														<div class="row" style="display:none;">
															<div class="col-md-12 col-sm-12 col-xs-12">
																<p class="payment-errors"></p>
															</div>
														</div>
													
												</div>
											</div>            
											<!-- CREDIT CARD FORM ENDS HERE -->
										</div>            
									</div>
								</div>
								
								<!-- End of Payment Page  -->
								
								
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>

        </div>



    </div>
</div>
@endsection


@push('js')
<!-- See why multiple instance of stripe has been loaded:  https://stripe.com/docs/stripe-js/v2#including-stripejs -->
<script src="https://js.stripe.com/v2/"></script>
<script src="https://js.stripe.com/v3/"></script>
<script src="{{ asset('assets/javascripts/jquery/jquery.payment.js') }}"></script>
<script type="text/javascript">

        $("input").focus(function(){
            $(this).removeClass("has-error");
        });

        $("#cardnum").payment('restrictNumeric');
        $("#cardnum").payment('formatCardNumber');
        $('#cardexp').payment('formatCardExpiry');
		$("#cardcvv").payment('formatCardCVC');
		


        // Handle form submission
		var form = document.getElementById('payment-form');
		
        //var payBtns = document.querySelectorAll('.btn-Stripe');
        var payBtns = document.querySelectorAll('.checkButton');
        var alertDiv = document.getElementById('carderrors');


	
        window.disablePayBtns = function() {
            for(var i=0; i < payBtns.length; i++){
                payBtns[i].disabled = true;
            }
        }

        window.enablePayBtns = function() {
            for(var i=0; i < payBtns.length; i++){
                payBtns[i].disabled = false;
            }
        }

        window.hideErrors = function() {
            alertDiv.classList.add('hidden')
        }

        window.displayErrors = function(errors) {

		
            if(typeof errors === "object" && errors.constructor === Array) {
                var errHtml = '<ul>';
                for(var i=0; i<errors.length;i++){
                    errHtml += '<li>' + errors[i] + '</li>';
                }
                errHtml += '</ul>';
            } else {
                errHtml = '<p>'+ errors + '</p>';
            }
            alertDiv.innerHTML = errHtml;
            alertDiv.classList.remove('hidden');
        }

        $.fn.toggleInputError = function(erred) {
            this.toggleClass('has-error', erred);
            return this;
        };

        form.addEventListener('submit', function(event) {

            event.preventDefault();
			var focus = false;
            disablePayBtns();
            hideErrors();
            // Get Card details
            var cardnum = document.getElementById('cardnum').value;
            var cardexp = $('#cardexp').payment('cardExpiryVal');
            var cardcvv = document.getElementById('cardcvv').value;
            var cardType = $.payment.cardType($('.cc-number').val());

            var iscardValid = $.payment.validateCardNumber(cardnum);
            var isExpValid = $.payment.validateCardExpiry(cardexp);
            var iscvvValid = $.payment.validateCardCVC(cardcvv, cardType);

            $('.cardnum').toggleInputError(!iscardValid);
            $('.cardexp').toggleInputError(!isExpValid);
            $('.cardcvv').toggleInputError(!iscvvValid);

		
            var errors = [];
            if(!iscardValid || !isExpValid || !iscvvValid){
                enablePayBtns();
                return false;
            }
		

			if(errors.length > 0) { enablePayBtns(); return displayErrors(errors) }
			
	

            Stripe.setPublishableKey('{{ config('services.stripe.key') }}');
            Stripe.card.createToken({
                number: cardnum,
                cvc: cardcvv,
                exp_month: cardexp.month,
                exp_year: cardexp.year
            }, function(status, response){
			
                if (response.error) {
                    enablePayBtns();
                    return displayErrors(response.error.message);
                } else {
                    var token = response.id;
                    document.getElementById('stripetoken').value = token;
                    form.submit();
                }
            });
        });


</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js" ></script>
@endpush


