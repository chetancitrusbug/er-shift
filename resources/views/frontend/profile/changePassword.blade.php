@extends('layouts.frontendDashboard')


@section('title',trans('dashboard.profile.change_pw'))

@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('dashboard.profile.change_pw',['works'=>'vimal'])</div>
                <div class="panel-body">

                    <a href="{{ url('/profile') }}" title="Back">
                        <!--<button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('dashboard.profile.back')
                        </button>-->

                    </a>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::open([
                        'method' => 'PATCH',
                        'class' => 'form-horizontal'
                    ]) !!}


                    <div class="form-group{{ $errors->has('current_password') ? ' has-error' : ''}}">
                        {!! Form::label('current_password', trans('dashboard.profile.current_pwd'), ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::password('current_password', ['class' => 'form-control','required'=>'required']) !!}
                            {!! $errors->first('current_password', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
                        {!! Form::label('password', trans('dashboard.profile.pwd'), ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::password('password', ['class' => 'form-control','required'=>'required']) !!}
                            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>


                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : ''}}">
                        {!! Form::label('password_confirmation', trans('dashboard.profile.pwd_confirmation'), ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::password('password_confirmation', ['class' => 'form-control','required'=>'required']) !!}
                            {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-4">
                            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('dashboard.profile.change_pw'), ['class' => 'btn btn-primary btn-submit']) !!}
                            <a href="{{ url('/profile') }}" >
                                <button type="button" class="btn btn-danger btn-cancel">Cancel</button>
                            </a>
                        </div>
                    </div>


                    {!! Form::close() !!}


                </div>
            </div>
        </div>
    </div>
@endsection