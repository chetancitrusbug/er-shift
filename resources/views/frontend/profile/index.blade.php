@extends('layouts.frontendDashboard')


@section('title',trans('dashboard.profile.my_profile'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('dashboard.profile.profile',['works'=>'vimal'])</div>
                <div class="panel-body">
                        <div class="box-content ">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="{{ url('/profile/edit') }}" class="btn btn-success btn-sm"
                                        title="Edit Profile">
                                         <i class="fa fa-edit" aria-hidden="true"></i> @lang('dashboard.profile.edit_profile')
                                    </a>
                                    <a href="{{ url('profile/change-password') }}" class="btn btn-info btn-sm"
                                         title="Change Password">
                                        <i class="fa fa-lock" aria-hidden="true"></i> @lang('dashboard.profile.change_pw') 
                                    </a>
                                </div>

                                <div class="col-md-6">
                                </div>
                            </div>
                            <hr>

                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    
                                    <tr>
                                        <td class="col-md-2">@lang('dashboard.profile.name') :</td>
                                        <td>{{@$emp_deatil->user->name}}</td>
                                        <td>@lang('dashboard.profile.email') :</td>
                                        <td>{{@$emp_deatil->user->email}}</td>
                                    </tr>
                                    <tr>
                                        <td>@lang('dashboard.profile.profile_name') :</td>
                                        <td>{{@$emp_deatil->emp_name}}</td>
                                        <td>@lang('dashboard.profile.company') :</td>
                                        <td>{{@$emp_deatil->comp_name}}</td>
                                    </tr>
                                    <tr>
                                        <td>@lang('dashboard.profile.phone') :</td>
                                        <td>{{ @$emp_deatil->tel_number}}</td>
                                        <td>@lang('dashboard.profile.comp_websitye') </td>
                                        <td>{{@$emp_deatil->comp_website}}</td>
                                       
                                    </tr>
                                    <tr>
                                        <td>@lang('dashboard.profile.profile_image')</td>
                                        <td>
                                            @if($emp_deatil->profile_pic != '')
                                            <img src={{$emp_deatil->profile_pic}} width="80px"/>
                                            @endif
                                        </td>
                                        <td>@lang('dashboard.profile.address') </td>
                                        <td>{{@$emp_deatil->address}}</td>
                                    </tr>
                                    <tr>
                                        <td>@lang('dashboard.profile.location') </td>
                                        <td>{{@$emp_deatil->location->name}}</td>
                                    </tr>
                                </table>
                            </div>


                    

                    {{--<pre>--}}
                    {{--{!! json_encode(Auth::user(),JSON_PRETTY_PRINT) !!}--}}
                    {{--</pre>--}}

                </div>
            </div>
        </div>
    </div>
@endsection