@extends('layouts.frontendDashboard')

@section('title',trans('dashboard.profile.edit_profile'))

@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('dashboard.profile.edit_profile',['works'=>'vimal'])</div>
                <div class="panel-body">
                <div class="box-content ">
                    {{-- <a href="{{ route('frontend.profile.index') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Back
                            </button>
    
                        </a> --}}
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ url('/profile/edit') }}" enctype='multipart/form-data'>
                        {{ csrf_field() }}
                            <input type="hidden" name="uid" value="{{ Auth::user()->id }}"/>
                        <div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">
                            <label for="fname" class="col-md-2 control-label">@lang('dashboard.profile.fname')</label>

                            <div class="col-md-6">
                                <input id="fname" type="text" class="form-control" name="fname" value="{{ ((Auth::user()->fname) ? Auth::user()->fname : old('fname') ) }}"  autofocus>

                                @if ($errors->has('fname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('lname') ? ' has-error' : '' }}">
                            <label for="lname" class="col-md-2 control-label">@lang('dashboard.profile.lname')</label>

                            <div class="col-md-6">
                                <input id="lname" type="text" class="form-control" name="lname" value="{{ ((Auth::user()->lname) ? Auth::user()->lname : old('lname') ) }}"  autofocus>

                                @if ($errors->has('lname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-2 control-label">@lang('dashboard.profile.email')</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ Auth::user()->email}}" readonly>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('emp_name') ? ' has-error' : '' }}">
                            <label for="emp_name" class="col-md-2 control-label">@lang('dashboard.profile.profile_name')</label>

                            <div class="col-md-6">
                                <input id="emp_name" type="text" class="form-control" name="emp_name" value="{{ ((@$emp_deatil->emp_name) ? @$emp_deatil->emp_name : old('emp_name') ) }}"  autofocus>

                                @if ($errors->has('emp_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('emp_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('profile_pic') ? ' has-error' : '' }}">
                            {!! Form::label('profile_pic',trans('dashboard.profile.profile_image'), ['class' => 'col-md-4 control-label']) !!}
                                
                            <div class="col-md-6">
                                {!! Form::file('profile_pic', null, ['class' => 'form-control']) !!}
                                
                                @if ($errors->has('profile_pic'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('profile_pic') }}</strong>
                                    </span>
                                @endif
                            </div>
                            @if(isset($emp_deatil) && $emp_deatil->profile_pic)
                                <img src={{ $emp_deatil->profile_pic }} alt="image" width="100">
                                @endif
                        </div>
                        
                        <div class="form-group{{ $errors->has('comp_name') ? ' has-error' : '' }}">
                            <label for="comp_name" class="col-md-2 control-label">@lang('dashboard.profile.copmp_name')</label>

                            <div class="col-md-6">
                                <input id="comp_name" type="text" class="form-control" name="comp_name" value="{{ ((@$emp_deatil->comp_name) ? @$emp_deatil->comp_name : old('comp_name') ) }}"  autofocus>

                                @if ($errors->has('comp_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('comp_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('comp_website') ? ' has-error' : '' }}">
                            <label for="comp_website" class="col-md-2 control-label">@lang('dashboard.profile.comp_websitye')</label>

                            <div class="col-md-6">
                                <input id="comp_website" type="text" class="form-control" name="comp_website" value="{{ ((@$emp_deatil->comp_website) ? @$emp_deatil->comp_website : old('comp_website') ) }}"  autofocus>

                                @if ($errors->has('comp_website'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('comp_website') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('comp_information') ? ' has-error' : '' }}">
                            <label for="comp_information" class="col-md-2 control-label">@lang('dashboard.profile.comp_information')</label>

                            <div class="col-md-6">
                                <input id="comp_information" type="text" class="form-control" name="comp_information" value="{{ ((@$emp_deatil->comp_information) ? @$emp_deatil->comp_information : old('comp_information') ) }}" >

                                @if ($errors->has('comp_information'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('comp_information') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('tel_number') ? ' has-error' : '' }}">
                            <label for="tel_number" class="col-md-2 control-label">@lang('dashboard.profile.tel_number')</label>

                            <div class="col-md-6">
                                <input id="tel_number" type="tel_number" class="form-control" name="tel_number" value="{{ ((@$emp_deatil->tel_number) ? @$emp_deatil->tel_number : old('tel_number') ) }}" 
                                onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="12" >

                                @if ($errors->has('tel_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tel_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-2 control-label">@lang('dashboard.profile.address')</label>

                            <div class="col-md-6">
                                <textarea id="address" class="form-control" name="address">{{ ((@$emp_deatil->address) ? @$emp_deatil->address : old('address') ) }} </textarea>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                            <label for="location" class="col-md-2 control-label">@lang('dashboard.profile.location')</label>

                            <div class="col-md-6">
                                {!! Form::select('location', $location, isset($emp_deatil->location_id) ? $emp_deatil->location_id : [], ['id' => 'location','class' => 'form-control  selectTag']) !!}
                            
                                @if ($errors->has('location'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <button type="submit" class="btn btn-primary btn-submit">
                                        @lang('dashboard.profile.update')
                                </button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
@endsection