<link href="{{ asset('assets/css/aos.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/style.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/custom.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/dev.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/rangeslider.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<link href="{{ asset('assets/css/sweetalert.css') }}" rel='stylesheet' type="text/css"/>
<link href="{{ asset('assets/css/datepicker-b4.min.css') }}" rel='stylesheet' type="text/css"/>
<style>

</style>
@stack('css')
