<script type="text/javascript" src="{{ asset('assets/js/modernizr.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-3.3.1.slim.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/popper.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/select2.full.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/datepicker-b4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/get-owlslider.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/aos.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/sweetalert.js') }}" ></script>
<script src="https://www.gstatic.com/firebasejs/5.3.1/firebase.js"></script>
<script defer src="https://www.gstatic.com/firebasejs/7.14.2/firebase-app.js"></script>
<script defer src="https://www.gstatic.com/firebasejs/7.14.2/firebase-auth.js"></script>
<script defer src="https://www.gstatic.com/firebasejs/7.14.2/firebase-firestore.js"></script>
<script>
/* select 2 */
$(".js-select2").select2({
    minimumResultsForSearch: -1
});
/* End of select 2 */

const firebaseConfigMain = {
  apiKey: "AIzaSyCTKyxl14N6wYjtEIlsu-tVe5gcLEUnkMQ",
  authDomain: "er-shift-6ba97.firebaseapp.com",
  databaseURL: "https://er-shift-6ba97.firebaseio.com",
  projectId: "er-shift-6ba97",
  storageBucket: "er-shift-6ba97.appspot.com",
  messagingSenderId: "976488637935",
  appId: "1:976488637935:web:e9b222e78940141440421a"
};

firebase.initializeApp(firebaseConfigMain);
var unread_messages = 0;

var firebase_database = firebase.database();
var firebase_storageRef = firebase.storage();

@auth
    var auth_invites = <?php echo json_encode(Auth::user()->invites); ?> ;
    var my_invite_count = "{{ Auth::user()->my_invite_count }}" ;
@endauth
@guest
    var auth_invites = '' ;
    var my_invite_count = 0 ;
@endguest

firebase_database.ref('ChatMessageModel').on('value',(snap)=>{
    unread_messages = 0;
    if (auth_invites.length > 0) {
        $.each(snap.val(), function( index, value ) {
            if ( $.inArray(parseInt(index), auth_invites ) > -1) {
                $.each(value, function( msg_index, msg_value ) {
                    if (msg_value.read_status == 0 && msg_value.senderId != '{{Auth::id()}}' && msg_value.receiverId == '{{Auth::id()}}') {
                        unread_messages += 1;
                    }
                });
            }
        });
    }
    unread_messages = parseInt(unread_messages) + parseInt(my_invite_count) ;
    if (parseInt(unread_messages) > 0) {
        $("#unread_mesage_count").html(unread_messages);
        $("#unread_mesage_count").show();
    } else {
        $("#unread_mesage_count").hide();
    }
    console.log('===============unread_messages=====================');
    console.log(unread_messages);
    console.log('====================================');
});
</script>
@stack('js')

