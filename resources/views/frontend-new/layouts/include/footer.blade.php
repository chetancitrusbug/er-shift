<footer>
    <div class="footer-root">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-8 col-md-12">

            <div class="footer-div" data-aos="fade-up">

              <div class="row" >
                <div class="col-md-4">
                    <div class="footer-list">
                        <ul class="footer-ul">
                            <li><a href="{{ $android_url  }}" target="_blank" class="link-footer">Download Android App </a></li>
                            <li><a href="{{ $ios_url  }}" target="_blank" class="link-footer">Download ios App </a></li>
                            <li><a target="_blank" href="{{config('setting.DOCTOR_URL')}}" class="link-footer">ED Staff Login</a></li>
                            @guest
                            <li><a href="#" class="link-footer sign-in-btn" data-toggle="modal" data-target="#sign-in-modal">Sign In</a></li>
                            @endguest
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-list">
                        <ul class="footer-ul">
                            <li><a href="{{ route('privacy') }}" class="link-footer">Privacy Policy </a></li>
                            <li><a href="{{ route('terms') }}" class="link-footer">Terms & Conditions </a></li>
                            <li><a href="{{ route('contact') }}" class="link-footer">Contact us </a></li>
                            <li><a href="{{route('shifts')}}" class="link-footer">Search Shifts </a></li>
                          {{-- <li><a href="#" class="link-footer">About </a></li>
                          <li><a href="#" class="link-footer">Careers</a></li> --}}

                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="footer-list">

          
                      <ul class="footer-ul">

                        <li class="link-footer">Address</li>
                        <li class="link-footer">ER Shifts</li>
                        <li class="link-footer">1028 South Bishop Ave,</li>
                        <li class="link-footer">#244, Rolla, MO 65401</li>
                        

                        {{-- <li><a  href="{{ route('shifts').'?doctor_type=RN' }}" class="link-footer">Find RN shifts </a></li>
                        <li><a  href="{{ route('shifts').'?doctor_type=NP' }}" class="link-footer">Find NP shifts </a></li>
                        <li><a href="{{ route('shifts').'?doctor_type=DR' }}" class="link-footer">Find DR shifts</a></li>
                        <li><a  href="{{ route('shifts').'?doctor_type=PA' }}" class="link-footer">Find PA shifts</a></li> --}}
                        {{-- <li><a href="#" class="link-footer">Join with us </a></li>
                        <li><a href="#" class="link-footer">Learn more </a></li>
                        <li><a href="#" class="link-footer">Community </a></li> --}}
                      </ul>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                    <div class="social-icon-div-root">
                        <ul class="footer-social-ul">
                            @if($linkedin_url)
                            <li>
                                <a href="{{ $linkedin_url }}" class="social-link linkedin-link"><i class="fab fa-linkedin-in"></i></a>
                            </li>
                            @endif
                            @if($facebook_url)
                            <li>
                                <a href="{{ $facebook_url }}" class="social-link facebook-link"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            @endif
                            @if($twitter_url)
                            <li>
                                <a href="{{ $twitter_url }}" class="social-link twitter-link"><i class="fab fa-twitter"></i></a>
                            </li>
                            @endif
                            @if($google_url)
                            <li>
                                <a href="{{ $google_url }}" class="social-link google-plus-link"><i class="fab fa-google-plus-g"></i></a>
                            </li>
                            @endif
                        </ul>
                    </div>
                    <div class="copyright-div">
                        <p>Copyright © {{Carbon\Carbon::now()->format('Y')}} ERShifts.com</p>
                    </div>
                </div>
              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  </footer>

  <a href="javascript:" class="return-to-top"><i class="fas fa-chevron-up"></i></a>
