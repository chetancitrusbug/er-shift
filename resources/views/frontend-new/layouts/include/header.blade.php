@guest
<header>
    <div class="header-div clearfix" >
        <div class="inner-top-header-div clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="header-container">

                            <div class="logo-div">
                                <a class="logo_link clearfix" href="{{ url('/') }}">
                                    <img src="{{ asset('assets/images/logo.png') }}" class="img-fluid logo_img main-logo" alt="logo" />
                                </a>
                            </div>

                            <nav class="nav-center-div">
                                <div class="top-nav1">
                                    <div class="cd-shadow-layer"></div>
                                    <div class="nav-m-bar"><a onclick="openNav()" class="opennav" data-placement="bottom" title="" data-original-title="Menu">
                                        <i class="menu-bars"><img src="{{ asset('assets/images/icons/menu-white.svg') }}" alt="menu" /></i></a>
                                    </div>
                                    <div class="nav-div clearfix" id="mySidenav" >
                                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                        <ul class="nav ullist-inline" id="nav-res" >
                                            <li><a href="{{ route('shifts') }}" class="nav-link">Find Provider</a></li>
                                            <li><a href="{{ route('contact') }}" class="nav-link">Contact</a></li>
                                            <li><a href="#" class="nav-link sign-in-btn" data-toggle="modal" data-target="#sign-in-modal">Sign In</a></li>
                                            <li> <a href="#" class="btn btn-outline-primary btn-gradient-primary get-started-btn ml-10 sign-up-btn" data-toggle="modal" data-target="#sign-up-modal">Get Started <i class="fe fe-arrow-right"></i></a> </li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div><!--  End of header with navigation div  -->
  </header>

  @else


  <header>
    <div class="header-div header-login-div clearfix" >
      <div class="inner-top-header-div clearfix">
          <div class="container">
              <div class="row">
                  <div class="col-lg-12 col-md-12">
                      <div class="header-container">

                          <div class="logo-div">
                              <a class="logo_link clearfix" href="{{ url('/') }}">
                                  <img src="{{ asset('assets/images/logo.png') }}" class="img-fluid logo_img main-logo" alt="logo" />
                              </a>
                          </div>

                          <nav class="nav-center-div">
                              <div class="top-nav1">
                                  <div class="cd-shadow-layer"></div>
                                  <div class="nav-m-bar"><a onclick="openNav()" class="opennav" data-placement="bottom" title="" data-original-title="Menu">
                                      <i class="menu-bars"><img src="{{ asset('assets/images/icons/menu-white.svg') }}" alt="menu" /></i></a>
                                  </div>
                                  <div class="nav-div clearfix" id="mySidenav" >
                                      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                      <ul class="nav ullist-inline" id="nav-res" >
                                          <li><a href="{{ route('shifts') }}" class="nav-link">Find Provider</a></li>
                                          <li><a href="{{ route('my-shifts') }}" class="nav-link">My Shifts</a></li>
                                          <li>
                                                <a href="{{ route('message') }}" class="nav-link">
                                                    <span>Messages</span>
                                                    <span id="unread_mesage_count" class="badge"></span>
                                                </a>
                                            </li>
                                            <li><a href="{{ route('plans') }}" class="nav-link">Subscription</a></li>
                                          <li>
                                            <div class="user-drop-down">
                                              <div class="dropdown drop-left dropdown-custom-top">
                                                  <a class="btn btn-default dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                      <div class="user-profile">
                                                        <div class="user-img">
                                                              <img src="{{ Auth::user()->employeeDetail ? Auth::user()->employeeDetail->profile_image_url : asset('assets/images/avatar.jpg')  }}" class="user-top-image" alt="user image" />
                                                          </div>
                                                          <div class="user-info">
                                                              <h3>{{ Auth::user()->name }}</h3>
                                                          </div>
                                                      </div>
                                                  </a>
                                                  <div class="dropdown-menu dropdown-custom-menu" aria-labelledby="dropdownMenuLink">
                                                    <ul>
                                                      <li><a class="dropdown-item" href="{{ route('profile.edit') }}">Update Profile</a></li>
                                                      <li><a class="dropdown-item" href="{{ route('profile.change-password') }}">Change password</a></li>
                                                      <hr />

                                                      <li class="logout-li"><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a></li>

                                                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"> {{ csrf_field() }} </form>

                                                    </ul>
                                                  </div>
                                              </div>
                                          </div>
                                          </li>
                                      </ul>
                                  </div>
                              </div>
                          </nav>

                      </div>

                  </div>
              </div>
          </div>
      </div>
    </div><!--  End of header with navigation div  -->
  </header>

@endguest
