
    <section class="shifts-card-section" >
        <div class="shifts-card-div">
          <div class="container container-1000">

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="heading-div" data-aos="fade-down">
                        <h2>Recent Shifts</h2>
                        <p>Most Recommended Shift by ER shift with all types of Different category</p>
                    </div>
                </div>
            </div>

            <div class="general-card-box-root">

            @foreach($recent_shifts as $key => $shifts)

            <a href="{{ route('shifts.view',$shifts->id) }}" class="basic-img-link">
              <div class="general-find-card-box" data-aos="fade-right">

                <div class="find-card-box-main">
                  <div class="find-card-box-top">
                    <div class="img-thumb">
                     
                        @if (Auth::check() && $shifts->doctor_user && $shifts->doctor_user->profile_image_url)
                          <img src="{{$shifts->doctor_user->profile_image_url }}" class="img-fluid img-thumb-main dc-img" alt="Shifts" />
                      @else
                          <img src="{{ asset('assets/images/avatar.jpg') }}" class="img-fluid img-thumb-main dc-img" alt="Shifts" />
                      @endif
                     
                    </div>

                    <div class="right-details-div">
                      @auth
                        {{ $shifts->doctor_user ? ( $shifts->doctor_user->profile_name ?? $shifts->doctor_user->user->name ) : '' }}
                      @endauth
                      <p>{{ $shifts->wish_title }}</p>
                    </div>


                  </div>
                  <div class="content-div">
                    <div class="label-root">
    
                      <div class="label-div">
                        <span class="icon-div"> <span class="custom-icon stethoscope-icon"></span> </span>
                        <span class="text">{{ $shifts->doctor_user && $shifts->doctor_user->profession ? $shifts->doctor_user->profession->title : '' }} {{ $shifts->doctor_user && $shifts->doctor_user->speciality ? $shifts->doctor_user->speciality->title : '' }}</span>
                      </div>
  
                      <div class="label-div">
                        <span class="icon-div"> <span class="custom-icon fe fe-map-pin"></span></span>
                        <span class="text">{{ $shifts->commaSepLocNames }}</span>
                      </div>
  
                      <div class="d-flex"></div>
  
                      <div class="label-div">
                        <span class="icon-div"> <span class="custom-icon fe fe-dollar-sign"></span></span>
                        <span class="text"> {{ $shifts->rate }}/hr</span>
                      </div>

                      <div class="label-div">
                        <span class="icon-div"> <span class="custom-icon fe fe-calendar"></span></span>
                        <span class="text">Ed Volume {{  $shifts->edvolume ? $shifts->edvolume->title : ''  }}</span>
                      </div>
                      
                    </div>
    
                    {{-- <div class="btn-save-top-right-div"> 
                      <a href="#" class="btn btn-general"><span class="material-icons"> chat </span> <span class="text-span">Chat</span> </a>
                    </div> --}}
    
                  </div>
                </div>
                
              </div>

            </a>
            

              @endforeach


              <div class="btn-row pt-10 d-flex" data-aos="fade-right">
                <a href="{{ route('shifts') }}" class="btn btn-outline-primary btn-gradient-primary">View All Shifts</a>
              </div>

            </div>

          </div>
        </div>
      </section>
