@guest
    @include('frontend-new.modal.login')
    @include('frontend-new.modal.signup')
    @include('frontend-new.modal.forgot-password')
@endguest

@push('js')

  <script type="text/javascript">


    @if(session('status') && session('status') != '' )

    var status = "{!! session('status') !!}";
    swal({
        title:'',
        text: status,
        type: 'success',
        textClass:"text-success",
        confirmButtonClass:'btn-success',
    });
    @endif

    @if($errors->any() && count($errors) > 0 )
    var error = "{!! $errors->first() !!}";
    swal({
        title:'',
        text: error,
        type: 'error',
        textClass:"text-danger",
        confirmButtonClass:'btn-danger',
    });
    @endif

    @if(session('info') && session('info') != '' )
    var info = "{!! session('info') !!}";
        swal('',info)
    @endif



    $(document).ready(function(){

        var login_form_validate = $("#login-form").validate({

            rules: {
                email: {
                    required: true,email:true
                },
                password: {
                    required: true,
                }
            },
            messages: {
                email: {
                    required: "Email address is required",
                    email: "Please enter a valid email address"
                },
                password: {
                    required: "Password is required",
                }
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "password") {
                    $("#error-place-"+element.attr("id")).html(error);
                } else {
                    error.insertAfter(element);
                }
            },
            onfocusout: function(element) {
                this.element(element);
            },
            success: function(error) {
                error.removeClass("error");
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        var fp_form_validate = $("#fp-form").validate({

        rules: {
            email: {
                required: true,email:true
            }
        },
        messages: {
            email: {
                required: "Email address is required",
                email: "Please enter a valid email address"
            },
        },
        onfocusout: function(element) {
            this.element(element);
        },
        success: function(error) {
            error.removeClass("error");
        },
        submitHandler: function(form) {
            form.submit();
        }
        });

        $('.sign-in-btn').on('click',function(){
            login_form_validate.resetForm();
            $("#forgot-password-modal").modal('hide');
            $("#sign-up-modal").modal('hide');
        })

        $('.fp-btn').on('click',function(){
            fp_form_validate.resetForm();
        })

        $('.sign-up-btn').on('click',function(){
            // signup_form_validate.resetForm();
            $("#forgot-password-modal").modal('hide');
        })


        if(!$("#sign-in-modal.show").hasClass('view-sign-in')){
          $("#sign-in-modal").addClass('view-sign-in');
          $('.hide-sign-in').click(function() {
            $("#sign-in-modal").modal('hide');
          });
        }

        if(!$("#sign-up-modal.show").hasClass('view-sign-up')){
          $("#sign-up-modal").addClass('view-sign-up');
          $('.hide-sign-up').click(function() {
            $("#sign-up-modal").modal('hide');
          });
        }

        if(!$("#forgot-password-modal.show").hasClass('view-forgot-ps')){
          $("#forgot-password-modal").addClass('view-forgot-ps');
        //   $('.hide-forgot-pass').click(function() {
        //     $("#forgot-password-modal").modal('hide');
        //   });
        }

        if(!$("#reset-password-modal.show").hasClass('view-reset-ps')){
          $("#reset-password-modal").addClass('view-reset-ps');
          $('.hide-reset-pass').click(function() {
            $("#reset-password-modal").modal('hide');
            $("#sign-in-modal").modal('show');
          });
        }

        /* End of popup */

        /* animation js init */
        AOS.init({
          duration: 1500,
          disable: function() {
          var maxWidth = 800;
          return window.innerWidth < maxWidth;
        }
        });
        /* End of animation js */

        /* password */

        $('#show_password').on('click', function(){
          var passwordField2 = $('#password');
          var passwordFieldType2 = passwordField2.attr('type');

          if(passwordField2.val() != '')
          {
              if(passwordFieldType2 == 'password')
              {
                  passwordField2.attr('type', 'html');
                  $(this).html('<i class="fe fe-eye password-hide"></i>');
              }
              else
              {
                  passwordField2.attr('type', 'password');
                  $(this).html('<i class="fe fe-eye-off password-view"></i>');
              }
          }
        });

        $('#show_password2').on('click', function(){
          var passwordField2 = $('#password2');
          var passwordFieldType2 = passwordField2.attr('type');

          if(passwordField2.val() != '')
          {
              if(passwordFieldType2 == 'password')
              {
                  passwordField2.attr('type', 'html');
                  $(this).html('<i class="fe fe-eye password-hide"></i>');
              }
              else
              {
                  passwordField2.attr('type', 'password');
                  $(this).html('<i class="fe fe-eye-off password-view"></i>');
              }
          }
        });

        $('#show_password3').on('click', function(){
          var passwordField2 = $('#password3');
          var passwordFieldType2 = passwordField2.attr('type');

          if(passwordField2.val() != '')
          {
              if(passwordFieldType2 == 'password')
              {
                  passwordField2.attr('type', 'html');
                  $(this).html('<i class="fe fe-eye password-hide"></i>');
              }
              else
              {
                  passwordField2.attr('type', 'password');
                  $(this).html('<i class="fe fe-eye-off password-view"></i>');
              }
          }
        });

        /* End of password */

      });
  </script>
  @endpush

