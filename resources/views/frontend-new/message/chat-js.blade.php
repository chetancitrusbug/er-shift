@push('js')
<script src="{{asset("assets/js/typeahead.bundle.min.js") }}"></script>
<script src="{{asset("assets/js/handlebars.min.js")}}"></script>
<script type="text/javascript" src="{{ asset('assets/js/moment.js') }}"></script>

<script type="text/javascript">
/*
 const firebaseConfig = {
  apiKey: "AIzaSyCTKyxl14N6wYjtEIlsu-tVe5gcLEUnkMQ",
  authDomain: "er-shift-6ba97.firebaseapp.com",
  databaseURL: "https://er-shift-6ba97.firebaseio.com",
  projectId: "er-shift-6ba97",
  storageBucket: "er-shift-6ba97.appspot.com",
  messagingSenderId: "976488637935",
  appId: "1:976488637935:web:e9b222e78940141440421a"
};


firebase.initializeApp(firebaseConfig);
*/

var users = [];
var user_chat = [];
var database = firebase.database();
var storageRef = firebase.storage();

var invites = <?php echo json_encode(Auth::user()->invites); ?> ;
var connections = <?php echo  json_encode(array_merge(Auth::user()->invites_from()->pluck('to_id')->toArray(),Auth::user()->invites_to()->pluck('from_id')->toArray())); ?> ;





database.ref('ChatMessageModel').on('value',(snap)=>{
    var unread_count = 0 ;
    if (invites.length > 0) {
        $.each(snap.val(), function( index, value ) {
            if ( $.inArray(parseInt(index), invites ) > -1) {
                user_chat[index] = value;
                $.each(value, function( msg_index, msg_value ) {
                    users[index] = msg_value;
                    if (msg_value.read_status == 0 && msg_value.senderId != '{{Auth::id()}}' && msg_value.receiverId == '{{Auth::id()}}') {
                        unread_count++;
                    }
                });
            }
            var unread_display =  $('.unread_mesage_count[data-invitation_id="'+parseInt(index)+'"]') ;
            if(unread_display.length == 1 && unread_count > 0 ){
                unread_display.html(unread_count);
            }else{
                unread_display.html("");
            }
        });
    }
});



database.ref('ChatMessageModel').on('child_added',(snap)=>{
    if (invites.length > 0) {
        $.each(snap.val(), function( index, value ) {
            if ( $.inArray(parseInt(index), invites ) > -1) {
                user_chat[index] = value;
                $.each(value, function( msg_index, msg_value ) {
                    users[index] = msg_value;
                    return false;
                });
            }
        });
    }
});

database.ref('ChatMessageModel').on('child_changed',(snap)=>{
    if (invites.length > 0) {
        $.each(snap.val(), function( index, value ) {
            if ( $.inArray(parseInt(value.inviteId), invites ) > -1) {
                user_chat[value.inviteId] = snap.val();
                if (value.inviteId == $(".msg_send_btn").attr('data-invitation_id')) {
                    if (value.inviteId  ) {
                        $("#chat_history").html("");
                        $.each(snap.val(), function( frb_msg_id, message ) {
                            if (message && message.file) {
                                AddFileWithMessage(message);
                            }else if (message && message.message) {
                                AddMessage(message);
                                if (message.read_status == 0 && message.senderId != '{{Auth::id()}}' && message.receiverId == '{{Auth::id()}}') {
                                    SetMessageAsRead(value.inviteId,frb_msg_id);
                                }
                            }
                        });
                    }
                }
                return false;
            }
        });
    }
});

function SetMessageAsRead(inv_id,id) {

    database.ref("ChatMessageModel/"+inv_id+"/"+id).update({read_status: 1 }).then(function(){
        var unread_display =  $('.unread_mesage_count[data-invitation_id="'+parseInt(inv_id)+'"]') ;
        unread_display.html("");
    }).catch(function(error) {
        console.log("Data could not be saved." + error);
    });
}


$(document).on('click', '.chat_list', function (e) {
    var invitation_id = $(this).attr("data-invitation_id");
    $(".msg_send_btn").attr('data-invitation_id',invitation_id)
    $(".msg_send_btn").attr('data-chat_user_id',$(this).attr("data-chat_user_id"))
    $(".msg_send_btn").prop('disabled',false);
    $("#chat_history").html("");

    $("#chat-message-header").html('<div class="chat-prfile-user">\
            <img src="'+$(this).attr("data-user_image")+'" alt="'+$(this).attr("data-user_name")+'">\
        </div>\
        <div class="chat-message-detail">\
            <h3>'+$(this).attr("data-user_name")+'</h3>\
        </div>');

    if (invitation_id ){
        if(user_chat[invitation_id] ) {
            $.each(user_chat[invitation_id], function( frb_msg_id, message ) {
                if (message && message.file) {
                    AddFileWithMessage(message);
                }else if (message && message.message) {
                    AddMessage(message);
                    if (message.read_status == 0 && message.senderId != '{{Auth::id()}}' && message.receiverId == '{{Auth::id()}}') {
                        SetMessageAsRead(invitation_id,frb_msg_id);
                    }
                }
            });
        }else{
            $.ajax({
                    type: "get",
                    url: "{{route('get-invite')}}" ,
                    data:{id:invitation_id},
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token()?>"
                    },
                    success: function (data) {
                        if(data.id > 0){
                            if(data.status == "0"){
                                $(".msg_send_btn").prop('disabled',true);
                                if(data.from_id == "{{ Auth::id() }}"){
                                    $("#chat_history").html("Your invitation is not accepted yet!!");
                                }else{
                                    var content = '<button class="btn btn-info update-invite" data-value="1" data-invite_id="'+data.id+'" >Accept</button>\
                                    <button class="btn btn-default update-invite" data-value="2" data-invite_id="'+data.id+'">Decline</button>'
                                    $("#chat_history").html(content);
                                }
                            }
                        }
                    },
            });
        }
        var div = document.getElementById('chat_history');
        $('#chat_history').animate({
            scrollTop: div.scrollHeight + div.clientHeight
        }, 500);

    }

});
/* Dynamically added tooltip */
$('body').tooltip({
    selector: '[data-toggle="tooltip"]'
});

$(document).on('change', '#customFile', function (e) {
    var fileName = $(this).val();
    var file_data =$('#customFile').prop('files')[0]
    if (fileName && file_data && file_data != '' && file_data != undefined) {
        $(".custom-file-label").html('<i class="fa fa-file" data-toggle="tooltip" title="'+file_data.name+'"></i>');
    } else {
        $(".custom-file-label").html('<i class="fe fe-paperclip"></i>');
    }
});

$(document).on('click', '.invite-reject-invite', function (e) {
    var inv_id = parseInt( $(this).attr('data-invitation_id') );
    var status = parseInt( $(this).attr('data-status') );
    $.ajax({
        type: "post",
        url: "{{route('update-invite')}}" ,
        data:{invitation_id:parseInt(inv_id),read_status:parseInt($(this).attr('data-read_status')),status:status},
        headers: {
            "X-CSRF-TOKEN": "<?php echo csrf_token()?>"
        },
        success: function (data) {
            if( data.code == 200){

                var message_counter = $("#unread_mesage_count").html();
                var update_count  = parseInt(message_counter) - 1 ;
                if (parseInt(update_count) > 0) {
                    $("#unread_mesage_count").html(update_count);
                    $("#unread_mesage_count").show();
                } else {
                    $("#unread_mesage_count").hide();
                }

                $("#chat_history").html("");
                $("#chat-message-header").html("");
                if(status == 1){
                    $(".msg_send_btn").prop('disabled',false);
                    $('.invite-reject-invite').remove();
                }
                if(status == 2){
                    $('.chat_list[data-invitation_id="'+inv_id+'"]').remove() ;

                }
                swal({
                    title:'',
                    text: data.message,
                    type: 'success',
                    textClass:"text-success",
                    confirmButtonClass:'btn-success',
                });
            }
            if(data.code == 400){
                swal({
                    title:'',
                    text: data.message,
                    type: 'error',
                    textClass:"text-danger",
                    confirmButtonClass:'btn-danger',
                });
            }
        },
    });
});

$(document).on('click', '.update-invite', function (e) {

    var inv_id = parseInt( $(this).attr('data-invite_id') );
    var value = parseInt( $(this).attr('data-value') );
    if(inv_id !='' && value != ''){
        $.ajax({
            type: "post",
            url: "{{route('update-invite')}}" ,
            data:{invitation_id:inv_id,read_status:1,status:value},
            headers: {
                "X-CSRF-TOKEN": "<?php echo csrf_token()?>"
            },
            success: function (data) {
                if( data.code == 200){



                    $("#chat_history").html("");
                    if(value == 1){
                        $(".msg_send_btn").prop('disabled',false);
                    }
                    if(value == 2){
                        $('.chat_list[data-invitation_id="'+inv_id+'"]').remove() ;
                    }
                    swal({
                        title:'',
                        text: data.message,
                        type: 'success',
                        textClass:"text-success",
                        confirmButtonClass:'btn-success',
                    });


                }
                if(data.code == 400){
                    swal({
                        title:'',
                        text: data.message,
                        type: 'error',
                        textClass:"text-danger",
                        confirmButtonClass:'btn-danger',
                    });
                }
            },
        });
    }



});

function AddMessage(message) {
    if (message.senderId == '{{Auth::id()}}') {
        $("#chat_history").append('<div class="outgoing_msg">\
            <div class="sent_msg">\
                <p>'+ message.message +'</p>\
                <span class="time_date">'+moment(message.time).format('YYYY-MM-DD h:mm a')+'</span>\
            </div>\
        </div>');
    } else {
        //<div class="incoming_msg_img"> <img src="'+message.userImage+'" alt="'+message.userName+'"> </div>\
        $("#chat_history").append('<div class="incoming_msg">\
            <div class="received_msg">\
                <div class="received_withd_msg">\
                    <p>'+ message.message +'</p>\
                    <span class="time_date"> '+moment(message.time).format('YYYY-MM-DD h:mm a')+'</span>\
                </div>\
            </div>\
        </div>');
    }
}
function AddFileWithMessage(message) {
    if (message.senderId == '{{Auth::id()}}') {
        var message_html = '<div class="outgoing_msg">\
            <div class="sent_msg">\
                <p class="file_detail"><a target="_blank" href="'+message.file.url_file+'"><i class="fe fe-file">'+message.file.name_file+'</i></a></p>';
                // <embed src='+message.file.url_file+'/>';
                if(message.message) {
                    message_html = message_html + '<p>'+ message.message +'</p>';
                }
                message_html = message_html + '<span class="time_date">'+moment(message.time).format('YYYY-MM-DD h:mm a')+'</span>\
            </div>\
        </div>';
        $("#chat_history").append(message_html);
    } else {
        //<div class="incoming_msg_img"> <img src="'+message.userImage+'" alt="'+message.userName+'"> </div>\
        var message_html = '<div class="incoming_msg">\
            <div class="received_msg">\
                <div class="received_withd_msg">\
                    <p class="file_detail"><a target="_blank" href="'+message.file.url_file+'"><i class="fe fe-file">'+message.file.name_file+'</i></a></p>';
                    // <embed src='+message.file.url_file+'/>';
                    if(message.message) {
                        message_html = message_html + '<p>'+ message.message +'</p>';
                    }
                    message_html = message_html + '<span class="time_date"> '+moment(message.time).format('YYYY-MM-DD h:mm a')+'</span>\
                </div>\
            </div>\
        </div>';
        $("#chat_history").append(message_html);
    }
}

$(document).on('click', '.msg_send_btn', function (e) {

    var file_url =  "" ;
    var fileExtension = "" ;
    var file_path = "" ;
    var file_data = $('#customFile').prop('files')[0];



    var invitation_id = $(this).attr("data-invitation_id");
    var chat_user_id = $(this).attr("data-chat_user_id") ;
    var message = $('.write_msg').val() ;
    if( (file_data == '' || file_data == undefined) && message == ""){
        swal('','Enter message to send');
        return false ;
    }
    var date = "{{ Carbon\Carbon::now()->format('Y-m-d H:i:s') }}" ;
    var d = new Date;
    dformat = [d.getFullYear().toString().padStart(2, '0'),
                (d.getMonth()+1).toString().padStart(2, '0'),
               d.getDate().toString().padStart(2, '0'),
               ].join('-')+' '+
              [d.getHours().toString().padStart(2, '0'),
               d.getMinutes().toString().padStart(2, '0'),
               d.getSeconds().toString().padStart(2, '0')].join(':');

    var user_id = "{{ Auth::id() }}" ;
    var date = dformat ;
    var image = "{{ Auth::user()->doctorDetail ? Auth::user()->doctorDetail->profile_image_url : asset('assets/images/avatar.jpg')  }}" ;
    var name = "{{ Auth::user()->name }}" ;
    var chatObj = {
            id: 0,
            inviteId: parseInt(invitation_id),
            message : message,
            read_status : 0,
            receiverId : parseInt(chat_user_id),
            senderId : parseInt(user_id),
            time : date,
            toId:parseInt(chat_user_id),
            type : "sender",
            userImage : image,
            userName: name,
    };
    var send_message = database.ref('ChatMessageModel/'+invitation_id+'/').push(chatObj);
    var last_message_key = send_message.ref.key;

    $('.write_msg').val('');
    if(file_data != '' && file_data != undefined ){
        var fileName = file_data.name;
        fileExtension = fileName.substr((fileName.lastIndexOf('.') + 1));
        var timestamp = Number(new Date());
        uploadTask = storageRef.ref('files/'+timestamp.toString()+'-'+fileName).put(file_data);

        uploadTask.on('state_changed', function(snapshot){
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                $("#file_uploader").show();
                $(".msg_send_btn").attr('disabled',true);
              var uploader = document.getElementById('file_uploader');
              uploader.value=progress;
            
          }, function(error) {console.log(error);
          }, function() {

               // get the uploaded image url back
               var name_file = "";
               var file_size = "";
               var contentType = "";
               uploadTask.snapshot.ref.getMetadata().then(function(metadata) {
                    name_file = metadata.name;
                    file_size = metadata.size;
                    contentType = metadata.contentType;
                }).catch(function(error) {

                });
               uploadTask.snapshot.ref.getDownloadURL().then(
                function(downloadURL) {
                    file_url = downloadURL;
                    var fileObj = {
                        type: 'file',
                        url_file: file_url,
                        name_file : name_file,
                        size_file : file_size.toString(),
                        // contentType:contentType,
                    };
                   
                    database.ref("ChatMessageModel/"+invitation_id+"/"+last_message_key).update({file: fileObj  }).then(function(){
                       
                    }).catch(function(error) {
                        console.log("Data could not be saved." + error);
                    });
                    $("#file_uploader").hide();
                    $(".msg_send_btn").attr('disabled',false);
                  
                    $("#customFile").val('').change();
                    document.getElementById("customFile").value = null;
                   
                    // print the image url
                    console.log(downloadURL);
            });
          });
    }

    var div = document.getElementById('chat_history');
    $('#chat_history').animate({
      scrollTop: div.scrollHeight + div.clientHeight
   }, 500);

        // send push notification of message
        if(message != ""){
            $.ajax({
                type: "post",
                url: "{{route('send-message')}}" ,
                data: { to_id:chat_user_id,invitation_id:invitation_id,message:message },
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token()?>"
                },
                success: function (data) {
                    if( data.code == 200 || data.code == 400){
                        console.log(data.message);
                    }
                },
            });
        }
        $('.write_msg').val("");
});

  // Add the following code if you want the name of the file appear on select
  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $(document).ready(function () {
        var classes = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url:"{{ route('employers') }}?name=%QUERY",
                wildcard: '%QUERY'
            }
        });

        
        var DOCTOR_URL = "{{  config('setting.DOCTOR_URL').'/images/doctor/' }}";
        var DEFAULT_URL =  "{{ url('/assets/images/avatar.jpg') }}";


        var content = ' <div class="search_list">\
                <div class="search_people">\
                <div class="search_chat_img">\
                    <a href="@{{ profile_image_url  profile_image }}" target="_blank"><img src="@{{ profile_image_url profile_image }}" alt="" class="img-fuid u-img" ></a>\
                </div>\
                <div class="search_text name-holder" data-user_id="@{{ uid }}" data-name="@{{ name }}">\
                <h1>@{{ name }}</h1>\
                </div>\
                </div>\
            </div> ';

        var empty_content = '<div class="search_list">\
                <div class="search_people">\
                <div class="search_text name-holder" >\
                <h1> Unable to find any users matching the current name </h1>\
                </div>\
                </div>\
            </div>  ' ;

        $('#prefetch .typeahead').typeahead({
            hint: false,
            highlight: true,
            minLength: 1,
        }, {
            limit: 10,
            display: 'name',
            source: classes,
            templates: {
                empty: empty_content,

                suggestion: Handlebars.registerHelper("profile_image_url", function(profile_image) {
                            if(profile_image){
                                return DOCTOR_URL+profile_image ;
                            }else{
                                return DEFAULT_URL;
                            }
                        }),
                suggestion:Handlebars.compile(content)
            }
        }).on('typeahead:asyncrequest', function() {
            $('#loader').show();
        })
        .on('typeahead:asynccancel typeahead:asyncreceive', function() {
            $('#loader').hide();
        });

        $(document).on('click', '.name-holder', function (e) {

            var name = $(this).data('name') ;
            var to_id = $(this).data('user_id') ;

            if(!connections.includes(to_id)){

                swal({
                title: "",
                text: "Do you want to send chat invitation to "+name+" ?",
                type: "",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                focusCancel:true,
                allowOutsideClick: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        $.ajax({
                            type: "post",
                            url: "{{route('send-invite')}}" ,
                            data:{to_id:to_id},
                            headers: {
                                "X-CSRF-TOKEN": "<?php echo csrf_token()?>"
                            },
                            beforeSend: function(){
                                $("#pageloader").show();
                            },
                            success: function (data) {
                                if( data.code == 200){
                                    swal({
                                        title:'',
                                        text: data.message,
                                        type: 'success',
                                        textClass:"text-success",
                                        confirmButtonClass:'btn-success',
                                    });
                                }
                                if(data.code == 400){
                                    swal({
                                        title:'',
                                        text: data.message,
                                        type: 'error',
                                        textClass:"text-danger",
                                        confirmButtonClass:'btn-danger',
                                    });
                                }
                                if( data.code == 501 ){
                                    swal({
                                        title:'',
                                        text: data.message,
                                        type: 'error',
                                        textClass:"text-danger",
                                        confirmButtonClass:'btn-danger',
                                    });
                                    var url = "{{ route('home') }}";
                                    window.location.href = url;
                                }
                            },
                            complete: function(){
                                $("#pageloader").hide();
                            }
                        });
                    }
                })

            }else{
                // show user in chat list
                var user_list = $('.chat_list[data-chat_user_id="'+to_id+'"]') ;
                if(user_list.length  == 1){
                    user_list.trigger('click')
                }else{
                    swal('','Your invitation request is pending');
                }
            }

        })
    })

</script>
@endpush
