@push('css')
<style>
.container{max-width:1170px; margin:auto;}
img{ max-width:100%;}
.inbox_people {
  background: #f8f8f8 none repeat scroll 0 0;
  float: left;
  overflow: hidden;
  width: 40%; border-right:1px solid #c4c4c4;
}
.inbox_msg {
  border: 1px solid #c4c4c4;
  clear: both;
  overflow: hidden;
}
.top_spac{ margin: 20px 0 0;}


.recent_heading {float: left; width:40%;}
.srch_bar {
  display: inline-block;
  text-align: right;
  width: 60%; padding:
}
.headind_srch{ padding:10px 29px 10px 20px;  border-bottom:1px solid #c4c4c4;}

.recent_heading h4 {
  color: #05728f;
  font-size: 21px;
  margin: auto;
}
.srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;margin-left:25px;}
.srch_bar .input-group-addon button {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  padding: 0;
  color: #707070;
  font-size: 18px;
}
.srch_bar .input-group-addon { margin: 0 0 0 -27px;}


.chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
.chat_ib h5 span{ font-size:13px; float:right;}
.chat_ib p{ font-size:14px; color:#989898; margin:auto}
.chat_img {
  float: left;
  width: 11%;
}
.chat_ib {
  float: left;
  padding: 0 0 0 15px;
  width: 88%;
}

.chat_people{ overflow:hidden; clear:both;}
.chat_list {
  border-bottom: 1px solid #c4c4c4;
  margin: 0;
  padding: 18px 16px 10px;
  cursor: pointer;
}
.inbox_chat { height: 550px; overflow-y: scroll;}

.active_chat{ background:#ebebeb;}

.incoming_msg_img {
  display: inline-block;
  width: 6%;
}
.received_msg {
  display: inline-block;
  padding: 0 0 0 10px;
  vertical-align: top;
  width: 92%;
 }
 .received_withd_msg p {
  background: #ebebeb none repeat scroll 0 0;
  border-radius: 3px;
  color: #646464;
  font-size: 14px;
  margin: 0;
  padding: 5px 10px 5px 12px;
  width: 100%;
}
.time_date {
  color: #747474;
  display: block;
  font-size: 12px;
  margin: 8px 0 0;
}
.received_withd_msg { width: 57%;}
.mesgs {
  float: left;
  padding: 30px 15px 0 25px;
  width: 60%;
}

 .sent_msg p {
  background: #05728f none repeat scroll 0 0;
  border-radius: 3px;
  font-size: 14px;
  margin: 0; color:#fff;
  padding: 5px 10px 5px 12px;
  width:100%;
}
.outgoing_msg{ overflow:hidden; margin:15px 0 15px;}
.incoming_msg{ overflow:hidden; margin:15px 0 15px;}
.sent_msg {
  float: right;
  width: 46%;
}
.input_msg_write input {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  color: #4c4c4c;
  font-size: 15px;
  min-height: 48px;
  width: 100%;
}

.type_msg {border-top: 1px solid #c4c4c4;position: relative;}
.msg_send_btn {
  background: #05728f none repeat scroll 0 0;
  border: medium none;
  border-radius: 50%;
  color: #fff;
  cursor: pointer;
  font-size: 17px;
  height: 33px;
  position: absolute;
  right: 0;
  top: 11px;
  width: 33px;
}
.messaging { padding: 0 0 50px 0;}
.msg_history {
  height: 516px;
  overflow-y: auto;
}

.parent-suggestion-holder {border:1px solid #ddd; margin-left: 30px;padding: 10px;position: relative;vertical-align: middle;}
.parent-suggestion-holder:hover { background:#f9f9f9;}
.parent-suggestion-holder a { display:block; position:relative;vertical-align: middle;}

.sent_msg embed,.outgoing_msg embed{
    max-height: 100px;
}

.msg_send_btn:disabled {
  background: #dddddd;
}

/* 04th may 2020 by aakhil */

.general-card-box-root .messaging .inbox_people .headind_srch {padding: 0; border-bottom: 0;}
.general-card-box-root .messaging .inbox_people .headind_srch .srch_bar {width: 100%; margin: 0px 0 10px 0;}
.general-card-box-root .messaging .inbox_people .headind_srch .recent_heading{ display: none;}
.general-card-box-root .messaging .inbox_msg {border: 0;}
.general-card-box-root .messaging .inbox_people {margin-right: 10px; width: calc(40% - 10px); border-right: 0; background-color: transparent; }

.general-card-box-root .messaging .mesgs{
background: #FFFFFF;
border: 1px solid #EAEBED;
border-radius: 4px;
padding: 0;
}
.general-card-box-root .messaging .mesgs .msg_history {padding: 20px 20px; height: 400px}
.general-card-box-root .messaging .inbox_people .headind_srch .twitter-typeahead input { background: #FFFFFF; border: 1px solid #EAEBED; border-radius: 4px; height: 48px; width: 100%; margin: 0; padding-left: 42px;}

.general-card-box-root .messaging .inbox_people .headind_srch .twitter-typeahead {
  display: flex !important;
  width: 100%;
}
.headind_srch  .srch_bar .stylish-input-group {
  position: relative;
}
.headind_srch  .srch_bar .input-group-addon {
  position: absolute;
    left: 40px;
    top: 15px;
}
.headind_srch  .srch_bar .input-group-addon  button {
  color: #CDD3D9;
}
.general-card-box-root .messaging .inbox_people .inbox_chat {
  overflow-y: auto;
}
.general-card-box-root .messaging .inbox_people .inbox_chat .chat_list{
background: #FFFFFF;
border: 1px solid #E8E8E8;
border-radius: 4px;
margin-bottom: 10px;
padding: 15px 20px;
}

.general-card-box-root .messaging .inbox_people .inbox_chat .chat_list.active-chat {
background-color: #F4F7FB;
}

.general-card-box-root .messaging .inbox_people .inbox_chat .chat_list .chat_people .chat_img {
  height: 55px;
  width: 55px;
  border-radius: 100px;
  position:relative;
  min-width: 55px;
}

.chat_img .active-user {
  height: 13px;
    width: 13px;
    border-radius: 100px;
    background-color: #77B342;
    position: absolute;
    left: 1px;
    top: 1px;
    display: block;
    z-index: 99;
    border: 3px solid #ffffff;
}


.general-card-box-root .messaging .inbox_people .inbox_chat .chat_list .chat_people {
  display: flex;
}
.general-card-box-root .messaging .inbox_people .inbox_chat .chat_list .chat_people a {
  display: flex;
  height: 100%;
  width: 100%;
}

.general-card-box-root .messaging .inbox_people .inbox_chat .chat_list .chat_people a img {
  height: 100%;
  width: 100%;
}

.general-card-box-root .messaging .inbox_people .inbox_chat .chat_list .chat_people .chat_ib h1 {
  font-size: 18px;
  color: #092A47;
  line-height: 22px;
  font-weight: 600;
  margin-bottom: 5px;
  }

  .general-card-box-root .messaging .inbox_people .inbox_chat .chat_list .chat_people .chat_ib span {
font-size: 14px;
color: #092A47;
line-height: 22px;
font-weight: 400;
background-color: transparent;
padding: 0;
display: flex;
justify-content: space-between;
  }

  .chat-action{
    display: flex;
    flex-direction: row;
  }

  .chat-action .btn  {
    font-size: 10px;
    line-height: 18px;
    font-weight: 700;
    height: 30px !important;
  }

.btn-accept {
    background-color: #004C8D;
    color: #ffffff !important;
    font-size: 11px;
    padding: 5px 6px;
  }

  .btn-remove {
    background: #EAEBED;
    color: #424242;
    margin-right: 5px;
  }
  .btn-remove i {
      font-size: 18px;
  }


    /* chat message */

.chat-message-header{
  padding: 30px 0;
  margin: 0 30px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-direction: row;
  border-bottom: 1px solid #EAEBED;
  background: #FFFFFF;
}

.chat-message-header .chat-prfile-user {height: 46px; width: 46px; min-width: 46px;  }
.chat-message-header .chat-prfile-user img {width: 100%; height: 100%; border-radius: 100px;}
.chat-message-header .chat-prfile-user {position: relative;}
.chat-message-header .chat-prfile-user .active-user {
  height: 13px;
    width: 13px;
    border-radius: 100px;
    background-color: #77B342;
    position: absolute;
    left: 1px;
    top: 1px;
    display: block;
    z-index: 99;
    border: 3px solid #ffffff;
}
.chat-message-header .chat-message-detail {padding-left: 12px;}
.chat-message-header .chat-message-detail h3 {
font-size: 20px;
color: #092A47;
font-weight: 600;
line-height: 22px;
}

.status-bar ul {margin: 0; padding: 0; list-style: none;}
.status-bar ul li {display: inline-block; padding-right: 18px; position: relative;}
.status-bar ul li:first-child::before {display: none}
.status-bar ul li::before {
  content: '';
    display: block;
    position: absolute;
    height: 4px;
    width: 4px;
    left: -12px;
    top: 10px;
    background-color: #D8D8D8;
    border-radius: 100px;
}
.status-bar ul li a {
font-size: 14px;
color: #092A47;
line-height: 22px;
font-weight: 500;
}


.outgoing_msg .sent_msg {
  max-width: 70%;
  width: auto;
}

.outgoing_msg .sent_msg p {
    background: #004C8D none repeat scroll 0 0;
    border: 1px solid #91B7DC;
    border-radius: 4px;
    font-size: 16px;
    color: #FFFFFF;
    text-align: right;
    line-height: 28px;
    }

.outgoing_msg .sent_msg .time_date {
  text-align: right;
}
.incoming_msg .received_msg {
  width: 100%;
}
.incoming_msg .received_withd_msg {
  width: 70%;
}
.incoming_msg .received_withd_msg p {
  font-size: 16px;
color: #004C8D;
text-align: left;
line-height: 25px;
border: 1px solid #004c8d;
background: transparent none repeat scroll 0 0;
}

.incoming_msg .received_withd_msg .time_date {
  text-align: left;
}


.type_msg{
  padding: 10px 10px;
  background: #DEE9F4;
  border-top: 0;
}

.type_msg .custom-file{
  position: absolute;
  left: 0;
  top: 0;
  width: 50px;
}

.type_msg .custom-file .custom-file-input{
  display: none;
}

.type_msg .custom-file .custom-file-label{
  width: 50px;
  top: 15px;
  left: 20px;
  background-color: transparent;
  border-color: transparent;
}

.type_msg .custom-file .custom-file-label i {font-size: 20px}
.type_msg .custom-file .custom-file-label::after {
  display:none;
}

.type_msg .custom-file .custom-file-label.selected i {
color: #b5131b;
}

.type_msg .input_msg_write input {
  min-height: 48px;
  background: #FFFFFF;
  border-radius: 100px;
  border-radius: 24px;
  padding-left: 60px;
  padding-right: 60px;
  box-shadow: none;
  outline: 0;
}
.type_msg .input_msg_write {
  height: 100%;
}


.type_msg .msg_send_btn { border-radius: 100px;
    height: 52px;
    width: 52px;
    display: flex;
    background-color: #B5131B;
    align-items: center;
    justify-content: center;
    top: 7px;
    right: 10px;
}


progress#file_uploader {
    width: 100%;
    height: 5px;
    position: absolute;
    top: 0;
    left: 0;
    background-color: green;
}

.file_detail a{
    color: inherit;
    font-size: inherit;
}

@media (max-width: 767px) {

  .inbox_people{
    width: 100% !important;
  }

  .inbox_people .inbox_chat { height: 300px;}

  .mesgs{
    width: 100% !important;
  }
  /* .general-card-box-root{
    overflow-x: scroll;
  }
  .general-card-box-root .messaging {
    min-width: 680px;
  } */
}

@media (max-width: 767px){
    .inbox_people .inbox_chat {
        height: auto;
    }
}
    </style>

@endpush
