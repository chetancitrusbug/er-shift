
        <div class="inbox_people">
            <div class="headind_srch">
                <div class="recent_heading">
                    <h4>Recent</h4>
                </div>
                <div class="srch_bar">
                    <div class="stylish-input-group" id="prefetch">
                    <input type="text" class="search-bar typeahead " placeholder="Search" >
                    <span class="input-group-addon">
                        <button type="button"> <i class="fe fe-search" aria-hidden="true"></i> </button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="inbox_chat" id="inbox_chat-users">
                @if ($chat_users)
                    @foreach ($chat_users as $users)
                        @if ($users->other_user(Auth::id()) && $users->other_user(Auth::id())->user && $users->status == 0)
                            {{-- <div class="chat_list active-chat" data-invitation_id="{{$users->id}}" data-chat_user_id="{{($users->from_id == Auth::id())?$users->to_id:$users->from_id}}" data-user_image="{{$users->other_user(Auth::id())->ProfileImageUrl}}">
                                <div class="chat_people">
                                    <div class="chat_img">
                                        @if ($users->other_user(Auth::id()) && $users->other_user(Auth::id())->user && $users->other_user(Auth::id())->ProfileImageUrl)
                                            <a  href="{{ $users->other_user(Auth::id())->ProfileImageUrl }}" target="_blank"><img src="{{$users->other_user(Auth::id())->ProfileImageUrl}}" alt="{{$users->other_user(Auth::id())->name}}" class="img-fuid u-img"></a>
                                        @else
                                            <a href="{{ asset('asset/images/avatar.jpg') }}" target="_blank"><img src="{{asset('asset/images/avatar.jpg')}}" alt="{{$users->other_user(Auth::id())->name}}"></a>
                                        @endif
                                    </div>
                                    <div class="chat_ib">
                                        <h1>{{$users->other_user(Auth::id())->user->name}}

                                        </h1>
                                        <span data-invitation_id="{{$users->id}}" class="unread_mesage_count badge"></span>
                                        {{-- <p>Test, which is a new approach to have all solutions
                                            astrology under one roof.
                                        </p>
                                    </div>
                                </div>
                            </div> --}}

                            <div class="chat_list " data-invitation_id="{{$users->id}}" data-chat_user_id="{{($users->from_id == Auth::id())?$users->to_id:$users->from_id}}" data-user_image="{{$users->other_user(Auth::id())->ProfileImageUrl}}"
                                data-user_name="{{$users->other_user(Auth::id())->user->name}}">
                                <div class="chat_people">
                                    <div class="chat_img">
                                    {{-- <span class="active-user"></span> --}}
                                        @if ($users->other_user(Auth::id()) && $users->other_user(Auth::id())->user && $users->other_user(Auth::id())->ProfileImageUrl)
                                            <a  href="#" target="_blank"><img src="{{ $users->other_user(Auth::id())->ProfileImageUrl }}" alt="{{$users->other_user(Auth::id())->name}}" class="img-fuid u-img"></a>
                                        @else
                                            <a href="{{ asset('asset/images/avatar.jpg') }}" target="_blank"><img src="{{ $users->other_user(Auth::id())->ProfileImageUrl }}" alt="{{$users->other_user(Auth::id())->name}}"></a>
                                        @endif
                                    </div>
                                    <div class="chat_ib">
                                        <h1>{{$users->other_user(Auth::id())->user->name}}
                                            {{-- <span class="chat_date">Dec 25</span> --}}
                                        </h1>
                                        <!-- <span data-invitation_id="{{$users->id}}" class="unread_mesage_count badge"></span> -->
                                        <span>
                                            {{-- Online --}}

                                        <div class="chat-action">
                                            @if ($users->to_id == Auth::id())
                                                <a href="#" data-read_status="1" data-status = "2" data-invitation_id="{{$users->id}}" id="btn-reject-request" class="btn btn-remove invite-reject-invite"><i class="fe fe-x"></i></a>
                                                <a href="#" data-read_status="1" data-status = "1" data-invitation_id="{{$users->id}}" id="btn-accept-request" class="btn btn-accept invite-reject-invite">Accept</a>
                                            @else
                                                <a href="#" data-read_status="1" data-status = "0" data-invitation_id="{{$users->id}}" class="btn btn-remove">Pending</i></a>

                                            @endif

                                        </div>
                                        </span>
                                        {{-- <p>Test, which is a new approach to have all solutions
                                            astrology under one roof.
                                        </p> --}}
                                    </div>
                                </div>
                            </div>
                        @elseif($users->other_user(Auth::id()) && $users->other_user(Auth::id())->user && $users->status == 1)
                            <div class="chat_list" data-invitation_id="{{$users->id}}" data-chat_user_id="{{($users->from_id == Auth::id())?$users->to_id:$users->from_id}}"
                                data-user_image="{{$users->other_user(Auth::id())->ProfileImageUrl}}"
                                data-user_name="{{$users->other_user(Auth::id())->user->name}}"

                                >
                                <div class="chat_people">
                                    <div class="chat_img">
                                        @if ($users->other_user(Auth::id()) && $users->other_user(Auth::id())->user && $users->other_user(Auth::id())->ProfileImageUrl)
                                            <a  href="{{ $users->other_user(Auth::id())->ProfileImageUrl }}" target="_blank"><img src="{{$users->other_user(Auth::id())->ProfileImageUrl}}" alt="{{$users->other_user(Auth::id())->name}}" class="img-fuid u-img"></a>
                                        @else
                                            <a href="{{ asset('asset/images/avatar.jpg') }}" target="_blank"><img src="{{asset('asset/images/avatar.jpg')}}" alt="{{$users->other_user(Auth::id())->name}}"></a>
                                        @endif
                                    </div>
                                    <div class="chat_ib">
                                        <h1>{{$users->other_user(Auth::id())->user->name}}
                                            {{-- <span class="chat_date">Dec 25</span> --}}
                                        </h1>
                                        <span data-invitation_id="{{$users->id}}" class="unread_mesage_count badge"></span>
                                        {{-- <p>Test, which is a new approach to have all solutions
                                            astrology under one roof.
                                        </p> --}}
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif
                {{-- <div class="chat_list active_chat">
                    <div class="chat_people">
                        <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                        <div class="chat_ib">
                            <h5>John Cena <span class="chat_date">Dec 25</span></h5>
                            <p>Test, which is a new approach to have all solutions
                                astrology under one roof.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="chat_list">
                    <div class="chat_people">
                        <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                        <div class="chat_ib">
                            <h5>John Cena <span class="chat_date">Dec 25</span></h5>
                            <p>Test, which is a new approach to have all solutions
                                astrology under one roof.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="chat_list">
                    <div class="chat_people">
                        <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                        <div class="chat_ib">
                            <h5>John Cena <span class="chat_date">Dec 25</span></h5>
                            <p>Test, which is a new approach to have all solutions
                                astrology under one roof.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="chat_list">
                    <div class="chat_people">
                        <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                        <div class="chat_ib">
                            <h5>John Cena <span class="chat_date">Dec 25</span></h5>
                            <p>Test, which is a new approach to have all solutions
                                astrology under one roof.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="chat_list">
                    <div class="chat_people">
                        <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                        <div class="chat_ib">
                            <h5>John Cena <span class="chat_date">Dec 25</span></h5>
                            <p>Test, which is a new approach to have all solutions
                                astrology under one roof.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="chat_list">
                    <div class="chat_people">
                        <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                        <div class="chat_ib">
                            <h5>John Cena <span class="chat_date">Dec 25</span></h5>
                            <p>Test, which is a new approach to have all solutions
                                astrology under one roof.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="chat_list">
                    <div class="chat_people">
                        <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                        <div class="chat_ib">
                            <h5>John Cena <span class="chat_date">Dec 25</span></h5>
                            <p>Test, which is a new approach to have all solutions
                                astrology under one roof.
                            </p>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>

