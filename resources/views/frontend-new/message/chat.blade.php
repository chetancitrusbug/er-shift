<div class="mesgs">
    <div id="chat-message-header" class="chat-message-header">
        {{-- <div class="chat-prfile-user">
            <span class="active-user"></span>
            <img src="http://13.235.236.166/laravel/er-shift-doctor/public/images/doctor/15873588645e9d2c90ad37b.png" alt="">
        </div>
        <div class="chat-message-detail">
            <h3>Tobias Williams</h3>
            <div class="status-bar">
                <ul>
                    <li><a href="#">Offline</a></li>
                    <li><a href="#">Last seen 3 hours ago</a></li>
                </ul>
            </div>
        </div> --}}
    </div>

    <div class="msg_history" id="chat_history" data-chat_user_id >
       {{--  <div class="incoming_msg">
            <div class="received_msg">
                <embed src="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf">
                <div class="received_withd_msg">
                    <p>Test which is a new approach to have all
                        solutions
                    </p>
                    <span class="time_date"> 11:01 AM    |    June 9</span>
                </div>
            </div>
        </div>
        <div class="outgoing_msg">

            <div class="sent_msg">
                <embed src="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf">
                <p>Test which is a new approach to have all
                    solutions
                </p>
                <span class="time_date"> 11:01 AM    |    June 9</span>
            </div>
        </div>
        <div class="incoming_msg">
            <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
            <div class="received_msg">
                <div class="received_withd_msg">
                    <p>Test, which is a new approach to have</p>
                    <span class="time_date"> 11:01 AM    |    Yesterday</span>
                </div>
            </div>
        </div>

        <div class="outgoing_msg">
            <div class="sent_msg">
                <p>Apollo University, Delhi, India Test</p>
                <span class="time_date"> 11:01 AM    |    Today</span>
            </div>
        </div>
        <div class="incoming_msg">
            <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
            <div class="received_msg">
                <div class="received_withd_msg">
                    <p>We work directly with our designers and suppliers,
                        and sell direct to you, which means quality, exclusive
                        products, at a price anyone can afford.
                    </p>
                    <span class="time_date"> 11:01 AM    |    Today</span>
                </div>
            </div>
        </div> --}}
    </div>
    <div class="type_msg">
        <div class="input_msg_write">
            <input type="text" class="write_msg" placeholder="Type a message" />
            <div class="custom-file">
                <input name="attachment" type="file" class="custom-file-input form-control" id="customFile">
                <label class="custom-file-label" for="customFile"><i class="fe fe-paperclip"></i></label>
              </div>
            <button class="msg_send_btn" type="button" disabled><i class="fa fa-send"></i></button>
            <progress value="0" id="file_uploader" max="100" style="display:none">0%</progress>
        </div>
    </div>
</div>
