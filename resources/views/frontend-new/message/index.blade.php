@extends('frontend-new.layouts.app')

@section('title', 'Messages')
@include('frontend-new.message.css.index-css')
@section('content')

<div class="main-middle-area inner-main-middle-area">

  <section class="update-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <div>
            <p>
              @if(Auth::user()->employeeDetail && Auth::user()->employeeDetail->profession_id > 0 )
              <a href="{{ route('shifts') }}" class="link">Find a Provider</a>
              @else
              <a href="{{ route('profile.edit') }}" class="link">Update profile</a> and <a href="{{ route('shifts') }}"
                class="link">Find a Provider</a></p>
            @endif
          </div>
        </div>
      </div>
    </div>
  </section>

 

  


  <section class="shifts-card-section shifts-card-inner-section">
    <div class="shifts-card-div">
      <div class="container">

        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="heading-left-div">
              <div class="text-title">
                <h2>Messages</h2>
              </div>
              <div class="right-side-div">

                {{-- <div class="selectbox-inline">
                      <div class="select-box select-common select-box-group select-custom2">
                        <select class="js-select2">
                            <option>All</option>
                            <option>Online</option>
                            <option>Offline</option>
                        </select>
                      </div>
                    </div> --}}

              </div>
            </div>
          </div>
        </div>

        <div class="general-card-box-root">

          <div class="messaging">
            <div class="inbox_msg">
              @include('frontend-new.message.users_list')
              @include('frontend-new.message.chat')
            </div>
          </div>

        </div>

      </div>
    </div>
  </section>

</div>


@endsection

@include('frontend-new.message.chat-js')



