@extends('frontend-new.layouts.app')

@section('title', 'Contact Us')

@section('content')

<div class="main-middle-area inner-main-middle-area">


    {{-- <section class="update-section">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div>
                <p> <a href="{{ route('shifts') }}" class="link">Find a Shift</a>.</p>
              </div>
            </div>
          </div>
        </div>
      </section> --}}


    <section class="changes-password-section">
        <div class="changes-password-inner">
            <div class="container">
                <!-- back to listing -->
                <div class="row">
                    <div class="col-lg-12">
                      <div class="back-to-listing">
                        <a href="{{ url()->previous() }}" ><i class="back-arrow-left"></i>Contact Us</a>
                      </div>
                    </div>
                  </div>
                <!-- back to listing end -->



                <form method="POST" action="{{ route('contact.add') }}" id="c-form">
                    {{ csrf_field() }}


                <div class="form-section">

                    <div class="content-record-div">
                        <h2>Talk to us.</h2>
                        <h3>Please fillout the simple form below and we'll get back to you within 24 hours. </h3></br></br>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">User Name *</label>
                                <div class="input-group-box password-box">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                    @if($errors->has('name'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for=""> Email Address *</label>
                                <div class="input-group-box password-box">
                                    <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" value="{{ old('email') }}" required autofocus placeholder="Enter your email...">
                                    @if($errors->has('email'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->has('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="">Now let's hear the details*</label>
                                <div class="input-group-box password-box">
                                    <textarea name="comment" class="form-control" id="comment" rows="4" placeholder="Let us know what you need"></textarea>
                                    @if($errors->has('comment'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->has('comment') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row ">
                    <div class="col-lg-12 mt-3 d-flex justify-content-end ">
                        <button type="submit" class="btn btn-secondary btn-secondary-link min-width-200"> Submit </button>
                    </div>
                </div>

                </form>

            </div>
    </section>

</div>

{{-- @include('frontend-new.layouts.include.footer') --}}

@endsection

@push('js')
<script>

	 $("#c-form").validate({

        rules: {
            name: {
                required: true
            },
			email: {
                required: true,
                email:true
            },
            comment:{
                required: true,
            }
        },
        messages: {
			name: {
                required: "Please enter your name",
            },
            email: {
                required: "Email is required",
                email: "Please enter a valid email address"
            },
            comment : {
                required: "Enter your message",

            },
        },
        onfocusout: function(element) {
            this.element(element);
        },
        success: function(error) {
            error.removeClass("error");
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

    $(document).ready(function(){

        AOS.init({
            duration: 1500,
            disable: function() {
            var maxWidth = 800;
            return window.innerWidth < maxWidth;
            }
        });


});
</script>
@endpush
