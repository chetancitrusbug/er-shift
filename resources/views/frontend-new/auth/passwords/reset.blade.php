@extends('frontend-new.layouts.app')

@section('title', 'Reset Password')

@section('content')

<div class="main-middle-area inner-main-middle-area">

    <section class="changes-password-section">
        <div class="changes-password-inner">
            <div class="container">
                <!-- back to listing -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="back-to-listing">
                            <a href="#"><i class="back-arrow-left"></i>{{ __('Reset Password') }}</a>
                        </div>
                    </div>
                </div>
                <!-- back to listing end -->

                <form method="POST" action="{{ route('password.request') }}" id="reset-form">
                    {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-section">

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">{{ __('E-Mail Address') }}*</label>
                                <div class="input-group-box password-box">
                                    <input id="r-email" type="email" class="form-control {{ $errors->has('email') ? 'has-error' : ''}}" name="email"
                                    value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                                    
                                </div>
                                @if($errors->has('email'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                    <label id="r-email-error" class="error" for="r-email"></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for=""> {{ __('Password') }}*</label>
                                <div class="input-group-box password-box">
                                    <input id="r-password" type="password" class="form-control {{ $errors->has('password') ? 'has-error' : ''}}"
                                    name="password" required autocomplete="new-password">
                                    <button type="button" id="reset_show_password2" name="show_password" class="pass-hide password-view-click">
                                        <i class="fe fe-eye-off password-view"></i>
                                    </button>
                                </div>
                                @if($errors->has('password'))
                                <span class="help-block" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                                <label id="r-password-error" class="error" for="r-password"></label>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Confirm Password*</label>
                                <div class="input-group-box password-box">
                                    <input id="r-password-confirm" type="password" class="form-control" name="password_confirmation" required
                                    autocomplete="new-password">
                                    <button type="button" id="reset_show_password3" name="show_password"
                                        class="pass-hide password-view-click">
                                        <i class="fe fe-eye-off password-view"></i>
                                    </button>
                                </div>
                                <label id="r-password-confirm-error" class="error" for="r-password-confirm"></label>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row ">
                    <div class="col-lg-12 mt-3 d-flex justify-content-end ">
                        <button type="submit" class="btn btn-secondary btn-secondary-link min-width-200"> {{ __('Reset Password') }}</button>
                    </div>
                </div>

                </form>

            </div>
    </section>

</div>

@endsection

@push('js')
<script>

	 $("#reset-form").validate({

        rules: {
            email: {
                required: true,email : true
            },
			password: {
                required: true,
                min:8
            },
            password_confirmation:{
                required: true,
                equalTo: "#r-password",
            }
        },
        messages: {
			email: {
                required: "Email id is required",
                email : "Please enter a valid email address"
            },
            password: {
                required: "Password is required",
                min: "Password must be atleast 8 charachters long"
            },
            password_confirmation : {
                required: "Please Confirm your Password",
                equalTo: "Password Confimartion does not match",
            },
        },
        onfocusout: function(element) {
            this.element(element);
        },
        success: function(error) {
            error.removeClass("error");
        },
        submitHandler: function(form) {
            form.submit();
        }
    });


$('#reset_show_password2').on('click', function(){
  var passwordField2 = $('#r-password');
  var passwordFieldType2 = passwordField2.attr('type');

  if(passwordField2.val() != '')
  {
      if(passwordFieldType2 == 'password')
      {
          passwordField2.attr('type', 'html');
          $(this).html('<i class="fe fe-eye password-hide"></i>');
      }
      else
      {
          passwordField2.attr('type', 'password');
          $(this).html('<i class="fe fe-eye-off password-view"></i>');
      }
  }
});

$('#reset_show_password3').on('click', function(){
  var passwordField2 = $('#r-password-confirm');
  var passwordFieldType2 = passwordField2.attr('type');

  if(passwordField2.val() != '')
  {
      if(passwordFieldType2 == 'password')
      {
          passwordField2.attr('type', 'html');
          $(this).html('<i class="fe fe-eye password-hide"></i>');
      }
      else
      {
          passwordField2.attr('type', 'password');
          $(this).html('<i class="fe fe-eye-off password-view"></i>');
      }
  }
});

</script>
@endpush
