@extends('frontend-new.layouts.app')

@section('title', 'View Shift Detail')

@section('content')

<div class="main-middle-area inner-main-middle-area">

    @if (Auth::check())
        <section class="update-section">
        <div class="container">
            <div class="row">
            <div class="col-lg-12 col-md-12">
                <div>
                <p>
                    @if(Auth::user()->employeeDetail)
                        <a href="{{ route('shifts.create') }}" class="link">Add a new shift</a>
                    @else
                        <a href="{{ route('profile.edit') }}" class="link">Update profile</a> and <a href="{{ route('shifts.create') }}" class="link">Add a new shift</a></p>
                    @endif
                </div>
            </div>
            </div>
        </div>
        </section>
    @endif
    
    <section class="shifts-card-section find-provider-card-section shifts-card-inner-section">
      <div class="shifts-card-div provider-card-div"> 
        <div class="container">

          <!-- back to listing -->
          <div class="general-new-shift-top-div">
              <div class="row">
                  <div class="col-lg-12 col-md-12">
                      <div class="back-to-listing back-to-listing2">
                      <a href="{{ url()->previous() }}" ><i class="back-arrow-left"></i> 
                        @auth
                        {{ $shift->doctor_user ? ( $shift->doctor_user->profile_name ?? $shift->doctor_user->user->name ) : '' }}
                        @endauth
                      </a>
                      </div>
                  </div>
              </div>
          </div>  
          <!-- back to listing end -->

          <div class="general-find-card-box-root">
              
              <div class="row mrl-8">

                <div class="col-lg-12 col-md-12 plr-8">
                  <div class="general-provider-card-box">

                    <div class="provider-card-box-main">
                        
                      <div class="find-card-box-top">
                        <div class="find-card-box-top-left">
                          <div class="img-thumb">
                           
                              @if (Auth::check() && $shift->doctor_user && $shift->doctor_user->profile_image_url)
                              <a href="{{ $shift->doctor_user->profile_image_url }}" target="_blank" class="basic-img-link">
                              <img src="{{$shift->doctor_user->profile_image_url }}" class="img-fluid img-thumb-main" alt="Shifts" />
                              </a>
                              @else
                              <a href="{{ $shift->doctor_user->profile_image_url }}" target="_blank" class="basic-img-link">
                                  <img src="{{ asset('assets/images/avatar.jpg') }}" class="img-fluid img-thumb-main" alt="Shifts" />
                                </a>
                              @endif
                            
                          </div>
                          <div class="right-details-div">
                            @auth<h2 class="basic-link">{{ $shift->doctor_user ? ( $shift->doctor_user->profile_name ?? $shift->doctor_user->user->name ) : '' }}</h2>@endauth
                            <p>{{ str_limit($shift->wish_title,45) }}</p>
                          </div>
                        </div>  
                        <div class="find-card-box-top-right">
                          <div class="btn-save-top-right-div"> 
                            <a  data-to_id="{{$shift->doctor_user->uid}}" href="#" class="btn btn-secondary btn-general-secondary send-invitation"><span class="material-icons"> chat </span> <span class="text-span">Send Request</span> </a>
                          </div>
                        </div>
                      </div>

                      <div class="content-div">

                        <div class="row mrl-8">

                          <div class="col-lg-4 col-md-4 width-40 plr-8">
                            <div class="shift-grid-column provider-grid-column">
                              <div class="title-div">
                                <h4>Wish</h4>
                              </div>
                              <div class="desc-div">
                                <p>{{ $shift->wish_title }}</p>
                              </div>
                            </div>
                            <div class="shift-grid-column provider-grid-column">
                              <div class="title-div">
                                <h4>ED-Volume</h4>
                              </div>
                              <div class="desc-div">
                                <p>{{  $shift->edvolume ? $shift->edvolume->title : ''  }}</p>
                              </div>
                            </div>
                          </div> 
                          
                          <div class="col-lg-8 col-md-8 width-60 plr-8">
                            <div class="provider-grid-column">
                              <div class="title-div">
                                <h4>Description</h4>
                              </div>
                              <div class="desc-div">
                                <p>{{ $shift->wish_desc }}</p>
                              </div>
                            </div>
                          </div>    

                          <div class="col-lg-4 col-md-4 width-40 plr-8">
                            <div class="shift-grid-column provider-grid-column">
                              <div class="title-div">
                                <h4>Desired rate</h4>
                              </div>
                              <div class="desc-div">
                                <p>$ {{ $shift->rate }} per Hours</p>
                              </div>
                            </div>
                          </div> 

                          <div class="col-lg-4 col-md-4 width-30 plr-8">
                            <div class="shift-grid-column provider-grid-column">
                              <div class="title-div">
                                <h4>Location</h4>
                              </div>
                              <div class="desc-div">
                                <p>{{ $shift->commaSepLocNames }}  
                                  {{-- <a href="#" class="link-arrow"> <span class="icon-span"><i class="arrow-right-icon"></i> </span> </a>  --}}
                                </p>
                              </div>
                            </div>
                          </div>
                          
                          <div class="col-lg-4 col-md-4 width-30 plr-8">
                            <div class="shift-grid-column provider-grid-column">
                              <div class="title-div">
                                <h4>Specialities</h4>
                              </div>
                              <div class="desc-div">
                                <p>{{ $shift->doctor_user && $shift->doctor_user->speciality ? $shift->doctor_user->speciality->title : '' }}</p>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-4 col-md-4 width-40 plr-8">
                            <div class="shift-grid-column provider-grid-column">
                              <div class="title-div">
                                <h4>Profession</h4>
                              </div>
                              <div class="desc-div">
                                <p>{{ $shift->doctor_user && $shift->doctor_user->profession ? $shift->doctor_user->profession->title : '' }}</p>
                              </div>
                            </div>
                          </div> 

                          <div class="col-lg-4 col-md-4 width-30 plr-8">
                            <div class="shift-grid-column provider-grid-column">
                              <div class="title-div">
                                <h4>Degree</h4>
                              </div>
                              <div class="desc-div">
                                <p>{{ $shift->doctor_user && $shift->doctor_user->degree ? $shift->doctor_user->degree->title : '' }}</p>
                              </div>
                            </div>
                          </div>
                          
                          <div class="col-lg-4 col-md-4 width-30 plr-8">
                            <div class="shift-grid-column provider-grid-column">
                              <div class="title-div">
                                <h4>Experience</h4>
                              </div>
                              <div class="desc-div">
                                <p>{{ $shift->doctor_user && $shift->doctor_user->experience ? $shift->doctor_user->experience->title : '' }} Years</p>
                              </div>
                            </div>
                          </div>

                        </div>    

                      </div>

                    </div>
                  
                  </div>
                </div>

              </div>
          


          </div>    

        </div>            
      </div>            
    </section>


    @include('frontend-new.components.recent-shifts')
    

  </div>


@endsection
@push('js')
<script>

$(document).on('click', '.send-invitation', function (e) {
    to_id = $(this).attr('data-to_id');
    if (to_id) {
        $.ajax({
            type: "post",
            url: "{{route('send-invite')}}" ,
            data:{to_id:to_id},
            headers: {
                "X-CSRF-TOKEN": "<?php echo csrf_token()?>"
            },
            beforeSend: function(){
                $("#pageloader").show();
            },
            success: function (data) {
                if( data.code == 200 || data.code == 400){
                    swal({
                        title:'',
                        text: data.message,
                        type: 'success',
                        textClass:"text-success",
                        confirmButtonClass:'btn-success',
                    });
                }
                if(data.code == 400 ){
                    swal({
                        title:'',
                        text: data.message,
                        type: 'error',
                        textClass:"text-danger",
                        confirmButtonClass:'btn-danger',
                    });
                }
                if( data.code == 501 ){
                    swal({
                        title:'',
                        text: data.message,
                        type: 'error',
                        textClass:"text-danger",
                        confirmButtonClass:'btn-danger',
                    });
                    var url = "{{ route('home') }}";
                    window.location.href = url;
                }
            },
            complete: function(){
                $("#pageloader").hide();
            }
        });
    }

});
</script>
@endpush



