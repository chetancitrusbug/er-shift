@extends('frontend-new.layouts.app')

@section('title', 'Shifts')

@section('content')


<div class="main-middle-area inner-main-middle-area">

    @auth
    <section class="update-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div>
                        <p>
                            <a href="{{ route('shifts.create')  }}" class="link">Add a new shift</a> as per your
                            requirements.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endauth



    {{ Form::open(['route' => [Route::currentRouteName()], 'method' => 'GET','id'=>'search_form','class' => ''])}}

    <section class="filter-section">
        <div class="filter-div">
          <div class="container">
            <div class="row m0 align-items-center justify-content-center">
                
                <div class="col-lg-12 col-md-12 p0">  
                  <div id="filter-collapse" class="collapse show">
                    <div class="row m0 bg-white-color">
  
                      <div class="col-lg-4 col-md-4 grid-30 col-box1 p0">  
                        <div class="form-group form-group-top search-box">
                            <input name="title" value="{{ request()->get('title') }}" type="text"
                            class="form-control form-search" placeholder="Search.." id="title" />
                        </div>
                      </div>
          
                      <div class="col-lg-4 col-md-4 grid-30 col-box2 p0">  
                        <div class="form-group form-group-top select2-change-group border-bottom-right-group">
                            <div class="select-box select-common select-custom2">
                                {!! Form::select('location', $locations, request()->get('location') != '' ?
                                request()->get('location') : [], ['id' => 'location','class' => 'form-control js-select2' , 'placeholder' => 'Select Location' ]) !!}
                            </div>
                        </div>

                     
                      </div>
  
                      <div class="col-lg-4 col-md-4 grid-30-160 col-box2 p0">  
                        <div class="form-group form-group-top select2-change-group border-bottom-right-group">
                            <div class="select-box select-common select-custom2">
                        
                              {!! Form::select('professions', $professions, request()->get('professions') != '' ? request()->get('professions') : null , ['id' => 'professions','class' => 'form-control js-select2', 'placeholder' => "Search by Profession" ]) !!}
                            </div>
                        </div>
                      </div>
  
                    </div>
                  </div>
                </div>
  
                <div class="col-lg-4 col-md-6 grid-30 col-box2 p0">  
                    <div class="form-group select2-change-group select2-first-group">
                        <div class="select-box select-common select-custom2">
                          
                          {{ Form::select('edvolume_id' , $edvolumes ,request()->get('edvolume_id') != '' ? request()->get('edvolume_id') : null ,['class' => 'form-control js-select2', 'id' => 'filter-edvolume_select', 'placeholder' => "Filter by ED Volume"] ) }}
                        </div>
                    </div>
                </div>
  
                <div class="col-lg-4 col-md-6 grid-30 col-box3 p0">  
                  <div class="form-group ">
                    <div class="date-picker-div date-picker-box">
                      <div class="date-50 from-date">
                        <input id="startDate" name="from_date" value="{{ request()->get('from_date') }}"
                        class="form-input date-picker " placeholder="From" readonly="readonly" />
                      </div>
                      <div class="date-50 to-date">
                        <input id="endDate" name="to_date" value="{{ request()->get('to_date') }}"
                                        class="form-input date-picker " placeholder="To" readonly="readonly" />
                      </div>
                    </div>
                  </div>
                </div>
  
                <div class="col-lg-4 col-md-6 grid-30-160 col-box4 p0">  
                  <div class="dropdown filter-range-slider-box">
                    <button class="btn btn-default btn-dropdown btn-range" id="filter-range" type="button" data-toggle="dropdown" aria-haspopup="false" aria-expanded="true">
                      Desired Rate <span class="text-span"> ${{ (request()->get('rate'))?request()->get('rate'):0 }} / hr </span>
                    </button>
                    <div class="dropdown-menu dropdown-filter" aria-labelledby="filter-range">
                      <div class="rangeslider-wrap rangeslider-wrap-custom">
                        <input id="range-rate" type="range" min="0" max="500" step=""
                        value="{{ (request()->get('rate'))?request()->get('rate'):0 }}" name="rate" labels="$0, $500">
                      </div>
                    </div>
                  </div>
                </div>
  
                <div class="col-lg-2 col-md-12 grid-160px col-box5 p0">  
                    <div class="filter-button-div">
                        <button type="button" class="btn btn-primary btn-filter-primary" data-toggle="collapse" data-target="#filter-collapse"> <i class="material-icons"> filter_list</i> </button>
                        <button type="submit" class="btn btn-secondary btn-search btn-filter-search"> <i class="fe fe-search"></i> </button>
                    </div>
                </div>
  
              </div>
  
          </div>    
        </div>  
      </section>  

    {{Form::close()}}

    @if ($all_shifts->count() > 0)

    <section class="shifts-card-section shifts-card-inner-section">
        <div class="shifts-card-div">
            <div class="container">
                <div class="row pull-right">
                    <button type="reset" class="btn btn-default reset-btn" id="reset-btn"> Reset Filters </button>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12">

                        <div class="heading-left-div">

                            <div class="text-title">
                                <h2>{{ $total }} {{ count(request()->all()) > 0 ? 'Search results' : 'Providers' }}  found</h2>
                            </div>
                            <div class="right-side-div">
                                <div class="selectbox-inline">

                                    <div class="select-box select-common select-box-group select-custom2">
                                        {{ Form::open(['route' => [Route::currentRouteName()], 'method' => 'GET','id'=>'sort_shifts','class' => 'form-horizontal'])}}
                                        <select id="sort-shift_select" name="sort" class="js-select2">
                                            <option value="">Sort by Rate</option>
                                            <option value="high" {{ (request()->get('sort') == 'high')?'selected':'' }}>
                                                High</option>
                                            <option value="low" {{ (request()->get('sort') == 'low')?'selected':'' }}>
                                                Low</option>
                                        </select>
                                        {{ Form::close() }}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="general-card-box-root">

                    <div class="row mrl-8">


                    @foreach($all_shifts as $key => $shifts)

                    <div class="col-lg-6 col-md-6 plr-8">
                        <div class="general-find-card-box">
        
                          <div class="find-card-box-main">
                            <div class="find-card-box-top">
                              <div class="img-thumb">
                                
                                @if (Auth::check() && $shifts->doctor_user && $shifts->doctor_user->profile_image_url)
                                    <a href="{{$shifts->doctor_user->profile_image_url }}" target="_blank" class="basic-img-link">
                                    <img src="{{$shifts->doctor_user->profile_image_url }}" class="img-fluid img-thumb-main dc-img" alt="Shifts" /></a>
                                @else
                                    <a href="{{ asset('assets/images/avatar.jpg') }}" target="_blank" class="basic-img-link">
                                    <img src="{{ asset('assets/images/avatar.jpg') }}" class="img-fluid img-thumb-main dc-img" alt="Shifts" /></a>
                                @endif
                                </a>
                              </div>
                              <a href="{{ route('shifts.view',$shifts->id) }}" class="basic-link">
                                <div class="right-details-div">
                                   @auth
                                   {{ $shifts->doctor_user ? ( $shifts->doctor_user->profile_name ?? $shifts->doctor_user->user->name ) : '' }}
                                   @endauth 
                                    <p>{{ str_limit($shifts->wish_title,30) }}</p>
                                </div>
                                </a>
                            </div>
                            <div class="content-div">
                              <div class="label-root">
              
                                <div class="label-div">
                                  <span class="icon-div"> <span class="custom-icon fe fe-map-pin"></span></span>
                                  <span class="text">{{ str_limit($shifts->commaSepLocNames,40) }}</span>
                                </div>
              
                                <div class="label-div">
                                  <span class="icon-div"> <span class="custom-icon fe fe-dollar-sign"></span></span>
                                  <span class="text">{{ $shifts->rate }}/hr</span>
                                </div>
        
                                <div class="d-flex"></div>
                                
                                <div class="label-div">
                                  <span class="icon-div"> <span class="custom-icon fe fe-clock"></span></span>
                                  <span class="text">ED Volume   {{  $shifts->edvolume ? $shifts->edvolume->title : ''  }}</span>
                                </div>
                                
                              </div>
              
                              {{-- <div class="btn-save-top-right-div"> 
                                <a href="#" class="btn btn-general"><span class="material-icons"> chat </span> <span class="text-span">Chat</span> </a>
                              </div> --}}
              
                            </div>
                          </div>
                          
                        </div>
                      </div>


                    @endforeach

                    {{$all_shifts->appends(request()->input())->links('frontend-new.layouts.include.pagination')}}

                    </div>


                   


                </div>

            </div>
        </div>
    </section>

    @else
    <section class="no-record-section">
        <div class="container">

            <!-- back to listing -->
            <div class="row">
                <div class="col-lg-10">
                    <div class="back-to-listing">
                        <a href="{{ url()->previous() }}"><i class="back-arrow-left"></i> No Search result found.</a>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="back-to-listing">
                        <button type="reset" class="btn btn-default reset-btn pull-right" id="reset-btn"> Reset Filters </button>
                    </div>
                </div>
            </div>
            <!-- back to listing end -->

            <div class="no-record-card-div">

                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="thumb-icon">
                            <img src="{{ asset('assets/images/web_search.svg') }}" class="img-no-record"
                                alt="No search result found" />
                        </div>
                        <div class="content-record-div">
                            <h4>No search result found</h4>
                            <p>Find provider with another keyword or Diffrent location.</p>
                            <div class="btn-row d-flex">
                                <a href="{{ route('home') }}"
                                    class="btn btn-outline-primary btn-gradient-primary text-uppercase">GO TO HOME
                                    PAGE</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    @endif

</div>


@endsection
@push('js')
<script src="{{asset('assets/js/rangeslider.min.js')}}"></script>
<script src="{{asset('assets/js/rangeslider-get.js')}}"></script>
<script>
    var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    $('#startDate').datepicker({
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        minDate: today,
        maxDate: function () {
            return $('#endDate').val();
        },
        multidates:true
    });
    $('#endDate').datepicker({
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        minDate: today,
        minDate: function () {
            if($('#startDate').val()){
                return $('#startDate').val();
            }
            return today;
        }
    });
    $("#endDate").click(function(){
        $(".gj-picker-bootstrap .datepicker").toggleClass("opendatepicker");
    });

    $(document).on('change', '#sort-shift_select', function (e) {
        this.form.submit();
    });
    $(document).on('change', '#range-rate', function (e) {
        $("#filter-range-sel-rate").html("$"+$(this).val()+" / hr");
    });

    $("#reset-btn").click(function(){
        $('#title').val("");
        $('#location').val("");
        $('#startDate').val("");
        $('#endDate').val("");
        $('#range-rate').val(0);
        $('#sort-shift_select').val("");
        $('#filter-edvolume_select').val("");
        $('#doctor_type').val("");
        $('#professions').val("");
        window.location.href="{{ route('shifts') }}";
    });


</script>
@endpush
