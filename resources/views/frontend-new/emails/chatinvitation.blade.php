<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>ER-Shifts</title>
		<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

	</head>
	<body style="margin:30px auto; width:850px;font-family: 'Montserrat', sans-serif;">
		<div style="background:#000; padding:20px; text-align:center;">
			<img class="brand_icon" src="{{asset('assets/images/logo_lg.png')}}" alt="logo" style="width: 75px;" />
		</div>
		<div style="align:center; border:1px solid #ccc; padding-top:30px; padding-bottom:40px">
			<table class="item_detail" style="border-collapse: collapse; width: 100%; text-align: center; margin:0px auto 20px auto;display: table;">
				<tr class="image-line" style="background: transparent;border: none !important;">
					<td colspan="3"  class="bottom-4" style="text-align: center;padding: 10px 20px;font-size: 15px;padding: 0;">
						<img src="{{asset('assets/images/top.jpg')}}" style="width: 100%;margin-bottom:-4px;">
					</td>
				</tr>
				<tr class="image-line" style="background: transparent;border: none !important;">
					<td colspan="6" class="item_tbl" style="text-align: center;padding: 25px 0px !important;background: #ededed;padding: 10px 20px;font-size: 15px;padding: 0;">
						<table class="tbl_order" style="background-color: #EDEDED;color: #000;border-bottom: 1px dashed #d3d3d3;width:100%;">
							<div class="wrap_table" style="text-align: left;width: 80%;margin: 0 auto;">
                                <h1 style="font-family: Avenir, Helvetica, sans-serif; color: rgb(47, 49, 51); font-size: 19px; font-weight: bold; margin-top: 0px;">Hello, {{$user->name}}</h1>
                                    <p style="color: rgb(116, 120, 126); font-family: Avenir, Helvetica, sans-serif; font-size: 16px; line-height: 1.5em;">There is an invitation request for you</p>
                                    <p style="color: rgb(116, 120, 126); font-family: Avenir, Helvetica, sans-serif; font-size: 16px; line-height: 1.5em;">
                                        User Name: {{$userFrom->name}}
                                    </p>
                                    <p style="color: rgb(116, 120, 126); font-family: Avenir, Helvetica, sans-serif; font-size: 16px; line-height: 1.5em;">
                                        User Email: {{$userFrom->email}}
                                    </p>
							</div>
						</table>
					</td>
				</tr>
				<tr class="image-line" style="background: transparent;border: none !important;">
					<td colspan="6"  class="top-4" style="text-align: center;top: -5px; position: relative;padding: 10px 20px;font-size: 15px;padding: 0;">
						<img src="{{asset('assets/images/bottom.jpg')}}" style="width: 100%;margin-top:-4px;">
					</td>
				</tr>
			</table>

			<h4 style="margin-top: 35px;color: #333333; margin-left: 23px;"></h4>

		</div>
		<div class="footer" style="background:#333333; color:#ffffff; padding:10px; text-align:center;">
            <p>Copyright © {{ Carbon\Carbon::now()->format('Y') }} All rights reserved</p>
			<a style="color:#ffffff;" href="https://employer.ershifts.com/">www.ErShifts.com</a>
		</div>
	</body>
</html>
