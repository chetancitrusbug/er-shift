@extends('frontend-new.layouts.app')

@section('title',trans('subscription.label.confirm_billing_payment'))

@section('content')

@push('css')
<style>
	input.has-error {
    border: 1px solid red !important;
}
.hidden {
    display: none;
}

</style>
@endpush

<div class="main-middle-area inner-main-middle-area">
    
    {{-- <section class="update-section">
    <div class="container">
        <div class="row">
        <div class="col-lg-12 col-md-12"> 
            <div>
            <p> <a href="update-profile.html" class="link">Upgrade subscription </a> and <a href="#" class="link">Add new shifts </a> </p>
            </div>
        </div>
        </div>
    </div>
    </section>  --}}
    
    <section class="add-newshift-section general-new-shift-section payment-confirmation-section">
        <div class="payment-confirmation-div">
            <div class="container">

                <!-- back to listing -->
                <div class="general-new-shift-top-div">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="back-to-listing">
                            <a href="{{ url()->previous() }}" ><i class="back-arrow-left"></i>Confirm Billing & Payment</a>
                            </div>
                        </div>
                    </div>
                </div>  
                <!-- back to listing end -->

                

                {!! Form::open(['url' => '/order/payment', 'id' =>'payment-form', 'class' => 'form-horizontal checkoutform', 'files' => true,'autocomplete'=>'off']) !!}
                {!! Form::hidden('billing_id',$billing->id, ['class' => 'form-control','id'=>'billing_id']) !!}
                {!! Form::hidden('step',2, ['class' => 'form-control','id'=>'step']) !!}
                <input type="hidden" name="token" id="stripetoken">

                <div class="form-section form-section2">

                    <div class="alert alert-danger hidden" id="carderrors">  </div>

                    <div class="row mrl-8">
                    
                    <div class="col-lg-6 col-md-6 plr-8">
                        <div class="form-group">
                            {!! Form::label('card_no',trans('subscription.label.card_no')) !!}
                            <div class="input-group-box password-box">
                            <input type="text" id="cardnum" class="payment-input form-control mmb10 cardnum" placeholder="XXXX-XXXX-XXXX-XXXX">
                            <button type="button"  class="pass-hide password-view-click">
                                        <i class="fa fa-credit-card password-view"></i>
                                    </button>
                            </div>
                            <label class="error"></label>
                            {!! $errors->first('card_no', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 plr-8">
                        <div class="form-group">
                            <label>Cardholder Name</label>
							<input type="text" name="card_holder_name" class="payment-input form-control" placeholder="Cardholder name">
                        </div>
                    </div>
                 
                    <div class="col-lg-6 col-md-6 plr-8">
                        <div class="form-group">
                            <label>EXP. DATE</label>
							<input type="text" id="cardexp" class="payment-input form-control cc-card cardexp" placeholder="MM/YYYY">
                        </div>
                    </div>
                    
                    {{-- <div class="col-lg-5 col-md-4 width-40 plr-8">
                        <div class="form-group">
                            <label for="">Year</label>
                            <input class="number Year-number form-control" type="text" id="Year-number" name="number" placeholder="YYYY" data-mask="0000" />
                        </div>
                    </div> --}}

                    <div class="col-lg-6 col-md-6 plr-8">
                        <div class="form-group">
                           
                            {!! Form::label('cvvNumber',trans('subscription.label.cvv_no'), ['class' => 'col-lg-3 control-label']) !!}										
							<input type="text" id="cardcvv" class="payment-input form-control  cardcvv" placeholder="XXX">
            				{!! $errors->first('cvvNumber', '<p class="help-block">:message</p>') !!} 
                            
                        </div>
                    </div>

                </div>
                
                
                </div>

                <div class="row">
                    <div class="col-lg-12 mt-3">
                        <div class="form-btn-bottom-div">
                        <a href="{{ url('/confirm-order/'.$billing->package) }}" class="btn btn-outline-primary btn-gradient-primary btn-big-btn">Back</a>
                       
                        <button name="btnSubmit" type="submit" id="card_form_submit_btn" class="btn btn-secondary btn-secondary-link btn-big-btn ml-20 checkButton">@lang('subscription.label.start_subscription')<i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </div>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </section>


</div>
@endsection


@push('js')
<!-- See why multiple instance of stripe has been loaded:  https://stripe.com/docs/stripe-js/v2#including-stripejs -->
<script src="https://js.stripe.com/v2/"></script>
<script src="https://js.stripe.com/v3/"></script>
<script src="{{ asset('assets/javascripts/jquery/jquery.payment.js') }}"></script>
<script type="text/javascript">

        $("input").focus(function(){
            $(this).removeClass("has-error");
        });

        $("#cardnum").payment('restrictNumeric');
        $("#cardnum").payment('formatCardNumber');
        $('#cardexp').payment('formatCardExpiry');
		$("#cardcvv").payment('formatCardCVC');
		


        // Handle form submission
		var form = document.getElementById('payment-form');
		
        //var payBtns = document.querySelectorAll('.btn-Stripe');
        var payBtns = document.querySelectorAll('.checkButton');
        var alertDiv = document.getElementById('carderrors');


	
        window.disablePayBtns = function() {
            for(var i=0; i < payBtns.length; i++){
                payBtns[i].disabled = true;
            }
        }

        window.enablePayBtns = function() {
            for(var i=0; i < payBtns.length; i++){
                payBtns[i].disabled = false;
            }
        }

        window.hideErrors = function() {
            alertDiv.classList.add('hidden')
        }

        window.displayErrors = function(errors) {

		
            if(typeof errors === "object" && errors.constructor === Array) {
                var errHtml = '<ul>';
                for(var i=0; i<errors.length;i++){
                    errHtml += '<li>' + errors[i] + '</li>';
                }
                errHtml += '</ul>';
            } else {
                errHtml = '<p>'+ errors + '</p>';
            }
            alertDiv.innerHTML = errHtml;
            alertDiv.classList.remove('hidden');
        }

        $.fn.toggleInputError = function(erred) {
            this.toggleClass('has-error', erred);
            return this;
        };

        form.addEventListener('submit', function(event) {

            event.preventDefault();
			var focus = false;
            disablePayBtns();
            hideErrors();
            // Get Card details
            var cardnum = document.getElementById('cardnum').value;
            var cardexp = $('#cardexp').payment('cardExpiryVal');
            var cardcvv = document.getElementById('cardcvv').value;
            var cardType = $.payment.cardType($('.cc-number').val());

            var iscardValid = $.payment.validateCardNumber(cardnum);
            var isExpValid = $.payment.validateCardExpiry(cardexp);
            var iscvvValid = $.payment.validateCardCVC(cardcvv, cardType);

            $('.cardnum').toggleInputError(!iscardValid);
            $('.cardexp').toggleInputError(!isExpValid);
            $('.cardcvv').toggleInputError(!iscvvValid);

		
            var errors = [];
            if(!iscardValid || !isExpValid || !iscvvValid){
                enablePayBtns();
                return false;
            }
		

			if(errors.length > 0) { enablePayBtns(); return displayErrors(errors) }
			
	

            Stripe.setPublishableKey('{{ config('services.stripe.key') }}');
            Stripe.card.createToken({
                number: cardnum,
                cvc: cardcvv,
                exp_month: cardexp.month,
                exp_year: cardexp.year
            }, function(status, response){
			
                if (response.error) {
                    enablePayBtns();
                    return displayErrors(response.error.message);
                } else {
                    var token = response.id;
                    document.getElementById('stripetoken').value = token;
                    form.submit();
                }
            });
        });


</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js" ></script>
@endpush


