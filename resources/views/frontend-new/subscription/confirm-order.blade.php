@extends('frontend-new.layouts.app')

@section('title',trans('subscription.label.confirm_order'))

@section('content')

<div class="main-middle-area inner-main-middle-area">

<section class="add-newshift-section general-new-shift-section payment-confirmation-section">
    <div class="payment-confirmation-div">
        <div class="container">

            <!-- back to listing -->
            <div class="general-new-shift-top-div">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="back-to-listing">
                        <a href="{{ url()->previous() }}" ><i class="back-arrow-left"></i>Confirm Billing & Payment</a>
                        </div>
                    </div>
                </div>
            </div>  
            <!-- back to listing end -->

            {!! Form::open(['url' => '/confirm-order', 'class' => 'form-horizontal', 'files' => true,'autocomplete'=>'off']) !!}
            {!! Form::hidden('package_id',$package->id, ['class' => 'form-control','id'=>'package_id']) !!}
         
            {!! Form::hidden('step',1, ['class' => 'form-control','id'=>'step']) !!}

            <div class="form-section form-section2">

                <div class="row mrl-8">
                
                <div class="col-lg-6 col-md-6 plr-8">
                    <div class="form-group">
                        {!! Form::label('name',trans('subscription.label.name'), ['class' => 'col-lg-3 control-label']) !!}
                         {!! Form::text('name',Auth::user()->name, ['class' => 'form-control','readonly']) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 plr-8">
                    <div class="form-group">
                        {!! Form::label('email',trans('subscription.label.email'), ['class' => 'col-lg-3 control-label']) !!}
                        {!! Form::text('email',Auth::user()->email, ['class' => 'form-control','readonly']) !!}
                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            
            
            </div>

            <div class="row">
                <div class="col-lg-6 mt-3"></div>
                <div class="col-lg-6 mt-3">
                    <div class="form-btn-bottom-div">
                    <button type="submit" class="btn btn-secondary btn-secondary-link btn-big-btn ml-20">@lang('subscription.label.next') <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
</section>

</div>

@endsection


