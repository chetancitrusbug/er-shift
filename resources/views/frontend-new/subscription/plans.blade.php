@extends('frontend-new.layouts.app')

@section('title','Subscription')

@section('content')


<div class="main-middle-area inner-main-middle-area">

<section class="update-section" style="background-color: {{ (($expire_in_days > 6) ? '#def4de' : '#f4dede') }}   ">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12"> 
          <div>
            {{-- <p> <a href="update-profile.html" class="link">Upgrade subscription</a> and <a href="add-new-shift.html" class="link">Add new shifts</a></p> --}}
            <p class="navbar-text">
                {{-- <span class="label {{ (($expire_in_days > 6) ? 'bg-success' : 'bg-danger') }} "> --}}
                @if(isset($plan) && @$plan != null && @$plan->isFree == 1)
                    @lang('dashboard.top_nav.free_trial_ends_in') {{ @$expire_in_days }}
                @elseif(@$subscription == null)
                    @lang('dashboard.top_nav.you_have_not_plan')
                @elseif(@$subscription != null && @$expire_in_days == 0)
                    @lang('dashboard.top_nav.your_plan_renew_within_24_hrs')
                @else
                   @if(@$subscription != null && @$expire_in_days < 30)
                           @lang('dashboard.top_nav.your_plan_expired_in') {{ @$expire_in_days }} @lang('dashboard.label.days')
                   @else
                           @lang('dashboard.top_nav.your_plan_expired_in') {{ round((@$expire_in_days)/30) }} @lang('dashboard.label.month')
                   @endif
            
               @endif
            
        </p>
          </div>
        </div>
      </div>
    </div>
  </section> 

  

  <section class="subscription-card-section">
    <div class="subscription-card-div"> 
      <div class="container">

        <div class="general-subscription-root">
          
          <div class="subscription-top-plans-div">
            <div class="row mrl-12">

              <div class="col-lg-3 col-md-4 width-20 plr-12">

                <div class="subscription-card-white-box">
                  <div class="top-div">
                    <h3>Current plan</h3>
                        <h2>$ {{ (@$isfree == 0) ? 0 : @$plan->amount }}</h2>
                  </div>
                  <div class="bottom-div">
                      <p>{{ (@$isfree == 0) ?  str_limit(@$subscription->packageDetail->title,18) : str_limit(@$plan->title,18) }}</p>
                  </div>
                </div>

              </div>

             

              <div class="col-lg-3 col-md-4 width-20 plr-12">

                <div class="subscription-card-white-box">
                  <div class="top-div">
                    <h3>Plan Expired on</h3>
                    <h2> @if(@$isfree == 0)
                        {{  @$expire_in_days }} @lang('dashboard.label.days')
                        @else
                            @if(@$expire_in_days < 30)
                                {{ @$expire_in_days }} @lang('dashboard.label.days')
                            @else
                                {{ round((@$expire_in_days)/30) }} @lang('dashboard.label.month')
                            @endif
                           
                           @endif</h2>
                  </div>
                  <div class="bottom-div">
                    <p>Days remaining</p>
                  </div>
                </div>

              </div>

              <div class="col-lg-3 col-md-4 width-20 plr-12">

                <div class="subscription-card-white-box">
                  <div class="top-div">
                    <h3>Total shift added</h3>
                    <h2>{{ @$totaljob }}</h2>
                  </div>
                  <div class="bottom-div">
                    <p>Numbers of shift</p>
                  </div>
                </div>

              </div>

              <div class="col-lg-3 col-md-4 width-40 plr-12">

                @if($current_subscription)
                <div class="subscription-card-white-box subscription-card-color2-box">
                  <div class="top-div">
                    <h3>{{$current_subscription->packageDetail->title}}</h3>
                    <h2>@lang('subscription.label.id') #: &nbsp;00{{$current_subscription->id}}</h2>
                  </div>
                  <div class="bottom-div row">
                      <div class="col-md-6">
                    <p>@lang('subscription.label.issued_on') <span class="text-semibold">
                        {{ \Carbon\Carbon::parse($current_subscription->start_date)->toFormattedDateString()}}</span></p>
                      </div>
                      <div class="col-md-6">
                    <p class="pull-right">@lang('subscription.label.expired_on') <span class="text-semibold">
                        {{ \Carbon\Carbon::parse( date('Y-m-d', strtotime($current_subscription->end_date. ' - 1 days')) )->toFormattedDateString()}}
                            </span></p>
                        </div>
                  </div>
                </div>
                @elseif($count_subscription <= 0 )
                <div class="subscription-card-white-box subscription-card-color2-box">
                    <div class="top-div">
                      <h3>{{ config('setting.free_trial_pack_info.title')}}</h3>
                      <h2>{{ config('setting.free_trial_pack_info.description')}}</h2>
                    </div>
                    <div class="bottom-div row">
                        <div class="col-md-6">
                      <p>{{ config('setting.free_trial_pack_info.unit')}}  @lang('subscription.label.compagnie') / {{ config('setting.free_trial_pack_info.FREE_TRIAL_DAY')}} @lang('subscription.label.days')</p>
                        </div>
                        <div class="col-md-6 ">
                      <p>
                        <form class="form-horizontal" method="POST" action="{{ url('billing/subscription/trial') }}">
                            {{ csrf_field() }}
                          <button type="submit" class="btn btn-default trial-btn "> Start Free Trial Now</button>
                        </form>
                          </p>
                          </div>
                    </div>
                  </div>
                @endif

              </div>

            </div>
          </div>

          <div class="subscription-middle-plans-div">

            <div class="nav-tab-root">
                            
              <ul class="nav nav-tabs custom-nav-tabs">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#purchase-subscription-package-tab">Purchase Subscription Package</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#subscription-history-tab">Subscription History</a>
                </li>
              </ul>

            </div>

            <div class="tab-content main-subscription-content">

              <div id="purchase-subscription-package-tab" class="tab-pane active">

                <div class="radio-btn-root"> 


                    @foreach($packages as $key=>$pack)

                  

                  <div class="radio-box active">
                    <div class="radiolist_root">
                      
                      <div class="row mrl-5">
                        <div class="col-md-2 width-90px plr-5">
                          {{-- <div class="radio-parent">
                            <div class="custom-control custom-radio custom-control-row">
                              <input type="radio" class="custom-control-input" id="task-one" name="plan-list" checked>
                              <label class="custom-control-label" for="task-one"> &nbsp; </label>
                            </div>
                          </div>     --}}
                        </div>
                        <div class="col-md-4 width-35-90px plr-5">
                          <h4>{{ $pack->title }}</h4>
                        </div>
                        <div class="col-md-4 width-25 plr-5">
                          <p>@if($pack->day < 30)
                            {{ $pack->day }} @lang('subscription.label.days')
                            @else
                            {{ round(($pack->day)/30) }} @lang('subscription.label.month')
                            @endif</p>
                        </div>
                        <div class="col-md-4 width-40 plr-5">
                           <p>${{ $pack->amount }} 
                            {{-- @if($current_subscription && $current_subscription->package == $pack->id)<span class="status-span status-active"> Activate </span> @endif--}}
                            </p> 
                         
                          <div class="cancel-plan-box">

                            @if($current_subscription && $current_subscription->package == $pack->id)
                            {{-- <button class="btn btn-cancel-plan"> <span class="material-icons"> cancel </span> Cancel  & Refund</button> --}}
                            <button class="btn btn-activate-now"> <span class="material-icons"> done </span> Activate </button>
                            @else
                            <button onclick="order({{ $pack->id }});" class="btn btn-order-now"> <span class="material-icons"> add_shopping_cart </span> @lang('subscription.label.order_now') </button>
                            @endif

                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  
                
               
                  @endforeach

                  

                </div>

              </div>

              <div id="subscription-history-tab" class="tab-pane fade">

                <div class="custom-history-table-div">
                  <div class="history-table-div"> 


                     @foreach($orders as $key => $value)
                                                          

                    
                                                           
                    <div class="history-tr-row active">
                      <div class="row mrl-5">
                        <div class="col-md-2 width-15 plr-5">
                          <h3>Id #: 00{{ $value->id }}</h3>
                        </div>
                        <div class="col-md-2 width-15 plr-5">
                          <p>{{ $value->package->title }}</p>
                        </div>
                        <div class="col-md-3 width-30 plr-5">
                          <p> <span class="span-text">Renew on: </span> {{ \Carbon\Carbon::parse($value->start_date)->format('M d, Y') }} </p>
                        </div>
                        <div class="col-md-3 width-30 plr-5">
                          <p class="status-p"> 
                              @if(\Carbon\Carbon::parse($value->expire_date)->gt(Carbon\carbon::now()))
                                <span class="status-round status-active"></span> <span class="span-text">Expires on: </span>
                              @else
                                <span class="status-round status-inactive"></span> <span class="span-text">Expired on: </span>
                              @endif
                               
                            &nbsp; {{ \Carbon\Carbon::parse($value->expire_date)->subDay(1)->format('M d, Y') }} 
                         </p>
                        </div>
                        <div class="col-md-2 width-10 plr-5">
                          <p class="text-right p-0"> $ {{ $value->package->amount }} </p>
                        </div>
                      </div>
                    </div>
                

                    @endforeach

                  </div>
                </div>

              </div>

            </div>

          </div>

        </div>    

      </div>            
    </div>            
  </section>
</div>

@endsection

@push('js')
<script>
    function order(pack_id)
    {
        var url ="{{ url('/confirm-order/') }}";
        url = url + "/" + pack_id;

        swal({
                title: "",
                text: "{{$orderChangeMsg}}",
                type: "",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                focusCancel:true,
                allowOutsideClick: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = url;
                    }
                })
    }
</script>
@endpush


