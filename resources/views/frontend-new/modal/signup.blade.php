<div class="modal modal-auth" id="sign-up-modal">
    <button type="button" class="close close-new" data-dismiss="modal" aria-label="Close"> <i class="fe fe-x"></i> </button>
    <div class="modal-dialog modal-dialog-centered">

      <div class="modal-content">

        <div class="modal-body">
            {{ Form::open(['route' => 'register', 'id'=>'createAnAccount_frm']) }}
                <div class="generalcard-box-top">

                    <div class="general-box-inner">

                        <div class="row">

                                <div class="col-lg-6 col-md-6 p0-login login-left">
                                    <div class="login-form border-right1" >
                                        <h3>Create an Account</h3>
                                        <div class="main-form-div">
                                            <div class="form-group">
                                                <input type="text" name="first_name" required class="form-control" placeholder="First name" />
                                            </div>
                                            <div class="form-group">
                                            <input type="text" name="last_name" required class="form-control" placeholder="Last name" />
                                            </div>
                                            <div class="form-group">
                                            <input type="email" name="email" required class="form-control" placeholder="Email ID" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 p0-login p0-mobile login-right">

                                        <div class="login-form account-form">
                                            <div class="main-form-div mt-56">
                                                <div class="form-group">
                                                    <div class="input-group-box password-box">
                                                        <input type="password" class="form-control" name="password" id="password2" placeholder="Password">
                                                        <button type="button" id="show_password2" class="pass-hide password-view-click">
                                                        <i class="fe fe-eye-off password-view"></i>
                                                        </button>
                                                    </div>
                                                    <span id="error-place-password2" class="error" for="password2"></span>
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group-box password-box">
                                                        <input type="password" class="form-control" name="password_confirmation" id="password3" placeholder="Confirm password">
                                                        <button type="button" id="show_password3" class="pass-hide password-view-click">
                                                        <i class="fe fe-eye-off password-view"></i>
                                                        </button>
                                                    </div>
                                                    <span id="error-place-password3" class="error" for="password3"></span>
                                                </div>
                                                <div class="checkbox-root">
                                                    <label for="keepme-signin2" class="checkbox"> <input type="checkbox" class="form-checkbox-outline" id="keepme-signin2"> Keep me signed in</label>
                                                </div>
                                                <div class="submit-row">
                                                    <a href="#"><button class="btn btn-block btn-secondary submit-btn f-700 sign-up-btn" type="submit">Sign up</button></a>
                                                </div>
                                            </div>
                                        </div>

                                </div>


                        </div>

                    </div>

                </div>
            {{Form::close()}}
        </div>

        <div class="modal-footer">
          <p>I have an Account <a href="#" class="link-btn hide-sign-up sign-in-btn" data-toggle="modal" data-target="#sign-in-modal">Sign in?</a></p>
        </div>


      </div>

    </div>
  </div>

@push('js')
    <script type="text/javascript">
        var signup_form_validate = $("#createAnAccount_frm").validate({

            rules: {
                email: {
                    required: true,email:true
                },
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                password: {
                    required: true,
                    minlength: 8,
                    maxlength:30,
                },
                password_confirmation: {
                    required: true,
                    equalTo: "#password2"
                },
            },
            messages: {
                email: {
                    required: "Email address is required",
                    email: "Please enter a valid email address"
                },
                first_name: {
                    required: "First name is required",
                },
                last_name: {
                    required: "Last name is required",
                },
                password: {
                    required: "Password is required",
                    minlength: "Password must be atleast 8 charachters long",
                    maxlength: "Password must be less than 30 charachters",
                },
                password_confirmation: {
                    required: "Please Confirm your Password",
                    equalTo: "Password confirmation does not match",
                }
            },
            onfocusout: function(element) {
                this.element(element);
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "password" || element.attr("name") == "password_confirmation") {
                    $("#error-place-"+element.attr("id")).html(error);
                } else {
                    error.insertAfter(element);
                }
            },
            success: function(error) {
                error.removeClass("error");
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    </script>
@endpush
