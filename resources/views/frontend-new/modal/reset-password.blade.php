<div class="modal modal-auth modal-auth-normal" id="reset-password-modal">
    <button type="button" class="close close-new" data-dismiss="modal" aria-label="Close"> <i class="fe fe-x"></i> </button>
    <div class="modal-dialog modal-dialog-centered">

      <div class="modal-content">

        <div class="modal-body">
          <div class="generalcard-box-top">

            <div class="general-box-inner">

                <div class="row justify-content-center">

                    <div class="col-lg-10 col-md-10 p0-mobile login-center">

                        <div class="login-form account-form">
                          <h3 class="text-center">Reset password</h3>
                          <div class="main-form-div">

                            <div class="form-group">
                              <div class="input-group-box password-box">
                                <input type="password" class="form-control" name="password" id="password4" placeholder="Password">
                                <button type="button" id="show_password4" name="show_password" class="pass-hide password-view-click">
                                    <i class="fe fe-eye-off password-view"></i>
                                </button>
                              </div>
                            </div>

                            <div class="form-group">
                              <div class="input-group-box password-box">
                                <input type="password" class="form-control" name="password" id="password5" placeholder="Confirm password">
                                <button type="button" id="show_password5" name="show_password" class="pass-hide password-view-click">
                                    <i class="fe fe-eye-off password-view"></i>
                                </button>
                              </div>
                            </div>

                            <div class="checkbox-root">
                                <label for="keepme-signin3" class="checkbox"> <input type="checkbox" class="form-checkbox-outline" id="keepme-signin3"> Keep me signed in</label>
                            </div>

                            <div class="submit-row">
                                <a href="#"><button class="btn btn-block btn-secondary submit-btn f-700" type="button">Submit</button></a>
                            </div>

                          </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>
        </div>

        <div class="modal-footer">
          <p>I have an Account <a href="#" class="link-btn hide-reset-pass" data-toggle="modal" data-target="sign-in-modal">Sign in?</a></p>
        </div>


      </div>

    </div>
  </div>
