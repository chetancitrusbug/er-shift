<div class="modal modal-auth modal-auth-normal" id="forgot-password-modal">
    <button type="button" class="close close-new" data-dismiss="modal" aria-label="Close"> <i class="fe fe-x"></i> </button>
    <div class="modal-dialog modal-dialog-centered">

      <div class="modal-content">

        <div class="modal-body">
          <div class="generalcard-box-top">

            <div class="general-box-inner">

                <div class="row justify-content-center">

                    <div class="col-lg-10 col-md-10 p0-mobile login-center">

                        <form method="POST" action="{{ route('password.email') }}" id="fp-form">
                            {{ csrf_field() }}

                        <div class="login-form account-form">
                          <h3 class="text-center">Forgot password</h3>
                          <div class="main-form-div space-form-div">

                            <div class="form-group">
                                <input id="fp-email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"  required autocomplete="email" autofocus>

                              <div class="error-message"> <p> Enter register Email ID </p> </div>
                            </div>

                            <div class="submit-row">
                                <button class="btn btn-block btn-secondary submit-btn f-700 hide-forgot-pass"  type="submit">Send Password Reset Link</button>
                                {{-- data-toggle="modal" data-target="#reset-password-modal" --}}
                            </div>

                          </div>
                        </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>
        </div>

        <div class="modal-footer">
          <p><a href="#" class="link-btn hide-forgot-pass sign-in-btn" data-toggle="modal" data-target="#sign-in-modal">Sign in</a> OR <a href="#" class="link-btn hide-forgot-pass sign-up-btn" data-toggle="modal" data-target="#sign-up-modal">Sign up.</a></p>
        </div>


      </div>

    </div>
  </div>
