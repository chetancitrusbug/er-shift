<div class="modal modal-auth" id="sign-in-modal">
    <button type="button" class="close close-new" data-dismiss="modal" aria-label="Close"> <i class="fe fe-x"></i>
    </button>
    <div class="modal-dialog modal-dialog-centered">

        <div class="modal-content">

            <div class="modal-body">
                <div class="generalcard-box-top">

                    <div class="general-box-inner">

                        <div class="row">


                            <div class="col-lg-6 col-md-6 p0-login login-left">


                                <form method="POST" action="{{ route('login') }}" id="login-form">
                                   {{ csrf_field() }}


                                    <div class="login-form border-right1">
                                        <h3>Login</h3>
                                        <div class="main-form-div">
                                            <div class="form-group">
                                                <input id="email" type="email"
                                                    class="form-control @error('email') is-invalid @enderror"
                                                    name="email" value="{{ old('email') }}" required
                                                    autocomplete="email" autofocus>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group-box password-box">

                                                    <input id="password" type="password"
                                                        class="form-control @error('password') is-invalid @enderror"
                                                        name="password" required autocomplete="current-password">

                                                    <button type="button" id="show_password" name="show_password"
                                                        class="pass-hide password-view-click">
                                                        <i class="fe fe-eye-off password-view"></i>
                                                    </button>
                                                </div>
                                                <span id="error-place-password" class="error" for="password"></span>
                                            </div>
                                            <div class="checkbox-root">
                                                <label for="keepme-signin" class="checkbox"> <input type="checkbox"
                                                        class="form-check-input form-checkbox-outline" name="remember"
                                                        id="keepme-signin" {{ old('remember') ? 'checked' : '' }}> Keep
                                                    me signed in</label>
                                            </div>
                                            <div class="submit-row">
                                                <button class="btn btn-block btn-secondary submit-btn f-700"
                                                    type="submit"> Login </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>



                            <div class="col-lg-6 col-md-6 p0-login login-right">
                                <div class="login-form">
                                    <p class="text-p">If you don&apos;t have an account click below.</p>
                                    <div class="main-form-div ">
                                        <div class="btn-account-root">
                                            <div class="account-row">
                                                <button
                                                    class="btn btn-block btn-gradient-primary btn-account common-btn hide-sign-in sign-up-btn"
                                                    type="button" data-toggle="modal"
                                                    data-target="#sign-up-modal">Create an account</button>
                                            </div>
                                            <div class="d-block text-center"><span class="or-span">OR</span></div>
                                            <div class="socialbtn-row">
                                                <a href="{{ route('login.google') }}"><button
                                                    class="btn btn-block btn-gradient-primary social-btn google-btn common-btn"
                                                    type="button">Sign in with Google <i
                                                        class="fab fa-google-plus-g icon-i"></i> </button></a>
                                                <a href="{{ route('login.facebook') }}"><button
                                                    class="btn btn-block btn-gradient-primary social-btn facebook-btn common-btn"
                                                    type="button">Sign in with facebook <i
                                                        class="fab fa-facebook-f icon-i"></i> </button></a>
                                                <a href="{{ route('login.linkedin') }}"><button
                                                    class="btn btn-block btn-gradient-primary social-btn linkedin-btn common-btn"
                                                    type="button">Sign in with Linkedin <i
                                                        class="fab fa-linkedin-in icon-i"></i> </button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <p>Did you <a href="#" class="link-btn hide-sign-in fp-btn" data-toggle="modal"
                        data-target="#forgot-password-modal">forgot your password?</a></p>
            </div>

        </div>
    </div>
</div>

@push('js')
    <script type="text/javascript">

    </script>
@endpush
