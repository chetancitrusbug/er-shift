<div class="modal modal-auth modal-auth-normal confirmation-modal" id="delete-modal">
    <button type="button" class="close close-new" data-dismiss="modal" aria-label="Close"> <i class="fe fe-x"></i> </button>
    <div class="modal-dialog modal-dialog-centered max-width-400">
        <div class="modal-content">
            <div class="modal-body">
                <div class="generalcard-box-top">
                    <div class="general-box-inner">
                        <div class="row justify-content-center">
                            <div class="col-lg-12 col-md-12">
                                <div class="confirm-inner-div">
                                    <div class="main-form-div ">
                                        <p class="text-center">Are you sure you want <span class="span-block"> to delete shift? </span> </p>
                                        <div class="submit-row confirm-row">
                                            {{ Form::open(['route' => ['shifts.delete', "10"], 'class' => 'inline','id'=>'del_shift_frm']) }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger f-700 mr-10">Delete</button>
                                                <a href="#" class="btn btn-default btn-cancel cancel-link" data-dismiss="modal" aria-label="Close">Cancel</a>
                                            {{Form::close()}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
        </div>
    </div>
</div>

@push('js')
<script>
$(document).on('click', '.delete-shift', function (e) {
    console.log('====================================');
    console.log($(this).attr('data-id'));
    console.log('====================================');
    var shift_delete_url = "{{route('shifts.delete',':del_id')}}";
    shift_delete_url = shift_delete_url.replace(':del_id',$(this).attr('data-id'));
    $("#del_shift_frm").attr('action',shift_delete_url);
    console.log('====================================');
    console.log(shift_delete_url);
    console.log('====================================');
});
</script>

@endpush