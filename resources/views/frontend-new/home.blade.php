@extends('frontend-new.layouts.app')

@section('title','ErShifts')
@push('css')
<style>
    .pay-shift-ul li {
        padding: 4px;
    }
</style>

@endpush

@section('top-header-content')

<section class="top-header-section">
  <div class="container">
      <div class="row">
          <div class="col-lg-12 col-md-12">
              <div>
                  <p>
                      To add available shifts go to <a href="{{ config('setting.DOCTOR_URL')  }}" class="link">Provider Login </a> </p>
              </div>
          </div>
      </div>
  </div>
</section>

@endsection

@section('content')

  <div class="main-middle-area">

    <section class="banner-section">
      <div class="banner-div">
        <div class="container">
            <div class="row align-items-center">

              <div class="col-lg-5 col-md-6" >
                <div class="content-banner" >
                    <div class="text-content" data-aos="fade-right">
                      <h2>Emergency Department Shifts On Demand</h2>
                      <p>The first electronic shift matching between providers and Staff <span class="span-block"> </span></p>
                    </div>
                    <div class="form-div" data-aos="fade-right">
                        {{ Form::open(['route' => ['shifts'], 'method'=> 'get', 'id'=>'shifts_search_frm']) }}
                            <div class="form-group">
                                <div class="selectbox-inline">
                                <div class="select-box select-common select-custom2">
                                    {{-- <select class="js-select2">
                                        <option>New York</option>
                                        <option>Chicago</option>
                                        <option>San Francisco</option>
                                    </select> --}}
                                    {!! Form::select('professions', $professions, null, ['id' => 'professions','class' => 'form-control js-select2', ]) !!}
                                    {{-- {{ Form::select('doctor_type' , config('setting.doctor_type') , null, ['class' => 'js-select2'] ) }} --}}
                                </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="selectbox-inline">
                                    <div class="select-box select-common select-custom2">
                                {{-- <input type="text" class="form-control" placeholder="Choose Location" /> --}}
                                    {{ Form::select('location' , App\State::all()->pluck('name','id')->prepend('Select Location','') , null,['class' => 'js-select2'] ) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="date-picker-div">
                                <div class="date-50 from-date">
                                    <input id="startDate" name="from_date" class="form-input date-picker " placeholder="From Date" readonly="readonly" />
                                </div>
                                <div class="date-50 to-date">
                                    <input id="endDate" name="to_date" class="form-input date-picker " placeholder="To Date" readonly="readonly" />
                                </div>
                                </div>
                            </div>

                            <div class="btn-row">
                                <button type="submit" class="btn btn-secondary btn-search">Search</button>
                                <a href="#" class="btn btn-default btn-watch" data-toggle="modal" data-target="#video-modal" data-src="{{ $home_video }}" >
                                <span class="material-icons mr-10"> play_circle_outline </span> Watch video
                                </a>
                            </div>


                            <div class="modal fade" id="video-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-top:10%;">
                                <div class="modal-dialog vidoe-modal" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabel">Let us show you how it works</h5>
                                      <button type="button" class="close" id="StopClickButton" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <iframe id="Video" class="youtube-video" width="560" height="315" src="{{ $home_video }}" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                            </div>

                        {{Form::close()}}

                        {{-- <div class="btn-row">
                                <a target="_blank" href="{{config('setting.EMPLOYER_URL')}}"><button type="submit" class="btn btn-outline-primary btn-gradient-primary">Employer Login</button></a>
                        </div> --}}

                    </div>

                </div>
              </div>

              <div class="col-lg-7 col-md-6">
                <div class="banner-img-div" >
                  <div class="img-thumb d-flex justify-content-end" data-aos="zoom-in">
                    <img src="{{ asset('assets/images/character-image.svg') }}" alt="banner" class="img-fluid img-responsive" />
                  </div>
                </div>
              </div>

            </div>
        </div>
      </div>
    </section><!--  end of banner  -->

    @include('frontend-new.components.recent-shifts')

    <section class="how-it-works-section">
      <div class="how-it-works-div">
          <div class="container container-1000">

            <div class="row">
              <div class="col-lg-12 col-md-12">
                  <div class="heading-div" data-aos="fade-down">
                      <h2>How It Works</h2>
                      <p>If you're a healthcare provider involved in Emergency Care, you can use ErShifts.com for free.</p>
                  </div>
              </div>
            </div>

            <div class="row" data-aos="fade-down">
              <div class="col-lg-4 col-md-4 hiw-col4">
                  <div class="hiw-card-box">
                      <div class="icons-top-div">
                          <a href="{{ route('shifts') }}">
                          <div class="img-round-thumb">
                              <img src="{{ asset('assets/images/icons/custom-icons/find.svg') }}" class="icons-img" alt="find" />
                          </div>
                          </a>
                      </div>
                      <div class="hiw-content-div">
                          <h4>Find an ED Shift</h4>
                          <p>Choose what kind of ED shift you are looking for based on specialty.</p>
                      </div>
                      <span class="position-top-right shape-arrow-icon"></span>
                  </div>
              </div>
              <div class="col-lg-4 col-md-4 hiw-col4">
                  <div class="hiw-card-box">
                      <div class="icons-top-div">
                          <div class="img-round-thumb">
                              <img src="{{ asset('assets/images/icons/custom-icons/completed-task.svg') }}" class="icons-img" alt="completed" />
                          </div>
                      </div>
                      <div class="hiw-content-div">
                          <h4>Apply Directly</h4>
                          <p>Screen employers with open ED shifts and send an invitation to chat and settle on pay rates.</p>
                      </div>
                      <span class="position-top-right shape-arrow-icon"></span>
                  </div>
              </div>
              <div class="col-lg-4 col-md-4 hiw-col4">
                <div class="hiw-card-box">
                    <div class="icons-top-div">
                        <a href="{{ route('message') }}">
                        <div class="img-round-thumb">
                            <img src="{{ asset('assets/images/icons/custom-icons/chat.svg') }}" class="icons-img" alt="chat" />
                        </div>
                        </a>
                    </div>
                    <div class="hiw-content-div">
                        <h4>Chat and Confirm</h4>
                        <p>Communicate with employers who have open ED shifts that match your desires. You have full control and you can say yes or no to a contract.</p>
                    </div>
                </div>
              </div>

            </div>

          </div>
      </div>
    </section>

    <section class="testimonials-section" >
      <div class="testimonials-div">
        <div class="container">

          <div class="row">
              <div class="col-lg-12 col-md-12">
                  <div class="heading-div" data-aos="fade-down">
                      <h2>You're in a good hand</h2>
                      <p> Emergency medicine providers are using ErShifts.com to match open shifts with dream rates.
                        <span class="span-block">Open ED Shifts are being filled more efficiently by Providers who have the availability and want to work.</span></p>
                      {{-- <p>Millions of people around the world have already made ER Shift the
                        <span class="span-block">place where their dream happens.</span> </p> --}}
                  </div>
              </div>
          </div>

          <div class="row">
              <div class="col-lg-12 col-md-12">
                  <div class="testimonials-box-root" data-aos="fade-down">

                      <div class="owl-carousel owl-theme owl-testimonials" id="owl-testimonials">

                        <div class="item">

                            <div class="testimonials-box">
                              <div class="testimonials-desc">
                                {{-- <p>Life before Company was very chaotic — we got a lot of phone calls, a lot of mistyped orders. So with Company, the ability to see the order directly from the customer makes it so streamlined.</p> --}}
                                <p>I like the fact that I can put in my worth at $50/ hr for my time and still keep my day job. I can make extra money on my days off. </p>
                              </div>
                              <div class="user-details-row">
                                <div class="testimonials-thumb">
                                  <img src="{{ asset('assets/images/pic-small1.png') }}" class="client-img" alt="client" />
                                </div>
                                <div class="testimonials-text">
                                  <h4>Cindy, RN</h4>
                                  {{-- <p>Dr. Simon Organization</p> --}}
                                </div>
                              </div>
                            </div>

                        </div>



                        <div class="item">

                            <div class="testimonials-box">
                              <div class="testimonials-desc">
                                <p>I am an ED physician and I get multiple phone calls daily from recruiters who don't care about what i need and do not know my availability. This site makes it easy to contact me buy my specifications. Don't waste my time, if you are not willing to pay me!!
                                </p>
                              </div>
                              <div class="user-details-row">
                                <div class="testimonials-thumb">
                                  <img src="{{ asset('assets/images/pic-small3.png') }}" class="client-img" alt="client" />
                                </div>
                                <div class="testimonials-text">
                                  <h4>John O, MD</h4>
                                  {{-- <p>Dr. Simon Organization</p> --}}
                                </div>
                              </div>
                            </div>

                        </div>

                        <div class="item">

                            <div class="testimonials-box">
                              <div class="testimonials-desc">
                                <p>I now get emails from employers/ recruiters with an idea of the kind of pay I am looking for.</p>
                              </div>
                              <div class="user-details-row">
                                <div class="testimonials-thumb">
                                  <img src="{{ asset('assets/images/pic-small2.png') }}" class="client-img" alt="client" />
                                </div>
                                <div class="testimonials-text">
                                  <h4>Mario M, PA
                                </h4>
                                  {{-- <p>Dr. Simon Organization</p> --}}
                                </div>
                              </div>
                            </div>

                        </div>

                        {{-- <div class="item">

                            <div class="testimonials-box">
                              <div class="testimonials-desc">
                                <p>Life before Company was very chaotic — we got a lot of phone calls, a lot of mistyped orders. So with Company, the ability to see the order directly from the customer makes it so streamlined.</p>
                              </div>
                              <div class="user-details-row">
                                <div class="testimonials-thumb">
                                  <img src="{{ asset('assets/images/pic-small1.png') }}" class="client-img" alt="client" />
                                </div>
                                <div class="testimonials-text">
                                  <h4>Martin Jones, Creative Cons</h4>
                                  <p>Dr. Simon Organization</p>
                                </div>
                              </div>
                            </div>

                        </div> --}}

                      </div>


                  </div>
              </div>
          </div>

        </div>
      </div>
    </section>

    <section class="app-mobile-section">
      <div class="container border-top1">
          <div class="app-mobile-div">

              <div class="row mt-20 align-items-center">

                  <div class="col-lg-6 col-md-7">

                      <div class="card-box app-mobile-card-box">
                          <div class="content-div" data-aos="fade-right">
                              <div class="header-left-common-div header-left2">
                                  {{-- <h3>ErShift Pay & <span class="span-block">Shift Matching</span> </h3> --}}
                                  <h3>For Employers</h3>
                                    <p>
                                        <ul class="pay-shift-ul">
                                            {{-- <li>Free Access To ErShifts.Com App</li>
                                            <li>Access To Hospitals / Agencies/ Other Employers</li>
                                            <li>Declare your Desired Pay Rate ($/Hr)</li>
                                            <li>Enter Dates That You Want To Work</li>
                                            <li>ErShifts.Com Matches Dates With Your Requests</li>
                                            <li>Chat With Employers Privately</li>
                                            <li>Run Your Own 1 Person Company (No middle men)</li>
                                            <li>You Can Still Keep Your Daily Job</li>
                                            <li>Upload Resume</li> --}}
                                            <p><i class="fa fa-check"></i>	Access to provider’s mobile App</p>
                                            <p><i class="fa fa-check"></i>	Access to willing ED staff </p>
                                            <p><i class="fa fa-check"></i>	Access to Dr/ RN/ PA/NP availability </p>
                                            <p><i class="fa fa-check"></i>	Increased ability to fill open shifts</p>
                                            <p><i class="fa fa-check"></i>	Open access to chat with selected candidates</p>
                                            <p><i class="fa fa-check"></i>	1st month free & 3 mth / 6mth /1yr packages</p>
                                            <p><i class="fa fa-check"></i>	Upload multiple jobs </p>
                                            <p><i class="fa fa-check"></i>	Access to resumes </p>
                                        </ul>
                                    </p>
                              </div>
                              <div class="store-icon-div">
                                  <a href="{{ $android_url  }}" target="_blank" class="btn btn-store mr-15">
                                      <img src="{{ asset('assets/images/app-store-black.png') }}" class="img-store-icon" alt="app-store">
                                  </a>
                                  <a href="{{ $ios_url  }}" target="_blank" class="btn btn-store">
                                      <img src="{{ asset('assets/images/google-play-black.png') }}" class="img-store-icon" alt="google-play">
                                  </a>
                              </div>
                          </div>
                      </div>

                  </div>

                  <div class="col-lg-6 col-md-5">
                      <div class="round-mobile-div">
                          <div class="center-app-div">
                              <img src="{{ asset('assets/images/app-splash.png') }}" alt="splash" class="img-app" data-aos="fade-left" />
                          </div>
                      </div>
                  </div>

              </div>

          </div>
      </div>
    </section>

    <section class="get-started-section">
      <div class="container">
          <div class="get-started-div">

              <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="heading-content-div text-center" data-aos="fade-down">
                        <h2>Follow Us for Free Tips and Insights</h2>
                        <p>Sign up for a ER SHift account or ask to see our platform in action and <span class="span-block">discover the functionalities.</span> </p>
                        <div class="btn-row">
                            @guest
                            <a href="#" class="btn btn-secondary btn-secondary-link sign-up-btn" data-toggle="modal" data-target="#sign-up-modal">Get Started <i class="fe fe-arrow-right"></i></a>
                            @endguest
                        </div>
                    </div>
                </div>
            </div>

          </div>
      </div>
    </section>

  </div>

 

@endsection

  @push('js')
  <script type="text/javascript">

        $("#video-modal").on('hidden.bs.modal', function (e) {
            $("#video-modal iframe").attr("src", $("#video-modal iframe").attr("src"));
        });

        /* Start datepicker range */
        var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        $('#startDate').datepicker({
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            minDate: today,
            maxDate: function () {
                return $('#endDate').val();
            }
        });
        $('#endDate').datepicker({
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            minDate: function () {
                if($('#startDate').val()){
                    return $('#startDate').val();
                }
                return today;
            }
        });
        $("#endDate").click(function(){
            $(".gj-picker-bootstrap .datepicker").toggleClass("opendatepicker");
          });
        /* End of datepicker range */

        var open_login_modal = "{{ Session::get('open_login_modal') }}";
        if(open_login_modal == 1){
            // $('.sign-in-btn').trigger('click');
            $("#forgot-password-modal").modal('hide');
            $("#sign-up-modal").modal('hide');
            $("#sign-in-modal").modal();
        }

  </script>
  @endpush
