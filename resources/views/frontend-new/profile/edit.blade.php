@extends('frontend-new.layouts.app')

@section('title', 'Update Profile')

@section('content')

<div class="main-middle-area inner-main-middle-area">

    {{-- <section class="update-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div>
              <p> <a href="{{ route('shifts.create') }}" class="link">Add Shift Request</a> and <a href="{{ route('shifts') }}" class="link">Find a Shift </a> </p>
            </div>
          </div>
        </div>
      </div>
    </section> --}}

    <section class="update-profile-section">
      <div class="update-profile-inner">
        <div class="container">
          <!-- back to listing -->
          <div class="row">
            <div class="col-lg-12">
              <div class="back-to-listing pb-0">
                <a href="{{ url()->previous() }}" ><i class="back-arrow-left"></i>Update Profile</a>
              </div>
            </div>
          </div>
          <!-- back to listing end -->


        <form method="POST" action="{{ route('profile.update') }}" id="profile-form" enctype="multipart/form-data">
           {{ csrf_field() }}

        <div class="profile-pic-upload">
            <div class="circle">
            <a href="{{ Auth::user()->employeeDetail ? Auth::user()->employeeDetail->profile_image_url : asset('assets/images/avatar.jpg')  }}" target="_blank"><img class="profile-pic" src="{{ Auth::user()->employeeDetail ? Auth::user()->employeeDetail->profile_image_url : asset('assets/images/avatar.jpg')  }}"></a>
            <label class="filelabel-icon" for="file-uploadpic">
                <i class="material-icons"> camera_alt </i>
            </label>
            </div>
            <div class="p-image">
                <label class="file-uploadpic-label" for="file-uploadpic"><span class="span-block">Upload profile picture</span> </label>
                <input class="file-upload" id="file-uploadpic" type="file" accept="image/*" name="profile_pic">
            </div>
        </div>
        @if($errors->has('profile_pic'))
        <span class="help-block" role="alert">
            <strong>{{ $errors->first('profile_pic') }}  </strong>
        </span>
        @endif
          <div class="form-section">

            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="">Profile name*</label>
                  {!! Form::text('emp_name', ($employee)?$employee->emp_name:'' , ['class' => 'form-control']) !!}
                  @if($errors->has('emp_name'))
                  <span class="help-block">
                      <strong>{{  $errors->first('emp_name')  }}</strong>
                  </span>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="">First name*</label>
                  {!! Form::text('fname', (isset($user->fname) && $user->fname != '' ) ? $user->fname : '', ['class' => 'form-control']) !!}

                  @if($errors->has('fname'))
                  <span class="help-block" role="alert">
                      <strong>{{ $errors->first('fname')  }}</strong>
                  </span>
                  @endif

                </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="">Last name*</label>
                {!! Form::text('lname', (isset($user->lname) && $user->lname != '' ) ? $user->lname : '', ['class' => 'form-control']) !!}
                @if($errors->has('lname'))
                <span class="help-block" role="alert">
                    <strong>{{  $errors->first('lname')  }}</strong>
                </span>
                @endif

              </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
                <label for="">Email*</label>
                {!! Form::text('email', $user->email, ['class' => 'form-control', 'disabled' => true, 'readonly' => true]) !!}
              </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group">
              <label for="">Phone*</label>
              {!! Form::text('tel_number', ($employee)?$employee->tel_number : '' , ['class' => 'form-control','id' => 'tel_number']) !!}
            </div>
          </div>

          <div class="col-lg-6">
            <div class="form-group">
              <label for="">@lang('dashboard.profile.copmp_name')*</label>
              {!! Form::text('comp_name', ($employee)?$employee->comp_name : '' , ['class' => 'form-control','id' => 'comp_name']) !!}
              @if ($errors->has('comp_name'))
              <span class="help-block">
                  <strong>{{ $errors->first('comp_name') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="col-lg-6">
            <div class="form-group">
              <label for="">@lang('dashboard.profile.comp_websitye') </label>
              {!! Form::text('comp_website', ($employee)?$employee->comp_website : '' , ['class' => 'form-control','id' => 'comp_website']) !!}
              @if ($errors->has('comp_website'))
              <span class="help-block">
                  <strong>{{ $errors->first('comp_website') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="col-lg-6">
            <div class="form-group">
              <label for="">@lang('dashboard.profile.comp_information') </label>
              {!! Form::textarea('comp_information', ($employee)?$employee->comp_information : '' , ['class' => 'form-control','id' => 'comp_information', 'rows' => 2]) !!}
              @if ($errors->has('comp_information'))
              <span class="help-block">
                  <strong>{{ $errors->first('comp_information') }}</strong>
              </span>
              @endif
            </div>
          </div>

       
            <div class="col-lg-6">
              <div class="form-group">
                <label for="">Address</label>
                {!! Form::textarea('address', ($employee)?$employee->address:'' , ['class' => 'form-control', 'rows' => 2]) !!}
              </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
                <label for="">State*</label>
                <div class="select-box select-common select-custom2">
                  {!! Form::select('location', $locations, (  isset($employee) && $employee->location_id > 0 ) ? $employee->location_id : null , ['id' => 'location','class' => 'form-control  js-select2' ]) !!}
                </div>
                <label id="location-error" class="error" for="location"></label>
              </div>
            </div>



                      
            </div>


            <div class="row">
              <div class="col-lg-12 mt-3 d-flex justify-content-end ">
                <button type="submit" class="btn btn-secondary btn-secondary-link min-width-200">Save</button>
              </div>
            </div>

          </form>


      </div>
    </section>

  </div>

@endsection

@push('js')
<script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
<script>
   
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });



    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


    $(".file-upload").on('change', function(){
        readURL(this);
    });

    $(".upload-button").on('click', function() {
        $(".file-upload").click();
    });

    $("#tel_number").inputmask({"mask": "999 999 9999"});

    jQuery.validator.addMethod("phone_number", function(value, element) {
        return this.optional(element) || /^[0-9," "]+$/i.test(value);
    }, "Numberic value only");
    
	 $("#profile-form").validate({
        rules: {
          emp_name: {
                required: true,maxlength:50
            },
            fname: {
                required: true,maxlength:50
            },
            lname: {
                required: true,maxlength:50
            },
            tel_number: {
                required: true,
                minlength:12,
                phone_number:true,
            },
            address: {
                required: true,
            },
            location: {
                required: true,
            },
            comp_name: {
                required: true,maxlength:50
            },
            comp_information : {
              required:false ,
                maxlength:50
            },
            comp_website : {
                required:false ,
                url: true,
                maxlength:80,
            }
         


        },
        messages: {
            emp_name:{
                  required:"Profile name is required",
              },
            fname: {
                  required: "First name is required",
              },
            lname: {
                required: "Last name is required",
            },
            tel_number: {
                required: "Phone number is required",
                minlength:"Enter the vaild number"
            },
            address: {
                required: "Enter your address",
            },
            location: {
                required: "Select Location",
            },
            comp_name: {
                required: "Enter company name",
            },
            

           


        },
        onfocusout: function(element) {
            this.element(element);
        },
        success: function(error) {
            error.removeClass("error");
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

</script>
@endpush


