@extends('frontend-new.layouts.app')

@section('title', 'Change Password')

@section('content')

<div class="main-middle-area inner-main-middle-area">


    <section class="update-section">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div>
                <p> <a href="{{ route('shifts.create') }}" class="link">Add Shift Request</a> and <a href="{{ route('shifts') }}" class="link">Find a Shift </a> </p>
              </div>
            </div>
          </div>
        </div>
      </section>


    <section class="changes-password-section">
        <div class="changes-password-inner">
            <div class="container">
                <!-- back to listing -->
                <div class="row">
                    <div class="col-lg-12">
                      <div class="back-to-listing">
                        <a href="{{ url()->previous() }}" ><i class="back-arrow-left"></i>Change Password</a>
                      </div>
                    </div>
                  </div>
                <!-- back to listing end -->

                <form method="POST" action="{{ route('profile.update-password') }}" id="cp-form">
                    {{ csrf_field() }}


                <div class="form-section">

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">{{ __('Current Password') }}*</label>
                                <div class="input-group-box password-box">
                                    <input id="r-current_password" type="password" class="form-control {{ $errors->has('current_password') ? ' has-error' : '' }} "
                                    name="current_password" required autocomplete="new-password">
                                    <button type="button" id="change_show_password1" name="show_password" class="pass-hide password-view-click">
                                        <i class="fe fe-eye-off password-view"></i>
                                    </button>   
                                </div>
                                @if($errors->has('current_password'))
                                <span class="help-block" role="alert">
                                    <strong>{{ $errors->first('current_password') }}</strong>
                                </span>
                                @endif
                                <label id="r-current_password-error" class="error" for="r-current_password"></label>
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for=""> {{ __('Password') }}*</label>
                                <div class="input-group-box password-box">
                                    <input id="r-password" type="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}"
                                    name="password" required autocomplete="new-password">
                                    <button type="button" id="change_show_password2" name="show_password" class="pass-hide password-view-click">
                                        <i class="fe fe-eye-off password-view"></i>
                                    </button>
                                </div>
                                @if($errors->has('password'))
                                <span class="help-block" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                                <label id="r-password-error" class="error" for="r-password"></label>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Confirm Password*</label>
                                <div class="input-group-box password-box">
                                    <input id="r-password-confirm" type="password" class="form-control" name="password_confirmation" required
                                    autocomplete="new-password">
                                    <button type="button" id="change_show_password3" name="show_password"
                                        class="pass-hide password-view-click">
                                        <i class="fe fe-eye-off password-view"></i>
                                    </button>
                                </div>
                                <label id="r-password-confirm-error" class="error" for="r-password-confirm"></label>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row ">
                    <div class="col-lg-12 mt-3 d-flex justify-content-end ">
                        <button type="submit" class="btn btn-secondary btn-secondary-link min-width-200"> Save </button>
                    </div>
                </div>

                </form>

            </div>
    </section>

</div>


@endsection

@push('js')
<script>

	 $("#cp-form").validate({

        rules: {
            current_password: {
                required: true
            },
			password: {
                required: true,
                min:8
            },
            password_confirmation:{
                required: true,
                equalTo: "#r-password",
            }
        },
        messages: {
			current_password: {
                required: "Please enter your current password",
            },
            password: {
                required: "Password is required",
                min: "Password must be atleast 8 charachters long"
            },
            password_confirmation : {
                required: "Please Confirm your Password",
                equalTo: "Password Confimartion does not match",
            },
        },
        onfocusout: function(element) {
            this.element(element);
        },
        success: function(error) {
            error.removeClass("error");
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

    $(document).ready(function(){

        AOS.init({
            duration: 1500,
            disable: function() {
            var maxWidth = 800;
            return window.innerWidth < maxWidth;
            }
        });


$('#change_show_password1').on('click', function(){
  var passwordField2 = $('#r-current_password');
  var passwordFieldType2 = passwordField2.attr('type');

  if(passwordField2.val() != '')
  {
      if(passwordFieldType2 == 'password')
      {
          passwordField2.attr('type', 'html');
          $(this).html('<i class="fe fe-eye password-hide"></i>');
      }
      else
      {
          passwordField2.attr('type', 'password');
          $(this).html('<i class="fe fe-eye-off password-view"></i>');
      }
  }
});

$('#change_show_password2').on('click', function(){
  var passwordField2 = $('#r-password');
  var passwordFieldType2 = passwordField2.attr('type');

  if(passwordField2.val() != '')
  {
      if(passwordFieldType2 == 'password')
      {
          passwordField2.attr('type', 'html');
          $(this).html('<i class="fe fe-eye password-hide"></i>');
      }
      else
      {
          passwordField2.attr('type', 'password');
          $(this).html('<i class="fe fe-eye-off password-view"></i>');
      }
  }
});

$('#change_show_password3').on('click', function(){
  var passwordField2 = $('#r-password-confirm');
  var passwordFieldType2 = passwordField2.attr('type');

  if(passwordField2.val() != '')
  {
      if(passwordFieldType2 == 'password')
      {
          passwordField2.attr('type', 'html');
          $(this).html('<i class="fe fe-eye password-hide"></i>');
      }
      else
      {
          passwordField2.attr('type', 'password');
          $(this).html('<i class="fe fe-eye-off password-view"></i>');
      }
  }
});


/* End of password */



});
</script>
@endpush
