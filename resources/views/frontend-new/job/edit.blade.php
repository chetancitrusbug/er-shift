@extends('frontend-new.layouts.app')

@section('title', 'Edit Shift Request')

@section('content')

<div class="main-middle-area inner-main-middle-area">

    <section class="update-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div>
                        <p> <a href="{{ route('profile.edit') }}" class="link">Update Profile </a> and <a
                                href="{{ route('shifts') }}" class="link">Find Providers </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="add-newshift-section">
        <div class="add-newshift-inner">
            <div class="container">
                <!-- back to listing -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="back-to-listing">
                            <a href="{{ url()->previous() }}"><i class="back-arrow-left"></i>Edit shift</a>
                        </div>
                    </div>
                </div>
                <!-- back to listing end -->

                {!! Form::model($job, [
                'method' => 'PATCH',
                'url' => route('shifts.update',[$job->id]),
                'class' => 'form-horizontal',
                'enctype'=>'multipart/form-data',
                'id' => 'job-form'
                ]) !!}

                @include('frontend-new.job.form')

                {!! Form::close() !!}

            </div>
        </div>
    </section>

</div>

@endsection
