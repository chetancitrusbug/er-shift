@extends('frontend-new.layouts.app')

@section('title', 'View Shift Detail')

@section('content')

<div class="main-middle-area inner-main-middle-area">
    @if (Auth::check())
        <section class="update-section">
        <div class="container">
            <div class="row">
            <div class="col-lg-12 col-md-12">
                <div>
                <p>
                    @if(Auth::user()->employeeDetail && Auth::user()->employeeDetail->profession_id > 0 )
                        <a href="{{ route('shifts.create') }}" class="link">Add a new shift</a>
                    @else
                        <a href="{{ route('profile.edit') }}" class="link">Update profile</a> and <a href="{{ route('shifts.create') }}" class="link">Add a new shift</a></p>
                    @endif
                </div>
            </div>
            </div>
        </div>
        </section>
    @endif

    <section class="ershift-detail-section">
      <div class="ershift-detail-inner">
        <div class="container">
          <!-- back to listing -->
          <div class="row">
            <div class="col-lg-12">
              <div class="back-to-listing">
                <a href="{{ url()->previous() }}" ><i class="back-arrow-left"></i> {{  $job->job_title }} </a>
              </div>
            </div>
          </div>
          <!-- back to listing end -->

          <!-- detail page -->
          <div class="detail-section">
            <div class="row row-7">
              <div class="col-lg-8 col-md-12 pad-7">
                <div class="detail-img">
                    @if($job->employee_user && $job->employee_user->profile_image_url)
                        <img src="{{$job->employee_user->profile_image_url}}" class="img-fluid img-thumb-main" alt="Shifts" />
                    @else
                        <img src="{{ asset('assets/images/detail-page.png') }}" class="img-fluid" />
                    @endif
                </div>
                <div class="detail-content">
                  <h3>JOB RESPONSIBILITY</h3>
                  <p>{{  $job->job_responsibility }}</p>
                </div>
                <div class="detail-content">
                  <h3>ADDRESS</h3>
                  <p>{{  $job->address }}</p>
                </div>
                <div class="detail-content">
                  <h3>LOCATION</h3>
                  <p>{{ $job->location ? $job->location->name : ''  }} </p>
                </div>
              </div>
              <div class="col-lg-4 col-md-12 pad-7">
                <div class="detail-content-info">
                  <div class="form-group">
                    <label>Name</label>
                    <p>{{ $job->employee_user->profile_name ?? $job->employee_user->user->name }}</p>
                  </div>

                  <div class="form-group">
                    <label>Desired rate</label>
                    <p>$ {{ $job->rate }} per Hours</p>
                  </div>

                  <div class="form-group">
                    <label>Date Needed</label>
                    <p>
                      @if(($job->from_date && $job->to_date ) ) 
                      {{ ($job->from_date && $job->to_date ) ?  Carbon\Carbon::parse($job->from_date)->format('d M Y').' to '.Carbon\Carbon::parse($job->to_date)->format('d M Y') : '' }}
                      @else
                      {{ implode(',',$job->job_dates()->pluck('job_date')->toArray() ) }}
                      @endif
                    </p>
                  </div>

                  <div class="form-group">
                    <label>Specialities</label>
                    <p> {{ $job->speciality ? $job->speciality->title : '' }} </p>
                  </div>

                  <div class="form-group">
                    <label>Profession</label>
                    <p>{{ $job->profession ? $job->profession->title : '' }} </p>
                  </div>

                  <div class="form-group">
                    <label>Degree</label>
                    <p>{{ $job->degree ? $job->degree->title : ''  }}</p>
                  </div>

                  <div class="form-group">
                    <label>Experience</label>
                    <p>{{ $job->experience ?  $job->experience->title.' Years' : '-' }} </p>
                  </div>

                  <div class="form-group ">
                    <label>ED Volume</label>
                    <p> {{ $job->ed_volume ? $job->ed_volume->title : ''  }}  
                      {{-- <span><i class="material-icons">keyboard_arrow_right</i></span> --}}
                    </p>
                  </div>

                  <div class="form-group mb-0">
                    <label>STATUS</label>
                    @if($job->status == "1")
                      <p class="mb-0 status-text-active">Active </p>
                    @else
                    <p class="mb-0 status-text-inactive">Inactive </p>
                    @endif
                  </div>

                </div>

                <div class="btns-groups">

                  <a href="{{ route('shifts.edit',['id'=>$job->id]) }}" class="btn btn-outline-primary btn-gradient-primary font-16 f-500 "> EDIT</a>
                  <a href="#" data-id="{{$job->id}}" class="btn btn-secondary btn-secondary-link font-16 f-700 delete-shift" data-toggle="modal" data-target="#delete-modal">Delete</a>
                   

                </div>

              </div>
            </div>
          </div>

             <!-- detail page end-->

        </div>
      </div>
    </section>

    @include('frontend-new.components.recent-shifts')
    @include('frontend-new.modal.delete-shift')

  </div>


@endsection
@push('js')
<script>

</script>
@endpush



