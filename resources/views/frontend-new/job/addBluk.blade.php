@extends('frontend-new.layouts.app')

@section('title',trans('jobs.label.jobs'))

@section('content')


<div class="main-middle-area inner-main-middle-area">

<section class="update-section">
<div class="container">
    <div class="row">
    <div class="col-lg-12 col-md-12"> 
        <div>
        <p> <a href="{{ route('plans') }}" class="link">Upgrade subscription </a> and <a href="{{ route('shifts.create') }}" class="link">Add new shifts </a> </p>
        </div>
    </div>
    </div>
</div>
</section> 


<section class="add-newshift-section general-new-shift-section">
    <div class="add-newshift-inner">  
      <div class="container">

        <!-- back to listing -->
        <div class="general-new-shift-top-div">
          <div class="row">
            <div class="col-lg-6 col-md-6">
              <div class="back-to-listing">
                <a href="{{ url()->previous() }}" ><i class="back-arrow-left"></i>Add Shift In Bulk</a>
              </div>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="add-btn-right">
                <a href="{{ route('shifts.create') }}" class="btn btn-dark-blue"> <span class="material-icons"> add </span> Add Shift</a>
              </div>
            </div>
          </div>
        </div>  
        <!-- back to listing end -->

        <div class="bulk-root-div">


            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif


          <div class="row mrl-8">
            <div class="col-lg-12 col-md-12 plr-8">
              <div class="csv-root-div">

                {!! Form::open(['url' => '/store-jobs', 'method'=>'POST','enctype' => 'multipart/data','files'=>'true']) !!}
                <div class="row mrl-5">
                  <div class="col-lg-3 col-md-4 left-csv plr-5">
                    <div class="download-csv-div">
                      <a href="{{ ($sample) ? $sample : '#' }}" class="download-btn" title=@lang('jobs.label.sample_csv')> 
                        <span class="download-box"> 
                          <span class="span-block">Download</span> 
                          <span class="span-block">@lang('jobs.label.sample_csv')</span> 
                          <span class="material-icons"> save_alt </span>  
                        </span>
                      </a>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-4 left-csv plr-5">
                    <div class="download-csv-div">
                      <a href="{{ route('download-csv-data') }}" class="download-btn" title=@lang('jobs.label.bulk_job_data')> 
                        <span class="download-box"> 
                          <span class="span-block">Download</span> 
                          <span class="span-block">@lang('jobs.label.bulk_job_data')</span> 
                          <span class="material-icons"> save_alt </span>  
                        </span>
                      </a>
                    </div>
                  </div>
                  
                

                  <div class="col-lg-6 col-md-4 right-csv plr-5">
                    <div class="upload-csv-div">
                      <div class="custom-file custom-file-general">
                        <input type="file" name="file" class="custom-file-input form-control file-upload" id="customFile" required >
                        <label class="custom-file-label" for="customFile">
                          <span class="custom-file-box">
                            <span class="span-block" id="text-view">Upload CSV File with Required fields</span> 
                            <span class="material-icons"> publish </span>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                    
                <div class="row mrl-5">
                  <div class="col-lg-12 col-md-12 plr-8">
                    <div class="scv-table-div">
                      <div class="title-div">
                        <h5>Download sample csv format for multiple jobs</h5>
                        <p>To upload multiple shifts please use the format below. We highly encourage you to have the pay rate. You will get more inquiries.</p>
                        <h5>**Max Job Rate is {{ (isset($jobMaxRate) ? $jobMaxRate : '0') }} </h5>
                      </div>

                      <p class="table-heading">Profession , Speciality & Degree</p>

                      <div class="table-row-div">
                        <div class="table-responsive table-responsive-custom">
                          <table class="table table-bordered table-custom-common table-degree" >
                            <thead>
                                <tr>
                                    <th>@lang('jobs.label.profession')</th>
                                    <th>@lang('jobs.label.speciality')</th>
                                    <th>@lang('jobs.label.degree')</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($shiftData as $item)
                                <tr>
                                    <th>{{ (isset($item['profession']) ? $item['profession'] : '') }}</th>
                                    <th>{{ implode(', ',$item['speciality']) }}</th>
                                    <th>{{ implode(', ',$item['degree']) }}</th>
                                </tr>
                                @endforeach
                            </tbody>

                          </table>
                        </div>
                      </div>

                    </br>
                    </br>

                      <p class="table-heading">CSV Format</p>

                      <div class="table-row-div">
                        <div class="table-responsive table-responsive-custom">
                          <table class="table table-bordered table-custom-common">
                            <thead>
                                <tr>
                                    <th>@lang('jobs.label.job_title')</th>
                                    <th>@lang('jobs.label.job_responsibility')</th>
                                    <th>@lang('jobs.label.profession')</th>
                                    <th>@lang('jobs.label.speciality')</th>
                                    <th>@lang('jobs.label.degree')</th>
                                    <th>@lang('jobs.label.experience')</th>
                                    <th>@lang('jobs.label.address')</th>
                                    <th>@lang('jobs.label.location')</th>
                                    <th>@lang('jobs.label.ed_volume')</th>
                                    <th>@lang('jobs.label.dates')</th>
                                    <th>@lang('jobs.label.shift')</th>
                                    <th>@lang('jobs.label.rate')</th>
                                </tr>

                              {{-- <tr>
                                <th class="width-180">Job Title</th>
                                <th class="width-300">Job Responsibility</th>
                                <th class="width-180">Profession</th>
                                <th class="width-75">Shift</th>
                                <th class="width-180">Speciality</th>
                                <th class="width-115">Address</th>
                                <th class="width-200">Experience in yrs</th>
                              </tr> --}}
                            </thead>
                            @if(isset($job))
                            <tbody>
                                <tr>
                                    <th>{{ $job->job_title }}</th>
                                    <th>{{ $job->job_responsibility }}</th>
                                    <th>{{ (isset($job->profession->title) ? $job->profession->title : '') }}</th>
                                    <th>{{ (isset($job->speciality->title) ? $job->speciality->title : '') }}</th>
                                    <th>{{ (isset($job->degree->title) ? $job->degree->title : '') }}</th>
                                    <th>{{ (isset($job->experience->title) ? $job->experience->title : '') }}</th>
                                    <th>{{ $job->address }}</th>
                                    <th>{{ (isset($job->location->name) ? $job->location->name : '') }}</th>
                                    <th>{{ (isset($job->edvolume->title) ? $job->edvolume->title : '') }}</th>
                                    <th>mm/dd/yyyy|12/25/2018</th>
                                    <th>{{ (isset($job->shift->title) ? $job->shift->title : '') }}</th>
                                    <th>{{ $job->rate }}</th>
                                </tr>
                            </tbody>
                            @endif
                          </table>
                        </div>
                      </div>


                    </div>
                  </div>    
                </div>

              </div>
            </div>
          </div>

        </div>

      <div class="row">
        <div class="col-lg-12 mt-3">
          <div class="form-btn-bottom-div">
            <a href="{{  route('my-shifts')  }}" class="btn btn-outline-primary btn-gradient-primary btn-big-btn">Cancel</a>
            {!! Form::submit(trans('jobs.label.upload'), ['class' => 'btn btn-secondary btn-secondary-link btn-big-btn ml-20 btn-submit']) !!}
          </div>
        </div>
      </div>
    </div>
  </section>

  
</div>


@endsection

@push('js')

<script>
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html("<span class='custom-file-box'><span class='span-block'>"+ fileName  +"</span>" + "<span class='material-icons'> publish </span> </span>'");
});
</script>

@endpush