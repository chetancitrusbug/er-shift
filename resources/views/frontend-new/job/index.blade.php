@extends('frontend-new.layouts.app')

@section('title', 'My Shifts')

@section('content')

<div class="main-middle-area inner-main-middle-area">


    @if( count($shifts) == 0 )
    <section class="update-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div>
                        <p> <a href="{{ route('shifts.create') }}" class="link">Add Shift</a> and <a
                                href="{{ route('shifts') }}" class="link">Find a Shift</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="no-record-section">
        <div class="container">

            <!-- back to listing -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="back-to-listing">
                        <a href="{{ url()->previous() }}"><i class="back-arrow-left"></i> My Shift</a>
                    </div>
                </div>
            </div>
            <!-- back to listing end -->

            <div class="no-record-card-div">

                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="thumb-icon">
                            <img src="{{ asset('assets/images/to_do_list.svg') }}" class="img-no-record"
                                alt="Shift is not added" />
                        </div>
                        <div class="content-record-div">
                            <h4>Shift is not added.</h4>
                            <p>Please add a new shift and publish your requirements.</p>
                            <div class="btn-row d-flex">
                                <a href="{{ route('shifts.create') }}"
                                    class="btn btn-outline-primary btn-gradient-primary text-uppercase">+ ADD NEW
                                    SHIFT</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    @else


    <section class="shifts-card-section shifts-card-inner-section">
        <div class="shifts-card-div"> 
          <div class="container">
  
            <div class="row">
                <div class="col-lg-9 col-md-9"> 
                  <div class="heading-left-div">
                    <div class="text-title">
                      <h2>Recently added</h2>
                    </div>
                    
                    {{-- <div class="right-side-div">
                      <div class="selectbox-inline">
                        <div class="select-box select-common select-box-group select-custom2">
                          <select class="js-select2">
                              <option>Sort by</option>
                              <option>High</option>
                              <option>Low</option>
                          </select>
                        </div>
                      </div>
                    </div> --}}
                    
                  </div>
                </div>

                <div class="col-lg-3 col-md-3">
                    <div class="add-btn-right">
                      <a href="{{ route('shifts.create') }}" class="btn btn-dark-blue"> <span class="material-icons"> add </span> Add Shift</a>
                    </div>
                  </div>
            </div>        
  
            <div class="general-shift-card-box-root">
              
              <div class="row mrl-5">

                @foreach ($shifts as $shift)
  
                <div class="col-lg-12 col-md-6 plr-5">
                  <div class="general-shift-card-box">

                    <a href="{{ route('my-shifts.view',['id'=>$shift->id]) }}" >
  
                    <div class="shift-card-box-main">
                      
                      <div class="row shift-grid-row">
  
                        <div class="col-lg-2 shift-grid-column width-20">
                          <div class="title-div">
                            <h3>Shift title</h3>
                          </div>
                          <div class="desc-div">
                            <p>{{ $shift->job_title }}</p>
                          </div>
                        </div> 
  
                        <div class="col-lg-2 shift-grid-column width-25">
                          <div class="title-div">
                            <h3>Shift responsibility</h3>
                          </div>
                          <div class="desc-div">
                            <p>{{ $shift->job_responsibility }}</p>
                          </div>
                        </div> 
  
                        <div class="col-lg-2 shift-grid-column width-10">
                          <div class="title-div">
                            <h3>Profession</h3>
                          </div>
                          <div class="desc-div">
                            <p>{{ $shift->profession ? $shift->profession->title : '' }}</p>
                          </div>
                        </div> 
  
                        <div class="col-lg-2 shift-grid-column width-20">
                          <div class="title-div">
                            <h3>Speciality</h3>
                          </div>
                          <div class="desc-div">
                            <p>{{ $shift->speciality ? $shift->speciality->title : '' }}</p>
                          </div>
                        </div> 
  
                        <div class="col-lg-2 shift-grid-column width-15">
                          <div class="title-div">
                            <h3>Degree</h3>
                          </div>
                          <div class="desc-div">
                            <p>{{ $shift->degree ? $shift->degree->title : '' }}</p>
                          </div>
                        </div> 
  
                        <div class="col-lg-2 shift-grid-column width-10">
                          <div class="title-div">
                            <h3>Status</h3>
                          </div>
                          <div class="desc-div">
                            @if($shift->status == "1")
                            <div class="status-div active-status"> <span class="status-dot"></span> <span class="status-text">Active</span> </div>
                            @else
                            <div class="status-div inactive-status"> <span class="status-dot"></span> <span class="status-text">InActive</span> </div>
                            @endif
                          </div>
                        </div>
  
                      </div> 
  
                    </div>

                  </a>

                    <div class="shift-card-box-bottom">
                      <div class="shift-box-bottom-right">
                        <div class="btn-group-row">
                          <a href="{{ route('shifts.edit',['id'=>$shift->id]) }}" class="btn btn-general"><span class="material-icons"> edit </span>Edit</a>
                          <a href="#" data-id="{{$shift->id}}" class="btn btn-general btn-delete ml-10 delete-shift" data-toggle="modal" data-target="#delete-modal"><span class="material-icons material-icons-outlined"> delete </span>Delete</a>
                        </div>
                      </div>
                    </div>
  
                  </div>
                </div>

               
  
                @endforeach
            </div>    

            {{$shifts->appends(request()->input())->links('frontend-new.layouts.include.pagination')}}

  
          </div>            
        </div>            
      </section>
  

    @endif

</div>



@include('frontend-new.modal.delete-shift')
@endsection
@push('js')
<script>
    $(document).ready(function() {
        // Configure/customize these variables.
        var showChar = 25;  // How many characters are shown by default
        var ellipsestext = "...";
        var moretext = "Show more >";
        var lesstext = "Show less";


        $('.more').each(function() {
            var content = $(this).html();

            if(content.length > showChar) {

                var c = content.substr(0, showChar);
                var h = content.substr(showChar, content.length - showChar);

                var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><div class="morecontent"><abbr>' + h + '</abbr>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></div>';

                $(this).html(html);
            }

        });

        $(".morelink").click(function(){
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
    });
</script>

@endpush
