@extends('frontend-new.layouts.app')

@section('title', 'Add Shift Request')

@section('content')

<div class="main-middle-area inner-main-middle-area">

    <section class="update-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div>
                        <p> <a href="{{ route('profile.edit') }}" class="link">Update Profile </a> and <a
                                href="{{ route('shifts') }}" class="link">Find Providers </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="add-newshift-section general-new-shift-section">
        <div class="add-newshift-inner">
            <div class="container">
                <!-- back to listing -->
                <div class="general-new-shift-top-div">
                    <div class="row">
                      <div class="col-lg-6 col-md-6">
                        <div class="back-to-listing">
                          <a href="{{ url()->previous() }}" ><i class="back-arrow-left"></i>Add a new Shift/Job</a>
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6">
                        <div class="add-btn-right">
                          <a href="{{ route('shifts.create.bulk') }}" class="btn btn-dark-blue"> <span class="material-icons"> add </span> Add Shifts In Bulk</a>
                        </div>
                      </div>
                    </div>
                  </div>  
                <!-- back to listing end -->

                {!! Form::open(['route' => 'shifts.store', 'class' => 'form-horizontal',
                'enctype'=>'multipart/form-data', 'id' => 'job-form' ]) !!}

                @include('frontend-new.job.form')

                {!! Form::close() !!}

            </div>
        </div>
    </section>

</div>

@endsection
