<div class="form-section">

    <div class="row">
        <div class="col-lg-6">
            <div class="form-group{{ $errors->has('job_title') ? ' has-error' : ''}}">
                {!! Form::label('job_title', trans('jobs.label.job_title').'*', ['class' => 'control-label']) !!}
                {!! Form::text('job_title', null, ['class' => 'form-control' ]) !!}
                {!! $errors->first('job_title', '<p class="help-block">:message</p>') !!}

            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group{{ $errors->has('job_responsibility') ? ' has-error' : ''}}">
                {!! Form::label('job_responsibility',trans('jobs.label.job_responsibility').'*', ['class' => '
                control-label'])
                !!}

                {!! Form::text('job_responsibility', null, ['class' => 'form-control']) !!}
                {!! $errors->first('job_responsibility', '<p class="help-block">:message</p>') !!}
            </div>

        </div>
        <div class="col-lg-6">
            <div class="form-group {{ $errors->has('profession_id') ? 'has-error' : ''}}">
                {!! Form::label('profession_id', trans('jobs.label.profession').'*' , ['class' => ' control-label']) !!}
                <div class="select-box select-common select-custom2">
                    {!! Form::select('profession_id',$profession,null, ['class' => 'form-control js-select2', 'id' =>
                    'profession_id' ])
                    !!}
                </div>
                {!! $errors->first('profession_id','<p class="help-block with-errors">:message</p>') !!}
                <label id="profession_id-error" class="error" for="profession_id"></label>
            </div>

        </div>

        <div class="col-lg-6">
            <div class="form-group {{ $errors->has('speciality_id') ? 'has-error' : ''}}">
                {!! Form::label('speciality_id',trans('jobs.label.speciality').'*', ['class' => ' control-label']) !!}
                <div class="select-box select-common select-custom2">
                    {!! Form::select('speciality_id',(isset($speciality) ? $speciality : ['' =>'Select
                    Speciality']),null,
                    ['class' => 'form-control js-select2', 'id' => 'speciality_id' ] ) !!}
                </div>
                {!! $errors->first('speciality_id','<p class="help-block with-errors">:message</p>') !!}
                <label id="speciality_id-error" class="error" for="speciality_id"></label>
            </div>

        </div>

        <div class="col-lg-6">
            <div class="form-group {{ $errors->has('experience_id') ? 'has-error' : ''}}">
                {!! Form::label('experience_id', trans('jobs.label.experience').'*', ['class' => ' control-label']) !!}
                <div class="select-box select-common select-custom2">
                    {!! Form::select('experience_id',$experience,null, ['class' => 'form-control js-select2', ]) !!}
                </div>
                {!! $errors->first('experience_id','<p class="help-block with-errors">:message</p>') !!}
                <label id="experience_id-error" class="error" for="experience_id"></label>
            </div>

        </div>


        <div class="col-lg-6">

            <div class="form-group {{ $errors->has('location_id') ? 'has-error' : ''}}">
                {!! Form::label('location_id', trans('jobs.label.location').'*', ['class' => ' control-label']) !!}
                <div class="select-box select-common select-custom2">
                    {!! Form::select('location_id',$location,null, ['class' => 'form-control js-select2', ]) !!}
                </div>
                {!! $errors->first('location_id','<p class="help-block with-errors">:message</p>') !!}
                <label id="location_id-error" class="error" for="location_id"></label>

            </div>
        </div>
        <div class="col-lg-6">

            <div class="form-group {{ $errors->has('degree_id') ? 'has-error' : ''}}">
                {!! Form::label('degree_id',trans('jobs.label.degree').'*', ['class' => ' control-label']) !!}
                <div class="select-box select-common select-custom2">
                    {!! Form::select('degree_id',$degree,null, ['class' => 'form-control js-select2', ]) !!}
                </div>
                {!! $errors->first('degree_id','<p class="help-block with-errors">:message</p>') !!}
                <label id="degree_id-error" class="error" for="degree_id"></label>
            </div>

        </div>
        <div class="col-lg-6">
            <div class="form-group {{ $errors->has('ed_volume_id') ? 'has-error' : ''}}">
                {!! Form::label('ed_volume_id',trans('jobs.label.ed_volume').'*', ['class' => ' control-label']) !!}
                <div class="select-box select-common select-custom2">
                    {!! Form::select('ed_volume_id',$ed_volume,null, ['class' => 'form-control js-select2', ]) !!}
                </div>
                {!! $errors->first('ed_volume_id','<p class="help-block with-errors">:message</p>') !!}
                <label id="ed_volume_id-error" class="error" for="ed_volume_id"></label>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="form-group {{ $errors->has('shift_id') ? 'has-error' : ''}}">
                {!! Form::label('shift_id', trans('jobs.label.shift').'*', ['class' => ' control-label']) !!}
                <div class="select-box select-common select-custom2">
                    {!! Form::select('shift_id',$shift,null, ['class' => 'form-control js-select2', ]) !!}
                </div>
                {!! $errors->first('shift_id','<p class="help-block with-errors">:message</p>') !!}
                <label id="shift_id-error" class="error" for="shift_id"></label>
            </div>

        </div>


        <div class="col-lg-6">
            <div class="form-group">
                <label>Desired Rate ( $/Hour ) *</label>
                <div class="rangeslider-wrap rangeslider-wrap-custom">
                    <input type="range" min="0" max="{{ $jobMaxRate }}" step="" 
                        labels="$0, ${{ $jobMaxRate }}" name="rate" value="{{ isset($job) ? $job->rate : 100 }}">
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="form-group{{ $errors->has('address') ? ' has-error' : ''}}">
                {!! Form::label('address', trans('jobs.label.address'), ['class' => ' control-label']) !!}

                {!! Form::textarea('address', null, ['class' => 'form-control', 'rows' => 2]) !!}
                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
            </div>

        </div>

        <div class="col-lg-6">
            <div class="form-group{{ $errors->has('dates') ? ' has-error' : ''}}">
                {!! Form::label('dates', trans('jobs.label.dates').'*', ['class' => ' control-label']) !!}

                {!! Form::text('dates', '', ['class' => 'dates date form-control',]) !!}


                {!! $errors->first('dates', '<p class="help-block">:message</p>') !!}
            </div>

        </div>

        <div class="col-lg-6">
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', trans('jobs.label.status'), ['class' => ' control-label']) !!}
                <div class="select-box select-common select-custom2">
                    {!! Form::select('status',['1'=>trans('jobs.label.active'),'0'=>trans('jobs.label.inactive')] ,null, ['class' => 'form-control
                    js-select2']) !!}
                </div>
                {!! $errors->first('status','<p class="help-block with-errors">:message</p>') !!}
            </div>

        </div>




    </div>

</div>

<div class="row ">
    <div class="col-lg-12 mt-3 d-flex justify-content-end ">
        <button type="submit" class="btn btn-secondary btn-secondary-link min-width-200">Save</button>
    </div>
</div>


@push('js')
<script type="text/javascript" src="{{ asset('assets/js/rangeslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/rangeslider-get.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css"
    rel="stylesheet" />

<script>
    var today = new Date(); today.setDate(today.getDate() + 1);
    $('.dates').datepicker({ multidate: true , format: 'yyyy-mm-dd', startDate: today });



    @if(isset($job->dates) && $job->dates != null)
        var arr = new Array();
        @foreach ($job->dates as $key => $item)
            arr.push(new Date("{{$item}}"));
        @endforeach
        console.log(arr)
        $('.dates').datepicker('setDates', arr);
    @endif


    
$(function() {
    $(document).on('change', '#profession_id', function (e) {
        var id = $('#profession_id').val();
        if(id != '')
        {
            var url ="{{ url('/getSpeciality/') }}";
            url = url + "/" + id;
            $.ajax({
                type: "get",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                        data = JSON.parse( data );
                        var select = $('#speciality_id');
                        select.empty();
                        select.append('<option value="">Select Speciality</option>');
                        $.each(data,function(key, value)
                        {
                            select.append('<option value=' + key + '>' + value + '</option>');
                        });
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
        else
        {
            var select = $('#speciality_id');
            select.empty();
            select.append('<option value="">Select Speciality</option>');
        }

    });
});


        $("#job-form").validate({

            rules: {
                job_title: {
                    required: true, maxlength:100
                },
                job_responsibility:{
                    required: true,maxlength:5000
                },
                profession_id: {
                    required: true
                },
                speciality_id: {
                    required: true
                },
                experience_id: {
                    required: true
                },
                location_id : {
                    required : true
                },
                degree_id: {
                    required: true
                },
                ed_volume_id: {
                    required: true
                },
                shift_id : {
                    required : true
                },
                address: {
                    required: false , maxlength:200
                },
                dates : {
                    required : true,
                    date: false,
                },
               
            },
            messages: {
                job_title: {
                    required: "Enter job title", 
                },
                job_responsibility:{
                    required: "Enter description or job responsibilities",
                },
                profession_id: {
                    required: 'Select Profession'
                },
                speciality_id: {
                    required: 'Select Speciality'
                },
                experience_id: {
                    required: 'Select Experience'
                },
                location_id : {
                    required : 'Select Location'
                },
                degree_id: {
                    required: 'Select Degree'
                },
                ed_volume_id: {
                    required: 'Select Ed Volume'
                },
                shift_id : {
                    required : 'Select Shift'
                },
                dates : {
                    required : 'Select Dates'
                },
            },
            onfocusout: function(element) {
                this.element(element);
            },
            success: function(error) {
                error.removeClass("error");
            },
            submitHandler: function(form) {
                form.submit();
            }
        });


</script>
@endpush