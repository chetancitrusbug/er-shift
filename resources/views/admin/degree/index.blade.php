@extends('layouts.admin')

@section('title','Degree')

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                        Degree
                </div>
            </div>
            <div class="box-content ">
                <div class="row">
                    <div class="col-md-6">
                        <a href="{{ url('/admin/degree/create') }}" class="btn btn-success btn-sm" title="Add New Degree">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New Degree </a>
                    </div>
                        <div class="col-md-6">
                            <select class="form-control navbar-form navbar-right selectStatusArea" name='status'>
                                <option value=''>All</option>
                                <option value='1'>Active</option>
                                <option value='0'>Inactive</option>
                            </select>
                        </div>
                        
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/degree', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
    
                        {!! Form::close() !!}
                </div>
                <div class="table-responsive" >
                    <table class="table table-borderless" id="degree-table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Profession</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script-head')
<script>
    $(function() { 
        var url ="{{ url('/admin/degree/') }}";
        var datatable = $('#degree-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{!! route("degreeData") !!}',
                    type: "get", // method , by default get
                },
                columns: [
                    { data: 'title',name:'title',"searchable" : true}, 
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var professions = '';
                            if(o.professions != null && o.professions.title != null)
                            professions = o.professions.title;
                            else
                            professions = '';
                            return professions;
                        }
                    }, 
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.status == '0')
                            status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-success btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Inactive</button></a>';
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> Active</button></a>";
                            return status;
                        }
                    }, 
                    
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>Edit</button></a>&nbsp;";
                                d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";                    
                            return e+d;
                        }
                    }
                ]
        });
        $( ".selectStatusArea" ).change(function() {  
            var status = $( ".selectStatusArea" ).val();
            var url ="{!! route('degreeData') !!}";
            datatable.ajax.url( url + '?status='+ status).load();            
        });
        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            
            var url ="{{ url('/admin/degree/') }}";
            url = url + "/" + id;
            var r = confirm("Are you sure you want to delete Degree ?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success('Action Success!', data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procede!',erro)
                    }
                });
            }
        });
    }); 
</script>
@endpush
