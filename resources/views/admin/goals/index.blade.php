@extends('layouts.backend')

@section('title',trans('goal.goals'))
@section('pageTitle',trans('goal.goals'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('goal.goals')
                                                  </div>

                               </div>
                <div class="box-content ">


                    <div class="row">
                        <div class="col-md-6">
                                <a href="{{ url('/admin/goals/create') }}" class="btn btn-success btn-sm"
                                   title="Add New Goal">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('goal.add_new_goal')
                                </a>
                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/goals', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                            
                            <div class="form-group">
                            {!! Form::select('status',array('all'=>'All','active'=>'Active','inactive'=>'Inactive'),'',['class'=>'form-control', 'id'=>'filter_status']) !!}
                            </div>
                            {!! Form::close() !!}
                            </div>
                        </div>
                    


                    <div class="table-responsive">
                        <table class="table table-borderless" id="goals-table">
                            <thead>
                            <tr>
                                <th data-priority="1">@lang('goal.id')</th>                         
                                <th data-priority="3">@lang('goal.title')</th>
                                <th data-priority="4">@lang('goal.details')</th>                          
                                <th data-priority="7">@lang('goal.status')</th>
                                <th data-priority="8">@lang('goal.actions')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/goals/') }}";
var img_path = "{{asset('images')}}";

        
        datatable = $('#goals-table').DataTable({
             "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
             ajax: {
                    url: '{!! route('GoalsControllerGoalsData') !!}',//"{{ url('admin/tickets/datatable') }}", // json datasource
                    type: "get", // method , by default get
                    data: function (d) {
                        d.filter_role = $('#filter_role').val();

                        d.filter_status = $('#filter_status').val();
                    }
                },
                columns: [
                    { data: 'id', name: 'Id',"searchable": false },
                    { 
                        "data" : null, 
                        "searchable" : false,
                         "orderable":false,
                        render : function(o){
                          return o.title;  
                        }
                    },
                    { data: 'details',name:'goals.details',"searchable" : false,"orderable":false,},
                              
                     {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                           if(o.status == 'inactive')
                            
                             status = "<a href='"+url+"/"+o.id+"?status=inactive' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('goal.inactive')</button></a>";
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('goal.active')</button></a>";
                            
                            return status;
                                            
                        }

                    }, 
					{ 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                           
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>edit</button></a>&nbsp;";

                               d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";
                               
                            return e+d;
                        }
                    }
                    
                ]
        });
    $('#filter_role').change(function() {
        datatable.draw();
    });
    $('#filter_status').change(function() {
        datatable.draw();
    });
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
		
		var url ="{{ url('/admin/goals/') }}";
        url = url + "/" + id;
		//alert(url);
        
        var r = confirm("Are you sure you want to delete Goal ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });

</script>
@endpush
