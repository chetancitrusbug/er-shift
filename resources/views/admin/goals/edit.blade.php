@extends('layouts.backend')

@section('title',trans('user.edit_user'))
@section('pageTitle',trans('user.edit_user'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('user.edit_user')</div>
                <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('user.back')
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($goal, [
                        'method' => 'PATCH',
                        'url' => ['/admin/goals', $goal->id],
                        'class' => 'form-horizontal',
                        'id'=>'formGoal',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.goals.form', ['submitButtonText' => trans('user.update')])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection

