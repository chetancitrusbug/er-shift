@extends('layouts.admin')
@section('title',trans('jobs.label.jobs'))
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    @lang('jobs.label.jobs')
                </div>
            </div>
            <div class="box-content box-1">
                <div class="row btnbtmspace">
                    <div class="col-md-6">
                        <a href="{{ url('/admin/addBlukJobs') }}" class="btn btn-success btn-sm" title=@lang('jobs.label.add_bulk_jobs')>
                        <i class="fa fa-plus" aria-hidden="true"></i> @lang('jobs.label.add_bulk_jobs') </a>
                    </div>
                </div>
                <div class="table-responsive" >
                    <table class="table table-borderless" id="job-table">
                        <thead>
                            <tr>
                                <th>Employer</th>
                                <th>@lang('jobs.label.job_title')</th>
                                <th>@lang('jobs.label.job_responsibility')</th>
                                <th>@lang('jobs.label.profession')</th>
                                <th>@lang('jobs.label.speciality')</th>
                                <th>@lang('jobs.label.degree')</th>
                                <th>@lang('jobs.label.status')</th>
                                <th>@lang('jobs.label.action')</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
    $(function() {
        var url ="{{ url('/admin/shift/') }}";
        var datatable = $('#job-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{!! route("shiftsData") !!}',
                    type: "get", // method , by default get
                },
                columns: [
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var emp = '';
                            if(o.employee != null)
                                emp = o.employee.emp_name;
                            else
                                emp = "";
                            return emp;
                        }
                    },
                    { data: 'job_title',name:'job_title',"searchable" : true},
                    { data: 'job_responsibility',name:'job_responsibility',"searchable" : true},
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var profession = '';
                            if(o.profession != null)
                            profession = o.profession.title;
                            else
                            profession = "";
                            return profession;
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var speciality = '';
                            if(o.speciality != null)
                            speciality = o.speciality.title;
                            else
                            speciality = "";
                            return speciality;
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var degree = '';
                            if(o.degree != null)
                            degree = o.degree.title;
                            else
                            degree = "";
                            return degree;
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.status == '0')
                            status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-warning btn-xs"> @lang("jobs.label.inactive")</button></a>';
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-warning btn-xs'> @lang('jobs.label.active')</button></a>";
                            return status;
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                                d = "<a href='javascript:void(0);' ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> @lang('jobs.label.delete')</button></a>&nbsp;";
                                return d;
                        }
                    }
                ]
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');

            var url ="{{ url('admin/shift/') }}";
            url = url + "/" + id;
            var r = confirm("@lang('jobs.label.sure_job_delete')");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(trans('jobs.label.action_success'), data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error(trans('jobs.label.action_not_success'),erro)
                    }
                });
            }
        });

    });
</script>
@endpush
