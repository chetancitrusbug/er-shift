@extends('layouts.admin')

@section('title','DoctorWish')

@section('content')
     <div class="row">
        <div class="col-md-12">
			<div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        DoctorWish
                    </div>
				</div>
					<div class="box-content ">

						{{--  <a href="{{ url('/admin/doctorwish/create') }}" class="btn btn-success btn-sm" title="Add New Doctor">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>  --}}

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/doctorwish', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ Request::get('search')}}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}
					</div>
					
					 <div class="box-content ">
						

                        

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>index</th>
                                        <th>Doctor</th>
                                        <th>Wish Title</th>
                                        <th>Rate</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i =1;?>
                                @foreach($doctorwishes as $item)
                             
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td><a href="{{ url('/admin/doctorwish', $item->mainid) }}">{{ $item->name }}</a></td>
                                        <td>{{ $item->wish_title }}</td>
                                        <td>{{ $item->rate }}</td>
                                        <td>
                                            <a href="{{ url('/admin/doctorwish/' . $item->mainid) }}" title="View Doctor"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/admin/doctorwish/' . $item->mainid . '/edit') }}" title="Edit Permission"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/doctorwish', $item->mainid],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Doctor',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    <?php $i++;?>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination"> {!! $doctorwishes->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
					 </div>
               
			</div>
        </div>
    </div>
@endsection	
