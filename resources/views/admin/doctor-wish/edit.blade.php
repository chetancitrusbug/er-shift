@extends('layouts.admin')

@section('title','Edit Doctor Wish')

@section('content')
     <div class="row">
        <div class="col-md-12">
			<div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        Edit Doctor Wish
                    </div>
				 </div>	
					 <div class="box-content ">
						<a href="{{ url('/admin/doctorwish') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <!--<div class="tab">-->
                        <!--        <button class="tablinks" onclick="openNext(event, 'SectionOne')">Section1</button>-->
                        <!--        <button class="tablinks" onclick="openNext(event, 'SectionTwo')">Section2</button>-->
                        <!--</div>-->
                        {!! Form::model($doctorwish, [
                            'method' => 'PATCH',
                            'url' => ['/admin/doctorwish', $doctorwish->id],
                            'class' => 'form-horizontal',
                            'enctype'=>'multipart/form-data'
                        ]) !!}
                        
                        @include ('admin.doctor-wish.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}
					 </div>
               
			</div>
        </div>
    </div>
@endsection	
 