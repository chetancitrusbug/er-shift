@extends('layouts.admin')

@section('title','Doctor Wish')

@section('content')
     <div class="row">
        <div class="col-md-12">
			<div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        Doctor Wish
                    </div>
				</div>
					 <div class="box-content ">
						<a href="{{ url('/admin/doctorwish') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/doctorwish/' . $doctorwish->id . '/edit') }}" title="Edit doctorwish"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method' => 'DELETE',
                            'url' => ['/admin/doctorwish', $doctorwish->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete DoctorWish',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>
                        
                   
 
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    
                                    <tr>
                                        <th>Doctor Name</th>
                                        <td> {{ $doctorwish->doctor_user ?  $doctorwish->doctor_user->user->name : '' }} </td>
                                    </tr>
                                    <tr>
                                        <th>Wish Title</th>
                                        <td> {{ $doctorwish->wish_title }} </td>
                                    </tr>
                                    <tr>
                                        <th>Wish Description</th>
                                        <td> {{ $doctorwish->wish_desc }} </td>
                                    </tr>
                                    
                                     {{--@php  $str = ''; @endphp
                                     @if($doctorwish->location_id != null)
                                        
                                        @php
                                        $arr = explode(',',$doctorwish->location_id);
                                       
                                        foreach ($arr as $key1 => $value1) {
                                            $State = App\State::where('id',$value1)->first();
                                            if($State != null)
                                            $str .= (($str == '') ? $State->name : ','.$State->name);
                                        }
                                        @endphp 
                                    @endif--}}
                                    <tr>
                                        <th>Location</th>
                                         <td> {{ $doctorwish->comma_sep_loc_names  }} </td>
                                    </tr>
                                   
                                    <tr>
                                        <th>Ed Volume</th>
                                        <td> {{ $doctorwish->edvolume ? $doctorwish->edvolume->title : '' }} </td>
                                    </tr>
                                    
                                    <tr>
                                        <th> Availbale Shifts : </th>
                                        <td>
                                            <table> 
                                             <th>From :</th> <th> To :</th>   <th> Shift :</th>
                                            @foreach( $doctorwish->date_shifts()->get() as $shifts )
                                               
                                                <tr>
                                                    <td class="col-md-3">   {{ $shifts->from }} </td>
                                                    <td class="col-md-3">  {{ $shifts->to }} </td>
                                                    <td class="col-md-3">  {{ $shifts->shift->title }}</td>
                                                </tr>
                                            @endforeach
                                            </table>
                                        </td
                                    </tr>
                                   
                                    <!--<tr>-->
                                    <!--    <th>Available date</th>-->
                                    <!--    <td> {{ $doctorwish->available_date }} </td>-->
                                    <!--</tr>-->
                                    <!--<tr>-->
                                    <!--    <th>Shift</th>-->
                                    <!--    <td> {{ $doctorwish->shift_id }} </td>-->
                                    <!--</tr>-->
                                    <tr>
                                        <th>Rate</th>
                                        <td> {{ $doctorwish->rate }} </td>
                                    </tr>
                                    
                                    <tr>
                                        <th>Status</th>
                                        
                                        <td> 
                                                @if($doctorwish->status == 1)
                                                    {{ 'Active' }}
                                                @else
                                                    {{ 'Inactive' }} 
                                                @endif
                                             </td>
                                    </tr>
                                </tbody>
                               
                            </table>
                        </div>
					 </div>
                
			</div>
        </div>
    </div>
@endsection	

