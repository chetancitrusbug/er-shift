@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@endpush
<div id="SectionOne" class="tabcontent" style="display:block;">
{{-- Doctor table form --}}
    <div class="form-group{{ $errors->has('doctor_id') ? ' has-error' : ''}}">
        {!! Form::label('doctor_id', 'Doctor: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('doctor_id', $doctorArray, null, ['class' => 'form-control  selectTag']) !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('wish_title') ? ' has-error' : ''}}">
        {!! Form::label('wish_title', 'Wish Title: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('wish_title', null, ['class' => 'form-control']) !!}
            {!! $errors->first('wish_title', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('wish_desc') ? ' has-error' : ''}}">
        {!! Form::label('wish_desc', 'Wish Desc:', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::textarea('wish_desc', null, ['class' => 'form-control']) !!}
            {!! $errors->first('wish_desc', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group{{ $errors->has('location_id') ? ' has-error' : ''}}">
        {!! Form::label('location_id', 'Location: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
        {!! Form::select('location_id[]', $locations, isset($doctorwish) && $doctorwish->locations ? $doctorwish->locations()->pluck('location_id')  : null, ['class' => 'form-control selectTag', 'multiple' => true , 'id' => 'location' ]) !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('ed_volum_id') ? ' has-error' : ''}}">
        {!! Form::label('ed_volum_id', 'ED Volume: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('ed_volum_id', $ed_volumes, null, ['class' => 'form-control']) !!}
        </div>
    </div>

    

    <div class="form-group">
        {!! Form::label('date_shifts', 'Availability :', ['class' => 'col-md-4 control-label']) !!}
        @if(isset($date_shifts))
        <table class="col-md-6"> 
                <th><label>From :</label></th> <th> To :</th>   <th> Shift :</th>
                <th> <a href="#" onClick="addRow();"  ><i class="fa fa-plus"></i></a>  </th>
               @foreach( $date_shifts as $shift )
                   <tr id="tr-{{ $shift->id }}">
                       <td class="col-md-3">  {!! Form::text('from[]', $shift->from,  ['class' => 'form-control fromdatepicker'  ]) !!}  </td>
                       <td class="col-md-3">  {!! Form::text('to[]', $shift->to,  ['class' => 'form-control todatepicker' ]) !!}  </td>
                       <td class="col-md-3"> {!! Form::select('shift_id[]', $shifts, $shift->shift->id , ['class' => 'form-control']) !!} </td>
                        <td class="col-md-3"><a href="#" class="removeParentRow" ><i class="fa fa-times"></i></a></button></td> 
                   </tr>
               @endforeach
        </table> 
        @endif
    </div>


    <div style="display:none;"> 
    <div id="hidden-new-row" >
    {!! Form::label('', '', ['class' => 'col-md-4 control-label']) !!}
    <table class="col-md-6"> 
        <tr>
        <td class="col-md-3">  {!! Form::text('from[]', null ,  ['class' => 'form-control fromdatepicker col-md-3' ]) !!}  </td>
        <td class="col-md-3">  {!! Form::text('to[]', null,  ['class' => 'form-control todatepicker col-md-3' ]) !!}  </td>
        <td class="col-md-3"> {!! Form::select('shift_id[]', $shifts, null , ['class' => 'form-control col-md-3']) !!} </td>   
        <td class="col-md-3"><a href="#" class="removeParentRow" ><i class="fa fa-times"></i></a></button></td> 
        </tr>
    </table>
    </div>
    </div>

    <div class="form-group">
        <div id="newRow"></div>
    </div>


    <div class="form-group{{ $errors->has('rate') ? ' has-error' : ''}}">
            {!! Form::label('rate', 'Rate: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('rate', null, ['class' => 'form-control']) !!}
                {!! $errors->first('rate', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group{{ $errors->has('priority_id') ? ' has-error' : ''}}">
            {!! Form::label('priority_id', 'Shift: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::select('priority_id', $priorities, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
                <div class="col-md-offset-4 col-md-4">
                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
    
    
</div>
    {{-- <div id="SectionTwo" class="tabcontent"> --}}


    {{-- <div class="form-group{{ $errors->has('available_date') ? ' has-error' : ''}}">
        {!! Form::label('available_date', 'Available Date: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('available_date', null, ['class' => 'form-control']) !!}
            {!! $errors->first('available_date', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('shift_id') ? ' has-error' : ''}}">
        {!! Form::label('shift_id', 'Shift: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('shift_id', $shifts, null, ['class' => 'form-control']) !!}
        </div>
    </div> --}}
   
    {{-- <div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
            {!! Form::label('status', 'Status: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::select('status', \config('setting.status'),(isset($employee->status) && $employee->status != null ) ? $employee->status : '', ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
            </div>
    </div>
   --}}
  
{{-- </div> --}}
     {{-- End Doctor Detail Form --}}




     @push('js')
     <script>

       
        
         $('body').delegate('.removeParentRow', 'click', function (e) { 
            $(this).closest('tr').remove()
         })

       

        function addRow(){
            var wrapper = $("#hidden-new-row").html();
           
            $("#newRow").append( wrapper );



        }

  
        $('#location').select2();

        $(function() {
            $( ".fromdatepicker" ).datepicker({
               dateFormat : 'yy-mm-dd',
                'minDate' : moment().format("YY-MM-DD"),
                'startDate' : moment()
            });

            $( ".todatepicker" ).datepicker({
                dateFormat : 'yy-mm-dd',
                'minDate' : moment().format("YY-MM-DD"),
                'startDate' : moment()
            });

     

    

     

     });
     </script>
     @endpush

