@extends('layouts.backend')

@section('title',trans('edeath.edit_edeath'))
@section('pageTitle',trans('edeath.edit_edeath'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('edeath.edit_edeath')</div>
                <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('sadmin.back')
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($country, [
                        'method' => 'PATCH',
                        'url' => ['/admin/expecteddeath', $country->id],
                        'class' => 'form-horizontal',
                        'id'=>'formExpecteddeath',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.expecteddeath.form', ['submitButtonText' => trans('sadmin.update')])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
