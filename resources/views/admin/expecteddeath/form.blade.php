
<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
        <h2 class="exp_country text-center">{{$country->name}}</h2>
</div>
<div class="form-group{{ $errors->has('male_average') ? ' has-error' : ''}}">
    {!! Form::label('male_average', trans('edeath.male_average'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('male_average', null, ['class' => 'form-control']) !!}
        {!! $errors->first('male_average', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('female_average') ? ' has-error' : ''}}">
    {!! Form::label('female_average', trans('edeath.female_average'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('female_average', null, ['class' => 'form-control']) !!}
        {!! $errors->first('female_average', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('male_years') ? ' has-error' : ''}}">
    {!! Form::label('male_years', trans('edeath.male_years'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('male_years', null, ['class' => 'form-control']) !!}
        {!! $errors->first('male_years', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('male_days') ? ' has-error' : ''}}">
    {!! Form::label('male_days', trans('edeath.male_days'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('male_days', null, ['class' => 'form-control']) !!}
        {!! $errors->first('male_days', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('female_years') ? ' has-error' : ''}}">
    {!! Form::label('female_years', trans('edeath.female_years'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('female_years', null, ['class' => 'form-control']) !!}
        {!! $errors->first('female_years', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('female_days') ? ' has-error' : ''}}">
    {!! Form::label('female_days', trans('edeath.female_days'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('female_days', null, ['class' => 'form-control']) !!}
        {!! $errors->first('female_days', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('edeath.create'), ['class' => 'btn btn-primary']) !!}
    </div>
</div>


