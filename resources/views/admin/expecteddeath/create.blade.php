@extends('layouts.backend')


@section('title',trans('edeath.add_edeath'))



@section('pageTitle')
    <i class="icon-tint"></i>

    <span>@lang('edeath.add_new_edeath')</span>


    @endsection


@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('edeath.add_new_edeath')</div>
                    <div class="panel-body">
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('edeath.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/expecteddeath', 'class' => 'form-horizontal','id'=>'formExpecteddeath','enctype'=>'multipart/form-data']) !!}

                        @include ('admin.expecteddeath.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection

