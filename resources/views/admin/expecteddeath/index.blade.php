@extends('layouts.backend')

@section('title',trans('edeath.edeaths'))
@section('pageTitle',trans('edeath.edeaths'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('edeath.edeaths')
                                                  </div>

                               </div>
                <div class="box-content ">


                    <div class="row">
                    
                    <div class="table-responsive">
                        <table class="table table-borderless" id="edeath-table">
                            <thead>
                            <tr>
                                <th data-priority="1">@lang('edeath.id')</th>                         
                                <th data-priority="3">@lang('edeath.name')</th>
                                <th data-priority="4">@lang('edeath.male_average')</th>  
                                <th data-priority="4">@lang('edeath.female_average')</th>                            
                                <th data-priority="8">@lang('edeath.actions')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/expecteddeath/') }}";

        datatable = $('#edeath-table').DataTable({
             "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
             ajax: {
                    url: '{!! route('ExpecteddeathControllerDeathsData') !!}',//"{{ url('admin/tickets/datatable') }}", // json datasource
                    type: "get", // method , by default get
                    data: function (d) {                     
                    }
                },
                columns: [
                    { data: 'id', name: 'Id',"searchable": false },
                    { 
                        "data" : null, 
                        "searchable" : false,
                        "orderable":false,
                        render : function(o){
                          return o.name;  
                        }
                    },
                    { 
                        "data" : null, 
                        "searchable" : false,
                        "orderable":false,
                        render : function(o){
                          return o.male_average +' ' + '%';  
                        }
                    },
					{ 
                        "data" : null, 
                        "searchable" : false,
                        "orderable":false,
                        render : function(o){
                          return o.female_average +' ' + '%';  
                        }
                    },    
					{ 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var d=""; var e="";
                               e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>edit</button></a>&nbsp;";
                               d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";
                
                            return e+d;
                        }
                    }
                    
                ]
        });
    $('#filter_role').change(function() {
        datatable.draw();
    });
    $('#filter_status').change(function() {
        datatable.draw();
    });
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
		
		var url ="{{ url('/admin/expecteddeath/') }}";
        url = url + "/" + id;
		//alert(url);
        
        var r = confirm("Are you sure you want to delete Country ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });

</script>
@endpush
