@extends('layouts.admin')


@section('title','My Profile')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Profile</div>
            <div class="panel-body">
                <div class="box-content ">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ url('/admin/profile/edit') }}" class="btn btn-success"
                                title="Edit Profile">
                                    <i class="fa fa-edit" aria-hidden="true"></i> Edit Profile
                            </a>
                            <a href="{{ url('/admin/profile/changePassword') }}" class="btn btn-info btnTop"
                                    title="Change Password">
                                <i class="fa fa-lock" aria-hidden="true"></i> Change Password
                            </a>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tr>
                                <td class="col-md-2">First Name :</td>
                                <td>{{@$user->fname}}</td>
                            </tr>
                            <tr>
                                <td>Last Name :</td>
                                <td>{{@$user->lname}}</td>
                            </tr>
                            <tr>
                                <td class="col-md-2">Name :</td>
                                <td>{{@$user->name}}</td>
                            </tr>
                            <tr>
                                <td>Email :</td>
                                <td>{{@$user->email}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection