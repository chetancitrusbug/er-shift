<div class="form-group {{ $errors->has('profession_id') ? 'has-error' : ''}}">
        {!! Form::label('profession_id', 'Profession', ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-6">
                {!! Form::select('profession_id',$profession,null, ['class' => 'form-control']) !!} {!! $errors->first('profession_id','<p class="help-block with-errors">:message</p>') !!}
        </div>
    </div>
    
<div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
    {!! Form::label('title', 'Title: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('status',['' =>'Select Status', '1'=>'Active','0'=>'Inactive'] ,null, ['class' => 'form-control']) !!} {!! $errors->first('status','<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
