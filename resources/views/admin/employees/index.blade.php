@extends('layouts.admin')

@section('title','Employers')

@section('content')
     <div class="row">
        <div class="col-md-12">
			<div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        Employers
                    </div>
				</div>
					<div class="box-content ">

						<a href="{{ url('/admin/employer/create') }}" class="btn btn-success btn-sm" title="Add New Employee">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New Employer
                        </a>
					</div>

					 <div class="box-content ">
                        <div class="table-responsive">
                            <table class="table table-borderless" id="emp-tabl">
                                <thead>
                                    <tr>
                                        <th> Name</th>
                                        <th> Email</th>
                                         <th>Login Type</th>
                                        <th>Profile Name</th>
                                        <th>Company</th>
                                        <th>Telephone</th>
                                       
                                        <th>Date Joined</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>

                        </div>
					 </div>

			</div>
        </div>
    </div>
@endsection
@push('script-head')
<script>

$(function() {
        var url ="{{ url('/admin/employer/') }}";
        var datatable = $('#emp-tabl').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{!! route("employeesData") !!}',
                type: "get", // method , by default get
            },
            columns: [
                { data: 'name',name:'name',"searchable" : true},
                { data: 'email',name:'email',"searchable" : true},
                 { data: 'login_type',name:'login_type',"searchable" : true, 'orderable' : true },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                       return (o.employee_detail && o.employee_detail.emp_name != 'undefined' ? o.employee_detail.emp_name : '');
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        return (o.employee_detail && o.employee_detail.comp_name != 'undefined' ? o.employee_detail.comp_name : '');
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        return (o.employee_detail && o.employee_detail.tel_number != 'undefined' ? o.employee_detail.tel_number : '');
                    }
                },
                 {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        return (o.employee_detail && o.employee_detail.created_at != 'undefined' ? moment(o.employee_detail.created_at).format('MM-DD-YYYY') : '');
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        var status = '';
                            if(o.is_active == 0)
                                status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>@lang("user.inactive")</button></a>';
                            else
                                status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-warning btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('user.active')</button></a>";

                            return status;
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        var e="";var d=""; var v="";
                        j= "<a href='"+url+"/jobs/"+o.id+"' data-id="+o.id+"><button class='btn btn-primary btn-xs' ><i class='fa fa-list' aria-hidden='true'></i> Jobs</button></a>&nbsp;";
                        e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></a>&nbsp;";
                        var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i>  </button></a>&nbsp;";
                        d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";
                        return j+v+e+d;
                    }
                }
            ]
    });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');

            var url ="{{ url('/admin/employer/') }}";
            url = url + "/" + id;
            var r = confirm("Are you sure you want to delete Employee ?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success('Action Success!', data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procede!',erro)
                    }
                });
            }
        });
    });
</script>
@endpush