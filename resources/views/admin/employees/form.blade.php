<ul class="nav nav-tabs">
    <li class="active"><a href="#tab1" data-toggle="tab">Basic Detail</a></li>
    <li class=""><a href="#tab2" data-toggle="tab">Professional Detail</a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="tab1">
        {{-- users table form --}}
        <div class="form-group{{ $errors->has('fname') ? ' has-error' : ''}}">
            {!! Form::label('fname', 'First Name: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('fname', (isset($user->fname) && $user->fname != '' ) ? $user->fname : '', ['class' => 'form-control']) !!}
                {!! $errors->first('fname', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group{{ $errors->has('lname') ? ' has-error' : ''}}">
                {!! Form::label('lname', 'Last Name: ', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('lname', (isset($user->lname) && $user->lname != '' ) ? $user->lname : '', ['class' => 'form-control']) !!}
                    {!! $errors->first('lname', '<p class="help-block">:message</p>') !!}
                </div>
        </div>
        @if(isset($user->email))
        <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
                {!! Form::label('email', 'Email: ', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('email', $user->email, ['class' => 'form-control','readonly']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
        </div>

        @else
        <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
                {!! Form::label('email', 'Email: ', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
                {!! Form::label('password', 'Password: ', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::password('password', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                </div>
        </div>
        @endif
        <div class="form-group {{ $errors->has('profile_pic') ? 'has-error' : ''}}">
            {!! Form::label('profile_pic', 'Profile Image:', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-3">
                {!! Form::file('profile_pic', null, ['class' => 'form-control']) !!}
                {!! $errors->first('profile_pic', '<p class="help-block with-errors">:message</p>') !!}
            </div>
            @if(isset($employee) && $employee->profile_pic)
                <div class="form-group ">
                    <img src={{ $employee->profile_pic }} alt="image" width="50">
                </div>
            @endif
        </div>
        {{-- End user form --}}
        <div class="form-group ">
            <div class="pull-right rightspace">
                <a class="btn btn-primary btnNext tab1Sec" >Next</a>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="tab2">

        <div class="form-group{{ $errors->has('emp_name') ? ' has-error' : ''}}">
                {!! Form::label('emp_name', 'Employer  Name: ', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('emp_name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('emp_name', '<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group{{ $errors->has('comp_name') ? ' has-error' : ''}}">
            {!! Form::label('comp_name', 'Company  Name: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('comp_name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('comp_name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group{{ $errors->has('comp_website') ? ' has-error' : ''}}">
            {!! Form::label('comp_website', 'Company Website: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('comp_website', null, ['class' => 'form-control']) !!}
                {!! $errors->first('comp_website', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group{{ $errors->has('comp_information') ? ' has-error' : ''}}">
                {!! Form::label('comp_information', 'Company Information:', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::textarea('comp_information', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('comp_information', '<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group{{ $errors->has('tel_number') ? ' has-error' : ''}}">
                {!! Form::label('tel_number','Telephone Number:', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('tel_number', null, ['class' => 'form-control','id' => 'tel_number']) !!}
                    {!! $errors->first('tel_number', '<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group{{ $errors->has('address') ? ' has-error' : ''}}">
            {!! Form::label('address', 'Address:', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group{{ $errors->has('locations') ? ' has-error' : ''}}">
            {!! Form::label('locations', 'Location:', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::select('locations[]', $location, isset($employee) && isset($employee->locations) && $employee->locations ? $employee->locations()->pluck('location_id') : [], ['id' => 'location','class' => 'form-control  selectTag', 'multiple'=>'multiple' ]) !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('is_active') ? 'has-error' : ''}}">
            {!! Form::label('is_active', 'Status', ['class' => 'col-sm-4 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::select('is_active',['1'=>'Active','0'=>'Inactive'] ,@$user->is_active, ['class' => 'form-control']) !!} {!! $errors->first('status','<p class="help-block with-errors">:message</p>') !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-4 col-md-4">
                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
        <div class="form-group ">
            <div class="pull-left ">
                <a class="btn btn-primary btnPrevious" >Previous</a>
            </div>
        </div>
    </div>
</div>
     {{-- End Doctor Detail Form --}}

@push('js')
<script>
$(document).ready(function() {

    $('.btnNext').click(function(){
        $('.nav-tabs > .active').next('li').find('a').trigger('click');
    });

    $('.btnPrevious').click(function(){
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    });
});
</script>
@endpush