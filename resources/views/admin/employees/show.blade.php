@extends('layouts.admin')

@section('title','Employer')

@section('content')
     <div class="row">
        <div class="col-md-12">
			<div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        Employer
                    </div>
				</div>
					 <div class="box-content ">
						<a href="{{ url('/admin/employer') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/employer/' . $user->id . '/edit') }}" title="Edit Employee"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Profile Image</th>
                                        <td>
                                            @if($employee->profile_pic != '')
                                            <img src={{$employee->profile_pic}} width="80px"/></td>
                                            @endif
                                    </tr>
                                    <tr>
                                        <th>Name</th>
                                        <td> {{ $user->name }} </td>
                                    </tr>
                                    <tr>
                                        <th>First Name</th>
                                        <td> {{ $user->fname }} </td>
                                    </tr>
                                    <tr>
                                        <th>Last Name</th>
                                        <td> {{ $user->lname }} </td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td> {{ $user->email }} </td>
                                    </tr>
                                    <tr>
                                        <th>Profile Name</th>
                                        <td> {{ $employee->emp_name }} </td>
                                    </tr>
                                    <tr>
                                        <th>Company Name</th>
                                        <td> {{ $employee->comp_name }} </td>
                                    </tr>
                                    <tr>
                                        <th>Company Website</th>
                                        <td> {{ $employee->comp_website }} </td>
                                    </tr>
                                    <tr>
                                        <th>Company Information</th>
                                        <td> {{ $employee->comp_information }} </td>
                                    </tr>
                                    <tr>
                                        <th>Telephone Number</th>
                                        <td> {{ $employee->tel_number }} </td>
                                    </tr>
                                    <tr>
                                        <th>Address</th>
                                        <td> {{ $employee->address }} </td>
                                    </tr>

                                    <tr>
                                        <th>Location</th>
                                         <td> {{ $employee->comma_sep_loc_names }} </td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td>
                                            {{ (($user->is_active == 1) ?  'Active' : 'Inactive') }}
                                        </td>
                                    </tr>
                                </tbody>

                            </table>
                        </div>
					 </div>

			</div>
        </div>
    </div>
@endsection

