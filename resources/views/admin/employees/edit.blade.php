@extends('layouts.admin')

@section('title','Edit Employer')

@section('content')
     <div class="row">
        <div class="col-md-12">
			<div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        Edit Employer
                    </div>
				 </div>
					 <div class="box-content ">
						<a href="{{ url('/admin/employer') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        {!! Form::model($employee, [
                            'method' => 'PATCH',
                            'url' => ['/admin/employer', $employee->uid],
                            'class' => 'form-horizontal',
                            'enctype'=>'multipart/form-data'
                        ]) !!}

                        @include ('admin.employees.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}
					 </div>

			</div>
        </div>
    </div>
@endsection
