@extends('layouts.admin')
@section('title',trans('jobs.label.jobs'))
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    @lang('jobs.label.jobs')
                </div>
            </div>
            <div class="box-content box-1">
                <div class="table-responsive" >
                    <table class="table table-borderless" id="job-table">
                        <thead>
                            <tr>
                                <th>@lang('jobs.label.job_title')</th>
                                <th>@lang('jobs.label.job_responsibility')</th>
                                <th>@lang('jobs.label.profession')</th>
                                <th>@lang('jobs.label.speciality')</th>
                                <th>@lang('jobs.label.degree')</th>
                                <th>@lang('jobs.label.action')</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
    $(function() {
        var site_url = "{{ url('/admin/employer/job') }}";
        var url ="{{ url('admin/jobsData/').'/'.$employee->uid }}";
        var datatable = $('#job-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                    url: url,
                    type: "get", // method , by default get
                },
                columns: [
                    { data: 'job_title',name:'job_title',"searchable" : true},
                    { data: 'job_responsibility',name:'job_responsibility',"searchable" : true},
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var profession = '';
                            if(o.profession != null)
                            profession = o.profession.title;
                            else
                            profession = "";
                            return profession;
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var speciality = '';
                            if(o.speciality != null)
                            speciality = o.speciality.title;
                            else
                            speciality = "";
                            return speciality;
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var degree = '';
                            if(o.degree != null)
                            degree = o.degree.title;
                            else
                            degree = "";
                            return degree;
                        }
                    },
                    {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        var v =  "<a href='"+site_url+"/"+"{{$employee->uid}}"+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i>  </button></a>&nbsp;";
                        return v;
                    }
                }
                ]
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');

            var url ="{{ url('/jobs/') }}";
            url = url + "/" + id;
            var r = confirm("@lang('jobs.label.sure_job_delete')");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(trans('jobs.label.action_success'), data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error(trans('jobs.label.action_not_success'),erro)
                    }
                });
            }
        });

        $(document).on('click', '.favou-item', function (e) {
            var id = $(this).attr('data-id');

            var url ="{{ url('/favourite/') }}";
            url = url + "/" + id;
                $.ajax({
                    type: "get",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        window.location=url;

                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error(trans('jobs.label.action_not_success'),erro)
                    }
                });
         });
    });
</script>
@endpush
