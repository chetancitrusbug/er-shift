@extends('layouts.admin')
@section('title','Job')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    Job
                </div>
            </div>

           <div class="box-content ">
            <a href="{{ url('/admin/employer/jobs/').'/'.$emp_id }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        <tr>
                            <th>@lang('jobs.label.job_title')</th>
                            <td> {{ $job->job_title }} </td>
                        </tr>
                        <tr>
                            <th>@lang('jobs.label.job_responsibility')</th>
                            <td> {{ $job->job_responsibility }} </td>
                        </tr>
                        <tr>
                            <th>@lang('jobs.label.profession')</th>
                            <td> {{ (isset($job->profession->title) ? $job->profession->title : '') }} </td>
                        </tr>
                        <tr>
                            <th>@lang('jobs.label.speciality')</th>
                            <td> {{ (isset($job->speciality->title) ? $job->speciality->title : '') }} </td>
                        </tr>
                        <tr>
                            <th>@lang('jobs.label.degree')</th>
                            <td> {{ (isset($job->degree->title) ? $job->degree->title : '')}} </td>
                        </tr>
                        <tr>
                            <th>@lang('jobs.label.experience')</th>
                            <td> {{ (isset($job->experience->title) ? $job->experience->title : '') }} </td>
                        </tr>
                        <tr>
                            <th>@lang('jobs.label.address')</th>
                            <td> {{ $job->address }} </td>
                        </tr>
                        <tr>
                            <th>@lang('jobs.label.location')</th>
                            <td> {{ (isset($job->location->name) ? $job->location->name : '') }} </td>
                        </tr>
                        <tr>
                            <th>@lang('jobs.label.ed_volume')</th>
                            <td> {{ (isset($job->edvolume->title) ? $job->edvolume->title : '') }} </td>
                        </tr>
                        <tr>
                            <th>@lang('jobs.label.dates')</th>
                            <td> {{ $job->dates }} </td>
                        </tr>
                        <tr>
                            <th>@lang('jobs.label.shift')</th>
                            <td> {{ (isset($job->shift->title) ? $job->shift->title : '')  }} </td>
                        </tr>


                        <tr>
                            <th>@lang('jobs.label.rate')</th>
                            <td> {{ $job->rate }} </td>
                        </tr>


                    </tbody>

                </table>
            </div>
        </div>

        </div>
    </div>
</div>
@endsection
