
@extends('layouts.admin')

@section('title','Users')

@section('content')
     <div class="row">
        <div class="col-md-12">
			<div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        Users
                    </div>
			    </div>	
				<div class="box-content ">
				    <a href="{{ url('/admin/users/create') }}" class="btn btn-success btn-sm" title="Add New User">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New User
                    </a>
                    <div class="table-responsive">
                        <table class="table table-borderless datatable responsive" id="users-dataTables">
                            <thead>
                                <tr>
                                    <th>ID</th><th>Name</th><th>Email</th><th>Status</th><th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
				</div>
            </div>
        </div>
    </div>
@endsection	
@push('script-head')
<script>
var url ="{{ url('/admin/users/') }}";
// var doctorwish ="{{ url('/admin/doctorwish/') }}";
var admin ="{{ url('/admin/loginasuser/') }}";
var img_path = "{{asset('images')}}";

        //Permissions
       
        
        datatable = $('#users-dataTables').DataTable({
            processing: true,
            serverSide: true,
             ajax: {
                    url: '{!! route('UsersControllerUsersData') !!}',//"{{ url('admin/tickets/datatable') }}", // json datasource
                    type: "get", // method , by default get
                    data: function (d) {
                       

                    }
                },
                columns: [
                    { data: 'id', name: 'Id',"searchable": false },
                    { data: 'name',name:'users.name',"searchable" : false},
                    { data: 'email',name:'users.email',"searchable" : false},
                    
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "deafultContent" : '-',
                        "render": function (o) {
                            var status = '';
                            if(o.is_active == 0)
                            
                                status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>@lang("user.inactive")</button></a>';
                            else
                                status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-warning btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('user.active')</button></a>";
                            
                            return status;
                        }

                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d=""; var logs="";
                            e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></a>&nbsp;";
                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i>  </button></a>&nbsp;";
                            // var y -  "<a href='"+doctorwish+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i>Doctor Wish</button></a>&nbsp;";
                            return v+e;
                        }
                    }
                ]
        });
    $('#filter_role').change(function() {
        datatable.draw();
    });
    $('#filter_status').change(function() {
        datatable.draw();
    });
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Customer ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });
</script>
@endpush


