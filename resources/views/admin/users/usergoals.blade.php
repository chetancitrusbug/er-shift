@extends('layouts.backend')

@section('title',trans('goal.usergoals'))
@section('pageTitle',trans('goal.usergoals'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('goal.usergoals')
                                                  </div>

                               </div>
                <div class="box-content ">


                    <div class="row">
                        


                    <div class="table-responsive">
                        <table class="table table-borderless" id="goals-table">
                            <thead>
                            <tr>
                                <th data-priority="1">@lang('goal.id')</th>                         
                                <th data-priority="3">@lang('goal.title')</th>
                                <th data-priority="4">@lang('goal.details')</th>                          
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/goals/') }}";
var img_path = "{{asset('images')}}";

        
        datatable = $('#goals-table').DataTable({
             "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
           
             ajax: {
                    url: '{!! route('GoalsControllerUsergoalsData') !!}',//"{{ url('admin/tickets/datatable') }}", // json datasource
                    type: "get", // method , by default get
                    data: function (d) {
                        d.user_id = "{{$user_id}}";
                    }
                },
                columns: [
                    { data: 'id', name: 'Id',"searchable": false},
                    { 
                        "data" : null, 
                        "searchable" : false,
                        "orderable":false,
                        render : function(o){
                          return o.title;  
                        }
                    },
                    { data: 'details',name:'goals.details',"searchable" : false,"orderable":false}
                    
                ]
        });
   
</script>
@endpush
