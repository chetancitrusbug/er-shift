@extends('layouts.admin')
@section('title',"Setting")
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                        Settings
                </div>
            </div>
    
    <div class="table-responsive" >
        <table class="table table-borderless" style="width:100%;" id="setting-table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Value</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>
</div>
@endsection
@push('js')
    <script>
        var url ="{{ url('/admin/setting-data') }}";
        var edit_url = "{{ url('/admin/setting') }}";

        datatable = $('#setting-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            ajax: {
                url:url,
                type:"get",
            },
            columns: [
                { data: 'name',name : 'name',"searchable": true, "orderable": true},
                { data: 'value',name : 'value',"searchable": true, "orderable": true},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e = v = d = m = "";
                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-sm' title='Edit' ><i class='fa fa-edit' ></i></button></a>&nbsp;";
                        d = "<a href='{{url('admin/setting')}}"+"/"+o.id+"' class='del-item btn btn-danger btn-sm' data-method='DELETE' data-modal-text='delete setting' data-id="+o.id+" ><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;";
                        return m+v+e+d;
                    }

                }
            ]
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            url = "{{url('admin/setting')}}/" + id;
            var r = confirm("Are you sure you want to Delete Setting?");

            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
        });

        $(document).on('change', '.status-change', function (e) {
            $("#loading").show();
            var id = $(this).data('id');
            var status = $(this).data('status');
            var table = $(this).data('table');

            url = "{{url('admin/change-status')}}/" + table + '/' + status + '/' + id;

            $.ajax({
                url: url ,
                success: function (data) {
                    datatable.draw();
                    toastr.success(data.message)
                    $("#loading").hide();
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procced!',erro)
                }
            });
        });
    </Script>
@endpush