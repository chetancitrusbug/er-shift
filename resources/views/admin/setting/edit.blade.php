@extends('layouts.admin')
@section('title',"Edit Setting")
@push('css')
    <style type="text/css">
        .logo{
            position: relative;
        }.logo-image{
            position: absolute;
            left: 490px;
            top: 250px;
        }img{
            height: 50px;
            width: 50px;
        }
    </style>
@endpush
@section('content')
<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <h4 class="card-title">Edit Setting  # {{$setting->name}}</h4>
            <div class="panel-body">
                <a href="{{ url('/admin/setting') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <br />
                <br />
                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::model($setting, ['method' => 'PATCH','url' => ['/admin/setting', $setting->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

                @include ('admin.setting.form')


            {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
               
@endsection