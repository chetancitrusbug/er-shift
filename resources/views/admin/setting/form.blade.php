<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', '* Name: ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('value') ? ' has-error' : ''}}">
    {!! Form::label('value', '* value: ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('value', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('value',  '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
<div class="col-md-offset-4 col-md-4">
    {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
</div>
</div>
