@extends('layouts.admin')

@section('title','Ed Staff')

@section('content')
     <div class="row">
        <div class="col-md-12">
			<div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                            Ed Staff
                    </div>
				</div>
					<div class="box-content ">

						<a href="{{ url('/admin/doctors/create') }}" class="btn btn-success btn-sm" title="Add New Ed Staff">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New Ed Staff
                        </a>
					</div>
					
					 <div class="box-content ">
					    <div class="table-responsive">
                             <table class="table table-borderless datatable responsive" id="doctor-dataTables">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Login Type</th>
                                         <th>Date Joined</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                
                            </table>
                            
                        </div>
					 </div>
               
			</div>
        </div>
    </div>
@endsection	
@push('script-head')
<script>
var url ="{{ url('/admin/doctors/') }}";
var admin ="{{ url('/admin/loginasuser/') }}";
var img_path = "{{asset('images')}}";

        //Permissions
       
        
        datatable = $('#doctor-dataTables').DataTable({
            processing: true,
            serverSide: true,
             ajax: {
                    url: '{!! route('DoctorControllerUsersData') !!}',//"{{ url('admin/tickets/datatable') }}", // json datasource
                    type: "get", // method , by default get
                    data: function (d) {
                       

                    }
                },
                columns: [
                    { data: 'name',name:'users.name',"searchable" : false},
                    { data: 'email',name:'users.email',"searchable" : false},
                     { data: 'login_type',name:'users.login_type',"searchable" : true, 'orderable' : true},
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            return (o.created_at != 'undefined' ? moment(o.created_at).format('MM-DD-YYYY') : '');
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "deafultContent" : '-',
                        "render": function (o) {
                            var status = '';
                            if(o.is_active == 0)
                            
                                status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>@lang("user.inactive")</button></a>';
                            else
                                status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-warning btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('user.active')</button></a>";
                            
                            return status;
                        }

                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d=""; var logs="";
                            e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></a>&nbsp;";
                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i>  </button></a>&nbsp;";
                            d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";                    
                            return v+e+d;
                        }
                    }
                ]
        });
    $('#filter_role').change(function() {
        datatable.draw();
    });
    $('#filter_status').change(function() {
        datatable.draw();
    });
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Ed-Staff ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });
</script>
@endpush