<ul class="nav nav-tabs">
    <li class="active"><a href="#tab1" data-toggle="tab">Basic Detail</a></li>
    <li class=""><a href="#tab2" data-toggle="tab">Professional Detail</a></li>
    <li class=""><a href="#tab3" data-toggle="tab">Job Detail</a></li>
</ul>
    
<div class="tab-content">
    <div class="tab-pane active" id="tab1">
        {{-- users table form --}}
        <div class="form-group{{ $errors->has('fname') ? ' has-error' : ''}}">
            {!! Form::label('fname', 'First Name: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('fname', (isset($user->fname) && $user->fname != '' ) ? $user->fname : '', ['class' => 'form-control']) !!}
                <p class="help-block err err1area fnameErr"></p>
                {!! $errors->first('fname', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group{{ $errors->has('lname') ? ' has-error' : ''}}">
            {!! Form::label('lname', 'Last Name: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('lname', (isset($user->lname) && $user->lname != '' ) ? $user->lname : '', ['class' => 'form-control']) !!}
                <p class="help-block err err1area lnameErr"></p>
                {!! $errors->first('lname', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        @if(isset($user->email))
        <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
            {!! Form::label('email', 'Email: ', ['class' => 'col-md-4 control-label','id' => '']) !!}
            <div class="col-md-6">
                {!! Form::text('email', $user->email, ['class' => 'form-control']) !!}
                <p class="help-block err err1area emailErr"></p>
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
       
        @else
        <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
            {!! Form::label('email', 'Email: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('email', null, ['class' => 'form-control']) !!}
                <p class="help-block err err1area emailErr"></p>
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
                {!! Form::label('password', 'Password: ', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::password('password', null, ['class' => 'form-control']) !!}
                    <p class="help-block err err1area passwordErr"></p>
                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                </div>
        </div>
    
        @endif     
        {{-- End user form --}}
        {{-- Start Doctor Detail Form --}}
        <div class="form-group {{ $errors->has('profile_image') ? 'has-error' : ''}}">
            
                {!! Form::label('profile_image', 'Profile Image:', ['class' => 'col-md-4 control-label']) !!}
                   <div class="col-md-3">
                        {!! Form::file('profile_image', null, ['class' => 'form-control']) !!}
                        <p class="help-block err err1area profile_imageErr"></p>
                        {!! $errors->first('profile_image', '<p class="help-block with-errors">:message</p>') !!}
                    </div>
                    @if(isset($doctor) && $doctor->profile_image)
                        <div class="form-group ">
                            <img src={{ $doctor->profile_image }} alt="image" width="50">
                        </div>
                    @endif
        
        </div>
        <div class="form-group ">
            <div class="pull-right rightspace">
                <a class="btn btn-primary btnNext tab1Sec" >Next</a>
            </div>
        </div>
        {{-- @if()
        <div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
                <div class="col-md-6">
                    <input type="hidden" name="roles[]" value="{{$userdefaulrole}}"/>
                </div>
        </div> --}}
        
    </div>
    <div class="tab-pane" id="tab2">
        <div class="form-group{{ $errors->has('profile_name') ? ' has-error' : ''}}">
                {!! Form::label('profile_name', 'Profile Name: ', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('profile_name', null, ['class' => 'form-control']) !!}
                    <p class="help-block err err2area profile_nameErr"></p>
                    {!! $errors->first('profile_name', '<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group{{ $errors->has('address') ? ' has-error' : ''}}">
                {!! Form::label('address', 'Address:', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group{{ $errors->has('tel_number') ? ' has-error' : ''}}">
                {!! Form::label('tel_number','Telephone Number:', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('tel_number', null, ['class' => 'form-control','id' => 'tel_number']) !!}
                    {!! $errors->first('tel_number', '<p class="help-block">:message</p>') !!}
                </div>
        </div>
    
        <div class="form-group{{ $errors->has('resume_type') ? ' has-error' : ''}}">
                {!! Form::label('resume_type', 'Resume Type: ', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::select('resume_type', array('' => 'Select','link' => 'Link','attachment'=>'Attachment'), null , ['class' => 'form-control']) !!}
                    {{-- {!! Form::text('resume_type', null, ['class' => 'form-control']) !!} --}}
                    {!! $errors->first('resume_type', '<p class="help-block">:message</p>') !!}
                </div>
        </div>
        <div class="form-group {{ $errors->has('resume') ? 'has-error' : ''}}" id="link">
            {!! Form::label('resume', 'Resume:', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-3">
                    {!! Form::text('resume',null, ['class' => 'form-control resume','id' => '']) !!}
                    {!! $errors->first('resume', '<p class="help-block with-errors">:message</p>') !!}
                </div>
                @if(isset($doctor) && $doctor->resume != '')
                    <div class="form-group clickhere">
                        <a href={{ '//'.$doctor->resume }} target="_blank"><button type="button" class='btn btn-success btn-xs'>Click Here</button> </a>
                        
                    </div>
                @endif
        </div>
        <div class="form-group {{ $errors->has('resume') ? 'has-error' : ''}}" id="attachment">
            
            {!! Form::label('resume', 'Resume:', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-3">
                    {!! Form::file('resume', null, ['class' => 'form-control resume attachmentResume','id' => '']) !!}
                    <p class="help-block err err2area resumeErr"></p>
                    {!! $errors->first('resume', '<p class="help-block with-errors">:message</p>') !!}
                </div>
                @if(isset($doctor) && $doctor->resume != '')
                    <div class="form-group clickhere">
                        <a href={{ $doctor->resume }} target="_blank"><button type="button" class='btn btn-success btn-xs'>Click Here</button> </a>
                        
                    </div>
                @endif
    
        </div>
    
    
        <div class="form-group{{ $errors->has('professions') ? ' has-error' : ''}}">
            {!! Form::label('profession', 'Profession: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::select('professions', $professions, isset($doctor->profession_id) ? $doctor->profession_id : null, ['id' => 'profession','class' => 'form-control', ]) !!}
                <p class="help-block err err2area professionErr"></p>
            </div>
        </div>
        <div class="form-group ">
            <div class="pull-left ">
                <a class="btn btn-primary btnPrevious" >Previous</a>
            </div>
            <div class="pull-right rightspace">
                <a class="btn btn-primary btnNext tab2Sec" >Next</a>
            </div>
        </div>
        
    </div>
    <div class="tab-pane" id="tab3">
        <div class="form-group{{ $errors->has('specialities') ? ' has-error' : ''}}">
            {!! Form::label('speciality', 'Speciality: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::select('specialities', $specialities, isset($doctor->speciality_id) ? $doctor->speciality_id : null, ['class' => 'form-control','id' =>'specialities']) !!}
                <p class="help-block err err3area specialityErr"></p>
            </div>
        </div>
        <div class="form-group{{ $errors->has('experiences') ? ' has-error' : ''}}">
            {!! Form::label('experience', 'Experience: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::select('experiences', $experiences, isset($doctor->experience_id) ? $doctor->experience_id : null, ['class' => 'form-control','id' =>'experience']) !!}
                <p class="help-block err err3area experienceErr"></p>
            </div>
        </div>
        <div class="form-group{{ $errors->has('degrees') ? ' has-error' : ''}}">
            {!! Form::label('degree', 'Degree: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::select('degrees[]', $degrees, isset($doctor->degree_id) ? $doctor->degree_id : [], ['id' => 'degree','multiple'=>'multiple','class' => 'form-control  selectTag']) !!}
                <p class="help-block err err3area degreeErr"></p>
            </div>
        </div>
        <div class="form-group{{ $errors->has('licenses') ? ' has-error' : ''}}">
            {!! Form::label('license[]', 'License: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::select('licenses[]', $licenses, isset($doctor->license_id) ? $doctor->license_id : [], ['id' => 'license','class' => 'form-control  selectTag','multiple'=>'multiple']) !!}
                <p class="help-block err err3area licenseErr"></p>
            </div>
        </div>
        <div class="form-group{{ $errors->has('locations') ? ' has-error' : ''}}">
            {!! Form::label('location', 'Location:', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::select('locations[]', $locations, isset($doctor->location_id) ? $doctor->locations()->pluck('location_id') : [], ['id' => 'location','class' => 'form-control  selectTag', 'multiple'=>'multiple' ]) !!}
            
            
            </div>
        </div>
        <div class="form-group {{ $errors->has('is_active') ? 'has-error' : ''}}">
            {!! Form::label('is_active', 'Status', ['class' => 'col-sm-4 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::select('is_active',['1'=>'Active','0'=>'Inactive'] ,@$user->is_active, ['class' => 'form-control']) !!} {!! $errors->first('status','<p class="help-block with-errors">:message</p>') !!}
            </div>
        </div>
        <div class="form-group ">
            <div class="pull-right ">
                {!! Form::button(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary tab3Sec']) !!}
            </div>
            <div class="pull-left rightspace">
                <a class="btn btn-primary btnPrevious" >Previous</a>
            </div>
        </div>
        
    </div>
</div>
@push('js')
<script>
$(document).ready(function() {
    $('#license').select2();
    $('#location').select2();
    $('#degree').select2();
    $('#link').hide();
	$('#attachment').hide();
    $("#resume_type").change(function(){
        //console.log($(this).val());
		if($(this).val() == "attachment")
		{
			$('#attachment').show();
			$('#link').hide();
		}
		else if($(this).val() == "link"){
			$('#link').show();
			$('#attachment').hide();
		}
		else{
		$('#link').hide();
		$('#attachment').hide();
		}
        $('.resume').val('');
	});

    $("#resume_type").trigger("change");

    $(".tab1Sec").click(function(){
        if($('#fname').val() == "")
		{
            $('.fnameErr').text('First name is required.');
        }
        else
        {
            $('.fnameErr').text('');
        }
        if($('#lname').val() == "")
		{
            $('.lnameErr').text('Last name is required.');
        }
        else
        {
            $('.lnameErr').text('');
        }

        if($('#email').val() == "")
		{
            $('.emailErr').text('Email is required.');
        }
        else
        {
            var email = $('#email').val();
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if (filter.test(email)) {
                $('.emailErr').text('');
            }
            else
            {
                $('.emailErr').text("Email format doesn't match.");
            }
        }
        if($('#password').val() == "")
		{
            $('.passwordErr').text('Password is required.');
        }
        else
        {
            $('.passwordErr').text('');
        }

        if($('.err1area').text() == '')
        {
            $('.nav-tabs > .active').next('li').find('a').trigger('click');
        }
        
    });

    $('#profile_image').change(function(){
        var file = this.files[0];
        var fileType = file["type"];
        var ValidImageTypes = ["image/jpg", "image/jpeg", "image/png"];
        if ($.inArray(fileType, ValidImageTypes) < 0) {
            $('.profile_imageErr').text('File type must be jpg,jpeg or png.');
        }
        else{
            $('.profile_imageErr').text('');
        }
    });
    
    $(".tab2Sec").click(function(){
        if($('#profile_name').val() == "")
		{
            $('.profile_nameErr').text('Profile name is required.');
        }
        else
        {
            $('.profile_nameErr').text('');
        }

        if($('#profession').val() == "")
		{
            $('.professionErr').text('Profession is required.');
        }
        else
        {
            $('.professionErr').text('');
        }

        if($('.err2area').text() == '')
        {
            $('.nav-tabs > .active').next('li').find('a').trigger('click');
        }
        
    });

    $(".tab3Sec").click(function(){

        if($('#specialities').val() == "")
		{
            $('.specialityErr').text('Speciality is required.');
        }
        else
        {
            $('.specialityErr').text('');
        }

        if($('#experience').val() == "")
		{
            $('.experienceErr').text('Experience is required.');
        }
        else
        {
            $('.experienceErr').text('');
        }

        var degreeCount = $("#degree :selected").length;

        if(degreeCount == 0)
		{
            $('.degreeErr').text('Degree is required.');
        }
        else
        {
            $('.degreeErr').text('');
        }

        var licenseCount = $("#license :selected").length;
        if(licenseCount == 0)
		{
            $('.licenseErr').text('License is required.');
        }
        else
        {
            if(licenseCount > 5)
            {
                $('.licenseErr').text('License must be lessthen 5 value');
            }
            else
            {
                $('.licenseErr').text('');
            }
        }
        $( ".tab1Sec" ).click();
        $( ".tab2Sec" ).click();

        if($('.err3area').text() == '' && $('.err1area').text() == '' && $('.err2area').text() == '')
        {
            $("form").submit();
        }
    });

	
    $('.btnPrevious').click(function(){
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    });
     
    
    
});
</script>
@endpush
