@extends('layouts.admin')

@section('title','Edit Ed Staff')

@section('content')
     <div class="row">
        <div class="col-md-12">
			<div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        Edit Ed Staff
                    </div>
				 </div>	
					 <div class="box-content ">
						<a href="{{ url('/admin/doctors') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        
                        {!! Form::model($doctor, [
                            'method' => 'PATCH',
                            'url' => ['/admin/doctors', $user->id],
                            'class' => 'form-horizontal',
                            'enctype'=>'multipart/form-data'
                        ]) !!}
                        
                        @include ('admin.doctors.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}
					 </div>
               
			</div>
        </div>
    </div>
@endsection	
 