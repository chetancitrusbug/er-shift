@extends('layouts.admin')

@section('title','Ed Staff')

@section('content')
     <div class="row">
        <div class="col-md-12">
			<div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                            Ed Staff
                    </div>
				</div>
					 <div class="box-content ">
						<a href="{{ url('/admin/doctors') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/doctors/' . $doctor->uid . '/edit') }}" title="Edit Doctor"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Profile Image</th>
                                        <td>
                                            @if($doctor->profile_image != '')
                                            <img src={{$doctor->profile_image}} width="80px"/></td>
                                            @endif
                                    </tr>
                                    <tr>
                                        <th>Name</th>
                                        <td> {{ $doctor->user->name }} </td>
                                    </tr>
                                    <tr>
                                        <th>First Name</th>
                                        <td> {{ $doctor->user->fname }} </td>
                                    </tr>
                                    <tr>
                                        <th>Last Name</th>
                                        <td> {{ $doctor->user->lname }} </td>
                                    </tr>
                                    <tr>
                                        <th>Profile Name</th>
                                        <td> {{ $doctor->profile_name }} </td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td> {{ $doctor->user->email }} </td>
                                    </tr>
                                    <tr>
                                        <th>Address</th>
                                        <td> {{ $doctor->address }} </td>
                                    </tr>
                                    <tr>
                                        <th>TelNo.</th>
                                        <td> {{ $doctor->tel_number }} </td>
                                    </tr>
                                    <tr>
                                        <th>Resume Type</th>
                                        <td> {{ (($doctor->resume_type == '' || $doctor->resume == '') ? trans('dashboard.home_view.not_uploaded') : $doctor->resume_type) }} </td>
                                    </tr>
                                    <tr>
                                        <th>Resume</th>
                                        <td>
                                        @if ($doctor->resume != '' )
                                            @if ($doctor->resume_type == 'attachment' )
                                            <a href={{ $doctor->resume }} target="_blank"><button class='btn btn-success btn-xs'>Click Here</button> </a>
                                            @elseif ($doctor->resume_type == 'link')
                                            <a href={{ '//'.$doctor->resume }} target="_blank"><button class='btn btn-success btn-xs'>Click Here</button> </a>
                                            @else
                                                @lang('dashboard.home_view.not_uploaded')
                                            @endif
                                        @else
                                            @lang('dashboard.home_view.not_uploaded')
                                        @endif
                                     </td>
                                    </tr>
                                    <tr>
                                        <th>Profession</th>
                                        <td> {{ @$doctor->profession->title }} </td>
                                    </tr>
                                    <tr>
                                        <th>Speciality</th>
                                        <td> {{ @$doctor->speciality->title }} </td>
                                    </tr>
                                    <tr>
                                        <th>Experience</th>
                                        <td> {{ @$doctor->experience->title }} </td>
                                    </tr>
                                    <tr>
                                        <th>Degree</th>
                                        <td> {{ @$doctor->degree_id }} </td>
                                    </tr>
                                    <tr>
                                        <th>License</th>
                                        <td> {{ @$doctor->license_id }} </td>
                                    </tr>
                                    <tr>
                                        <th>Location</th>
                                        <!--<td> {{ @$doctor->location->name }} </td>-->
                                         <td> {{ $doctor->comma_sep_loc_names }} </td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td> 
                                            {{ (($doctor->user->is_active == 1) ?  'Active' : 'Inactive') }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
					 </div>
			</div>
        </div>
    </div>
@endsection	

