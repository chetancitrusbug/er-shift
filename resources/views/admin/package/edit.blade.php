@extends('layouts.admin')
@section('title','Edit Package')
@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"> Edit Package </div>
                <div class="panel-body">
                    <a href="{{ url('/admin/package') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($package, [
                        'method' => 'PATCH',
                        'url' => ['/admin/package', $package->id],
                        'class' => 'form-horizontal',
                        'id'=>'formpackage',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.package.form', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
