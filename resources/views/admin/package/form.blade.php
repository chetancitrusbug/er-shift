<div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
    {!! Form::label('title', 'Title: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(!isset($package))
<div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
    {!! Form::label('description', 'Description: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('amount') ? ' has-error' : ''}}">
    {!! Form::label('amount', 'Amount: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('amount', null, ['class' => 'form-control', 'required' => 'required', 'min' => 1]) !!}
        {!! $errors->first('amount', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('day') ? ' has-error' : ''}}">
    {!! Form::label('day', 'Billing Cycle Days: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('day', null, ['class' => 'form-control', 'required' => 'required', 'min' =>  1]) !!}
        {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@else
@endif   

<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('status',['' =>'Select Status', '1'=>'Active','0'=>'Inactive'] ,null, ['class' => 'form-control']) !!} {!! $errors->first('status','<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

<!-- <div class="form-group {{ $errors->has('isFree') ? 'has-error' : ''}}">
    <div class="col-sm-6">
        {{ Form::checkbox('isFree', null) }} Free Package
        {!! $errors->first('isFree','<p class="help-block with-errors">:message</p>') !!}
    </div>
</div> -->

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
