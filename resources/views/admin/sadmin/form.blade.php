
<div class="form-group{{ $errors->has('first_name') ? ' has-error' : ''}}">
    {!! Form::label('first_name', trans('sadmin.first_name'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('last_name') ? ' has-error' : ''}}">
    {!! Form::label('last_name', trans('sadmin.last_name'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(!(Route::currentRouteName() == 'sadmin.edit'))
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email',trans('sadmin.email'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
    {!! Form::label('password',trans('user.password'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password('password', ['class' => 'form-control']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@else
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email',trans('user.email'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::email('email', null, ['class' => 'form-control','readonly']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('user.create'), ['class' => 'btn btn-primary']) !!}
    </div>
</div>

@push('script-head')

<script type="text/javascript">
 
    $("formUser").validate({
        //var cpf = $('#cpf').val();
        //cpf=cpf.replace(".","");
        //cpf = cpf.replace("-","");
        //cpf= cpf.replace(".","");
        //alert(cpf);
        rules: { 
          
            name:{
                required:true
            },
            email:{
                required:true,
                email: true
            },
            password:{
             //    required:true,
             //   pwcheck: true
            },
            
        },
        messages: {
            
            name:{
                required:"Name Is required"
            },
            email:{
                required:"Email is required",
                email: "Enter Valid Email address"
            },
            password:{
               // required:"Password is required",
               // pwcheck: "Password must be contain 1 uppercase, 1 lowercase and 1 number value"
            },
            

        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    $.validator.addMethod("pwcheck", function(value) {
        return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
            && /[a-z]/.test(value) // has a lowercase letter
            && /\d/.test(value) // has a digit
    });
    
   
</script>
@endpush

