@extends('layouts.admin')

@section('title','Email Template')

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                        Email Template
                </div>
            </div>
            <div class="box-content ">

                <div class="table-responsive" >
                    <table class="table table-borderless" id="emailtemplate-table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script-head')
<script>
    $(function() {
        var url ="{{ url('/admin/emailtemplate/') }}";
        var datatable = $('#emailtemplate-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{!! route("emailtemplateData") !!}',
                    type: "get", // method , by default get
                },
                columns: [
                    { data: 'title',name:'title',"searchable" : true},

                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>Edit</button></a>&nbsp;";

                            return e;
                        }
                    }
                ]
        });


    });
</script>
@endpush
