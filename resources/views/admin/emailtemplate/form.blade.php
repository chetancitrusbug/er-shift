<div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
    {!! Form::label('title', 'Title: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-6"></div>
    <div class="col-md-6">
        <h4> {{@$emailtemplate->desc}} <h4>
    </div>
</div>
<br>
<div class="form-group{{ $errors->has('content') ? ' has-error' : ''}}">
    {!! Form::label('content', 'Email Content: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('content', null, ['class' => 'summernote','id' => 'content']) !!}
        {!! $errors->first('content', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>





<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
@push('script-head')
<script>
    $('.summernote').summernote({
    height: 500,                 // set editor height
    minHeight: null,             // set minimum height of editor
    maxHeight: null,             // set maximum height of editor
    focus: true,                  // set focus to editable area after initializing summernote
});

</script>
    @endpush