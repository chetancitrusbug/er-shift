@extends('layouts.admin')
@section('title','Add Email Template')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    Create Email Template
                </div>
                <div class="actions">

                </div>
            </div>
            <div class="box-content panel-body">
                <a href="{{ url('/admin/emailtemplate') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <br />
                <br />
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    {!! Form::open(['url' => '/admin/emailtemplate', 'class' => 'form-horizontal','id'=>'formemailtemplate']) !!}
                        @include ('admin.emailtemplate.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

