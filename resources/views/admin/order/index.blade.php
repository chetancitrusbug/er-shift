@extends('layouts.admin')

@section('title','Order')

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    Order
                </div>
            </div>
            <div class="box-content ">
                <div class="row">
                        <div class="col-md-6 pull-right">
                            <select class="form-control navbar-form navbar-right selectStatusArea" name='status'>
                                <option value=''>All</option>
                                <option value='1'>Success</option>
                                <option value='0'>Failed</option>
                            </select>
                        </div>
                        
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/profession', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
    
                        {!! Form::close() !!}
                </div>
                <div class="table-responsive" >
                    <table class="table table-borderless" id="order-table">
                        <thead>
                            <tr>
                                <th>Employee</th>
                                <th>Package</th>
                                <th>Amount</th>
                                <th>Start Date </th>
                                <th>End Date </th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script-head')
<script>
    $(function() { 
        var url ="{{ url('/admin/order/') }}";
        var datatable = $('#order-table').DataTable({
            processing: true,
            serverSide: true,
            order : [[ 4, "desc" ]],
            ajax: {
                    url: '{!! route("orderData") !!}',
                    type: "get", // method , by default get
                },
                columns: [
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            return o.userName;
                        }
                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            return o.packages;
                        }
                    },
                    { data: 'amount',name:'amount',"searchable" : false}, 
                    { data: 'start_date',name:'start_date',"searchable" : false}, 
                    { data: 'expire_date',name:'expire_date',"searchable" : false},
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                                e= "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-primary btn-xs'>View</button></a>&nbsp;";
                                
                            return e;
                        }
                    }
                ]
        });
        $( ".selectStatusArea" ).change(function() {  
            var status = $( ".selectStatusArea" ).val();
            var url ="{!! route('orderData') !!}";
            datatable.ajax.url( url + '?status='+ status).load();            
        });
        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            
            var url ="{{ url('/admin/order/') }}";
            url = url + "/" + id;
            var r = confirm("Are you sure you want to delete Order ?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success('Action Success!', data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procede!',erro)
                    }
                });
            }
        });
    }); 
</script>
@endpush
