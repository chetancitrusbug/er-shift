
@extends('layouts.admin')

@section('title','View Order')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Order</div>
            <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a> 
                    
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <td>Id</td>
                                <td>{{ $order->id }}</td>
                            </tr>
                            <tr>
                                <td>Employee</td>
                                <td>{{ $order->user->name }}</td>
                            </tr>
                            <tr>
                                <td>Package</td>
                                <td>{{ $order->package->title }}</td>
                            </tr>
                            <tr>
                                <td>Amount</td>
                                <td>{{ $order->package->amount }}</td>
                            </tr>
                            <tr>
                                <td>Start Date</td>
                                <td>{{ $order->start_date }}</td>
                            </tr>
                            <tr>
                                <td> End Date</td>
                                <td>{{ $order->expire_date }}</td>
                            </tr>
                            <tr>
                                <td> Status</td>
                                <td>{{ (($order->status == 0) ? 'Inactive' : 'Active') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection