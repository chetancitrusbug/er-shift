<!DOCTYPE html>
<html>
	@include ('email.include.header', ['lang' => $lang])
	<body style="margin:30px auto; ">
		<div style="background:#000; padding:20px; text-align:center;">
			<img class="brand_icon" src="{{ asset('assets/images/logo_lg.png') }}" alt="logo"/>
		</div>
		<div style="align:center; border:1px solid #ccc; padding-top:30px; padding-bottom:40px">
			<h2>@lang('subscription.billing.admin_create_subscription',['item_name'=>$package->title],$lang)</h2>
                        @include ('email.include.order-table', ['lang' => 'en'])
			<h5 style="text-align: center;margin-top: 55px;font-size: 15px;color: #555;">
			</h5>
		</div>
		@include ('email.include.footer',['lang' => $lang])
	</body>
</html>
