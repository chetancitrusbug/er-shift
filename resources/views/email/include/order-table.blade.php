<table>
    <tr>
        <th>Billing Id</th>
        <th>Package Name</th>
        <th>Package Detail</th>
        <th>Price / Days</th>



    </tr>
    <tr>
        <td>{{ $billing->subscription_id}}</td>
        <td>{{  $package->title}}</td>
        <td>{{  $package->description}}</td>
        <td>${{  $package->amount }} / {{  $package->day}}</td>
    </tr>
</table>
 
<h4 style="margin-top: 35px;">Buyer Detail</h4>
<table class="buyer_detail">
    <tr style="background-color:#f2f2f2">
        <th>Buyer Name</th>
        <td>{{$billing->user->name}}</th>
    </tr>
    <tr>
        <th>Buyer Email</th>
        <td>{{$billing->user->email}}</td>
    </tr>
    
</table>

<h4 style="margin-top: 35px;">Payment Detail</h4>
<table class="buyer_detail">
    <tr>
        <th>Price / Days</th>
        <td>${{  $package->amount }} / {{  $package->day}}</td>
    </tr>
    @if($Order)
    <tr>
        <th>Invoice Id</th>
        <td>{{  $Order->invoice_id }}</td>
    </tr>
     
    <tr>
        <th>Billing Cycle</th>
        <td>{{  $Order->start_date }} - {{  $Order->expire_date}}</td>
    </tr>
    @endif
</table>

