@extends('layouts.app')
@section('title','Login')
@section('content')
<section class="header-section other-header-section">
<div class="login-container">
    <div class="login-top-container">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-lg-7 col-md-9 col-sm-12">
                    
                    <div class="content_div">
                        <div class="logo-div fadeInLeft animated"><a href="{{url('/')}}"><img src="{{asset('images/logo.png')}}" alt=""></a></div> 
                    </div>  
                    
                    <div class="card_div">
                        <div class="row justify-content-md-center">
                            <div class="col-lg-7 col-md-7 col-sm-10">
                                <div class="login-div-content">
                                    <h2>@lang('login.label.log_in')</h2>
                                    <p>Don't have an account? <a href="{{ url('/register') }}">@lang('login.label.register') </a> </p>
                                </div>
                                 <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="exampleInputEmail1">@lang('login.label.e_mail')</label>
                                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ old('email') }}" required autofocus placeholder="Enter your email...">
                                       
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="exampleInputPassword1">@lang('login.label.password')</label>
                                        <input id="exampleInputPassword1" type="password" class="form-control" name="password" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="forgot-password">
                                        <a href="{{ route('password.request') }}">@lang('login.label.forgot_your_password')</a>
                                    </div>
                                    
                                    <div class="btn-div">
                                        <button class="btn btn-startfree" type="submit">@lang('login.label.log_in')<!--Login to your account--></button>
                                        <p>or</p>
                                        <a class="btn btn-facebook" href="{{ url('/auth/linkedin') }}"><span><i class="fa fa-linkedin"></i>@lang('login.label.login_using_linkedin')<!--Login with facebook--></span></a>
                                    </div>
                                </form>
                            </div>  
                        </div>
                    </div>
                    
                </div><!-- end of col -->
            </div>
        </div>
    </div>
</div>
</section>
@endsection
