@extends('layouts.app')

@section('content')
<div class="login-container">
    <div class="login-top-container">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-lg-7 col-md-9 col-sm-12">
                    
                    <div class="content_div">
                        <div class="logo-div fadeInLeft animated"><a href="{{url('/')}}"><img src="{{asset('images/logo.png')}}" alt=""></a></div> 
                    </div>  
                    
                    <div class="card_div">
                        <div class="row justify-content-md-center">
                            <div class="col-lg-7 col-md-7 col-sm-10">
                                <div class="login-div-content">
                                    <h2>@lang('forgot_pwd.label.reset_password')</h2>
                                    <p>Don't have an account? <a href="{{ url('/register') }}">@lang('login.label.register') </a> </p>
                                </div>
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email">@lang('forgot_pwd.label.e_mail')</label>

                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                    <div class="btn-div">
                                        <button class="btn btn-startfree" type="submit">@lang('forgot_pwd.label.send_reset_link')<!--Login to your account--></button>
                                        
                                    </div>
                                    
                                </form>
                                 
                            </div>  
                        </div>
                    </div>
                    
                </div><!-- end of col -->
            </div>
        </div>
    </div>
</div>

@endsection
