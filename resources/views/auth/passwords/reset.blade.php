@extends('layouts.app')

@section('content')
<div class="login-container">
    <div class="login-top-container">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-lg-7 col-md-9 col-sm-12">
                    
                    <div class="content_div">
                        <div class="logo-div fadeInLeft animated"><a href="{{url('/')}}"><img src="{{asset('images/logo.png')}}" alt=""></a></div> 
                    </div>  
                    
                    <div class="card_div">
                        <div class="row justify-content-md-center">
                            <div class="col-lg-7 col-md-7 col-sm-10">
                                <div class="login-div-content">
                                    <h2>@lang('forgot_pwd.label.reset_password')</h2>
                                    <p>Don't have an account? <a href="{{ url('/register') }}">@lang('login.label.register') </a> </p>
                                </div>
                                 
                                {{-- <form class="form-horizontal" method="POST" action="{{ route('password.request') }}"> --}}
                                <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="token" value="{{ $token }}">

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" >@lang('forgot_pwd.label.e_mail')</label>

                                        
                                            <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" >@lang('forgot_pwd.label.password')</label>

                                            <input id="password" type="password" class="form-control" name="password" required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif

                                    </div>

                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label for="password-confirm">@lang('forgot_pwd.label.confirm_password')</label>
                                        
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            @endif
                                        
                                    </div>

                                    <div class="btn-div">
                                        <button class="btn btn-startfree" type="submit">@lang('forgot_pwd.label.reset_password')<!--Login to your account--></button>
                                    </div>
                                    
                                </form>
                            </div>  
                        </div>
                    </div>
                    
                </div><!-- end of col -->
            </div>
        </div>
    </div>
</div>

@endsection
