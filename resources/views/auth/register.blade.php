@extends('layouts.app')
@section('title','Registration')
@section('content')
<section class="header-section other-header-section">
<div class="login-container">
    <div class="login-top-container">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-lg-7 col-md-9 col-sm-12">
                    
                    <div class="content_div">
                        <div class="logo-div fadeInLeft animated"><a href="{{url('/')}}"><img src="{{asset('images/logo.png')}}" alt=""></a></div> 
                    </div>  
                    
                    <div class="card_div">
                        <div class="row justify-content-md-center">
                            <div class="col-lg-7 col-md-7 col-sm-10">
                                <div class="login-div-content">
                                    <h2>@lang('register.label.register')</h2>
                                    <p>Have already Sign Up? <a href="{{ url('/login') }}">@lang('login.label.log_in') </a> </p>
                                </div>
                                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                                    {{ csrf_field() }}
                                     <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name">@lang('register.label.uname')</label>
                                        
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                    <div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">
                                        <label for="fname" >@lang('register.label.fname')</label>

                                        <input id="fname" type="text" class="form-control" name="fname" value="{{ old('fname') }}" required autofocus>

                                        @if ($errors->has('fname'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('fname') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                    <div class="form-group{{ $errors->has('lname') ? ' has-error' : '' }}">
                                        <label for="lname" >@lang('register.label.lname')</label>

                                        
                                            <input id="lname" type="text" class="form-control" name="lname" value="{{ old('lname') }}" required autofocus>

                                            @if ($errors->has('lname'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('lname') }}</strong>
                                                </span>
                                            @endif
                                       
                                    </div>
                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="exampleInputEmail1">@lang('register.label.e_mail')</label>
                                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ old('email') }}" required autofocus placeholder="Enter your email...">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="exampleInputPassword1">@lang('login.label.password')</label>
                                        <input id="exampleInputPassword1" type="password" class="form-control" name="password" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="password-confirm">@lang('register.label.confirm_password')</label>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                    
                                    <div class="btn-div">
                                        <button class="btn btn-startfree" type="submit">@lang('register.label.register')<!--Login to your account--></button>
                                        <p>or</p>
                                        <a class="btn btn-facebook" href="{{ url('/auth/linkedin') }}"><span><i class="fa fa-linkedin"></i>@lang('register.label.join_using_linkedin')<!--Login with facebook--></span></a>
                                    </div>
                                </form>
                            </div>  
                        </div>
                    </div>
                    
                </div><!-- end of col -->
            </div>
        </div>
    </div>
</div>
</section>
@endsection
