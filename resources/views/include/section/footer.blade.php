<footer>
		<div class="footer-div">

		<div class="footer-col-div clearfix">

			<div class="container">
			<div class="row">

			<div class="col-md-3 col-sm-3">
				<div class="footer-link-div clearfix">
					<img src="images/design/footer-logo.png" alt="" class="img-responsive footer-logo">
				</div><!-- end of footer-link-div -->
			</div><!-- end of footer div -->

			<div class="col-md-3 col-sm-3">
				<div class="footer-link-div clearfix">
					<h4>Learn more</h4>

					<ul>
						<li><a href="{{ $ios_url }}">Download ios App</a></li>
						<li><a href="{{ url('/privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ url('/terms-condition') }}">Terms & Conditions</a></li>
					</ul>

				</div><!-- end of footer-link-div -->
			</div><!-- end of footer div -->

			<div class="col-md-3 col-sm-3">
				<div class="footer-link-div clearfix">
					<h4>Support</h4>

					<ul>
						<li><a href="{{ url('/contact') }}" >Contact Us</a></li>
					</ul>

				</div><!-- end of footer-link-div -->
			</div><!-- end of footer div -->

			<div class="col-md-3 col-sm-3">
				<div class="footer-link-div clearfix">
					<h4>Address</h4>

					<p><strong>ER Shifts</strong></p>
					<p> 1028 South Bishop Ave, <br> #244, Rolla, MO 65401</p>

				</div><!-- end of footer-link-div -->
			</div><!-- end of footer div -->


			</div><!-- end of row -->
			</div><!-- end of container -->

		</div><!-- end of footer-col-div -->
		</div><!-- end of footer div -->

		<div class="copyright-div clearfix">
			<div class="container">
			<div class="row">


			<div class="col-md-6 col-sm-6 pull-left">
				<p>Copyright &copy; 2018 ERShifts.com</p>
			</div><!-- end of col -->
			<div class="col-md-6 col-sm-6 pull-right">
				<ul class="social-footer">
					@if ($twitter_url != null)
                        <li><a href="{{ $twitter_url }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    @endif
                    @if ($facebook_url != null)
                        <li><a href="{{ $facebook_url }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    @endif
                    @if ($instagram_url != null)
                        <li><a href="{{ $instagram_url }}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    @endif
                    @if ($google_url != null)
                        <li><a href="{{ $google_url }}" target="_blank"><i class="fa fa-google"></i></a></li>
                    @endif                  
                    @if ($linkedin_url != null)
                        <li><a href="{{ $linkedin_url }}" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    @endif
				</ul>
			</div><!-- end of col -->



		</div><!-- end of copyright-div -->

	</footer>