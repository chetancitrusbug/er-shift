<section class="multiweather-filters-container clearfix">
        	
    <div class="container">
    <div class="row">
    
        <div class="multiweather-filters-div clearfix">
        
            <h3>Multiple Weather Filters</h3>
            
             <div class="multiweather-filters-blk clearfix">
                 
                <ul class="clearfix">
                    <li>
                        <div class="weather-blk">
                            <img src="{!! asset('/frontend/images/filter-1.png') !!}" alt="Rainfall" class="img-responsive">
                            <span>Rainfall</span>
                        </div>
                    </li> 
                    <li>
                        <div class="weather-blk">
                            <img src="{!! asset('/frontend/images/filter-2.png') !!}" alt="Snowfall" class="img-responsive">
                            <span>Snowfall</span>
                        </div>
                    </li> 
                    <li>
                        <div class="weather-blk">
                            <img src="{!! asset('/frontend/images/filter-3.png') !!}" alt="Sunshine" class="img-responsive">
                            <span>Sunshine</span>
                        </div>
                    </li> 
                    <li>
                        <div class="weather-blk">
                            <img src="{!! asset('/frontend/images/filter-4.png') !!}" alt="Cloud Cover" class="img-responsive">
                            <span>Cloud Cover</span>
                        </div>
                    </li> 
                    <li>
                        <div class="weather-blk">
                            <img src="{!! asset('/frontend/images/filter-5.png') !!} " alt="Temperature" class="img-responsive">
                            <span>Temperature</span>
                        </div>
                    </li> 
                    <li class="left">
                        <div class="weather-blk">
                            <img src="{!! asset('/frontend/images/filter-6.png') !!}" alt="Wind Speed" class="img-responsive">
                            <span>Wind Speed</span>
                        </div>
                    </li> 
                    <li>
                        <div class="weather-blk">
                            <img src="{!! asset('/frontend/images/filter-7.png') !!}" alt="Humidity" class="img-responsive">
                            <span>Humidity</span>
                        </div>
                    </li> 
                    <li>
                        <div class="weather-blk">
                            <img src="{!! asset('/frontend/images/filter-8.png') !!}" alt="Thunder" class="img-responsive">
                            <span>Thunder</span>
                        </div>
                    </li> 
                    <li>
                        <div class="weather-blk">
                            <img src="{!! asset('/frontend/images/filter-9.png') !!}" alt="Air Pressure" class="img-responsive">
                            <span>Air Pressure</span>
                        </div>
                    </li>                        
                </ul>
             
             </div>
            
        
        </div><!-- end of howitworks-div -->
    
    </div>
    </div>

</section><!-- end of howitworks-container -->