
	<header>
		<div class="header-div clearfix">
			<div class="container-fluid">
			<div class="row">
			<div class="col-md-12 col-sm-12">
			
				<div class="logo-div fadeInLeft animated"><a href="{{url('/')}}"><img src="images/logo.png" alt=""></a></div>
				
				<div class="nav-div fadeInDownBig animated">
					<nav class="navbar navbar-expand-lg">
					  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					  </button>

					  <div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
						  {{-- <li class="nav-item ">
							<a class="nav-link" href="features.html">Features</a>
						  </li> --}}
						  <li class="nav-item">
							<a class="nav-link" href="{{ url('/price') }}">Pricing</a>
						  </li>
						  <li class="nav-item">
							<a class="nav-link" href="{{ url('/contact') }}">Contact</a>
						  </li>
						  <li class="nav-item">
							<a class="nav-link" href="{{ url('/login') }}">Login</a>
						  </li>
						  
						</ul>
						{{-- <form class="form-inline top-startup">
						  <input class="form-control" type="text" placeholder="Enter Email..." aria-label="Enter Email...">
						  <button class="btn btn-startfree " type="submit">Start Free</button>
						</form> --}}
					  </div>
					</nav>
				</div><!-- end of nav div -->
			
			</div>
			</div>
			</div>
		
		</div><!-- end of header-div -->
	</header>
	@include('include.frontend.page_notification')
	