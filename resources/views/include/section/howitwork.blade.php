<section class="howitworks-container clearfix">
        	
    <div class="container">
    <div class="row">
    
        <div class="howitworks-div clearfix">
        
            <div class="iphone-img"><img src="{!! asset('/frontend/images/iphone.png') !!}" alt="iphone" class="img-responsive"></div>
            <h3>How it works</h3>
            <div class="txt-div clearfix">
            <p class="txt-1">Is your business weather dependant?</p>
            <p class="txt-2">Are you paying for Facebook adverts during weather conditions that don't  benefit your business?</p>
            <p class="txt-3">For example, running an advert to sell Ice Cream across the UK when its only sunny in the south.</p>
            <p class="txt-4">Our tool can help! <span><a href="#" class="learmore-link">Learn more</a></span></p>
            </div>
        
        </div><!-- end of howitworks-div -->
    
    </div>
    </div>

</section><!-- end of howitworks-container -->