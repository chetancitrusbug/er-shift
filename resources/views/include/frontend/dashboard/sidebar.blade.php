<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<!--div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">Victoria Baker</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
									</div>
								</div>

							
							</div>
						</div>
					</div-->
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								
								<!-- <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li> -->
								<li class="active"><a href="{{ url('/dashboard') }}"><i class="fas fa-home"></i> <span>@lang('dashboard.side_bar.dashboard')</span></a></li>
								<li>
									<a href="{{ url('/billing/subscription') }}"><i class="fab fa-cc-stripe"></i><span>@lang('dashboard.side_bar.subscription')</span></a>
									<ul>
										<li><a href="{{ url('/billing/subscription') }}"><i class="icon-clipboard3"></i> @lang('dashboard.side_bar.list')</a></li>
										{{-- <li><a href="#"><i class="fab fa-facebook-f sidebar-nav-icon"></i> Facebook</a></li> --}}
									</ul>
								</li>
								<li>
									<a href="{{ url('/jobs') }}"><i class="fab fa-searchengin"></i><span>@lang('dashboard.side_bar.jobs')</span></a>
									<ul>
											<li><a href="{{ url('/jobs/create') }}"><i class="fas fa-plus"></i> @lang('dashboard.side_bar.create_job')</a></li>
											<li><a href="{{ url('/jobs') }}"><i class="icon-clipboard2"></i> @lang('dashboard.side_bar.jobs')</a></li>
											<li><a href="{{ url('/addBlukJobs') }}"><i class="fas fa-plus-square "></i>@lang('dashboard.side_bar.add_bulk_job')</a></li>
									</ul>
								</li>
{{-- 						
								<li><a href="#"><i class="fa fa-cogs sidebar-nav-icon"></i><span> Account Details</span></a></li>
								<li><a href="#"><i class="fa fa-credit-card sidebar-nav-icon"></i><span> Billing</span></a></li> --}}
								<!-- /main --> 
							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->