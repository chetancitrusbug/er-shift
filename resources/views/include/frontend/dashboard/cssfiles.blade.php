<link href="//fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="{!! asset('/frontend/dashboard/css/icons/icomoon/styles.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('/frontend/dashboard/css/minified/bootstrap.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('/frontend/dashboard/css/minified/core.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('/frontend/dashboard/css/minified/components.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('/frontend/dashboard/css/minified/colors.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('/frontend/dashboard/css/style.css') !!}" rel="stylesheet" type="text/css">
<link href="//use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link href="{!! asset('assets/stylesheets/demo.css') !!}" media="all" rel="stylesheet" type="text/css"/>

<!-- dev css -->
<link href="{!! asset('/frontend/dashboard/css/dev.css') !!}" rel="stylesheet" type="text/css">

<!-- Data Table -->
<link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="//cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

