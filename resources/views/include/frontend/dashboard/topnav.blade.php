    <!-- Main navbar -->
	<div class="navbar navbar-default header-highlight">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{ url('/dashboard') }}"><img src="assets/images/logo_lg.png" alt="" width="150px" height="200px"></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
				</li>
			</ul>
			<p class="navbar-text"><span class="label {{ (($expire_in_days > 6) ? 'bg-success' : 'bg-danger') }} ">
				@if(isset($plan) && @$plan != null && @$plan->isFree == 1)
					@lang('dashboard.top_nav.free_trial_ends_in') {{ @$expire_in_days }}
				@elseif(@$subscription == null)
					@lang('dashboard.top_nav.you_have_not_plan')
				@elseif(@$subscription != null && @$expire_in_days == 0)
					@lang('dashboard.top_nav.your_plan_renew_within_24_hrs')
				@else
				   @if(@$subscription != null && @$expire_in_days < 30)
						   @lang('dashboard.top_nav.your_plan_expired_in') {{ @$expire_in_days }} @lang('dashboard.label.days')
				   @else
						   @lang('dashboard.top_nav.your_plan_expired_in') {{ round((@$expire_in_days)/30) }} @lang('dashboard.label.month')
				   @endif

			   @endif
			</span></p>

			<ul class="nav navbar-nav navbar-right">


				<!--<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>
						Settings
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">

					</ul>
				</li> start setting -->

				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="assets/images/placeholder.jpg" alt="">
						<span>Employer - {{ Auth::user()->name }}</span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<!--<li><a href="{{ url('profile/edit') }}"><i class="fa fa-wrench icon-push"> </i> Edit Profile</a></li>-->
						<li><a href="{{ url('profile/change-password') }}"><i class="fa fa-lock icon-push"> </i> @lang('dashboard.top_nav.change_password')</a></li>
						<li><a href="{{ url('/profile') }}"><i class="icon-user-plus"></i> @lang('dashboard.top_nav.my_profile')</a></li>
						<li class="divider"></li>
						<li><a href="{{url('/billing/history')}}"><i class="fab fa-cc-stripe"></i> @lang('dashboard.top_nav.subscription_history') </a></li>
						<li><a href="{{ url('/logout') }}"
								onclick="event.preventDefault();
								document.getElementById('logout-form').submit();">
								<i class="icon-switch2"></i> @lang('dashboard.top_nav.logout')</a>
								<form id="logout-form" action="{{ url('/logout') }}" method="POST"  style="display: none;">
											 {{ csrf_field() }}
								</form>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->
