

<section class="footer-container clearfix">
    <div class="container">
        <div class="row">
            <div class="footer-div clearfix">
                <div class="col-20">
                    <a href="index.html"><img src="{!! asset('/frontend/images/footer-logo.png') !!}" alt="weather footer logo" class="img-responsive m-center"></a>
                </div>
                <div class="col-20">
                    <ul>
                        <li><h3>Product</h3></li>
                        <li><a href="sign-up.html">Sign Up</a></li>
                        <li><a href="learn-more.html">Learn More</a></li>
                        <li><a href="faqs.html">FAQ's</a></li>
                        <li><a href="plan.html">Prices</a></li>
                    </ul>
                </div>
                <div class="col-20">
                    <ul>
                        <li><h3>Company</h3></li>
                        <li><a href="support.html">Support</a></li>
                    </ul>
                </div>
                <div class="col-20">
                    <ul>
                        <li><h3>Legal</h3></li>
                        <li><a href="{{ url('/privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ url('/terms-condition') }}">Terms & Conditions</a></li>
                    </ul>
              </div>
                <div class="col-20">
                    <div class="icon-div">
                        <span class="top-social-span">
                            @if ($twitter_url != null)
                                <a href="{{ $twitter_url }}" target="_blank"><i class="fa fa-twitter"></i></a>
                            @endif
                            @if ($facebook_url != null)
                                <a href="{{ $facebook_url }}" target="_blank"><i class="fa fa-facebook"></i></a>
                            @endif
                            @if ($instagram_url != null)
                                <a href="{{ $instagram_url }}" target="_blank"><i class="fa fa-instagram"></i></a>
                            @endif
                            @if ($google_url != null)
                                <a href="{{ $google_url }}" target="_blank"><i class="fa fa-google"></i></a>
                            @endif                  
                            @if ($linkedin_url != null)
                                <a href="{{ $linkedin_url }}" target="_blank"><i class="fa fa-linkedin"></i></a>
                            @endif
                        </span>2
                    </div>
                    <p><a href="#">
                            <?php $today = getdate(); ?>
                            Copyright &copy; {{$today['year']}} {{ config('app.name') }}
                    </p>
                </div>


            </div><!-- end of footer-div -->
        </div>
    </div>
</section>   