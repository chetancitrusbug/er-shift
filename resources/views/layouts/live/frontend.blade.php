<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="en-tr"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="en-tr"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie10 lt-ie9" lang="en-tr"> <![endif]-->
<!--[if IE 9]> <html class="no-js lt-ie10 lt-ie9" lang="en-tr"> <![endif]-->
<!--[if lt IE 10]> <html class="no-js lt-ie10" lang="en-tr"> <![endif]-->
<!--[if !IE]> > <![endif]-->
<html lang="en">
<head>
    <title>@yield('title','Home') | {{ config('app.name') }}</title>
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
    
    <link href='{!! asset('assets/images/favicon.png') !!}' rel='shortcut icon' type='image/png'>
    

    <link href="{{ asset('assets/stylesheets/demo.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/stylesheets/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/stylesheets/animate.css') }}" rel="stylesheet">
    <script src="{{ asset('frontend/js/modernizr.js') }}" type="text/javascript"></script>
     <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--[if IE]>
        <script src="js/html5.js"></script>
    <![endif]-->
</head>
<body >
<div id="wrapper">
    
        <!-- <header> -->
            <!-- <div class="header-div clearfix"> -->
                <!-- <div class="container-fluid"> -->
                    <!-- <div class="row"> -->
                        <!-- <div class="col-md-12 col-sm-12"> -->
                            
                            <!-- <div class="logo-div fadeInLeft animated"><a href="index.html"><img src="images/logo.png" alt=""></a></div> -->
                            
                            <!-- <div class="nav-div fadeInDownBig animated"> -->
                                <!-- <nav class="navbar navbar-expand-lg"> -->
                                    <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> -->
                                    <!-- <span class="navbar-toggler-icon"></span> -->
                                    <!-- </button> -->
                                    <!-- <div class="collapse navbar-collapse" id="navbarSupportedContent"> -->
                                        <!-- <ul class="navbar-nav mr-auto"> -->
                                            <!-- <li class="nav-item "> -->
                                                <!-- <a class="nav-link" href="features.html">Features</a> -->
                                            <!-- </li> -->
                                            <!-- <li class="nav-item"> -->
                                                <!-- <a class="nav-link" href="pricing.html">Pricing</a> -->
                                            <!-- </li> -->
                                            <!-- <li class="nav-item"> -->
                                                <!-- <a class="nav-link" href="contact.html">Contact</a> -->
                                            <!-- </li> -->
                                            <!-- <li class="nav-item active"> -->
                                                <!-- <a class="nav-link" href="login.html">Login</a> -->
                                            <!-- </li> -->
                                        <!-- </ul> -->
                                        <!-- <form class="form-inline top-startup"> -->
                                            <!-- <input class="form-control" type="text" placeholder="Enter Email..." aria-label="Enter Email..."> -->
                                            <!-- <button class="btn btn-startfree " type="submit">Start Free</button> -->
                                        <!-- </form> -->
                                    <!-- </div> -->
                                <!-- </nav> -->
                            <!-- </div><!-- end of nav div --> 
                        <!-- </div> -->
                    <!-- </div> -->
                <!-- </div> -->
            <!-- </div><!-- end of header-div --> 
        <!-- </header> -->
    
        <div class="col-md-4 container errAppLayout">
            @include('include.frontend.page_notification')
        </div>
    
        @yield('content')
    <!-- end of header-section -->

</div><!-- end or wrapper -->
<script src="{{ asset('js/app.js') }}"></script>
<!-- <script src="js/jquery.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script> -->
</body>
</html>
