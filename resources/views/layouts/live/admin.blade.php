<!DOCTYPE html>
<html>
<head>
    <title>@yield('title','Home') | {{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta content='text/html;charset=utf-8' http-equiv='content-type'>
    <meta content='Flat administration template for Twitter Bootstrap. ' name='description'>
    <link href='{!! asset('assets/images/favicon.png') !!}' rel='shortcut icon' type='image/png'>
    @include('include.backend.cssfiles')
    @yield('headExtra')
	@stack('css')
</head>
<body class='contrast-red'>
@include('include.backend.topnav')
<div id='wrapper'>
    <div id='main-nav-bg'></div>
	@include('include.backend.sidebar')
	<section id='content'>
        <div class='container'>

            <div class='row' id='content-wrapper'>
                <div class='col-xs-12 page_notification'>
					@include('include.backend.page_notification')
					<div class="row">
                        <div class="col-sm-12">
                            <div class="page-header">
                                @if(View::hasSection('pageHeader'))
                                    @yield('pageHeader')
                                @endif
                            </div>
                        </div>
                    </div>
                    @yield('content')
				</div>
            </div>
			@include('include.backend.footer')
        </div>
    </section>
	
</div>
<script>
        function openNext(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
        </script>
@include('include.backend.jsfiles')

@stack('js')
@stack('script-head')
</body>
</html>