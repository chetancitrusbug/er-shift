<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href='{!! asset('assets/images/favicon.png') !!}' rel='shortcut icon' type='image/png'>
	<title>@yield('title', trans('dashboard.label.home') ) | {{ config('app.name') }}</title>

	<!-- Global stylesheets -->
	@include('include.frontend.dashboard.cssfiles')

	<!-- /global stylesheets -->

    <!-- Js-->
    @include('include.frontend.dashboard.jsfiles')

</head>

<body>

	@include('include.frontend.dashboard.topnav')
    

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			@include('include.frontend.dashboard.sidebar')
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
			
				<section class="top-section">
					<div class="page-header">
						<div class="page-header-content">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-12 clearfix">
									<div class="page-title">
										<h4><span class="text-semibold">@lang('dashboard.label.welcome')</span> - {{ Auth::user()->name }}</h4>
									</div>
								</div>
								<div class="col-lg-8 col-md-8 col-sm-12 clearfix">
									<div class="heading-elements-div">
										<div class="heading-btn-group-div">
											<ul class="clearfix ul-dashboard">
												<li class="col-lg-3 col-md-3 col-sm-3 col-sm-12">
													<div class="pull-left">
														<a href="{{url('/billing/subscription')}}" class="btn btn-link btn-float has-text">
															@lang('dashboard.label.current_plan')
															<span class="account-span">
																@if(@$isfree == 0)
																	{{  @$subscription->packageDetail->title}}
																@else
																	{{ @$plan->title }}
																@endif
															</span>
														
														</a>
													</div>
													<div class="pull-left">
														<div class="span-responsive">
															<i class="icon-box icon-round"></i>
														</div>
													</div>
													
												</li>
												<li class="col-lg-3 col-md-3 col-sm-3 col-sm-12">
													<div class="pull-left">
														<a href="{{url('/billing/plans')}}" class="btn btn-link btn-float has-text"> @lang('dashboard.label.plan_expired_on')<span class="account-span">
														@if(@$isfree == 0)
														{{  @$expire_in_days }} @lang('dashboard.label.days')
														@else
															@if(@$expire_in_days < 30)
																{{ @$expire_in_days }} @lang('dashboard.label.days')
															@else
																{{ round((@$expire_in_days)/30) }} @lang('dashboard.label.month')
															@endif
														   
														   @endif
														</span>
														</a>
													</div>	
													<div class="pull-left">
														<div class="span-responsive">
															<i class="icon-box icon-round"></i>
														</div>
													</div>
												</li>
												
												<li class="col-lg-3 col-md-3 col-sm-3 col-sm-12">
													<div class="pull-left">
														<a href="{{ url('/jobs') }}" class="btn btn-link btn-float has-text"> @lang('dashboard.label.total_no_jobs') 
															<span class="account-span">{{ @$totaljob }}</span>
														</a>
													</div>	
													<div class="pull-left">
														<div class="span-responsive">
															<i class="icon-star-full2 icon-round"></i>
														</div>
													</div>
												</li>
												<li class="col-lg-3 col-md-3 col-sm-3 col-sm-12">
													<div class="pull-left">
														<a href="#" class="btn btn-link btn-float has-text">@lang('dashboard.label.total_fav_jobs')
														<span class="account-span"> {{ @$totalfavourit }}</span></a>
													</div>
													<div class="pull-left">
														<div class="span-responsive">
															<i class="icon-star-full2 icon-round"></i>
														</div>
													</div>
												</li>
											 
											</ul>	
										</div>
									</div>
								</div>
							</div>
						</div>			
					</div><!-- /page header -->

				</section>
				@include('include.frontend.dashboard.page_notification')
				<!-- Content area -->
				<div class="content">
					@yield('content')
				</div>
				<!-- /content area -->
				<!-- Footer -->
				<div class="footer text-muted">
					&copy; <a href="#" target="_blank"> @lang('dashboard.label.erShift') <?php echo date('Y');?> </a> @lang('dashboard.label.all_rights')
				</div>
				<!-- /footer -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


</body>
</html>
@stack('js')
@stack('script-head')