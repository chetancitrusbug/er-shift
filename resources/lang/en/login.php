<?php

return [


    'label'=>[
    	'e_mail' => 'E-mail Address',
        'password' => 'Password',
        'register' => 'Register',
        'log_in' => 'Log In',
        'remember_me' => 'Remember me',
        'forgot_your_password' => 'Forgot Your Password?',
        'login_using_linkedin' => 'Login Using Linkedin',
        'logout' => 'Logout',
    ]

];