<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
 */
    'add_user' => 'Add Admin',
    'add_new_user' => 'Add New Admin',
    'back' => 'Back',
    'edit_user' => 'Edit Admin',
    'update' => 'Update',
    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'role' => 'Role',
    'create' => 'Create',
    'users' => 'Admin',
    'id' => 'ID',
    'status' => 'Status',
    'actions' => 'Actions',
    'blocked' => 'Blocked',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'view' => 'View',
    'view_user' => 'View Admin',
    'user' => 'Admin',
    'last_login_at' => 'Last Login At',
    'first_name'=>'First Name',
    'last_name'=>'Last Name',


];
