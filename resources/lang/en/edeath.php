<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
 */
    'add_edeath' => 'Add Expected Death',
    'add_new_edeath' => 'Add New Expected Death',
    'back' => 'Back',
    'edit_edeath' => 'Edit Expected Death',
    'update' => 'Update',
    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'role' => 'Role',
    'create' => 'Create',
    'edeaths' => 'Expected Deaths',
    'id' => 'ID',
    'status' => 'Status',
    'actions' => 'Actions',
    'blocked' => 'Blocked',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'view' => 'View',
    'view_death' => 'View Expected Death',
    'death' => 'Admin',
    'last_login_at' => 'Last Login At',
    'male_average'=>'Male Average',
    'female_average'=>'Female Average',
    'male_years'=>'Male Years',
    'male_days'=>'Male Days',
    'female_years'=>'Female Years',
    'female_days'=>'Female Days',
   
];
