<?php

return [

    'label'=>[
        'register' => 'Register',
        'uname' => 'User Name',
        'fname' => 'First Name',
        'lname' => 'Last Name',
		'e_mail' => 'E-mail Address',
		'password' => 'Password',
		'confirm_password' => 'Confirm Password',
        'join_using_linkedin' => 'Join Using Linkedin',

    ]

];