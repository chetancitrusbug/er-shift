<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
 */
    'add_goal' => 'Add Goal',
    'add_new_goal' => 'Add New Goal',
    'back' => 'Back',
    'edit_goal' => 'Edit Goal',
    'update' => 'Update',
    'title' => 'Title',
    'details'=>'Description',
    'start_date' => 'Start Date',
    'due_date' => 'Due Date',
    'create' => 'Create',
    'goals' => 'Goals',
    'id' => 'ID',
    'status' => 'Status',
    'actions' => 'Actions',
    'blocked' => 'Blocked',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'view' => 'View',
    'logs' => 'Logs',
    'view_goal' => 'View Goal',
    'goal' => 'Goal',
    'last_login_at' => 'Last Login At',   
    'usergoals'=>'User Goals',

];
