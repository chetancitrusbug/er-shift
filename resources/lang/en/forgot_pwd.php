<?php

return [

    'label'=>[
        'reset_password' => 'Reset Password',
        'send_reset_link' => 'Send Password Reset Link',
		'e_mail' => 'E-mail Address',
		'password' => 'Password',
		'confirm_password' => 'Confirm Password',
    ]

];
