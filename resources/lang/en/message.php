<?php

return [

'job' => [
        'shift_added' => 'Shift has been added successfully!',
        'bulk_shifts_added' => 'Shifts has been added successfully!',
        'shift_updated' => 'Shift has been updated successfully!',
        'shift_deleted' => 'Your shift has been deleted successfully!',
        'shift_not_found' => 'Shift not found!',
        'csv_data_missing_shift_not_added' => 'Data missing, Shifts could not be added' ,
        'select_file' => 'Please select file' ,
        'select_csv_file' =>  'Please select only CSV file',
],

'doctor_shift' => [
    'provider_details_not_found' => 'Provider details not found !!!', 
],

'profile' => [
    'entered_same_password_while_change' => 'Current password and new password are same.' ,
    'password_changed' => 'Password has been changed successfully!',
    'enter_correct_current_password' => 'Please enter correct current password',
    'something_went_wrong' => 'Something wrong. Please try again later!',
    'profile_updated' => 'Your profile has been updated successfully!' ,
],

'subscription' => [
    //when selected subscription package not found 
    'something_went_wrong' => 'Something wrong. Please try again later!',
    'stripe_user_not_created' => 'Stripe User not created , please try again later!' ,
    'subscription_not_started' => 'Subscription not started, please try again later!',
    'subscription_started' => 'Your Subscription has been started!',
    'subscription_package_cancelled' => 'Subscription package has been cancelled sucessfully!',
    'cannot_cancel_free_plan' => 'Sorry! You cannot cancel free plan ',
    'subscription_not_found' => 'Subscription not found, might Be deleted!',
    'no_active_package_purchase_new_plan' => 'Sorry, you do not have an active package. Please purchase a new subscription Package', 
    'package_not_found' => 'Package not found, might be expired or deleted!',
    'already_purchased_plan_choose_another' => 'You have already purchased this plan. Please choose another plan !!',
    'free_trial_plan_activated' => 'Your free trial plan for :days has been activated!',

],

'login' => [

    'email_not_exist' => 'This email does not exist. Please register!', 
    'incorrect_password' => 'You have entered wrong password', 
    'invalid_credential' => 'You have entered invalid credentials',
    'inactive_account' => 'Your account is inactive. Please activate your account from mail.',
    'email_registered_with_social_login' =>  'This email is registered with :login_type , Please try to login via :login_type ',

],

'register' => [
    'register_success' => 'Registration Successful, Please Check Email to activate your account.',
    'activation_link_expired' => 'Activation link is expired',
    'account_activated' => 'Your account has been activated. Please login!' ,
    'account_already_activated' => 'Your account have already been activated.Please login!',

],


'contact' => [
    'request_submitted_thankyou_for_contact' => 'Your request has been submitted. Thank you for Contacting us!!!',
    // when contact request not saved in our system  
    'something_went_wrong' => 'Something wrong. Please try again later!',
]

];