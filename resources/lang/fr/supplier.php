<?php 

return [

	'supplier' => 'Fournisseur',
	'add_new' => 'Ajouter de nouvelles',
	'id' => 'ID',
	'name' => 'Nom',
	'email' => 'E-mail',
	'phone' => 'Téléphone',
	'mobile' => 'Mobile',
	'cpnj' => 'CPNJ',
	'address' => 'Répondre',
	'city' => 'Ville',
	'state' => 'State',
	'country' => 'Pays',
	'zip' => 'Zip',
	'operator' => 'Opérateur',
	'suppliers' => 'Fournisseurs',
	'actions' => 'Actions',
	'edit' => 'Modifier',
	'view' => 'Voir',
	'delete' => 'Supprimer',
	'create_new_supplier' => 'Créer nouveau fournisseur',
	'back' => 'Router',
	'edit_supplier' => 'Fournisseur Modifier',
	'approver' => "L'approbateur",
	'add_supplier' => 'Ajouter Fournisseur',
	'close' => 'Fermer',

];