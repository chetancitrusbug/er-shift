<?php

return [

	'my_profile' => 'Mon profil',
	'edit_profile' => 'Modifier votre profil',
	'change_password' => 'Changer de mot de passe',
	'name' => 'Nom',
	'email' => 'E-mail',
	'language' => 'Language',
	'joined' => 'Rejoint',
	'back' => 'Router',

];