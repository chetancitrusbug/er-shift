<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1.0/',  'namespace' => 'Api'], function () {
    Route::post('user/login', 'UsersController@login');
    Route::post('user/register', 'UsersController@register');
    Route::post('user/forgotpassword', 'UsersController@forgotPassword');
});


Route::group(['prefix' => 'v1.0/', 'middleware' => ['api_pass'], 'namespace' => 'Api'], function () {
    Route::post('user/changepassword', 'UsersController@changePassword');
    Route::get('user/logout', 'UsersController@logout');

    // Doctor
    Route::post('doctorUpdate', 'DoctorController@doctorUpdate');
    Route::get('doctorDetail', 'DoctorController@doctorDetail');

    // Subscription
    Route::get('cancelSubscription', 'SubscriptionController@cancelSubscription');

    // Employee
    Route::get('employeeDetail', 'EmployeeController@employeeDetail');
    Route::post('employeeProfile', 'EmployeeController@employee_profile_update');

    // DoctorWish
    Route::post('addDoctorWish', 'DoctorWishController@addDoctorWish');
    Route::post('updateDoctorWish', 'DoctorWishController@updateDoctorWish');
    Route::post('deleteDoctorWish', 'DoctorWishController@deleteDoctorWish');
    Route::get('listDoctorWish', 'DoctorWishController@listDoctorWish');
    Route::get('viewDoctorWish', 'DoctorWishController@viewDoctorWish');

    // Favourite
    Route::post('addDeleteFavourite', 'FavouriteController@addDeleteFavourite');
    Route::get('listFavourite', 'FavouriteController@listFavourite');
    Route::post('addDeleteEmployeeFavourite', 'FavouriteController@addDeleteEmployeeFavourite');
    Route::get('listFavouriteEmployee', 'FavouriteController@listFavouriteEmployee');

    //list
    Route::get('listState', 'ListController@listState');
    Route::get('listProfesion', 'ListController@listProfesion');
    Route::get('listSpeciality', 'ListController@listSpeciality');
    Route::get('listDegree', 'ListController@listDegree');
    Route::get('listLocation', 'ListController@listLocation');
    Route::get('listLicense', 'ListController@listLicense');
    Route::get('listExperience', 'ListController@listExperience');
    Route::get('listEDVolume', 'ListController@listEDVolume');
    Route::get('listShift', 'ListController@listShift');
    Route::get('listPriority', 'ListController@listPriority');
    Route::get('listAll', 'ListController@listAll');

    //Invitation
    Route::post('inviteUser','InvitationController@inviteUser');
    Route::post('inviteUserUpdate','InvitationController@inviteUserUpdate');
    Route::post('inviteUserList','InvitationController@inviteUserList');

    //Chat
    Route::post('sendMessage','ChatController@sendMessage');
    Route::post('updateMessage','ChatController@updateMessage');
    Route::post('chatList','ChatController@chatList');
    
     //Employee Create Doctor job
     Route::get('job/listjob', 'DoctorjobController@list_job');
     Route::get('job/viewjob', 'DoctorjobController@view_job');
     Route::post('job/createjob','DoctorjobController@create_job');
     Route::post('job/updatejob','DoctorjobController@update_job');
     Route::get('job/deletejob', 'DoctorjobController@delete_job');

    //search list
     Route::post('recommanded','SearchController@recommended_job');
     Route::post('search','SearchController@search');
     Route::post('jobDetail','SearchController@jobDetail');

    // Jobs
    Route::post('employeeJobList', 'HomeController@employeeJobList');

     Route::post('/home','HomeController@index');

     //Package
     Route::post('packagelist', 'PackageController@list_package');
     Route::post('packageview', 'PackageController@Package_detail');

    //Order
    Route::post('orderlist', 'OrderdetailController@list_of_total_order');
    Route::post('orderview', 'OrderdetailController@order_view');
    //payment
    Route::post('payment', 'PaymentController@DoPayment');

    
    
 });
