<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */




Route::get('auth/linkedin', 'Auth\SocialLoginController@redirectToLinkedin')->name('login.linkedin');
Route::get('auth/linkedin/callback', 'Auth\SocialLoginController@handleLinkedinCallback');

Route::get('auth/facebook', 'Auth\SocialLoginController@redirectToFacebook')->name('login.facebook');
Route::get('auth/facebook/callback', 'Auth\SocialLoginController@handleFacebookCallback');

Route::get('auth/google', 'Auth\SocialLoginController@redirectToGoogle')->name('login.google');
Route::get('auth/google/callback', 'Auth\SocialLoginController@handleGoogleCallback');

Route::group(array('middleware' => 'setting'), function() {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/privacy-policy', 'HomeController@privacy')->name('privacy');
    Route::get('/terms-condition', 'HomeController@terms')->name('terms');
    Route::get('/contact', 'HomeController@contactus')->name('contact');
    Route::post('/contact/add', 'HomeController@contactAdd')->name('contact.add');
    Auth::routes();

    Route::post('/login', 'Auth\LoginController@authenticate')->name('login');
    Route::get('/login', 'HomeController@index')->name('login');
    Route::get('/register', 'HomeController@index')->name('register');
    Route::get('/password/reset', 'HomeController@index')->name('password.request');
    Route::get('/activate-account/{token}', 'Auth\RegisterController@activateAccount');

    Route::get('/shifts', 'DoctorShiftsController@index')->name('shifts');
    Route::get('/shifts/{id}/view', 'DoctorShiftsController@show')->name('shifts.view');

    Route::post('send-invite', 'InvitationController@SendInvite')->name('send-invite');

});


Route::group(['middleware' => ['auth','plandata', 'not_admin', 'setting']], function () {


    // subscription & billing
    Route::get('/billing/plans', 'SubscriptionController@plans')->name('plans');
    Route::get('/confirm-order/{id}', 'SubscriptionController@confirmOrder');
    Route::post('/confirm-order', 'SubscriptionController@doOrderStep1');
    Route::get('/order/payment/{id}', 'SubscriptionController@confirmOrderPayment');
    Route::post('/order/payment', 'SubscriptionController@doOrderPayment');

    Route::group(['middleware' => ['expiredPlan']], function () {

        // profile
        Route::get('/profile', 'ProfileController@index');
        Route::get('/profile/edit', 'ProfileController@edit')->name('profile.edit');
        Route::post('/profile/update', 'ProfileController@update')->name('profile.update');
        Route::get('profile/change-password', 'ProfileController@changePassword')->name('profile.change-password');
        Route::post('/profile/update-password', 'ProfileController@updatePassword')->name('profile.update-password');

        // employers jobs or shifts 
        Route::get('/my-shifts', 'JobController@index')->name('my-shifts');
        Route::get('/my-shifts/create', 'JobController@create')->name('shifts.create');
        Route::get('/my-shifts/{id}/edit', 'JobController@edit')->name('shifts.edit');
        Route::get('/my-shifts/{id}/view', 'JobController@show')->name('my-shifts.view');
        Route::post('/my-shifts/store', 'JobController@store')->name('shifts.store');
        Route::patch('/my-shifts/{id}/update', 'JobController@update')->name('shifts.update');
        Route::delete('/my-shifts/destroy/{id}', 'JobController@destroy')->name('shifts.delete');

        // Bulk shifts or jobs 
        Route::get('addBlukJobs', 'JobController@addBlukJobs')->name('shifts.create.bulk');
        Route::get('getSpeciality/{id}', 'JobController@getSpeciality')->name('speciality');
        Route::get('downloadcsv', 'JobController@download')->name('download-csv-data');
        Route::post('/store-jobs', 'JobController@storeJobs')->name('shifts.store.bulk');

        // subscription & billing
        Route::post('/billing/subscription/trial', 'SubscriptionController@doSubscriptionTrial');
        Route::get('cancelSubscription/{id}', 'SubscriptionController@cancelSubscription');

        // chat
        Route::post('send-message', 'InvitationController@SendMessage')->name('send-message');
        Route::get('/message', 'MessageController@index')->name('message');
        Route::get('/find-employers', 'MessageController@getDoctors')->name('employers');
        Route::get('get-invite', 'InvitationController@getInviteStatus')->name('get-invite');
        Route::post('update-invite', 'InvitationController@updateInvite')->name('update-invite');

    });

});



Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {

    Route::get('/', 'Admin\AdminController@index');

    //setting
    Route::resource('/setting', 'Admin\SettingController');
    Route::get('/setting-data', 'Admin\SettingController@datatable');

    //Profile
    Route::get('profile', 'Admin\ProfileController@index');
    Route::get('profile/edit', 'Admin\ProfileController@edit');
    Route::post('profile/updateProfile', 'Admin\ProfileController@updateProfile');
    Route::get('/profile/changePassword', 'Admin\ProfileController@changePassword');
    Route::patch('/profile/changePassword', 'Admin\ProfileController@updatePassword');

    // roles & permissions
    Route::resource('roles', 'Admin\RolesController');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::get('/usersdata', ['as' => 'UsersControllerUsersData', 'uses' => 'Admin\UsersController@datatable']);

    Route::resource('users', 'Admin\UsersController');

    // doctor & doctor wish  
    Route::resource('doctors', 'Admin\DoctorController');
    Route::get('/doctorsdata', ['as' => 'DoctorControllerUsersData', 'uses' => 'Admin\DoctorController@datatable']);
    Route::resource('doctorwish', 'Admin\DoctorJobPostingController');

    // employer & employers jobs 
    Route::resource('employer', 'Admin\EmployeeController');
    Route::get('/employeesData', ['as' => 'employeesData','uses' => 'Admin\EmployeeController@datatable']);
    Route::get('employer/jobs/{id}', 'Admin\EmployeeController@listJobs');
    Route::get('/jobsData/{id}', 'Admin\EmployeeController@jobDatatable');
    Route::get('employer/job/{emp_id}/{id}', 'Admin\EmployeeController@showJob');

    // emailtemplate
    Route::resource('emailtemplate', 'Admin\EmailTemplateController');
    Route::get('/emailtemplateData', ['as' => 'emailtemplateData','uses' => 'Admin\EmailTemplateController@datatable']);
    
    // Bulk Jobs
    Route::get('downloadcsv', 'Admin\JobController@download');
    Route::get('addBlukJobs', 'Admin\JobController@addBlukJobs');
    Route::resource('shift', 'Admin\JobController');
    Route::post('/store-shift', 'Admin\JobController@storeJobs');
    Route::get('/shiftData', ['as' => 'shiftsData','uses' => 'Admin\JobController@datatable']);

    // Profession
    Route::resource('profession', 'Admin\ProfessionController');
    Route::get('/professionData', ['as' => 'professionData','uses' => 'Admin\ProfessionController@datatable']);

    // Package
    Route::resource('package', 'Admin\PackageController');
    Route::get('/packageData', ['as' => 'packageData','uses' => 'Admin\PackageController@datatable']);
    Route::get('package/generate-stripe-plan/{id}', 'Admin\PackageController@generateStripePlan');

    // Order
    Route::resource('order', 'Admin\OrderController');
    Route::get('/orderData', ['as' => 'orderData','uses' => 'Admin\OrderController@datatable']);

    // Speciality
    Route::resource('speciality', 'Admin\SpecialityController');
    Route::get('/specialityData', ['as' => 'specialityData','uses' => 'Admin\SpecialityController@datatable']);

    // Degree
    Route::resource('degree', 'Admin\DegreeController');
    Route::get('/degreeData', ['as' => 'degreeData','uses' => 'Admin\DegreeController@datatable']);

    // crud generator
    Route::get('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
    Route::post('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

});
	
Route::get('/renew', function(){
   Artisan::call('RenewalSubscriptionCommand:renewalSubscriptionCommand');
});

Route::get('/reminderUser', function(){
   Artisan::call('ReminderCommand:reminderCommand');
});

Route::get('/notification', function(){
   Artisan::call('NotificationCommand:notificationCommand');
});



//======================================================================================================================
// routes not in use now


// Route::get('linkedin', function () {
//     return view('linkedinAuth');
// });
// Route::get('auth/linkedin', 'Auth\AuthController@redirectToLinkedin');
// Route::get('auth/linkedin/callback', 'Auth\AuthController@handleLinkedinCallback');

// Route::group(array('middleware' => 'setting'), function() {
//     Route::get('/price', 'HomeController@pricing');
// });

// Route::group(['middleware' => ['auth','plandata', 'not_admin', 'setting']], function () {
//     Route::group(['middleware' => ['expiredPlan']], function () {
//         // Recommanded list
//         Route::get('/dashboard/{id}', 'HomeController@show');
//         Route::get('/dashboardData', ['as' => 'dashboardData','uses' => 'HomeController@datatable']);
//         Route::get('/dashboard', 'HomeController@dashboard');
//         Route::get('/billing/subscription', 'SubscriptionController@subscription');
//         Route::get('/billing/history', 'SubscriptionController@history');
//         // Jobs
//         Route::resource('jobs', 'JobController');
//         Route::get('/jobsData', ['as' => 'jobsData','uses' => 'JobController@datatable']);
//         //favourit
//         Route::get('/favourite/{id}', 'FavouriteController@index');
//         Route::get('/favourite/view/{job_id}/{id}', 'FavouriteController@show');
//         Route::get('/favourite-data/', ['as' => 'favouriteData','uses' => 'FavouriteController@datatable']);
//     });
// });
