<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeDetail extends Model
{
    //
    protected $table = 'employee_detail';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'uid', 'emp_name', 'comp_name', 'comp_website', 'comp_information', 'tel_number', 'address','status','profile_pic' ,'location_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    public function user(){
        return $this->hasOne('App\User','id','uid')->select( 'id', 'name' ,'fname' ,'lname' ,'email');
    }


    public function location(){
        return $this->hasOne('App\State','id','location_id')->select( 'id', 'name');
    }

    public function jobs(){
        return $this->hasMany('App\JobDetail','employee_id', 'uid');
    }

    public function locations(){
        return $this->hasMany('App\Locations','field_id','id')->where( 'type', 'employee_detail')->where('location_id', '!=', 0);
    }

    public function getCommaSepLocNamesAttribute(){
        
        $loc_arr = [] ;
        foreach($this->locations as  $loc){
            $loc_arr[] =  $loc->name ;
        }
        return  implode(', ',$loc_arr) ; 
    } 

    public function getProfileImageUrlAttribute(){

        $image = url('/assets/images/avatar.jpg') ;
        if($this->profile_pic != ""){
            if( file_exists( public_path('/images/employee/'.$this->profile_pic)  ) ){
                $image  = url('/images/employee/'.$this->profile_pic) ;
            }
        }
        return $image ;
    }

}
