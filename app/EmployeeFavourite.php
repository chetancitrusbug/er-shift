<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class EmployeeFavourite extends Model
{
    protected $table = 'favourite_employee_details';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'doctor_wish_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id')->select('id', 'name');
    }
    public function employee()
    {
        return $this->belongsTo('App\EmployeeDetail','user_id','uid')->select('id', 'uid', 'emp_name', 'comp_name', 'comp_website','comp_information','tel_number','location_id','address',
        DB::raw('(CASE WHEN employee_detail.profile_pic != "" THEN CONCAT( "'.url('').'",CONCAT("/images/employee/"),employee_detail.profile_pic) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS profile_pic'));
    }
    public function doctor_wish()
    {
        return $this->belongsTo('App\DoctorWish','doctor_wish_id','id');
    }
}
