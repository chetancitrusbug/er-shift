<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\HasRoles;
class DoctorDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use HasRoles;
    protected $fillable = [
        'uid', 'profile_image', 'profile_name',  'address', 'tel_number', 'resume_type', 'resume', 'profession_id', 
'speciality_id', 'experience_id', 'degree_id', 'license_id' ,'status' , 'location_id' ,  'resume_link' , 'resume_attachment'  ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function speciality(){
        return $this->belongsTo('App\Speciality');
    }

    public function profession(){
        return $this->belongsTo('App\Profession');
    }

    public function experience(){
        return $this->belongsTo('App\Experience');
    }
    public function user(){
        return $this->hasOne('App\User','id','uid')->select( 'id', 'name' ,'fname' ,'lname' ,'email','is_active');
    }
    public function location(){
        return $this->belongsTo('App\State','location_id','id')->select( 'id', 'name');
    }

    public function license(){
        return $this->belongsTo('App\State','license_id','id')->select( 'id', 'name');
    }

    public function degree(){
        return $this->belongsTo('App\Degree')->select( 'id', 'title');
    }
   
    public function doctor_wishes(){
        return $this->hasMany('App\DoctorWish','doctor_id');
    }

    public function locations(){
        return $this->hasMany('App\Locations','field_id','id')->where( 'type', 'doctor_details')->where('location_id', '!=', 0);
    }

    public function getCommaSepLocNamesAttribute(){
        
        $loc_arr = [] ;
        foreach($this->locations as  $loc){
            $loc_arr[] =  $loc->name ;
        }
        return  implode(', ',$loc_arr) ; 
    }

    public function getProfileImageUrlAttribute(){

        $image = url('/assets/images/avatar.jpg') ;
        if($this->profile_image != ""){
            try {
                if( getimagesize( config('setting.DOCTOR_URL').'/images/doctor/'.$this->profile_image  ) ){
                    $image  = config('setting.DOCTOR_URL').'/images/doctor/'.$this->profile_image ;
                }
            } catch (\Throwable $th) {
                //throw $th;
                \Log::info($th);
            }

        }
        return $image ;
    }


}
