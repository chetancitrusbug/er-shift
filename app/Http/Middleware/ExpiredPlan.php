<?php

namespace App\Http\Middleware;
use Auth;
use App\Subscription;
use Session;
use Closure;

class ExpiredPlan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $subscriptions = Subscription::where('user_id',Auth::user()->id)->where('status',1)->latest()->first();
        if($subscriptions == null)
        {
            return redirect('billing/plans')->withErrors( __('message.subscription.no_active_package_purchase_new_plan') );
        }
        return $next($request);
    }
}
