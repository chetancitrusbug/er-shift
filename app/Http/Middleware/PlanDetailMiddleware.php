<?php

namespace App\Http\Middleware;

use App\Favourite;
use App\JobDetail;
use App\Subscription;
use Auth;
use Closure;

class PlanDetailMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $isfree = 0;
        $expire_in_days = 0;
        $now = \Carbon\Carbon::now()->toDateString();
        $plan = null;
        if (Auth::user()) {
            
            $subscription = Subscription::with('packageDetail')->where('status', 1)->where('user_id', Auth::user()->id)->latest()->first();
           
            $totaljob = JobDetail::where('status', 1)->where('employee_id', Auth::user()->id)->get()->count();

            $totalfavourit = JobDetail::
                            join('favourite', 'job_detail.id', '=', 'favourite.job_id')
                            ->groupBy('job_detail.id')
                            ->where('job_detail.employee_id', Auth::user()->id)->get()->count();
            
            if ($subscription) {
                
                if ($subscription->package_detail != null) {
                    $plan = json_decode($subscription->package_detail);
                    $isfree =1;
                } else {
                    $plan = null;
                    $isfree = 0;
                }
                $enddate = \Carbon\Carbon::parse($subscription->end_date);
                $expire_in_days = $enddate->diffInDays($now) - 1;
            }
            else {
                $plan = null;
                $isfree = 0;
                $expire_in_days =  0;
            }
            //dd($plan);
            view()->composer('*', function ($view) use ($plan, $isfree, $subscription, $totalfavourit, $totaljob,$expire_in_days) {
                $view->with(compact('plan', 'isfree', 'subscription', 'totaljob', 'totalfavourit','expire_in_days'));
            });

        }
        $request->merge(array("expire_in_days" => $expire_in_days));

        return $next($request);
    }
}
 