<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if(Auth::user()->hasRole('SU')){
                return redirect('/admin'); 
            }
            else if(Auth::user()->user_type == "employee"){
                  return redirect('/shifts');
            }
            else
            { 
                Session::flash('flash_error', __('message.login.invalid_credential') );
                Session::flash('is_active_error', 'Yes');    
                Auth::logout();
            }
        }

        return $next($request);
    }
}
