<?php

namespace App\Http\Middleware;

use App\Setting;
use Closure;

class SettingMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $Setting = Setting::where('key','home_video')->first();
        $home_video = ((($Setting) && ($Setting->value != null)) ? $Setting->value : '');
        $Setting = Setting::where('key','android_url')->first();
        $android_url = ((($Setting) && ($Setting->value != null)) ? $Setting->value : '');
        $Setting = Setting::where('key', 'ios_url')->first();
        $ios_url = ((($Setting) && ($Setting->value != null)) ? $Setting->value : '');
        $Setting = Setting::where('key', 'facebook_url')->first();
        $facebook_url = ((($Setting) && ($Setting->value != null)) ? $Setting->value : '');
        $Setting = Setting::where('key', 'google_url')->first();
        $google_url = ((($Setting) && ($Setting->value != null)) ? $Setting->value : '');
        $Setting = Setting::where('key', 'twitter_url')->first();
        $twitter_url = ((($Setting) && ($Setting->value != null)) ? $Setting->value : '');
        $Setting = Setting::where('key', 'instagram_url')->first();
        $instagram_url = ((($Setting) && ($Setting->value != null)) ? $Setting->value : '');
        $Setting = Setting::where('key', 'linkedin_url')->first();
        $linkedin_url = ((($Setting) && ($Setting->value != null)) ? $Setting->value : '');

        view()->composer('*', function ($view) use ($home_video,$android_url,$ios_url,$linkedin_url,$instagram_url,$twitter_url,$google_url,$facebook_url) {
            $view->with(compact('home_video','android_url','ios_url','linkedin_url','instagram_url','twitter_url','google_url','facebook_url'));
        });
        return $next($request);
    }
}
