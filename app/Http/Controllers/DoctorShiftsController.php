<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DoctorWish ;
use App\State;
use App\Shift;
use App\Profession;
use App\Edvolume;
use App\Subscription;
use Session;
use Carbon\Carbon;
use Auth;

class DoctorShiftsController extends Controller
{
    //
    public function index(Request $request){

        if(Auth::check()){
            $subscriptions = Subscription::where('user_id',Auth::user()->id)->where('status',1)->latest()->first();
            if($subscriptions == null)
            {
                return redirect('billing/plans')->withErrors( __('message.subscription.no_active_package_purchase_new_plan') );
            }
        }

        $locations = State::select('id','name')->where('status',1)->pluck('name','id');

        $professions = Profession::where('status', 1)->pluck('title','id');

        $edvolumes = Edvolume::where('status', 1)->pluck('title','id');

        $all_shifts =  DoctorWish::whereHas('doctor_user')->with('edvolume','doctor_user', 'locations' , 'date_shifts');

        if ($request->has('doctor_type') &&   in_array( $request->doctor_type ,array_keys( config('setting.doctor_type')) )  ) {

            $prof_ids = Profession::where('title','LIKE',"%".config('setting.doctor_type.'.$request->doctor_type)."%" )->get()->pluck('id');

            $all_shifts = $all_shifts->whereHas('doctor_user', function ($q) use ($request) {
                $q->whereIn('doctor_details.profession_id', $prof_ids );     
           });
        }

        if ($request->has('professions') &&   $request->professions > 0  ) {

            $all_shifts = $all_shifts->whereHas('doctor_user', function ($q) use ($request) {
                $q->where('doctor_details.profession_id', $request->professions );     
           });
        }


        if ($request->has('title') && !empty($request->get('title'))) {

            $all_shifts = $all_shifts->where(function($q)use($request){
                $q->where('wish_title', 'LIKE', "%$request->title%")
                ->orWhere('wish_desc', 'LIKE', "%$request->title%")
                ->orWhereHas('doctor_user', function ($q) use ($request) {
                    $q->where('doctor_details.profile_name', 'LIKE' , "%$request->title%" )
                    ->orWhere('doctor_details.address', 'LIKE' , "%$request->title%" );     
               });
                
            });
        }

        if ($request->has('location') && !empty($request->get('location'))) {

            $all_shifts = $all_shifts->whereHas('locations', function ($q) use ($request) {
                $q->where('location.location_id', $request->location);
            });
        }

       

        if ($request->has('edvolume_id') && !empty($request->get('edvolume_id'))) {

            $all_shifts = $all_shifts->where('ed_volum_id', $request->edvolume_id) ;
        }

        if($request->has('from_date') && !empty($request->get('from_date'))  && empty($request->get('to_date')) ){

            $from_date = Carbon::createfromFormat('m/d/Y',$request->get('from_date'))->format('Y-m-d') ;

            $all_shifts = $all_shifts->whereHas('date_shifts', function ($q) use ($from_date) {
                $q->whereDate('date_shift.from', '>=', $from_date);
            });

        }
        if ($request->has('to_date') && !empty($request->get('to_date')) && empty($request->get('from_date')) ) {

            $to_date = Carbon::createfromFormat('m/d/Y',$request->get('to_date'))->format('Y-m-d') ;

            $all_shifts = $all_shifts->whereHas('date_shifts', function ($q) use ($to_date) {
                $q->whereDate('date_shift.to', '<=', $to_date);
            });
        }

        if ($request->has('from_date') && !empty($request->get('from_date')) && $request->has('to_date') && !empty($request->get('to_date'))) {

            $from_date = Carbon::createfromFormat('m/d/Y', $request->get('from_date'))->format('Y-m-d') ;
            $to_date = Carbon::createfromFormat('m/d/Y', $request->get('to_date'))->format('Y-m-d') ;

            $all_shifts = $all_shifts->whereHas('date_shifts', function ($q) use ($from_date, $to_date) {
                $q->whereDate('date_shift.from', '>=', $from_date)
                ->whereDate('date_shift.from', '<=', $to_date)
                ->whereDate('date_shift.to', '>=', $from_date)
                ->whereDate('date_shift.to', '<=', $to_date);
            });
        }

        if ($request->has('rate') && !empty($request->get('rate')) && $request->rate != 0 ) {

            $all_shifts = $all_shifts->where('rate', '>=', floatval($request->rate));
        }

        $all_shifts = $all_shifts->where('status', 1) ;

        if ($request->has('sort') && !empty($request->get('sort'))) {
            $sort = 'ASC';
            if ($request->get('sort') == 'high') {
                $sort = 'DESC';
            }
            $all_shifts->orderBy('rate',$sort);
        } else {
            $all_shifts->latest();
        }

        $total = $all_shifts->count() ;

        $all_shifts = $all_shifts->orderBy('id','desc')->paginate(10);

        return view('frontend-new.doctor-shifts.index', compact('all_shifts','locations','professions','edvolumes' ,'total'));
    }

    public function show($id){

        $shift = DoctorWish::where('id', $id)->whereHas('doctor_user')->with('doctor_user','edvolume')->first();

        $recent_shifts = DoctorWish::whereHas('doctor_user')->with('doctor_user','edvolume')->latest()->limit(3)->get();

        if(!$shift){
            return back()->withErrors( __('message.doctor_shift.provider_details_not_found') );
        }
        return view('frontend-new.doctor-shifts.view', compact('shift','recent_shifts') );
    }

}
