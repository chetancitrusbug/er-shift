<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeDetail;
use Auth;
use App\User;
use App\State;
use Hash;
use Session;

class ProfileController extends Controller
{
    public function index(){
        $emp_deatil = EmployeeDetail::with('user','location')->where('uid',Auth::user()->id)->first();
        if(file_exists(public_path('images/employee/'.$emp_deatil->profile_pic)) && ($emp_deatil->profile_pic != null))
        {
            $emp_deatil->profile_pic = url('').'/images/employee/'.$emp_deatil->profile_pic;
        }
        else
        {
            $emp_deatil->profile_pic = '';
        }
        return view('frontend.profile.index', compact('emp_deatil'));
    }
    public function edit()
    {   
        $locations = State::select('id','name')->where('status',1)->pluck('name','id');

        $user = Auth::user();
        $employee = $user ? $user->EmployeeDetail : [] ;

        if($user && $employee){
           
            return view('frontend-new.profile.edit', compact('locations', 'user', 'employee'));
        }
        return redirect()->back();
       
    }
    public function update(Request $request){

        $user = Auth::user();
        $employee = $user ? $user->employeeDetail : [] ;

        if ($user && $employee) {
  
            $requestData = $request->all();
            $rules = array(
                'fname' => 'required|max:50',
                'lname' => 'required|max:50',
                'emp_name'=>'required|max:50',
                'comp_name'=>'required|max:100',
                'location'=>'required',
                'profile_pic'=>'sometimes|mimes:jpg,jpeg,png|max:10000',
            );
            
            if (isset($requestData['comp_website']) && ($requestData['comp_website'] != '')) {
                $rules['comp_website'] = 'required|regex:/^(https?:\/\/)?([\dA-Za-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
            }
            $this->validate($request, $rules);
            
            $data = $request->except('_token');
            $data['fname'] = $request->fname;
            $data['lname'] = $request->lname;
            $data['name'] = $request->fname.' '.$request->lname;
            $user->update($data);

            $input = array();
            $input['emp_name']=$request->emp_name;
            $input['comp_name']=$request->comp_name;
            $input['comp_website']=$request->comp_website;
            $input['comp_information']=$request->comp_information;
            $input['tel_number'] = $request->tel_number;
            $input['address'] = $request->address;
            $input['location_id'] =  $request->location;

            if ($request->file('profile_pic')) {
                $image = $request->file('profile_pic');
                $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('images/employee/'), $filename);
                $input['profile_pic'] = $filename;
            }


            $employee->update($input);
            return redirect('profile/edit')->withStatus(  __('message.profile.profile_updated') );
            
        }

        return redirect()->back();
    
   }
    public function changePassword()
    {
        return view('frontend-new.profile.change-password');
    }
    public function updatePassword(Request $request)
    {

        $messages = [
            'current_password.required' => __('Please enter current password'),
            'password_confirmation.same' => __('The confirm password and new password must match.'),
        ];

        $this->validate($request,
            [
                'current_password' => 'required',
                'password' => 'required|min:6|max:255',
                'password_confirmation' => 'required|same:password',
            ], $messages);


        $cur_password = $request->input('current_password');


        $user = Auth::user();
 
        if (Hash::check($cur_password, $user->password)) {
            if (Hash::check($request->input('password'),$user->password)) {
                Session::flash('flash_message', __('message.profile.entered_same_password_while_change') );
                return redirect()->back()->withInfo( __('message.profile.entered_same_password_while_change') );
            }
            else
            {
                $user->password = Hash::make($request->input('password'));
                $user->save();
                Session::flash('flash_message', __('message.profile.password_changed') );
                return redirect()->back()->with('status' , __('message.profile.password_changed') );
            }
        } else {
            $error = array('current-password' => __('message.profile.enter_correct_current_password'));
            return redirect()->back()->withErrors($error);
        }
        Session::flash('flash_message', __('message.profile.something_went_wrong')  );
        return redirect()->back();
    }
}
