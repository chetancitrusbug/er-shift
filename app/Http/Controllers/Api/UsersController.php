<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use App\DoctorDetail;
use App\Education;
use App\Experience;
use App\EmployeeDetail;
use App\DoctorWish;
use App\State;
use App\Profession;
use App\Speciality;
use App\Degree;
use App\Shift;
use App\Edvolume;
use App\Priority;
use App\JobDetail;
use App\Subscription;
use App\Package;
use Illuminate\Http\Request;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use DB;
use Carbon;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;
class UsersController extends Controller
{
     public function register(Request $request){

        $data = array();
        $code = 200;
        $messages = 'Registration Successful, Please Check your email for activate your account.';
        $status = 'true';
        $error = '';

        if($request->login_type == 'simple') {
            $rules = array(
                'user_type'=>'required|in:employee,doctor',
                'login_type'=>'required',
                'device_token'=>'required',
                'device_type'=>'required',
                'email'=>'required|email',
                'password'=>'required',
                'fname'=>'required',
                'lname'=>'required',
            );

            $validator = \Validator::make($request->all(), $rules, []);
            if ($validator->fails()) {
                $validation = $validator;
                $status = false;
                $code = 400;
                $msgArr = $validator->messages()->toArray();
                $messages = reset($msgArr)[0];

            } else {
                $user_type = $request->user_type;
                $login_type = $request->login_type;
                $email = $request->email;
                $device_token = $request->device_token;
                $device_type = $request->device_type;

                $user = User::where('email',$request->email)->where('user_type',$request->user_type)->first();
                if($user){
                    $messages = 'Email already exists, please use another emaill address';
                    $code = 400;
                    $status = 'false';
                }
                else {
                    $user['api_token'] = md5(uniqid());
                    $user['user_type'] = $request->user_type;
                    $user['login_type']= $request->login_type;
                    $user['email'] = $request->email;
                    $user['name'] = $request->fname.' '.$request->lname;
                    $user['fname'] = $request->fname;
                    $user['lname'] = $request->lname;
                    $user['is_active'] = 0;
                    $user['activation_token'] = sha1(time() . uniqid() . $user['email']);
                    $user['activation_time'] = \Carbon\Carbon::now();
                    $user['password'] = Hash::make($request->password);
                    $user['device_token'] = $request->device_token;
                    $user['device_type'] = $request->device_type;
                    $user['social_media_id'] = '';
                    $user['is_status'] = 0;
                    if($request->firebasetoken == null)
                    {
                        $user['firebasetoken'] = '';
                    }
                    else {
                        $user['firebasetoken'] = $request->firebasetoken;
                    }
                    $userResult = User::create($user);
                    if($request->user_type == 'doctor')
                    {
                        $this->subscribeMailChimp($user['email'],'doctor');
                    }
                    else
                    {
                        $this->subscribeMailChimp($user['email'],'employee');
                    }
                    $stripe = Stripe::make(config('services.stripe.secret'));
                    if($userResult->stripe_id=="" || !$userResult->stripe_id){

                        $customer = $stripe->customers()->create([
                            'email' => $userResult->email,
                        ]);

                          if($customer && isset($customer['id'])){
                            $userResult->stripe_id = $customer['id'];
                            $userResult->save();
                        }
                    }
                    $userDetails['uid'] = $userResult->id;
                    if($request->phone_no == null)
                    {
                        $userDetails['tel_number'] = '';
                    }
                    else {
                        $userDetails['tel_number'] = $request->phone_no;
                    }
                    if($request->user_type == 'doctor')
                    {
                        $userDetails['profile_image'] = '';
                        $userDetails['profile_name'] = $request->fname.' '.$request->lname;
                        $userDetails['address'] = '';
                        $userDetails['resume_type'] = '';
                        $userDetails['resume'] = '';
                        $userDetails['profession_id'] = '0';
                        $userDetails['speciality_id'] = '0';
                        $userDetails['experience_id'] = '0';
                        $userDetails['degree_id'] = '';
                        $userDetails['license_id'] = '';
                        $userDetails['location_id'] = '';
                        DoctorDetail::create($userDetails);
                    }
                    else
                    {
                        $userDetails['profile_pic'] = '';
                        $userDetails['emp_name'] = $request->fname.' '.$request->lname;
                        $userDetails['comp_name'] = '';
                        $userDetails['comp_website'] = '';
                        $userDetails['comp_information'] = '';
                        $userDetails['address'] = '';
                        $userDetails['location_id'] = '';
                        //dd($userDetails);
                        EmployeeDetail::create($userDetails);

                        // Free package set
                        $package = Package::where('isFree', 1)->first();
                        if($package != null)
                        {
                            $subScriptionDeatils['user_id'] = $userResult->id;
                            $subScriptionDeatils['package'] = $package->id;
                            $subScriptionDeatils['amount'] = 0;
                            $subScriptionDeatils['start_date'] = date('Y-m-d');
                            $subScriptionDeatils['end_date'] = date('Y-m-d', strtotime(date('Y-m-d'). ' + '.$package->day.' days'));
                            $subScriptionDeatils['subscription_id'] = 0;
                            $subScriptionDeatils['status'] = 1;
                            Subscription::create($subScriptionDeatils);
                        }
                    }

                    $userResult->notify(new  \App\Notifications\ActivationLink($userResult));
                    $userResult->notify(new  \App\Notifications\Welcome($userResult));
                    $userResult = User::find($userResult->id) ;
                    $data = $this->responseData($userResult,$request->user_type);
                    $messages = 'Registration Successful, Please Check Email to activate your account.';
                    //$code = 200;
                    $code = 201;
                }
            }
        }
        elseif ($request->login_type == 'facebook' || $request->login_type == 'google' || $request->login_type == 'linkedIn') {
            $rules = array(
                'user_type'=>'required|in:employee,doctor',
                'device_token'=>'required',
                'device_type'=>'required',
                'social_media_id'=>'required',
                'fname'=>'required',
                'lname'=>'required',
            );
            if (isset($request->user_type) && ($request->user_type == 'employee')) {
                $rules['login_type'] = 'required|in:linkedIn';
            }
            elseif (isset($request->user_type) && ($request->user_type == 'doctor')) {
                $rules['login_type'] = 'required|in:facebook,google,linkedIn';
            }
            if ($request->login_type == 'facebook')
            {
                if (!isset($request->email) && !isset($request->phone_no))
                {
                    $rules['phone_no'] = 'required';
                    $rules['email'] = 'required|email';
                }
            }
            $validator = \Validator::make($request->all(), $rules, []);
            if ($validator->fails()) {
                $validation = $validator;
                $status = false;
                $code = 400;
                $msgArr = $validator->messages()->toArray();
                $messages = reset($msgArr)[0];
            } else {
                $user = User::where('user_type',$request->user_type);
                if ($request->login_type != 'facebook')
                {
                    $user = $user->where('email',$request->email);
                }
                else{
                    $user = $user->where('social_media_id',$request->social_media_id);

                    /*if (isset($request->phone_no))
                    {
                        $user = $user->where('phone_no',$request->phone_no);
                    }*/
                }
                $user = $user->first();
                if($user){
                    if(($user->login_type == $request->login_type) && ($user->social_media_id == $request->social_media_id))
                    {
                        $user->api_token = md5(uniqid());
                        $user->fname = $request->fname;
                        $user->lname = $request->lname;
                        $user->device_token = $request->device_token;
                        $user->device_type = $request->device_type;
                        if($request->firebasetoken != null)
                        {
                            $user->firebasetoken = $request->firebasetoken;
                        }
                        if ($request->login_type != 'facebook')
                        {
                            $user->email = $request->email;
                        }

                        $user->update();
                        if ($request->login_type != 'facebook')
                        {
                            $stripe = Stripe::make(config('services.stripe.secret'));
                            if($user->stripe_id=="" || !$user->stripe_id){

                                $customer = $stripe->customers()->create([
                                    'email' => $user->email,
                                ]);

                                if($customer && isset($customer['id'])){
                                    $user->stripe_id = $customer['id'];
                                    $user->save();
                                }
                            }
                        }

                        if($request->phone_no != null)
                        {
                            $userDetails['tel_number'] = $request->phone_no;

                            if($request->user_type == 'doctor')
                            {
                                $DoctorDetail = DoctorDetail::select('doctor_details.*',
                                DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "'.url('').'",CONCAT("/images/doctor/"),doctor_details.profile_image) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS profile_image')
                                )->where('uid',$user->id)->first();
                                $DoctorDetail->update($userDetails);
                            }
                            else
                            {
                                $EmployeeDetail = EmployeeDetail::select('employee_detail.*',
                                DB::raw('(CASE WHEN employee_detail.profile_pic != "" THEN CONCAT( "'.url('').'",CONCAT("/images/employee/"),employee_detail.profile_pic) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS profile_pic')
                                )->where('uid',$user->id)->first();
                                $EmployeeDetail->update($userDetails);
                            }
                        }


                        $data = $this->responseData($user,$request->user_type);

                        $messages = 'Successful login	';
                    }
                    else
                    {
                        if($user->login_type != $request->login_type)
                        {
                            $messages = 'Please Choose different type of login.';
                        }
                        else if($user->social_media_id != $request->social_media_id)
                        {
                            $messages = 'You had already login with this email with onother Social Media.';
                        }
                        else{
                            $messages = 'Email/ User already exists, please use another emaill address	';

                        }
                        $code = 400;
                        $status = 'false';
                    }
                }
                else {
                    $user = array();
                    $user['api_token'] = md5(uniqid());
                    $user['user_type'] = $request->user_type;
                    $user['login_type']= $request->login_type;
                    $user['name'] = $request->fname.' '.$request->lname;
                    $user['fname'] = $request->fname;
                    $user['lname'] = $request->lname;
                    $user['is_active'] = 1;
                    $user['activation_time'] = \Carbon\Carbon::now();
                    $user['social_media_id'] = $request->social_media_id;
                    $user['activation_token'] = sha1(time() . uniqid() . $user['social_media_id']);
                    $user['device_token'] = $request->device_token;
                    $user['device_type'] = $request->device_type;
                    $user['password'] = '';
                    if($request->firebasetoken != null)
                    {
                        $user['firebasetoken']  = $request->firebasetoken;
                    }
                    else
                    {
                        $user['firebasetoken']  = '';
                    }
                    if ($request->login_type != 'facebook')
                    {
                        $user['email'] = $request->email;
                    }
                    else
                    {
                        $user['email']  = '';
                    }
                    $userResult = User::create($user);
                    if(isset($request->email))
                    {
                        if($request->user_type == 'doctor')
                        {
                            $this->subscribeMailChimp($user['email'],'doctor');
                        }
                        else
                        {
                            $this->subscribeMailChimp($user['email'],'employee');
                        }
                    }


                    if ($request->login_type != 'facebook')
                    {
                        $stripe = Stripe::make(config('services.stripe.secret'));
                        if($userResult->stripe_id=="" || !$userResult->stripe_id){

                            $customer = $stripe->customers()->create([
                                'email' => $userResult->email,
                            ]);

                            if($customer && isset($customer['id'])){
                                $userResult->stripe_id = $customer['id'];
                                $userResult->save();
                            }
                        }
                    }
                    $userDetails['uid'] = $userResult->id;
                    if($request->phone_no != null)
                    {
                        $userDetails['tel_number'] = $request->phone_no;
                    }
                    else
                    {
                        $userDetails['tel_number'] = '';
                    }
                    if($request->user_type == 'doctor')
                    {
                        $userDetails['profile_image'] = '';
                        $userDetails['profile_name'] = $request->fname.' '.$request->lname;
                        $userDetails['address'] = '';
                        $userDetails['resume_type'] = 'link';
                        $userDetails['resume'] = '';
                        $userDetails['profession_id'] = '0';
                        $userDetails['speciality_id'] = '0';
                        $userDetails['experience_id'] = '0';
                        $userDetails['degree_id'] = '';
                        $userDetails['license_id'] = '';
                        $userDetails['location_id'] = '';
                        DoctorDetail::create($userDetails);
                    }
                    else
                    {
                        $userDetails['profile_pic'] = '';
                        $userDetails['emp_name'] = $request->fname.' '.$request->lname;
                        $userDetails['comp_name'] = '';
                        $userDetails['comp_website'] = '';
                        $userDetails['comp_information'] = '';
                        $userDetails['address'] = '';
                        $userDetails['location_id'] = '';
                        EmployeeDetail::create($userDetails);

                        // Free package set
                        $package = Package::where('isFree', 1)->first();
                        if($package != null)
                        {
                            $subScriptionDeatils['user_id'] = $userResult->id;
                            $subScriptionDeatils['package'] = $package->id;
                            $subScriptionDeatils['amount'] = 0;
                            $subScriptionDeatils['start_date'] = date('Y-m-d');
                            $subScriptionDeatils['end_date'] = date('Y-m-d', strtotime(date('Y-m-d'). ' + '.$package->day.' days'));
                            $subScriptionDeatils['subscription_id'] = 0;
                            $subScriptionDeatils['status'] = 1;
                            Subscription::create($subScriptionDeatils);
                        }

                    }
                    if ($request->login_type != 'facebook')
                    {
                        //Welcome Mail Send
                       $this->welcomeMail($user);



                        // \Mail::send('email.welcome', compact('user'), function ($message) use ($user) {
                        //     $message
                        //     ->to($user['email'])
                        //     ->subject('Welcome to Er-Shift');
                        // });
                    }
                    $userResult->notify(new  \App\Notifications\Welcome($userResult));
                    $data = $this->responseData($userResult,$request->user_type);
                    $messages = 'Registration Successfully';
                }
            }
        }
        else {
            if($request->login_type != '') {
                $status = false;
                $code = 400;
                $messages = 'Login type not match.';
            }
            else {
                $status = false;
                $code = 400;
                $messages = 'The login type field is required.';
            }
        }
        return response()->json(['result'=>$data,'code'=>$code,'message'=>$messages,'status'=>$status]);
        exit;
     }

    public function responseData($user,$user_type) {
        $responseArr = array();
        $user_id = $user->id;
        unset($user->id);

        $responseArr['user'] = $user;
        $responseArr['user']['user_id'] = $user_id;
        if($user_type == 'doctor')
        {
            $DoctorDetail = DoctorDetail::select('doctor_details.*',
            DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "'.url('').'",CONCAT("/images/doctor/"),doctor_details.profile_image) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS profile_image')
            )->where('uid',$user_id)->first();
            if($DoctorDetail != null){
                unset($DoctorDetail->id,$DoctorDetail->created_at,$DoctorDetail->updated_at);
                $DoctorDetail = $this->doctorResponseDetails($DoctorDetail);
                $responseArr['user']['details'] = $DoctorDetail;
            }
            else
            {
                $responseArr['user']['details'] = [];
            }
        }
        else
        {
            $subscriptions = Subscription::where('user_id',$user_id)->where('status',1)->latest()->first();
            $responseArr['packageStatus'] = (($subscriptions != null) ? 1 : 0);
            $responseArr['current_date'] = date('Y-m-d');
            $responseArr['expiry_date'] = ((($subscriptions != null) && ($subscriptions->end_date)) ? $subscriptions->end_date : '');
            $EmployeeDetail = EmployeeDetail::select('employee_detail.*',
            DB::raw('(CASE WHEN employee_detail.profile_pic != "" THEN CONCAT( "'.url('').'",CONCAT("/images/employee/"),employee_detail.profile_pic) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS profile_pic')
            )->where('uid',$user_id)->first();
            if($EmployeeDetail != null){
                unset($EmployeeDetail->id,$EmployeeDetail->created_at,$EmployeeDetail->updated_at);
                $responseArr['user']['details'] = $EmployeeDetail;
            }
            else
            {
                $responseArr['user']['details'] = [];
            }
        }
        return $responseArr;
    }

    public function login(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Successful login	';
        $status = 'true';
        $error = '';

        $user_type = $request->user_type;
        $login_type = $request->login_type;
        $email = $request->email;
        $password = $request->password;
        $device_token = $request->device_token;
        $device_type = $request->device_type;
        $rules = array(
            'user_type'=>'required',
            'login_type'=>'required',
            'device_token'=>'required',
            'device_type'=>'required',
            'email'=>'required|email',
            'password'=>'required',
        );
        $validator = \Validator::make($request->all(), $rules, []);

        //dd($validator->passes(), $validator->messages()->toArray());
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

        } else {
            if($login_type == 'simple')
            {
                $user = User::where('email',$email)->where('user_type',$user_type)->where('login_type',$login_type)->first();
                if($user){
                    if($user->is_active == 1)
                    {
                        if(Hash::check($password, $user->password)){
                            $user->api_token = md5(uniqid());
                            $user->device_token = $request->device_token;
                            $user->device_type = $device_type;
                            if($request->firebasetoken != null)
                            {
                                $user->firebasetoken = $request->firebasetoken;
                            }
                            $user->save();
                            $data = $this->responseData($user,$request->user_type);
                            $messages = 'Successful login';
                        }else{
                            $messages = 'Password is incorrect';
                            $code = 400;
                            $status = 'false';
                        }
                    }else{
                        $messages = 'Account is inactive.Please activate your account.';
                        $code = 400;
                        $status = 'false';
                    }
                }else{
                    $messages = 'This email does not exist. Please register';
                    $code = 400;
                    $status = 'false';
                }
            }
            else
            {
                $messages = 'Only simple login';
                $code = 400;
                $status = 'false';
            }
        }
        return response()->json(['result'=>$data,'code'=>$code,'message'=>$messages,'status'=>$status]);
        exit;
    }

    public function doctorResponseDetails($DoctorDetail){


        if($DoctorDetail != null)
        {
            $user_id = $DoctorDetail->uid;
            if($DoctorDetail->resume_type == 'attachment')
            {
                if(file_exists(public_path('images/doctor/pdf/'.$DoctorDetail->resume)) && ($DoctorDetail->resume != null))
                {
                    $DoctorDetail->resume = url('') .'images/doctor/pdf/'.$DoctorDetail->resume;
                }
                else
                {
                    $DoctorDetail->resume = '';
                }
            }
            if($DoctorDetail->profession_id != null)
            {
                $Profession = Profession::where('id',$DoctorDetail->profession_id)->first();
                $DoctorDetail->profession_id = (($Profession != null ) ? $Profession->title : '');
            }
            else
            {
                $DoctorDetail->profession_id = '';
            }

            if($DoctorDetail->speciality_id != null)
            {
                $Speciality = Speciality::where('id',$DoctorDetail->speciality_id)->first();
                $DoctorDetail->speciality_id = (($Speciality != null ) ? $Speciality->title : '');
            }
            else
            {
                $DoctorDetail->speciality_id = '';
            }

            if($DoctorDetail->experience_id != null)
            {
                $Experience = Experience::where('id',$DoctorDetail->experience_id)->first();
                $DoctorDetail->experience_id = (($Experience != null ) ? $Experience->title : '');
            }
            else
            {
                $DoctorDetail->experience_id = '';
            }

            if($DoctorDetail->degree_id != null)
            {
                $arr = explode(',',$DoctorDetail->degree_id);
                $str = '';
                foreach ($arr as $key1 => $value1) {
                    $Degree = Degree::where('id',$value1)->first();
                    if($Degree != null)
                    $str .= (($str != null ) ? ','.$Degree->title : $Degree->title);
                }
                $DoctorDetail->degree_id = $str;
            }
            else
            {
                $DoctorDetail->degree_id = '';
            }

            if($DoctorDetail->license_id != null)
            {
                $arr = explode(',',$DoctorDetail->license_id);
                $str = '';
                foreach ($arr as $key1 => $value1) {
                    $State = State::where('id',$value1)->first();
                    if($State != null)
                    $str .= (($str != null ) ? ','.$State->name : $State->name);
                }
                $DoctorDetail->license_id = $str;
            }
            else
            {
                $DoctorDetail->license_id = '';
            }

        }
        return $DoctorDetail;
    }

    public function logout(Request $request) {
        $data = [];
        $message = "";

        $code = 200;
        $status = true;
        if ($request->has('api_token')) {
            $api_token = $request->get('api_token');
            User::where("api_token", $api_token)->update(["device_token" => "","device_type" => "","firebasetoken" => "","api_token" => ""]);
        }
        $message = "Logout successfully";
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function forgotPassword(Request $request){
        $data = array();
        $code = 200;
        $messages = '';
        $status = 'true';
        $error = '';
        $email = $request->email;
        $user_type = $request->user_type;
        $user = User::where('email',$email)->where('user_type',$user_type)->where('is_active','!=',0)->first();

        if($user)
        {
            $response = Password::sendResetLink($request->only('email'), function (Message $message) {
                $message->subject($this->getEmailSubject());
            });
            if ($response == "passwords.sent") {
                $messages = 'Password reset link has been sent to your registered email';
            } else if ($response == "passwords.user") {
                $status = false;
                $messages = 'User not found';
                $code = 400;
            } else {
                $status = false;
                $messages = 'Something went wrong ! Please try again later';
                $code = 400;
            }
        }
        else{
            $messages = 'Email/ User does not exist. Please register ';
            $code = 400;
            $status = 'false';
        }


        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function changePassword(Request $request){
        $data = [];
        $messages = "";
        $status = true;

        $code = 200;
        $id = $request->user_id;
        $rules = array(
            'password' => 'required|min:6|max:255',
            'password_confirmation' => 'required|same:password',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::where("id", $id)->first();
            //dd($user);
            if ($user == null) {
                $messages = 'User not Found';
				$status = false;
				$code=400;
            }
            else{
                if($user->login_type == 'simple')
                {
                    if (Hash::check($request->input('password'),$user->password)) {
                        $messages = 'Current password and new password are the same	';
                        $status = false;
                        $code=400;
                    }
                    else{
                        $user->password = Hash::make($request->input('password'));
                        $user->save();
                        $messages = 'Password changed successfully.';
                    }
                }
                else{
                    $messages = 'Only for simple login';
                    $status = false;
                    $code=400;
                }
            }
        }

		return response()->json(['status'=> $status,'result' => $data, 'message' => $messages,'code'=>$code]);
        exit;
    }
}
