<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\State;
use App\DoctorDetail;
use App\EmployeeDetail;
use App\DoctorWish;
use DB;
use App\JobDetail;
use App\Setting;
use App\Subscription;
use App\JobDates;

class SearchController extends Controller
{
    public function recommended_job(Request $request)
    {

        $result = [];
        $message = "success";
        $status = true;
        $code = 200;
        $validation = [];
        $rules = array(
            'user_type' => 'required',
            'api_token' => 'required',
            'user_id' => 'required|integer',
        );
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            if ($request->user_type == "doctor") {
                $user = User::where('id', $request->user_id)
                    ->where('user_type', $request->user_type)
                    ->first();
                if ($user) {
                    $Setting = Setting::where('key','default_paginate')->first();
                    $perPage = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 10);
                    $doctorDetail = DoctorDetail::where('uid', $request->user_id)
                        ->first();
                            /*$SearchDetail = DoctorWish::
                                        join('job_detail', 'doctor_wish.ed_volum_id', '=', 'job_detail.ed_volume_id')
                                        //->with('favourite','profession','speciality','degree','experience','ed_volume','shift','employee')
                                    //  ->whereRaw('doctor_wish.ed_volum_id = job_detail.ed_volume_id and doctor_wish.rate = job_detail.rate and doctor_wish.shift_id = job_detail.shift_id')
                                        ->whereRaw('job_detail.location_id In (doctor_wish.location_id) ')
                                        ->where('doctor_wish.status',1)
                                        ->paginate($perPage);*/
                    $employeeWishId = $this->recommendedGetEmployeeWishId($request->user_id);
                     $SearchDetail = JobDetail::with('favourite', 'profession', 'speciality', 'degree', 'experience', 'edvolume', 'shift')
                    //     ->join('doctor_wish', 'job_detail.ed_volume_id', '=', 'doctor_wish.ed_volum_id')
                         ->leftjoin('employee_detail', 'employee_detail.uid', '=', 'job_detail.employee_id')
                         ->leftjoin('users', 'users.id', '=', 'employee_detail.uid')
                        ->select('job_detail.*','job_detail.id as job_id','employee_detail.comp_name as company_name',
                         DB::raw('(CASE WHEN employee_detail.profile_pic != "" THEN CONCAT( "'.url('').'",CONCAT("/images/employee/"),employee_detail.profile_pic) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS employee_image')
                         ,DB::raw('(CASE WHEN employee_detail.emp_name != "" THEN employee_detail.emp_name ELSE users.name END) AS employeeName')
                );
                if($employeeWishId != ''){
                    $SearchDetail->whereIn('job_detail.id', $employeeWishId);
                }else{
                    $SearchDetail->whereRaw('job_detail.location_id = '. $doctorDetail->location_id);
                }
                    //     ->whereRaw('job_detail.location_id In (doctor_wish.location_id) ')
                    //     ->where('doctor_wish.status', 1)
                    //     ->groupBy('job_detail.id')
                    $SearchDetail->where('job_detail.status', 1);
                    $SearchDetail->orderBy('job_detail.id','desc');
                    $SearchDetail = $SearchDetail->paginate(
                             $perPage
                         );
                                            /*$SearchDetail = $SearchDetail->unique();
                        dd($SearchDetail); */  
                
                    if (($SearchDetail)) {
                        foreach ($SearchDetail as $key => $value) {
                            if ($value->location_id != null) {
                                $arr = explode(',', $value->location_id);
                                $str = '';
                                foreach ($arr as $key1 => $value1) {
                                    $State = State::select('id', 'name')->where('id', $value1)->first();
                                    if ($State != null)
                                        $str .= (($str != null ) ? ','.$State->name : $State->name);;
                                }
                                $SearchDetail[$key]->location = $str;
                            } else {
                                $SearchDetail[$key]->location = '';
                            }

                            $SearchDetail[$key]->edvolume_title = ((($SearchDetail[$key]->edvolume) && ($SearchDetail[$key]->edvolume->title != null )) ? $SearchDetail[$key]->edvolume->title : '');
                            $SearchDetail[$key]->profession_title = ((($SearchDetail[$key]->profession) && ($SearchDetail[$key]->profession->title != null )) ? $SearchDetail[$key]->profession->title : '');
                            
                            $SearchDetail[$key]->speciality_title = ((($SearchDetail[$key]->speciality) && ($SearchDetail[$key]->speciality->title != null )) ? $SearchDetail[$key]->speciality->title : '');

                            $SearchDetail[$key]->experience_title = ((($SearchDetail[$key]->experience) && ($SearchDetail[$key]->experience->title != null )) ? $SearchDetail[$key]->experience->title : '');
                            $SearchDetail[$key]->degree_title = ((($SearchDetail[$key]->degree) && ($SearchDetail[$key]->degree->title != null )) ? $SearchDetail[$key]->degree->title : '');
                            $SearchDetail[$key]->shift_title = ((($SearchDetail[$key]->shift) && ($SearchDetail[$key]->shift->title != null )) ? $SearchDetail[$key]->shift->title : '');


                            unset($value->degree,$value->experience,$value->shift,$value->profession,$value->speciality,$value->ed_volume,$value->favourite,$value->created_at, $value->updated_at, $value->deleted_at);
                            
                        }
                        
                        $result = $SearchDetail->toArray();
                        $result['next_page_url'] = (($result['next_page_url'] == null) ? '' : $result['next_page_url']);
                        $result['prev_page_url'] = (($result['prev_page_url'] == null) ? '' : $result['prev_page_url']);
                        
                        $message = 'success';
                    } else {
                        $message = 'No Data Found';
                        $code = 400;
                        $status = 'false';
                    }
                }
            } 
            elseif ($request->user_type == "employee") {
                $user = User::where('id', $request->user_id)
                    ->where('user_type', $request->user_type)
                    ->first();
                if ($user) {
                    $EmployeeDetail = EmployeeDetail::where('uid', $request->user_id)
                        ->first();
                    $Setting = Setting::where('key','default_paginate')->first();
                    $perPage = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 10);
                    //'job_detail.location_id' , '=' , 'doctor_wish.location_id'
                                                /*->whereRaw('job_detail.location_id In (doctor_wish.location_id) and 
                                                job_detail.rate = doctor_wish.rate and job_detail.shift_id = doctor_wish.shift_id') */

                    /*$SearchDetail = JobDetail::select('doctor_wish.*')->with('favourite', 'profession', 'speciality', 'degree', 'experience', 'ed_volume', 'shift', 'employee')
                        ->join('doctor_wish', 'job_detail.ed_volume_id', '=', 'doctor_wish.ed_volum_id')
                    
                        ->whereRaw('job_detail.location_id In (doctor_wish.location_id) ')
                        ->where('job_detail.status', 1)
                        ->groupBy('doctor_wish.id')
                        ->paginate($perPage);*/

                    $doctorWishId = $this->recommendedGetDoctorWishId($request->user_id);

                    $SearchDetail = DoctorWish::select(
                        'doctor_wish.*',
                        DB::raw('(CASE WHEN doctor_details.profile_name != "" THEN doctor_details.profile_name ELSE users.name END) AS doctorName'),
                        DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "' . url('') . '",CONCAT("/images/doctor/"),doctor_details.profile_image) ELSE "' . url('/assets/images/avatar.jpg') . '" END) AS doctorImage')
                    )->with('edvolume')
                        ->join('users', 'users.id', '=', 'doctor_wish.doctor_id')
                        ->join('doctor_details', 'doctor_details.uid', '=', 'doctor_wish.doctor_id');

                    if($doctorWishId != ''){
                        $SearchDetail->whereIn('doctor_wish.id', $doctorWishId);
                    }else{
                        $SearchDetail->where('doctor_wish.location_id','=' , $EmployeeDetail->location_id);
                    }
                    $SearchDetail->where('doctor_wish.status', 1);
                    $SearchDetail->orderBy('doctor_wish.id','desc');
                    $SearchDetail  = $SearchDetail->paginate($perPage);

                        // $SearchDetail = DoctorWish::select('doctor_wish.*',
                        // DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "'.url('').'",CONCAT("/images/doctor/"),doctor_details.profile_image) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS doctorImage')
                        // ,DB::raw('(CASE WHEN doctor_details.profile_name != "" THEN doctor_details.profile_name ELSE users.name END) AS doctorName')
                        // )->with('edvolume','shift')
                        // ->join('users', 'users.id', '=', 'doctor_wish.doctor_id')
                        // ->join('doctor_details', 'doctor_details.uid', '=', 'doctor_wish.doctor_id')
                        // ->join('job_detail', 'job_detail.ed_volume_id', '=', 'doctor_wish.ed_volum_id')
                        // ->whereRaw('job_detail.location_id In (doctor_wish.location_id) ')
                        // ->where('job_detail.status', 1)
                        // ->groupBy('doctor_wish.id')
                        // ->paginate($perPage);

                    if ($SearchDetail != null) {
                        foreach ($SearchDetail as $key => $value) {
                            $SearchDetail[$key]->doctor_wish_id = $value->id;
                            if ($value->location_id != null) {
                                $arr = explode(',', $value->location_id);
                                $str = '';
                                foreach ($arr as $key1 => $value1) {
                                    $State = State::select('id', 'name')->where('id', $value1)->first();
                                    if ($State != null)
                                        $str .= (($str != null ) ? ','.$State->name : $State->name);;
                                }
                                $SearchDetail[$key]->location = $str;
                            } else {
                                $SearchDetail[$key]->location = '';
                            }

                            
                            $SearchDetail[$key]->edvolume_title = ((($SearchDetail[$key]->edvolume) && ($SearchDetail[$key]->edvolume->title != null )) ? $SearchDetail[$key]->edvolume->title : '');
                            
                            unset($value->edvolume,$value->created_at, $value->updated_at, $value->deleted_at);
                            
                        }
                        
                        $result = $SearchDetail->toArray();
                        $result['next_page_url'] = (($result['next_page_url'] == null) ? '' : $result['next_page_url']);
                        $result['prev_page_url'] = (($result['prev_page_url'] == null) ? '' : $result['prev_page_url']);
                        $subscriptions = Subscription::where('user_id',$request->user_id)->where('status',1)->latest()->first();
                        $result['packageStatus'] = (($subscriptions != null) ? 1 : 0);
                        $result['current_date'] = date('Y-m-d');
                        $result['expiry_date'] = ((($subscriptions != null) && ($subscriptions->end_date)) ? $subscriptions->end_date : '');
                        

                        $message = 'success';
                    } else {
                        $message = 'No Data Found';
                        $code = 400;
                        $status = 'false';
                    }
                }
            } else {
                $status = false;
                $message = 'User type must be employee and doctor';
                $code = 400;
            }
        }


        return response()->json(['result' => $result, 'status' => $status, 'message' => $message, 'code' => $code]);

    }
    public function search(Request $request)
    {
        $result = [];
        $message = "success";
        $status = true;
        $code = 200;
        $validation = [];
        $rules = array(
            'user_type' => 'required',
            'api_token' => 'required',
            'user_id' => 'required|integer',
        );
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
           
            $user = User::where('id', $request->user_id)->where('user_type', $request->user_type)->first();

            $Setting = Setting::where('key','default_paginate')->first();
            $perPage = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 10);

            if(!$user){
                $status = false;
                $message = 'User must be employee or doctor';
                $code = 400;
            }else{

                if($user->user_type == 'doctor'){

                    $SearchDetail = JobDetail::select('job_detail.*','job_dates.job_date')->whereHas('employee')->with('favourite', 'profession', 'speciality', 'degree', 'experience', 'edvolume', 'shift', 'employee')
                    ->leftjoin('job_dates', 'job_dates.job_id', 'job_detail.id');
                    
                  
                    if($request->keyword != ''){
                        $SearchDetail = $SearchDetail->where(function($q)use($request){
                            $q->where('job_detail.job_title', 'LIKE', "%$request->keyword%")
                            ->orWhere('job_detail.job_responsibility', 'LIKE', "%$request->keyword%");
                        });
                    }
                    if($request->location_id != '' && $request->location_id != 0){
                        $SearchDetail = $SearchDetail->where('job_detail.location_id', $request->location_id);
                    }
                    if($request->shift_id != '' &&  $request->shift_id != 0 ){
                        $SearchDetail = $SearchDetail->where('job_detail.shift_id', $request->shift_id);
                    }
                    if($request->from_date != '' && $request->from_date != null){
                        $SearchDetail = $SearchDetail->whereDate('job_dates.job_date', $request->from_date);
                    }
                    if ($request->to_date != '' && $request->to_date != null) {
                        $SearchDetail =  $SearchDetail->whereDate('job_dates.job_date', $request->to_date);
                    }
                    if ($request->from_date != '' && $request->to_date != '') {
                        $SearchDetail = $SearchDetail->whereBetween('job_dates.job_date', [$request->from_date , $request->to_date ]);
                    }
				// 	if($request->rate != '' && $request->rate != 0 && floatval($request->rate) != 10.0){
    //                     $SearchDetail = $SearchDetail->where(function($q)use($request){
    //                         $q->where('job_detail.rate', '<=', floatval($request->rate)+10)
    //                         ->where('job_detail.rate','>=', floatval( $request->rate)-10);  
    //                     });
				// 	}
				   if($request->rate != '' ){
                        $SearchDetail = $SearchDetail->where('job_detail.rate', '>', floatval($request->rate));    
					}
                    $SearchDetail = $SearchDetail->where('job_detail.status', 1)->groupBy('job_detail.id')->orderBy('job_detail.id','desc')->paginate($perPage);

                    if (($SearchDetail)) {
                        foreach ($SearchDetail as $key => $value) {
                            $SearchDetail[$key]->job_id = $value->id;
                            $SearchDetail[$key]->location = '';
                            if ($value->location_id != null) {
                                $arr = explode(',', $value->location_id);
                                $str = '';
                                foreach ($arr as $key1 => $value1) {
                                    $State = State::select('id', 'name')->where('id', $value1)->first();
                                    if ($State != null)
                                        $str .= (($str != null ) ? ','.$State->name : $State->name);;
                                }
                                $SearchDetail[$key]->location = $str;
                            }
                            $SearchDetail[$key]->edvolume_title = ((($SearchDetail[$key]->edvolume) && ($SearchDetail[$key]->edvolume->title != null )) ? $SearchDetail[$key]->edvolume->title : '');
                            $SearchDetail[$key]->profession_title = ((($SearchDetail[$key]->profession) && ($SearchDetail[$key]->profession->title != null )) ? $SearchDetail[$key]->profession->title : '');
                            
                            $SearchDetail[$key]->speciality_title = ((($SearchDetail[$key]->speciality) && ($SearchDetail[$key]->speciality->title != null )) ? $SearchDetail[$key]->speciality->title : '');

                            $SearchDetail[$key]->experience_title = ((($SearchDetail[$key]->experience) && ($SearchDetail[$key]->experience->title != null )) ? $SearchDetail[$key]->experience->title : '');
                            $SearchDetail[$key]->degree_title = ((($SearchDetail[$key]->degree) && ($SearchDetail[$key]->degree->title != null )) ? $SearchDetail[$key]->degree->title : '');
                            $SearchDetail[$key]->shift_title = ((($SearchDetail[$key]->shift) && ($SearchDetail[$key]->shift->title != null )) ? $SearchDetail[$key]->shift->title : '');

                            unset($value->degree,$value->experience,$value->shift,$value->profession,$value->speciality,$value->ed_volume,$value->favourite,$value->created_at, $value->updated_at, $value->deleted_at);
                        }
                        $result = $SearchDetail->toArray();
                        $result['next_page_url'] = (($result['next_page_url'] == null) ? '' : $result['next_page_url']);
                        $result['prev_page_url'] = (($result['prev_page_url'] == null) ? '' : $result['prev_page_url']);
                        
                        $message = 'success';
                    } else {
                        $message = 'No Data Found';
                        $code = 400;
                        $status = 'false';
                    }
                
                }elseif($user->user_type == 'employee'){

                        $SearchDetail = DoctorWish::whereHas('doctor')->with('doctor','date_shifts','locations');

                    if($request->keyword != ''){
                        $SearchDetail = $SearchDetail->where(function($q)use($request){
                            $q->where('wish_title', 'LIKE', "%$request->keyword%")
                            ->orWhere('wish_desc', 'LIKE', "%$request->keyword%");
                        });
                    }
                    if($request->location_id != '' && $request->location_id != 0 ){
                        
                        // $SearchDetail = $SearchDetail->where('doctor_wish.location_id', 'LIKE', "%$request->location_id%");
                        $SearchDetail = $SearchDetail->whereHas('locations', function ($q) use ($request) {
                             $q->where('location.location_id', $request->location_id);
                        });
                        
                    }
                    if($request->shift_id != '' && $request->shift_id != 0  ){
                        // $SearchDetail = $SearchDetail->where('date_shift.shift_id', $request->shift_id);
                        $SearchDetail = $SearchDetail->whereHas('date_shifts', function ($q) use ($request) {
                            $q->where('date_shift.shift_id', $request->shift_id);
                       });
                    }
                    if($request->from_date != '' && $request->from_date != null){
                        // $SearchDetail = $SearchDetail->whereDate('date_shift.from', '>=', $request->from_date);
                        $SearchDetail = $SearchDetail->whereHas('date_shifts', function ($q) use ($request) {
                            $q->whereDate('date_shift.from', '>=', $request->from_date);
                        });
                       
                    }
                    if ($request->to_date != '' && $request->to_date != '') {
                        // $SearchDetail = $SearchDetail->orWhereDate('date_shift.to', '<=', $request->to_date);
                        $SearchDetail = $SearchDetail->whereHas('date_shifts', function ($q) use ($request) {
                            $q->orWhereDate('date_shift.to', '<=', $request->to_date);
                        });
                    }
                    if ($request->to_date != '' && $request->from_date != '') {           
                        $SearchDetail = $SearchDetail->whereHas('date_shifts', function ($q) use ($request) {
                            $q->whereDate('date_shift.from', '>=', $request->from_date)
                            ->whereDate('date_shift.from', '<=', $request->to_date)
                            ->whereDate('date_shift.to', '>=', $request->from_date)
                            ->whereDate('date_shift.to', '<=', $request->to_date);
                        });
                    }
                    // if($request->rate != '' && floatval($request->rate) != 10.0 ){
                    //     $SearchDetail = $SearchDetail->where(function($q)use($request){
                    //         $q->where('rate', '<=', floatval($request->rate)+10)
                    //         ->where('rate','>=', floatval( $request->rate)-10);  
                    //     });   
                    // }
                    
                    if($request->rate != ''){
                        $SearchDetail = $SearchDetail->where('rate', '>', floatval($request->rate));
                    }
                    
                    $SearchDetail =  $SearchDetail->where('status', 1)->orderBy('id','desc')->paginate($perPage);

                    if (($SearchDetail)) { 
                        foreach ($SearchDetail as $key => $value) {
                            $SearchDetail[$key]->location =  $SearchDetail[$key]->comma_sep_loc_names ;
                            // if ($value->location_id != null) {
                            //     $arr = explode(',', $value->location_id);
                            //     $str = '';
                            //     foreach ($arr as $key1 => $value1) {
                            //         $State = State::select('id', 'name')->where('id', $value1)->first();
                            //         if ($State != null)
                            //             $str .= (($str != null ) ? ','.$State->name : $State->name);;
                            //     }
                            //     $SearchDetail[$key]->location = $str;
                            // } else {
                            //     $SearchDetail[$key]->location = '';
                            // }
                            $SearchDetail[$key]->edvolume_title = ((($SearchDetail[$key]->edvolume) && ($SearchDetail[$key]->edvolume->title != null )) ? $SearchDetail[$key]->edvolume->title : '');
                            
                            unset($value->locations,$value->edvolume,$value->created_at, $value->updated_at, $value->deleted_at); 
                        }
                        $result = $SearchDetail->toArray();
                        $result['next_page_url'] = (($result['next_page_url'] == null) ? '' : $result['next_page_url']);
                        $result['prev_page_url'] = (($result['prev_page_url'] == null) ? '' : $result['prev_page_url']);
                        $subscriptions = Subscription::where('user_id',$request->user_id)->where('status',1)->first();
        
                        $result['current_date'] = date('Y-m-d');
                        $result['expiry_date'] = ((($subscriptions != null) && ($subscriptions->end_date)) ? $subscriptions->end_date : '');
                        
                        $message = 'success';
                    } else {
                        $message = 'No Data Found';
                        $code = 400;
                        $status = 'false';
                    }

                }               
                
            }
           
        }
        return response()->json(['result' => $result, 'status' => $status, 'message' => $message, 'code' => $code]);

    }



}
