<?php

namespace App\Http\Controllers\Api;
use App\User;
use App\JobDetail;
use App\DoctorWish;
use App\EmployeeFavourite;
use App\Favourite;
use App\DoctorDetail;
use App\EmployeeDetail;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class FavouriteController extends Controller
{
    public function addDeleteFavourite(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Saved successfully';
        $status = 'true';

        $rules = array(
            'user_type'=>'required|in:doctor',
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
        );
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

        } else {
            $job_detail = JobDetail::find($request->job_id);
            $user = User::where('id',$request->user_id)->where('user_type',$request->user_type)->first();

            if($job_detail != null && $user != null){
                $favouriteDetails = Favourite::where('job_id',$request->job_id)->where('user_type',$request->user_type)->where('user_id',$request->user_id)->first();
                if($favouriteDetails){
                    $favouriteDetails = $favouriteDetails->delete();
                    $messages = 'Saved item deleted ';
                }
                else {
                    $favourite = Favourite::create($request->all());
                }

            }else{
                $messages = 'Shift/User not found';
                $code = 400;
                $status = 'false';
            }
        }
        return response()->json(['result'=>$data,'code'=>$code,'message'=>$messages,'status'=>$status]);
        exit;
    }

    public function listFavourite(Request $request){

        $result = [];
        $message = "success";
        $status = true;
        $code = 200;
        $requestData = $request->all();
        $rules = array(
            'user_id'=>'required|integer',
            'user_type'=> 'required|in:employee,doctor'
        );
        if (isset($requestData['user_type']) && ($requestData['user_type'] == 'employee')) {
            $rules['job_id'] = 'required|integer';
        }
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];

        } else {
            if($request->user_type == 'doctor')
            {
                $user = User::where('id',$request->user_id)->where('user_type',$request->user_type)->first();

                if($user){
                    $Setting = Setting::where('key','default_paginate')->first();
                    $perPage = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 10);
                    $Favourite = Favourite::with('job')->select('favourite.*','favourite.id as favourite_id')->where('user_id',$request->user_id)->paginate($perPage);

                    if(count($Favourite) > 0){
                        $result = $Favourite->toArray();
                        $result['next_page_url'] = (($result['next_page_url'] == null) ? '' : $result['next_page_url']);
                        $result['prev_page_url'] = (($result['prev_page_url'] == null) ? '' : $result['prev_page_url']);
                    }else{
                        $message = 'Favorite not found';
                        $code = 400;
                        $status = 'false';
                    }
                }else{
                    $message = 'User not found';
                    $code = 400;
                    $status = 'false';
                }
            }
            else{
                $user = User::where('id',$request->user_id)->where('user_type',$request->user_type)->first();

                if($user){
                    $Setting = Setting::where('key','default_paginate')->first();
                    $perPage = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 10);
                    $Favourite = Favourite::select('favourite.*','favourite.id as favourite_id')->with('user','doctor')->where('job_id',$request->job_id)->paginate($perPage);

                    if(count($Favourite) > 0){
                        $result = $Favourite->toArray();
                        $result['next_page_url'] = (($result['next_page_url'] == null) ? '' : $result['next_page_url']);
                        $result['prev_page_url'] = (($result['prev_page_url'] == null) ? '' : $result['prev_page_url']);
                    }else{
                        $message = 'Saved Shift not found';
                        $code = 400;
                        $status = 'false';
                    }
                }else{
                    $message = 'User not found';
                    $code = 400;
                    $status = 'false';
                }

            }
        }
        return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=> $code]);

    }

    public function addDeleteEmployeeFavourite(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Saved successfully';
        $status = 'true';

        $rules = array(
            'user_id'=>'required|integer',
            'doctor_wish_id'=>'required|integer',
        );
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

        } else {
            $DoctorWish = DoctorWish::find($request->doctor_wish_id);
            $user = User::where('id',$request->user_id)->where('user_type','employee')->first();

            if($DoctorWish != null && $user != null){
                $EmployeeFavourite = EmployeeFavourite::where('doctor_wish_id',$request->doctor_wish_id)->where('user_id',$request->user_id)->first();
                if($EmployeeFavourite){
                    $EmployeeFavourite = $EmployeeFavourite->delete();
                    $messages = 'Saved item deleted ';
                }
                else {
                    $data = EmployeeFavourite::create($request->all());
                }

            }else{
                if($DoctorWish == null && $user == null)
                $messages = 'Wish/User not found';
                else if($user == null)
                $messages = 'User not found';
                else
                $messages = 'Doctor Wish not found';
                $code = 400;
                $status = 'false';
            }
        }
        return response()->json(['result'=>$data,'code'=>$code,'message'=>$messages,'status'=>$status]);
        exit;
    }

    public function listFavouriteEmployee(Request $request){

        $result = [];
        $message = "success";
        $status = true;
        $code = 200;
        $requestData = $request->all();
        $rules = array(
            'user_id'=>'required|integer',
            'user_type'=> 'required|in:employee,doctor'
        );
        if (isset($requestData['user_type']) && ($requestData['user_type'] == 'doctor')) {
            $rules['doctor_wish_id'] = 'required|integer';
        }
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];

        } else {
            if($request->user_type == 'employee')
            {
                $user = User::where('id',$request->user_id)->where('user_type',$request->user_type)->first();

                if($user){
                    $Setting = Setting::where('key','default_paginate')->first();
                    $perPage = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 10);
                    $Favourite = EmployeeFavourite::with('doctor_wish')->where('user_id',$request->user_id)->paginate($perPage);

                    if(count($Favourite) > 0){
                        $result = $Favourite->toArray();
                        $result['next_page_url'] = (($result['next_page_url'] == null) ? '' : $result['next_page_url']);
                        $result['prev_page_url'] = (($result['prev_page_url'] == null) ? '' : $result['prev_page_url']);
                    }else{
                        $message = 'Favorite not found';
                        $code = 400;
                        $status = 'false';
                    }
                }else{
                    $message = 'User not found';
                    $code = 400;
                    $status = 'false';
                }
            }
            else{
                $user = User::where('id',$request->user_id)->where('user_type',$request->user_type)->first();

                if($user){
                    $Setting = Setting::where('key','default_paginate')->first();
                    $perPage = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 10);
                    $Favourite = EmployeeFavourite::with('employee','user')->where('doctor_wish_id',$request->doctor_wish_id)->paginate($perPage);

                    if(count($Favourite) > 0){
                        $result = $Favourite->toArray();
                        $result['next_page_url'] = (($result['next_page_url'] == null) ? '' : $result['next_page_url']);
                        $result['prev_page_url'] = (($result['prev_page_url'] == null) ? '' : $result['prev_page_url']);
                    }else{
                        $message = 'Saved Shift not found';
                        $code = 400;
                        $status = 'false';
                    }
                }else{
                    $message = 'User not found';
                    $code = 400;
                    $status = 'false';
                }

            }
        }
        return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=> $code]);

    }
}