<?php

namespace App\Http\Controllers\Api;
use App\Subscription;
use App\EmployeeDetail;
use App\Package;
use App\User;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    public function cancelSubscription(Request $request){       
        $result = [];
        $message = "Subscription Package cancelled sucessfully";
        $status = true;
        $code = 200;  
        $rules = array(
            'user_id'=>'required|integer',
            'package_id'=>'required|integer',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;

        } else {
            $user = User::where('id',$request->user_id)->first();
            $EmployeeDetail = EmployeeDetail::where('uid',$request->user_id)->first();
            if($EmployeeDetail != null && $user != null){
                $requestData = $request->all();
                $Package = Package::where('id',$request->package_id)->first();
                
                if($Package != null && $Package->isFree != 1){
                    $Subscription = Subscription::where('user_id',$request->user_id)->where('package',$request->package_id)->where('status',1)->first();
                    //dd($Subscription);
                    if($Subscription != null)
                    {
                        $stripe = Stripe::make(config('services.stripe.secret'));
                        try {
                            $canSub = $stripe->subscriptions()->cancel($user->stripe_id, $Subscription->subscription_id);
                            $updateRequestData = array(
                                'status' => 2);
                            $Subscription->update($updateRequestData);
                        }
                        catch (\Exception $e) {
                            $message = $e->getMessage();
                            $code = 400;
                            $status = 'false';
                        }
                    }
                    else{
                        $message = "No Subscription Found, Might Be Deleted";
                        $code = 400;
                        $status = false;
                    }
                }
                else{
                    $message = 'No Package Found, Might Be Deleted';
                    $code = 400;
                    $status = false;
                }
            }
            else{
                $message = 'No User Found';
                $code = 400;
                $status = false;
            }
        }
        return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
}
