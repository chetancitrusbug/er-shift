<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\DoctorDetail;
use App\EmployeeDetail;
use App\JobDetail;

class HomeController extends Controller
{

    public function index(Request $request){

        $result = [];
        $message = "success";
        $status = true;
        $code = 200;

        if ($request->doctor_id) {
            $speciality = '';
            $doctor_detail = DoctorDetail::with('speciality','experience')->find($request->doctor_id);

            if($doctor_detail){
                if($doctor_detail->speciality){
                    $speciality =  $doctor_detail->speciality->title;
                }
                $jobs = JobDetail::Where('special_looking_for', 'LIKE', "%$speciality%")
                                ->orWhere('experience_id' , $doctor_detail->experience->id)
                                ->orWhere('job_address', 'LIKE', "%$doctor_detail->address%")->get();


                if(count($jobs) > 0){

                    $result = $jobs;
                    $message = 'success';
                }
                else{

                    $message = 'No Recommended Shifts Found';
                    $code = 400;
                    $status = 'false';
                }
            }
            else {
                $status = false;
                $message = 'No Doctor Found';
                $code = 400;
            }


        } else {
            $status = false;
            $message = 'Not valid data';
            $code = 400;
        }
        return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function employeeJobList(Request $request){

        $result = [];
        $message = "success";
        $status = true;
        $code = 200;

        if ($request->employee_id) {
            $speciality = '';
            $EmployeeDetail = EmployeeDetail::find($request->employee_id);

            if($EmployeeDetail){
                if($EmployeeDetail->speciality){
                    $speciality =  $EmployeeDetail->speciality->title;
                }
                $jobs = JobDetail::Where('employee_id', $request->employee_id)
                                ->orWhere('experience_id' , $doctor_detail->experience->id)
                                ->orWhere('job_address', 'LIKE', "%$doctor_detail->address%")->get();


                if(count($jobs) > 0){

                    $result = $jobs;
                    $message = 'success';
                }
                else{

                    $message = 'No Recommended Shifts Found';
                    $code = 400;
                    $status = 'false';
                }
            }
            else {
                $status = false;
                $message = 'No employee found';
                $code = 400;
            }


        } else {
            $status = false;
            $message = 'Not valid data';
            $code = 400;
        }
        return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
}
