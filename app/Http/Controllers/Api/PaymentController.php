<?php

namespace App\Http\Controllers\Api;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscription;
use App\Order;
use App\Package;

use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;
use App\Http\Controllers\EmailController;
use App\Billingcycle;

class PaymentController extends Controller
{
    private $mail_function;
    public function __construct()
    {
       $this->mail_function = new EmailController();
    }
    public function DoPayment(Request $request)
    {
        $result = [];
        $message = "success";
        $status = true;
        $code = 200;  
       
        $rules=array(
            'user_id'=>'integer|required',
            'package_id'=>'integer|required',
            'token_id'=>'required',
            'stripe_id'=>'required',
         );
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $package = Package::where("id",$request->package_id)->where('status',1)->first();
                if(!$package){
                    $message = 'No Package Found';
                    $code = 400;
                    $status = 'false';
                }
                else
                {
                    $requestData = [];
                    $requestData['user_id']=$request->user_id;
                    $requestData['package']=$request->package_id;
                    $requestData['amount']=$package->amount;
                    $requestData['status']=3;
                    $requestData['start_date']= Carbon\Carbon::now(); 
                    $requestData['end_date']=Carbon\Carbon::now();  
                    $requestData['package_detail']=  json_encode($package);
                    $requestData['plan_id']= $package->id;
                    $billing = Subscription::create($requestData);

                    $billings = Subscription::with('packageDetail')->where("id", $billing->id)->first();
                    if(!$billings){
                        $message = 'No Subscription Found';
                        $code = 400;
                        $status = 'false';
                    }
                    else{
                        $stripe = Stripe::make(config('services.stripe.secret'));
                        try {
                            
                            $subscriptionsData = $stripe->subscriptions()->all($request->stripe_id);
                            if(isset($subscriptionsData) && $subscriptionsData != null && count($subscriptionsData['data']) > 0)
                            {
                                foreach ($subscriptionsData['data'] as $key => $value) {
    
                                    try {
                                        $cancelled = $stripe->subscriptions()->cancel($request->stripe_id, $value['id']);
                                        $CancelSubscription = Subscription::where("subscription_id", $value['id'])->first();
                                        if($CancelSubscription != null &&  $cancelled )
                                        {
                                            $update['status'] = 2;
                                            $CancelSubscription->update($update);
                                        }
                                    }  catch (\Exception $e) {
                                        $message = $e->getMessage();
                                        $code = 400;
                                        $status = 'false';
                                    }
                                  
                                }
                            }
    
                        }  catch (\Exception $e) {
                            \Log::info($e);
                            $message = $e->getMessage();
                            $code = 400;
                            $status = 'false';
                        }
                      
                    
                        try {
                            /*$card = $stripe->tokens()->create([
                                'card' => [
                                    'number' => '4242424242424242',
                                    'exp_month' => '12',
                                    'exp_year' => '2023',
                                    'cvc' => '123'
                                ],
                            ]);
                            $stripe->cards()->create($request->stripe_id, $card['id']);*/
                            $stripe->cards()->create($request->stripe_id, $request->token_id);
                            $subscription = $stripe->subscriptions()->create($request->stripe_id, [
                                'plan' => $billings->packageDetail->id,
                            ]);                            
                            
                            if($subscription && isset($subscription['id'])){
                                $billingcyle = new Billingcycle();
                                $billingcyle->stopAllSubscriptionByUserId($request->user_id,$request->stripe_id);
                                
                                $billings->subscription_id =  $subscription['id'];
                                $billings->customer_stripe_id =  $request->stripe_id;
                                $billings->status =  1;
                                $billings->save();
                               
                              //  $this->mail_function->sendMailOnBillingStart($billings->id);
                                $billingcyle = new Billingcycle();
                                $billingcyle->checkBillingCycle();  
                                
                                $result = "Payment Successfully Done..";
                            }
                            else{
                                $message = 'Credential is invalid';
                                $code = 400;
                                $status = 'false';
                            }
                        }
                        catch (\Exception $e) {
                             \Log::info($e);
                            $message = $e->getMessage();
                            $code = 400;
                            $status = 'false';
                        }
                    }
                    
                }
          
       }
        
       return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
}
