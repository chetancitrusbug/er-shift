<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\JobDetail;
use App\EmployeeDetail;
use App\State;
use App\User;
use App\Setting;
use App\Favourite;
use DB;

class DoctorjobController extends Controller
{

    public function view_job(Request $request){

        $result = [];
        $message = "success";
        $status = true;
        $code = 200;
        $requestData = $request->all();
        $valmessages = [
            'job_id.required' => 'Shift field is required.'
        ];
        $rules = array(
            'user_id'=>'required|integer',
            'user_type'=> 'required|in:employee,doctor',
            'job_id'=>'required|integer',
        );

        $validator = \Validator::make($request->all(), $rules, $valmessages);

        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            if($request->user_type == 'doctor')
            {
                $user = User::where('id',$request->user_id)->where('user_type',$request->user_type)->where('is_active',1)->first();
                if($user){
                    $jobDetail = JobDetail::with('favourite', 'profession', 'speciality', 'degree', 'experience', 'ed_volume', 'shift')
                    ->leftjoin('employee_detail', 'employee_detail.uid', '=', 'job_detail.employee_id')
                    ->leftjoin('users', 'users.id', '=', 'employee_detail.uid')
                    ->select('job_detail.*','job_detail.id as job_id','employee_detail.comp_name as company_name',
                    DB::raw('(CASE WHEN employee_detail.profile_pic != "" THEN CONCAT( "'.url('').'",CONCAT("/images/employee/"),employee_detail.profile_pic) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS employee_image')
                    ,DB::raw('(CASE WHEN employee_detail.emp_name != "" THEN employee_detail.emp_name ELSE users.name END) AS employeeName')
                    )
                    ->where('job_detail.id', $request->job_id)
                    ->where('job_detail.status', 1)
                    ->first();

                    if ($jobDetail != null) {
                        if ($jobDetail->location_id != null) {
                            $arr = explode(',', $jobDetail->location_id);
                            $str = '';
                            foreach ($arr as $key1 => $value1) {
                                $State = State::select('id', 'name')->where('id', $value1)->first();
                                if ($State != null)
                                    $str .= (($str != null ) ? ','.$State->name : $State->name);;
                            }
                                $jobDetail->location = $str;
                        } else {
                            $jobDetail->location = '';
                        }
                        $jobDetail->edvolume_title = ((($jobDetail->ed_volume) && ($jobDetail->ed_volume->title != null )) ? $jobDetail->ed_volume->title : '');
                        $jobDetail->profession_title = ((($jobDetail->profession) && ($jobDetail->profession->title != null )) ? $jobDetail->profession->title : '');
                        $jobDetail->speciality_title = ((($jobDetail->speciality) && ($jobDetail->speciality->title != null )) ? $jobDetail->speciality->title : '');
                        $jobDetail->experience_title = ((($jobDetail->experience) && ($jobDetail->experience->title != null )) ? $jobDetail->experience->title : '');
                        $jobDetail->degree_title = ((($jobDetail->degree) && ($jobDetail->degree->title != null )) ? $jobDetail->degree->title : '');
                        $jobDetail->shift_title = ((($jobDetail->shift) && ($jobDetail->shift->title != null )) ? $jobDetail->shift->title : '');
                        $favouriteDetails = Favourite::where('job_id',$request->job_id)->where('user_id',$request->user_id)->first();
                        $jobDetail->save_for_later = (($favouriteDetails != null) ? 1 : 0);

                        unset($jobDetail->degree,$jobDetail->experience,$jobDetail->shift,$jobDetail->profession,$jobDetail->speciality,$jobDetail->ed_volume,$jobDetail->favourite,$jobDetail->created_at, $jobDetail->updated_at, $jobDetail->deleted_at);
                        $result = $jobDetail;
                    }
                    else{
                        $message = 'No Shift found';
                        $code = 400;
                        $status = false;
                    }
                }else{
                    $message = 'User not found';
                    $code = 400;
                    $status = 'false';
                }
            }
            else{
                $user = User::where('id',$request->user_id)->where('user_type',$request->user_type)->where('is_active',1)->first();

                if($user){
                    $jobDetail = JobDetail::with('favourite', 'profession', 'speciality', 'degree', 'experience', 'ed_volume', 'shift')
                    ->leftjoin('employee_detail', 'employee_detail.uid', '=', 'job_detail.employee_id')
                    ->leftjoin('users', 'users.id', '=', 'employee_detail.uid')
                    ->select('job_detail.*','job_detail.id as job_id','employee_detail.comp_name as company_name',
                    DB::raw('(CASE WHEN employee_detail.profile_pic != "" THEN CONCAT( "'.url('').'",CONCAT("/images/employee/"),employee_detail.profile_pic) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS employee_image')
                    ,DB::raw('(CASE WHEN employee_detail.emp_name != "" THEN employee_detail.emp_name ELSE users.name END) AS employeeName')
                    )
                    ->where('job_detail.id', $request->job_id)
                    ->where('job_detail.employee_id', $request->user_id)
                    ->where('job_detail.status', 1)
                    ->first();

                    if ($jobDetail != null) {
                        if ($jobDetail->location_id != null) {
                            $arr = explode(',', $jobDetail->location_id);
                            $str = '';
                            foreach ($arr as $key1 => $value1) {
                                $State = State::select('id', 'name')->where('id', $value1)->first();
                                if ($State != null)
                                    $str .= (($str != null ) ? ','.$State->name : $State->name);;
                            }
                                $jobDetail->location = $str;
                        } else {
                            $jobDetail->location = '';
                        }
                        $jobDetail->edvolume_title = ((($jobDetail->ed_volume) && ($jobDetail->ed_volume->title != null )) ? $jobDetail->ed_volume->title : '');
                        $jobDetail->profession_title = ((($jobDetail->profession) && ($jobDetail->profession->title != null )) ? $jobDetail->profession->title : '');
                        $jobDetail->speciality_title = ((($jobDetail->speciality) && ($jobDetail->speciality->title != null )) ? $jobDetail->speciality->title : '');
                        $jobDetail->experience_title = ((($jobDetail->experience) && ($jobDetail->experience->title != null )) ? $jobDetail->experience->title : '');
                        $jobDetail->degree_title = ((($jobDetail->degree) && ($jobDetail->degree->title != null )) ? $jobDetail->degree->title : '');
                        $jobDetail->shift_title = ((($jobDetail->shift) && ($jobDetail->shift->title != null )) ? $jobDetail->shift->title : '');

                        unset($jobDetail->degree,$jobDetail->experience,$jobDetail->shift,$jobDetail->profession,$jobDetail->speciality,$jobDetail->ed_volume,$jobDetail->favourite,$jobDetail->created_at, $jobDetail->updated_at, $jobDetail->deleted_at);
                        $result = $jobDetail;
                    }
                    else{
                        $message = 'No Shift found, Might be deleted';
                        $code = 400;
                        $status = false;
                    }
                }else{
                    $message = 'User not found';
                    $code = 400;
                    $status = 'false';
                }
            }
        }
        return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=> $code]);

    }

    public function list_job(Request $request){

        $result = [];
        $message = "success";
        $status = true;
        $code = 200;

        $rules = array(
            'employee_id'=>'required|integer',
        );
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;
        } else {
            $EmployeeDetail = EmployeeDetail::where('uid',$request->employee_id)->first();
            if($EmployeeDetail){
                $Setting = Setting::where('key','default_paginate')->first();
                $perPage = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 10);
                $job_detail = JobDetail::with('favourite','profession','speciality','degree','experience','ed_volume','shift','employee')->where('employee_id',$request->employee_id)->where('status', 1)->orderBy('id','desc')->paginate($perPage);
                if(count($job_detail) > 0){
                    foreach ($job_detail as $key => $value) {
                        $job_detail[$key]->job_id =$value->id;
                        $job_detail[$key]->shift_title = (($value->shift != null) ? $value->shift->title : '');
                        $job_detail[$key]->ed_volume_title = (($value->ed_volume != null) ? $value->ed_volume->title : '');

                        $job_detail[$key]->experience_title = (($value->experience != null) ? $value->experience->title : '');
                        $job_detail[$key]->profession_title = (($value->profession != null) ? $value->profession->title : '');
                        $job_detail[$key]->speciality_title = (($value->speciality != null) ? $value->speciality->title : '');
                        $job_detail[$key]->degree_title = (($value->degree != null) ? $value->degree->title : '');
                        $job_detail[$key]->favouriteCount = count($job_detail[$key]->favourite);
                        if($value->location_id != null)
                        {
                            $arr = explode(',',$value->location_id);
                            $str = '';
                            foreach ($arr as $key1 => $value1) {
                                $State = State::select('id','name')->where('id',$value1)->first();
                                if($State != null)
                                $str .= (($str != null ) ? ','.$State->name : $State->name);
                            }
                            $job_detail[$key]->location_title = $str;
                        }
                        else
                        {
                            $job_detail[$key]->location_title = '';
                        }
                        unset($job_detail[$key]->location,$job_detail[$key]->id,$job_detail[$key]->deleted_at,$job_detail[$key]->created_at,$job_detail[$key]->updated_at,$job_detail[$key]->profession,$job_detail[$key]->speciality,$job_detail[$key]->degree,
                        $job_detail[$key]->experience,$job_detail[$key]->ed_volume,$job_detail[$key]->shift
                        ,$job_detail[$key]->employee,$job_detail[$key]->favourite);
                    }
                    $result = $job_detail->toArray();
                    $result['next_page_url'] = (($result['next_page_url'] == null) ? '' : $result['next_page_url']);
                    $result['prev_page_url'] = (($result['prev_page_url'] == null) ? '' : $result['prev_page_url']);
                    $message = 'success';
                }
                else{
                    $message = 'No Shift created yet';
                    $code = 400;
                    $status = 'false';
                }
            }
            else{
                $message = 'No employee found';
                $code = 400;
                $status = false;
            }
        }
        return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function create_job(Request $request){
        $result = [];
        $message = "Shift added successfully";
        $status = true;
        $code = 200;
        $requestData = $request->all();

        $valmessages = [
            'job_title.required' => 'Shift title field is required.',
            'job_responsibility.required' => 'Shift responsibility field is required.'
        ];

        $rules = array(
            'employee_id'=>'required|integer',
            'job_title'=>'required',
            'job_responsibility'=>'required',
            'profession_id'=>'required|integer',
            'speciality_id'=>'sometimes|integer',
            'degree_id'=>'required|integer',
            'experience_id'=>'required|integer',
            'location_id'=>'required|integer',
            'ed_volume_id'=>'required|integer',
            'from_date'=>'required|date|date_format:Y-m-d|after:tomorrow',
            'to_date'=>'required|date|date_format:Y-m-d|after:from_date',
            'shift_id'=>'required|integer',
            'rate'=>'required',
        );

        $validator = \Validator::make($request->all(), $rules, $valmessages);

        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $EmployeeDetail = EmployeeDetail::where('uid',$request->employee_id)->first();
            if($EmployeeDetail){
                $requestData['address'] = (isset($request->address) ? $request->address : '');
                $requestData['speciality_id'] = (isset($request->speciality_id) ? $request->speciality_id : 0);

                $result = JobDetail::create($requestData);
                $result->job_id = $result->id;
                unset($result->deleted_at,$result->id,$result->created_at,$result->updated_at);
            }
            else{
                $message = 'No employee found';
                $code = 400;
                $status = false;
            }
        }
        return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function update_job(Request $request){
        $result = [];
        $message = "Shift sucessfully updarge";
        $status = true;
        $code = 200;
        $valmessages = [
            'job_title.required' => 'Shift title field is required.',
            'job_responsibility.required' => 'Shift responsibility field is required.'
        ];

        $rules = array(
            'job_id'=>'required|integer',
            'employee_id'=>'required|integer',
            'job_title'=>'required',
            'job_responsibility'=>'required',
            'profession_id'=>'required|integer',
            'speciality_id'=>'sometimes|integer',
            'degree_id'=>'required|integer',
            'experience_id'=>'required|integer',
            'location_id'=>'required|integer',
            'ed_volume_id'=>'required|integer',
            'from_date'=>'required|date|date_format:Y-m-d|after:tomorrow',
            'to_date'=>'required|date|date_format:Y-m-d|after:from_date',
            'shift_id'=>'required|integer',
            'rate'=>'required',
        );

        $validator = \Validator::make($request->all(), $rules, $valmessages);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;

        } else {
            $EmployeeDetail = EmployeeDetail::where('uid',$request->employee_id)->first();
            if($EmployeeDetail){
                $requestData = $request->all();
                $job_detail = JobDetail::where('id',$request->job_id)->where('employee_id',$request->employee_id)->first();

                if($job_detail){
                    $requestData['address'] = (($request->address != null) ? $request->address : '');
                    $requestData['speciality_id'] = (($request->speciality_id != null) ? $request->speciality_id : 0);

                    $job_detail->update($requestData);
                    $job_detail->job_id = $job_detail->id;
                    $result = $job_detail;
                    unset($result->deleted_at,$result->id,$result->created_at,$result->updated_at);
                }
                else{
                    $message = 'No Shift found, Might be deleted';
                    $code = 400;
                    $status = false;
                }
            }
            else{
                $message = 'No employee found';
                $code = 400;
                $status = false;
            }
        }
        return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function delete_job(Request $request){
        $result = [];
        $message = "Shift/s deleted succesfully";
        $status = true;
        $code = 200;
        $rules = array(
            'job_id'=>'required|integer',
            'employee_id'=>'required|integer',
        );
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;
        } else {
            $EmployeeDetail = EmployeeDetail::where('uid',$request->employee_id)->first();
            if($EmployeeDetail){
                $requestData = $request->all();
                $job_detail = JobDetail::where('id',$request->job_id)->where('employee_id',$request->employee_id)->first();
                if($job_detail){
                    $job_detail = $job_detail->delete();
                }
                else{
                    $message = 'No Shift found';
                    $code = 400;
                    $status = false;
                }
            }
            else{
                $message = 'No employee found';
                $code = 400;
                $status = false;
            }
        }
        return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=> $code]);

    }
}
