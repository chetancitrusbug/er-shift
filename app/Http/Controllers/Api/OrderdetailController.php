<?php

namespace App\Http\Controllers\Api;
use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderdetailController extends Controller
{
    public function list_of_total_order(Request $request){

        $result = [];
        $message = "success";
        $status = true;
        $code = 200;  
       
        $rules=array(
            'user_id'=>'integer|required',
        );
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {	
            $Order = Order::select('transaction.id as order_id','transaction.pack_id','transaction.start_date','transaction.expire_date','transaction.subscription_id','transaction.invoice_id','package.id as package_id','package.title','package.description','package.amount','package.day','users.stripe_id')
							->leftjoin('package', 'transaction.pack_id', '=', 'package.id')
							->leftjoin('users', 'transaction.emp_id', '=', 'users.id')
                            ->where('transaction.status',1)
                            ->where('transaction.emp_id',$request->user_id)
                            ->orderBy('transaction.id','desc')
                            ->get();
							//dd($Order);
						
            if($Order){
                if(count($Order) > 0){
					
                    foreach($Order as $ord){
						if($ord->day < 30)
						{
							$ord['duration'] = $ord->day.' '.'days';
						}
						else{
							$ord->day = round(($ord->day)/30);
							$ord['duration'] = $ord->day.' '.'month';
						}
                        unset($ord->package_id,$ord->day);
                    }
                    /*foreach($Order as $emp){
                        if($emp->employee == null){
                            unset($emp->employee);    
                        }
                        else
                        {
                            unset($emp->employee->id,$emp->employee->uid,$emp->employee->status,$emp->employee->created_at,$emp->employee->updated_at);
                        }
                        
                        
                    }
                    foreach($Order as $user){
                        unset($user->user->id,$user->user->is_active,$user->user->social_media_id,
                        $user->user->device_token,$user->user->device_type,$user->user->activation_time,
                        $user->user->activation_token,$user->user->login_type,$user->user->user_type,
                        $user->user->status,$user->user->type,$user->user->created_at,$user->user->updated_at);
                        
                    }
                    foreach($Order as $order){
                      unset($order->id,$order->created_at,$order->updated_at,$order->pack_id,$order->emp_id,
                    $order->status);
                    }*/
                   
                    $result = $Order;
                    $message = 'success';
                }
                else{
                    
                    $message = 'No Record Found';
                    $code = 400;
                    $status = 'false';
                }
            }
            else{
                $message = 'Not Found Any Order';
                $code = 400;
                $status = 'false';
            }   
            
          
        }
        return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
    public function order_view(Request $request){
        $result = [];
        $message = "success";
        $status = true;
        $code = 200;  
       
        $rules=array(
            'order_id'=>'integer|required',
            'user_id'=>'integer|required',
        );
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
			 $Order = Order::select('transaction.id as order_id','transaction.pack_id','transaction.start_date','transaction.expire_date','transaction.subscription_id','transaction.invoice_id','package.id as package_id','package.title','package.description','package.amount','package.day','users.stripe_id')
			->leftjoin('package', 'transaction.pack_id', '=', 'package.id')
			->leftjoin('users', 'transaction.emp_id', '=', 'users.id')
			->where('transaction.status',1)
			->where('transaction.emp_id',$request->user_id)
			->where('transaction.id',$request->order_id)
			->get();
			if($Order){
				if(count($Order) > 0){
					foreach($Order as $ord){
							if($ord->day < 30)
							{
								$ord['duration'] = $ord->day.' '.'days';
							}
							else{
								$ord->day = round(($ord->day)/30);
								$ord['duration'] = $ord->day.' '.'month';
							}
							unset($ord->package_id,$ord->day);
						}
						$result = $Order;
						$message = 'success';
					}
					else{
						$message = 'Not Record Found';
						$code = 400;
						$status = 'false';
					}  
				
					
            }
			else{
                $message = 'Not Found Any Order';
                $code = 400;
                $status = 'false';
            }  
             

        }
        return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
}
