<?php

namespace App\Http\Controllers\Api;
use App\User;
use App\Invitation;
use App\Chat;
use App\DoctorDetail;
use App\EmployeeDetail;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
  
class ChatController extends Controller
{
    public function sendMessage(Request $request) {
        $data = [];
        $message = "Message sent successfully";
        $status = true;
        $code = 200;
        
        $requestData = $request->all();
        $rules = array(
            'invitation_id'=>'required|integer',
            'from_id'=>'required|integer',
            'to_id'=>'required|integer',
            'message' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $UesrFrom = User::find($request->from_id);
            $UesrTo = User::find($request->to_id);
            $UesrInvitation = Invitation::where('id',$request->invitation_id)
                                        ->where('status',1)
                                        /*->where('from_id', $request->from_id)
                                        ->orWhere('from_id', $request->to_id)
                                        ->where('to_id', $request->from_id)
                                        ->orWhere('to_id', $request->to_id)*/
                                ->first();
            if($UesrFrom != null && $UesrTo != null && $UesrInvitation != null)
            {
                if($UesrFrom->api_token == $request->api_token) {
                    $Chat = Chat::create($requestData);
                    $data = $Chat;
                    $data->chat_id = $data->id;
                    $fields['data'] = array(
                        'title' => 'New Message arrived',
                        'body' => 'Message from '. $UesrFrom->name,
                        'message' => 'Message from '. $UesrFrom->name,
                        'type' => 'chatmessage',
                        'user_type' => $UesrFrom->user_type,
                        'user_id' => $request->to_id,
                        'from_id' => $request->from_id ,
                        'to_id' => $request->to_id
                    );
                    $fields['to'] = $UesrTo->firebasetoken;
                    $fields['device_token'] = $UesrTo->device_token;
                    $fields['device_type'] = $UesrTo->device_type;
                    $this->sendPushNotification($fields);
                    unset($data->id);
                }
                else {
                    $status = false;
                    $code = 400;
                    $message = 'From Uesr not login';
                }
            }
            else
            {
                $status = false;
                $code = 400;
                if($UesrFrom == null && $UesrTo == null && $UesrInvitation == null)
                $message = 'From Id & To Id not match';
                else if($UesrFrom == null)
                $message = 'From Id not match';
                else if($UesrTo == null)
                $message = 'To Id not match';
                else
                $message = 'Uesr Invitation not match';
            }
        }
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function updateMessage(Request $request) {
        $data = [];
        $message = "Message update Successfully";
        $status = true;
        $code = 200;
        
        $requestData = $request->all();
        $rules = array(
            'from_id'=>'required|integer',
            'invitation_id'=>'required|integer',
            'read_status'=>'required|in:0,1',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
           
            $UesrInvitation = Invitation::where('id',$request->invitation_id)->first();
            $Chat = Chat::where('from_id',$request->from_id)
            ->where('invitation_id',$request->invitation_id)
            ->where('read_status',0)
            ->pluck('id');
            
            if($UesrInvitation != null && count($Chat) > 0)
            {
                //->pluck('id')
                $updateArr['read_status'] = $request->read_status;
                $Chat = Chat::whereIn('id',$Chat)->update($updateArr);
            }
            else
            {
                $status = false;
                $code = 400;
                if($Chat == null)
                $message = 'To Id not match';
                if($UesrInvitation == null)
                $message = 'Uesr Invitation Id not match';
            }
        }
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function chatList(Request $request) {
        $data = [];
        $message = "";
        $status = true;
        $code = 200;
        
        $requestData = $request->all();
        $rules = array(
            'user_id'=>'required|integer',
            'invitation_id'=>'required|integer',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $User = User::where('id',$request->user_id)->first();
            $UesrInvitation = Invitation::where('id',$request->invitation_id)->where('status',1)->first();
            
            if($UesrInvitation != null && $User != null)
            {
                $Setting = Setting::where('key','default_paginate')->first();
                $per_page = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 10);
                $chat = Chat::leftJoin('users', 'users.id', '=', 'chat.to_id')
                ->leftJoin('doctor_details', 'doctor_details.uid', '=', 'chat.to_id')
                ->leftJoin('employee_detail', 'employee_detail.uid', '=', 'chat.to_id')
                ->select('chat.read_status','chat.from_id','chat.to_id','chat.message', 'chat.created_at as time', 'chat.id','users.name as user',
                DB::raw('(CASE WHEN users.user_type = "employee" THEN "employee" ELSE "doctor" END) AS userType'),
                DB::raw('(CASE WHEN users.user_type = "doctor" THEN doctor_details.profile_name ELSE employee_detail.emp_name END) AS userName'),
                DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "'.url('/images/doctor/').'","/",doctor_details.profile_image) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS doctorImage'),
                DB::raw('(CASE WHEN employee_detail.profile_pic != "" THEN CONCAT( "'.url('/images/employee/').'","/",employee_detail.profile_pic) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS employeeImage'), 
                DB::raw('(CASE WHEN chat.from_id = ' . $request->user_id . ' THEN "sender" ELSE "reciever" END) AS type'))
                ->where('chat.invitation_id',$request->invitation_id)
                ->orderBy('time' , 'asc')->paginate($per_page);
                $data = $chat->toArray();
                $data['sender_id'] = (($UesrInvitation->from_id == $request->user_id) ? $UesrInvitation->to_id : $UesrInvitation->from_id);
                $data['next_page_url'] = (($data['next_page_url'] == null) ? '' : $data['next_page_url']);
                $data['prev_page_url'] = (($data['prev_page_url'] == null) ? '' : $data['prev_page_url']);
            }
            else
            {
                $status = false;
                $code = 400;
                if($User == null)
                $message = 'User Id not match';
                if($UesrInvitation == null)
                $message = 'Uesr Invitation Id not match';
            }
        }
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
}
