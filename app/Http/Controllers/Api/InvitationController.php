<?php

namespace App\Http\Controllers\Api;
use App\User;
use App\Invitation;
use App\DoctorDetail;
use App\EmployeeDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use DB;

class InvitationController extends Controller
{
    public function inviteUser(Request $request) {
        $data = [];
        $message = "Invited Successfully";
        $status = true;
        $code = 200;
        
        $requestData = $request->all();
        $rules = array(
            'from_id'=>'required|integer',
            'to_id'=>'required|integer',
            'init'=>'required|in:doctor,employee'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $UesrFrom = User::find($request->from_id);
            $UesrTo = User::find($request->to_id);
            $UesrInit = User::where('id',$request->from_id)->where('user_type',$request->init)->first();
            
            if($UesrFrom != null && $UesrTo != null && $UesrInit != null)
            {
                if($UesrFrom->api_token == $request->api_token) {
                    if($UesrFrom->user_type == $UesrTo->user_type )
                    {
                        $status = false;
                        $code = 400;
                        $message = "Both users have same type. You can't chat";
                    }
                    else
                    {
                        $InvitationDetailFirst = Invitation::Where('from_id',$request->from_id)
                                            ->Where('to_id',$request->to_id)
                                            ->first();
                        $InvitationDetailSecond = Invitation::Where('to_id',$request->from_id)
                                            ->Where('from_id',$request->to_id)
                                            ->first();
                        
                        if($InvitationDetailFirst == null && $InvitationDetailSecond == null)
                        {
                            $Invitation = Invitation::create($requestData);
                            $data = $Invitation;
                            $data->invitation_id = $data->id;
                            $fields['data'] = array(
                                'title' => 'New chat request arrived',
                                'body' => 'Request from '. $UesrFrom->name,
                                'message' => 'Request from '. $UesrFrom->name,
                                'type' => 'chatinvitation',
                                'user_type' => $request->init,
                                'request_user_name' => $UesrTo->name,
                                'user_id' => $request->to_id,
                                'from_id' => $request->from_id ,
                                'to_id' => $request->to_id
                            );

                            $fields['to'] = $UesrTo->firebasetoken;
                            $fields['device_token'] = $UesrTo->device_token;
                            $fields['device_type'] = $UesrTo->device_type;
                            $this->sendPushNotification($fields);
                            unset($data->id);
                            if($UesrFrom->user_type == 'doctor' )
                            $message = 'Employer invited succesfully';
                            else
                            $message = 'Employee invited successfully';
                        }
                        else
                        {
                            $status = false;
                            $code = 400;
                            if($InvitationDetailFirst != null)
                            {
                                if($InvitationDetailFirst['status'] == 0)
                                {
                                    $InvitationDetailFirst->invitation_id = $InvitationDetailFirst->id;
                                    if($UesrFrom->user_type == 'doctor' )
                                    $message = 'Employer already invited';
                                    else
                                    $message = 'Already invited to chat';
                                    $data = $InvitationDetailFirst;
                                    $code = 400;
                                }
                                else if($InvitationDetailFirst['status'] == 1)
                                {
                                    $InvitationDetailFirst->invitation_id = $InvitationDetailFirst->id;
                                    $message = 'Invitation Accepted ';
                                    $data = $InvitationDetailFirst;
                                    $code = 201;
                                }
                                else if($InvitationDetailFirst['status'] == 2)
                                $message = 'Invitation Declined';
                            }
                            else if($InvitationDetailSecond != null)
                            {
                                if($InvitationDetailSecond['status'] == 0)
                                {
                                    $InvitationDetailSecond->invitation_id = $InvitationDetailSecond->id;
                                    $message = 'Already invited to chat';
                                    $data = $InvitationDetailSecond;
                                    $code = 400;
                                }
                                else if($InvitationDetailSecond['status'] == 1)
                                {
                                    $InvitationDetailSecond->invitation_id = $InvitationDetailSecond->id;
                                    $message = 'Invitation Accepted ';
                                    $data = $InvitationDetailSecond;
                                    $code = 201;
                                }
                               
                                else if($InvitationDetailSecond['status'] == 2)
                                $message = 'Invitation Declined';
                            }
                            else
                            {
                                $message = 'Already invited to chat';
                                $code = 201;
                            }
                            
                        }
                    }
                }
                else {
                    $status = false;
                    $code = 400;
                    $message = 'Uesr not login';
                }
            }
            else
            {
                $status = false;
                $code = 400;
                if($UesrFrom == null && $UesrTo == null && $UesrInit == null)
                $message = 'User & Rec not found';
                else if($UesrFrom == null)
                $message = 'User not found';
                else if($UesrTo == null)
                $message = 'Rec not found';
                else
                $message = 'Uesr type not found';
            }
        }
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function inviteUserUpdate(Request $request) {   
        $data = [];
        $message = "Invitation Updated";
        $status = true;
        $code = 200;  
        $rules = array(
            'invitation_id'=>'required|integer',
            'user_id'=>'required|integer',
            'read_status'=>'required|in:0,1',
            'status'=>'required|in:0,1,2',
        );
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;
        } else {
            $UesrTo = User::find($request->user_id);
            if($UesrTo != null)
            {
                if($UesrTo->api_token == $request->api_token) {
                    $InvitationDetail = Invitation::Where('to_id',$request->user_id)
                                        ->Where('id',$request->invitation_id)
                                        ->first();
                    if($InvitationDetail && $InvitationDetail['status'] == 0) {
                        $updateArr['status'] = $request->status;
                        $updateArr['read_status'] = $request->read_status;
                        $InvitationDetail = $InvitationDetail->update($updateArr);
                        if($request->status == 1)
                        $message = "Invitation Accepted";
                        if($request->status == 2)
                        $message = "Invitation Decline";
                    }   
                    else if($InvitationDetail && $InvitationDetail['status'] == 1) {
                        $InvitationDetail->invitation_id = $InvitationDetail->id;
                        $status = false;
                        $data = $InvitationDetail;
                        $code = 201;
                        $message = 'Already Accepted';
                    }
                    else if($InvitationDetail && $InvitationDetail['status'] == 2) {
                        $status = false;
                        $code = 400;
                        $message = 'Already Decline';
                    }
                    else {
                        $status = false;
                        $code = 400;
                        $message = 'Not Found Invitation';
                    }
                }
                else {
                    $status = false;
                    $code = 400;
                    $message = 'Uesr not login';
                }
            }
            else
            {
                $status = false;
                $code = 400;
                $message = 'Uesr not match';
            }
        }
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function inviteUserList(Request $request) {
        $data = [];
        $message = "Success";
        $status = true;
        $code = 200;
        
        $requestData = $request->all();
        $rules = array(
            'user_id'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $UesrFrom = User::find($request->user_id);
            if($UesrFrom != null)
            {
                if($UesrFrom->api_token == $request->api_token) {
                    $Setting = Setting::where('key','default_paginate')->first();
                    $perPage = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 10);
                    
                     $invitationDetails = [];

                    $invitationDetails = Invitation::with('chat')->where('status', '!=', 2)->where(function($q)use($request){
                        $q->where('from_id',$request->user_id)
                            ->orWhere('to_id',$request->user_id);
                    })->orderBy('created_at', 'desc')->paginate($perPage);

                    
                    foreach ($invitationDetails as $key => $value) {
                        if ($value->doctor && $value->doctor->user && $value->employee && $value->employee->user ) {
                            $invitationDetails[$key]->chatDate = ((($value->chat != null) && ($value->chat->chatDate != null)) ? $value->chat->chatDate : '');
                            $invitationDetails[$key]->chatReadStatus = ((($value->chat != null) && ($value->chat->read_status != null)) ? $value->chat->read_status : 0);
                            $invitationDetails[$key]->invitationDate = $value->created_at->format('Y-m-d H:i:s') ;
                            $invitationDetails[$key]->invitation_id = $value->id ;
                            $user = $value->other_user($request->user_id) ;
                            $invitationDetails[$key]->doctorImage = ($value->doctor && $value->doctor->profile_image != '' && (file_exists(public_path('images/doctor/'.$value->doctor->profile_image))))  ?  url('/images/doctor').'/'.$value->doctor->profile_image : url('/assets/images/avatar.jpg') ;
                            $invitationDetails[$key]->employeeImage = ($value->employee && $value->employee->profile_pic != '' && (file_exists(public_path('images/employee/'.$value->employee->profile_pic))))  ?  url('/images/employee').'/'.$value->employee->profile_pic : url('/assets/images/avatar.jpg') ;
                            $invitationDetails[$key]->userProfileName = isset($user->emp_name) ? $user->emp_name : isset($user->profile_name) ? ($user->profile_name) : '' ;
                            $invitationDetails[$key]->userName = $user->user ? $user->user->name : '' ;
                            $invitationDetails[$key]->user_id = $user->uid;
                            unset($value->chat, $value->created_at , $value->updated_at, $value->doctor,$value->employee);
                        }                  
                    }
                    
                    
                    // //dd($UesrFrom);
                    // $invitationDetails = Invitation::with('chat')
                    // ->where('invitation.to_id',$request->user_id)
                    // ->where('invitation.status','!=',2)
                    // ->select('invitation.created_at as invitationDate','invitation.from_id',
                    // 'invitation.to_id','invitation.init','invitation.read_status',
                    // 'invitation.status','invitation.id as invitation_id')
                    // ->orWhere('invitation.from_id',$request->user_id)
                    // ->orderBy('invitation.created_at' , 'desc')
                    // ->paginate($perPage);
                    // //dd($invitationDetails);
                    // if($UesrFrom->user_type == 'doctor')
                    // {
                    //     $userList = User::where('users.user_type','employee')
                    //     ->leftJoin('doctor_details', 'doctor_details.uid', '=', 'users.id')
                    //     ->leftJoin('employee_detail', 'employee_detail.uid', '=', 'users.id')
                    //     ->select(DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "'.url('/images/doctor/').'","/",doctor_details.profile_image) ELSE "" END) AS doctorImage'),
                    //         DB::raw('(CASE WHEN employee_detail.profile_pic != "" THEN CONCAT( "'.url('/images/employee/').'","/",employee_detail.profile_pic) ELSE "" END) AS employeeImage'),
                    //         DB::raw('(CASE WHEN users.user_type = "doctor" THEN doctor_details.profile_name ELSE employee_detail.emp_name END) AS userProfileName'),                                       
                    //         'users.name as userName','users.id as user_id')
                    //     ->get();
                    // }  
                    // else
                    // {
                    //     $userList = User::where('users.user_type','doctor')
                    //     ->leftJoin('doctor_details', 'doctor_details.uid', '=', 'users.id')
                    //     ->leftJoin('employee_detail', 'employee_detail.uid', '=', 'users.id')
                    //     ->select(DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "'.url('/images/doctor/').'","/",doctor_details.profile_image) ELSE "" END) AS doctorImage'),
                    //         DB::raw('(CASE WHEN employee_detail.profile_pic != "" THEN CONCAT( "'.url('/images/employee/').'","/",employee_detail.profile_pic) ELSE "" END) AS employeeImage'),
                    //         DB::raw('(CASE WHEN users.user_type = "doctor" THEN doctor_details.profile_name ELSE employee_detail.emp_name END) AS userProfileName'),                                       
                    //         'users.name as userName','users.id as user_id')
                    //     ->get();
                    // }
                    
                    // foreach ($invitationDetails as $key => $value) {
                    //     $invitationDetails[$key]->chatDate = ((($value->chat != null) && ($value->chat->chatDate != null)) ? $value->chat->chatDate : '');
                    //     $invitationDetails[$key]->chatReadStatus = ((($value->chat != null) && ($value->chat->read_status != null)) ? $value->chat->read_status : 0);
                    //     if($value->from_id == $request->user_id)
                    //     {
                    //         $user_id = $value->to_id;
                    //     }
                    //     else
                    //     {
                    //         $user_id = $value->from_id;
                    //     }
                    //     unset($value->chat); 
                    //     foreach ($userList as $userKey => $userValue) {
                    //         if($user_id == $userValue->user_id)
                    //         {
                    //             $invitationDetails[$key] = array_merge($invitationDetails[$key]->toArray() ,$userValue->toArray());
                    //         }
                    //     }
                                          
                    // }
                    
                    if(count($invitationDetails) > 0)
                    {
                        $data = $invitationDetails->toArray();
                        $data['next_page_url'] = (($data['next_page_url'] == null) ? '' : $data['next_page_url']);
                        $data['prev_page_url'] = (($data['prev_page_url'] == null) ? '' : $data['prev_page_url']);
                    }
                    else {
                        $status = false;
                        $code = 400;
                        $message = 'Invitation have no data';
                    }
                }
                else {
                    $status = false;
                    $code = 400;
                    $message = 'Uesr not login';
                }
            }
            else
            {
                $status = false;
                $code = 400;
                $message = 'User not match';
            }
        }
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    /*public function inviteUserList(Request $request) {
        $data = [];
        $message = "Success";
        $status = true;
        $code = 200;
        
        $requestData = $request->all();
        $rules = array(
            'user_id'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $UesrFrom = User::find($request->user_id);
            if($UesrFrom != null)
            {
                if($UesrFrom->api_token == $request->api_token) {
                    $Setting = Setting::where('key','default_paginate')->first();
                    $perPage = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 10);
                    $InvitationDetail = Invitation::
                    where('invitation.to_id',$request->user_id)
                    ->orWhere('invitation.from_id',$request->user_id)
                    ->leftJoin('chat', function($query) {
                        $query->on('chat.invitation_id', '=', 'invitation.id')
                            ->where('chat.created_at', '=', DB::raw("(select max(`created_at`) from chat)"));
                    })
                        ->leftJoin('users', 'users.id', '=', 'invitation.from_id')
                        ->leftJoin('doctor_details', 'doctor_details.uid', '=', 'invitation.from_id')
                        ->leftJoin('employee_detail', 'employee_detail.uid', '=', 'invitation.from_id')
                        ->select(DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "'.url('/images/doctor/').'","/",doctor_details.profile_image) ELSE "" END) AS doctorImage'),
                        DB::raw('(CASE WHEN employee_detail.profile_pic != "" THEN CONCAT( "'.url('/images/employee/').'","/",employee_detail.profile_pic) ELSE "" END) AS employeeImage'),
                        DB::raw('(CASE WHEN invitation.init = "doctor" THEN doctor_details.profile_name ELSE employee_detail.emp_name END) AS userName'),                                       
                        DB::raw('(CASE WHEN chat.invitation_id = invitation.id THEN  (select max(`created_at`) from chat) ELSE "" END) AS chatDate'),                                       
                        'invitation.created_at as invitationDate','invitation.from_id','invitation.to_id','invitation.init','invitation.read_status','invitation.status','invitation.created_at as time','invitation.id as invitation_id','users.name as user')
                    
                    ->orderBy('invitation.created_at' , 'desc')
                    ->paginate($perPage);
                    if(count($InvitationDetail) > 0)
                    {
                        $data = $InvitationDetail->toArray();
                        $data['next_page_url'] = (($data['next_page_url'] == null) ? '' : $data['next_page_url']);
                        $data['prev_page_url'] = (($data['prev_page_url'] == null) ? '' : $data['prev_page_url']);
                    }
                    else {
                        $status = false;
                        $code = 400;
                        $message = 'Invitation have no data';
                    }
                }
                else {
                    $status = false;
                    $code = 400;
                    $message = 'Uesr not login';
                }
            }
            else
            {
                $status = false;
                $code = 400;
                $message = 'User not match';
            }
        }
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }*/
}
