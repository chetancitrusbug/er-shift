<?php

namespace App\Http\Controllers\Api;
use App\Education;
use App\Experience;
use App\DoctorWish;
use App\State;
use App\Profession;
use App\Speciality;
use App\Degree;
use App\Edvolume;
use App\Priority;
use App\DoctorDetail;
use App\DateShift;
use App\Setting;
use App\User;
use App\Locations;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DoctorWishController extends Controller
{
    public function addDoctorWish(Request $request) {
        $data = [];
        $flag = 1;
        $message = "ED Staff wish added";
        $status = true;
        $code = 200;
        
        $requestData = $request->all();
        $rules = array(
            'user_id'=>'required|integer',
            'wish_title'=>'required',
            'location_id'=>'required',
            'ed_volum_id'=>'required|integer',
            'dateJson'=>'required|json',
            'rate'=>'required',
            'priority_id'=>'sometimes|integer'
        );
        if(isset($requestData['location_id'])){
            if(!is_array(json_decode($requestData['location_id'])))
            $flag = 0;
        }

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        }
        else if ($flag == 0) {
            $status = false;
            $code = 400;
            $message = 'Location id must be array';
        } else {
            $DoctorDetail = DoctorDetail::where('uid',$request->user_id)->first();
            if($DoctorDetail)
            {
                // $requestData['doctor_id'] = $request->user_id;
                $requestData['doctor_id'] = $DoctorDetail->id ;
                // if(isset($requestData['location_id'])){
                //     if(is_array(json_decode($requestData['location_id'])))
                //     $requestData['location_id'] = implode(",",json_decode($requestData['location_id']));
                // }
                
                $DoctorWish = DoctorWish::create($requestData);
                
                $locations = [];
                if(isset($requestData['location_id'])){
                    if(is_array(json_decode($requestData['location_id']))){
                        $locations = json_decode($requestData['location_id']) ;
                        $locations = array_filter($locations, function($l) { return $l != 0; }) ;
                    }
                }
                foreach($locations as $loc){
                    Locations::create([
                        'type' => 'doctor_wish',
                        'field_id' => $DoctorWish->id ,
                        'location_id' => $loc
                    ]);
                }
                
                $dateArr = json_decode($requestData['dateJson']);
                
                foreach ($dateArr as $key => $value) {
                    $dateAddArr = array();
                    $dateAddArr['from'] = $value->from;
                    $dateAddArr['to'] = $value->to;
                    $dateAddArr['shift_id'] = $value->shift_id;
                    $dateAddArr['doctor_id'] = $request->user_id;
                    $dateAddArr['doctor_wish_id'] = $DoctorWish->id;
                    DateShift::create($dateAddArr);
                }
                $DoctorWish = DoctorWish::with('edvolume','priority')->find($DoctorWish->id);
                $DoctorWish->doctor_wish_id = $DoctorWish->id;
                $dateShift = DateShift::where('doctor_wish_id',$DoctorWish->id)->get();
                $DoctorWish->dateShift = (($dateShift != null) ? $dateShift : []);
                $DoctorWish->ed_volum_id = (($DoctorWish->edvolume != null) ? $DoctorWish->edvolume->title : '');
                $DoctorWish->priority_id = (($DoctorWish->priority != null) ? $DoctorWish->priority->title : '');
                $DoctorWish->location_id = $DoctorWish->comma_sep_loc_names ;
                // if($DoctorWish->location_id != null)
                // {
                //     $arr = explode(',',$DoctorWish->location_id);
                //     $str = '';
                //     foreach ($arr as $key1 => $value1) {
                //         $State = State::where('id',$value1)->first();
                //         if($State != null)
                //         $str .= (($str == '') ? $State->name : ','.$State->name);
                //     }
                //     $DoctorWish->location_id = $str;
                // }
                // else
                // {
                //     $DoctorWish->location_id = '';
                // }

                unset($DoctorWish->priority,$DoctorWish->edvolume,$DoctorWish->id,$DoctorWish->created_at,$DoctorWish->updated_at);

                $data = $DoctorWish;
            }
            else
            {
                $status = false;
                $code = 400;
                $message = 'Doctor not found';
            }
        }
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function updateDoctorWish(Request $request) {   
        $data = [];
        $flag = 1;
        $message = "ED staff wish Updated";
        $status = true;
        $code = 200;  
        $rules = array(
            'doctor_wish_id'=>'required|integer',
            'user_id'=>'required|integer',
            'wish_title'=>'required',
            'location_id'=>'required',
            'ed_volum_id'=>'required|integer',
            'dateJson'=>'required|json',
            'rate'=>'required',
            'priority_id'=>'sometimes|integer'
        );
        if(isset($requestData['location_id'])){
            if(!is_array(json_decode($requestData['location_id'])))
            $flag = 0;
        }
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;
        } else if ($flag == 0) {
            $status = false;
            $code = 400;
            $message = 'Location id must be array';
        } else {
            $DoctorDetail = DoctorDetail::where('uid',$request->user_id)->first();
            if($DoctorDetail)
            {
                $id = $request->doctor_wish_id;
                $requestData = $request->all();
                // $DoctorWish = DoctorWish::with('edvolume','priority')->where('id',$id)->where('doctor_id',$request->user_id)->first();
                $DoctorWish = DoctorWish::with('edvolume','priority')->where('id',$id)->where('doctor_id',$DoctorDetail->id)->first();
                if($DoctorWish){
                    // if(isset($requestData['location_id'])){
                    //     if(is_array(json_decode($requestData['location_id'])))
                    //     $requestData['location_id'] = implode(",",json_decode($requestData['location_id']));
                    // }  
                    unset($requestData->doctor_wish_id);
                    $DoctorWish->update($requestData);
                    
                    $locations = [];
                    if(isset($requestData['location_id'])){
                        if(is_array(json_decode($requestData['location_id']))){
                            $locations = json_decode($requestData['location_id']) ;
                            $locations = array_filter($locations, function($l) { return $l != 0; }) ;
                        }
                    }
                    $DoctorWish->locations()->forceDelete();
                    foreach($locations as $loc){
                        Locations::create([
                            'type' => 'doctor_wish',
                            'field_id' => $DoctorWish->id ,
                            'location_id' => $loc
                        ]);
                    }

                    DateShift::where('doctor_wish_id',$id)->delete();
                    $dateArr = json_decode($requestData['dateJson']);
                
                    foreach ($dateArr as $key => $value) {
                        $dateAddArr = array();
                        $dateAddArr['from'] = $value->from;
                        $dateAddArr['to'] = $value->to;
                        $dateAddArr['shift_id'] = $value->shift_id;
                        $dateAddArr['doctor_id'] = $request->user_id;
                        $dateAddArr['doctor_wish_id'] = $id;
                        DateShift::create($dateAddArr);
                    }


                    $DoctorWish->doctor_wish_id =$DoctorWish->id;
                    
                    $DoctorWish->ed_volum_id = (($DoctorWish->edvolume != null) ? $DoctorWish->edvolume->title : '');
                    $DoctorWish->priority_id = (($DoctorWish->priority != null) ? $DoctorWish->priority->title : '');
                     $DoctorWish->location_id = $DoctorWish->comma_sep_loc_names ;
                    // if($DoctorWish->location_id != null)
                    // {
                    //     $arr = explode(',',$DoctorWish->location_id);
                    //     $str = '';
                    //     foreach ($arr as $key1 => $value1) {
                    //         $State = State::where('id',$value1)->first();
                    //         if($State != null)
                    //         $str .= (($str == '') ? $State->name : ','.$State->name);
                    //     }
                    //     $DoctorWish->location_id = $str;
                    // }
                    // else
                    // {
                    //     $DoctorWish->location_id = '';
                    // }
                    $dateShift = DateShift::where('doctor_wish_id',$DoctorWish->id)->get();
                    $DoctorWish->dateShift = (($dateShift != null) ? $dateShift : []);
                    unset($DoctorWish->priority,$DoctorWish->edvolume,$DoctorWish->id,$DoctorWish->created_at,$DoctorWish->updated_at);

                    $data = $DoctorWish;
                }
                else{
                    $message = 'No ED staff wishe found';
                    $code = 400;
                    $status = 'false';
                }
            }
            else
            {
                $status = false;
                $code = 400;
                $message = 'Doctor not found';
            }
        }
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function deleteDoctorWish(Request $request){
        $data = [];
        $message = "ED staff Wish Deleted";
        $status = true;
        $code = 200;  
        $rules = array(
            'doctor_wish_id'=>'required|integer',
            'user_id'=>'required|integer',
        );
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;
        } else {
            $DoctorDetail = DoctorDetail::where('uid',$request->user_id)->first();
            if($DoctorDetail)
            {
                $id = $request->doctor_wish_id;
                // $DoctorWish = DoctorWish::where('id',$id)->where('doctor_id',$request->user_id)->first();
                 $DoctorWish = DoctorWish::where('id',$id)->where('doctor_id', $DoctorDetail->id)->first();
                if($DoctorWish){
                     $DoctorWish->locations()->delete() ;
                    DateShift::where('doctor_wish_id',$id)->delete();
                    $DoctorWish = $DoctorWish->delete();
                }
                else{
                    $message = 'No ED staff wishe found';
                    $code = 400;
                    $status = 'false';
                }
            }
            else
            {
                $status = false;
                $code = 400;
                $message = 'Doctor not found';
            }
        }
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);

    }

    public function listDoctorWish(Request $request) {
        $data = [];
        $message = "success";
        $status = true;
        $code = 200;  
        $rules = array(
            'user_id'=>'required',
        );
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;
        } else {

            if($request->paginate ==""){
                $perPage = 10;
            }
            else
            {
                $perPage = $request->paginate;
            }
            $Setting = Setting::where('key','default_paginate')->first();
            $perPage = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 10);
            
            $user = User::find($request->user_id);
            
         
            // $DoctorWish = DoctorWish::with('edvolume','priority')->where('status',1)->where('doctor_id',$request->user_id)->paginate($perPage);
            // dd($DoctorWish);
            
           
            
            if($user ){
                
                $DoctorWish = $user->doctorDetail ? $user->doctorDetail->doctor_wishes()->paginate($perPage)  : [];
                // dd( $user->doctorDetail->doctor_wishes()->paginate(2) ) ;
                if(count($DoctorWish) > 0){
                    foreach ($DoctorWish as $key => $value) {
                        $DoctorWish[$key]->doctor_wish_id =$value->id;
                        
                        $DoctorWish[$key]->ed_volum_id = (($value->edvolume != null) ? $value->edvolume->title : '');
                        $DoctorWish[$key]->priority_id = (($value->priority != null) ? $value->priority->title : '');
                        $DoctorWish[$key]->location_id =  isset($value->comma_sep_loc_names) ? $value->comma_sep_loc_names : "" ; 
                        // if($value->location_id != null)
                        // {
                        //     $arr = explode(',',$value->location_id);
                        //     $str = '';
                        //     foreach ($arr as $key1 => $value1) {
                        //         $State = State::where('id',$value1)->first();
                        //         if($State != null)
                        //         $str .= (($str == '') ? $State->name : ','.$State->name);
                        //     }
                        //     $DoctorWish[$key]->location_id = $str;
                        // }
                        // else
                        // {
                        //     $DoctorWish[$key]->location_id = '';
                        // }
                        unset($value->priority,$value->edvolume,$value->id,$value->created_at,$value->updated_at);
                    }
                    $data = $DoctorWish->toArray();
                    $data['next_page_url'] = (($data['next_page_url'] == null) ? '' : $data['next_page_url']);
                    $data['prev_page_url'] = (($data['prev_page_url'] == null) ? '' : $data['prev_page_url']);
                    $message = 'success';
                }
                else{
                    $message = 'No ED staff wishes found';
                    $code = 400;
                    $status = 'false';
                } 
            }else{
                    $message = 'DOctor detail not found';
                    $code = 400;
                    $status = 'false';
                } 
            
        }
               
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function myDoctorWish(Request $request) {
        $data = [];
        $message = "success";
        $status = true;
        $code = 200;  
        if($request->user_id){
            // $DoctorWish = DoctorWish::where('status',1)->where('doctor_id',$request->user_id)->get();
            
            $user = User::find($request->user_id);
            if($user){
            $DoctorWish = $user->doctorDetail ? $user->doctorDetail->doctor_wishes  : [];
                if(count($DoctorWish) > 0){
                    foreach ($DoctorWish as $key => $value) {
                        $DoctorWish[$key]->doctor_wish_id =$value->id;
                        
                        if($value->ed_volum_id != null)
                        {
                            $Edvolume = Edvolume::where('id',$value->ed_volum_id)->first();
                            $DoctorWish[$key]->ed_volum_id = $Edvolume->title;
                        }
                        else
                        {
                            $DoctorWish[$key]->ed_volum_id = '';
                        }
                        
                        $DoctorWish[$key]->location_id = isset($value->comma_sep_loc_names) ? $value->comma_sep_loc_names : "" ;
                        
                        // if($value->location_id != null)
                        // {
                        //     $arr = explode(',',$value->location_id);
                        //     $str = '';
                        //     foreach ($arr as $key1 => $value1) {
                        //         $State = State::where('id',$value1)->first();
                        //         if($State != null)
                        //         $str .= (($str == '') ? $State->name : ','.$State->name);
                        //     }
                        //     $DoctorWish[$key]->location_id = $str;
                        // }
                        // else
                        // {
                        //     $DoctorWish[$key]->location_id = '';
                        // }
        
                        unset($value->id,$value->created_at,$value->updated_at);
                    }
                    $data = $DoctorWish;
                    $message = 'success';
                }
                else{
                    $message = "You Don't have any Doctor Wish";
                    $code = 400;
                    $status = 'false';
                }
            }else{
                $message = "Doctor detail not Found";
                $code = 400;
                $status = 'false';
            }
        }else{
            $message = 'No Doctor Found';
            $code = 400;
            $status = 'false';
        }
        
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function viewDoctorWish(Request $request) {
        $data = [];
        $message = "success";
        $status = true;
        $code = 200;  
        $requestData = $request->all();
        $rules = array(
            'user_id'=>'required|integer',
            'user_type'=> 'required|in:employee,doctor',
            'doctor_wish_id'=>'required|integer',
        );
     
        $validator = \Validator::make($request->all(), $rules, []);
        
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            if($request->user_type == 'doctor')
            {
                $user = User::where('id',$request->user_id)->where('user_type',$request->user_type)->where('is_active',1)->first();
                if($user){
                    
                    
                    $doctorWishDetail = DoctorWish::select('doctor_wish.*','doctor_wish.id as doctor_wish_id',
                        DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "'.url('').'",CONCAT("/images/doctor/"),doctor_details.profile_image) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS doctorImage')
                        ,DB::raw('(CASE WHEN doctor_details.profile_name != "" THEN doctor_details.profile_name ELSE users.name END) AS doctorName')
                        )->with('edvolume')
                        // ->join('users', 'users.id', '=', 'doctor_wish.doctor_id')
                        // ->join('doctor_details', 'doctor_details.uid', '=', 'doctor_wish.doctor_id')
                        ->join('doctor_details', 'doctor_details.id', '=', 'doctor_wish.doctor_id')
                          ->join('users', 'users.id', '=', 'doctor_details.uid')
                        ->where('doctor_wish.status',1)
                        ->where('doctor_wish.id',$request->doctor_wish_id)
                        ->where('doctor_details.uid',$request->user_id)
                        ->first();

                    if($doctorWishDetail != null)
                    {
                        $dateShiftArr = DateShift::select('from','to','shift_id',
                        DB::raw('(CASE WHEN shift.title != "" THEN shift.title ELSE "" END) AS shift_title')
                        )
                        ->join('shift', 'shift.id', '=', 'date_shift.shift_id')
                        ->where('doctor_wish_id',$request->doctor_wish_id)->get();
                        
                        
                        $doctorWishDetail->dateShiftArr = (($dateShiftArr != null) ? $dateShiftArr : []);
                             $doctorWishDetail->location = $doctorWishDetail->comma_sep_loc_names  ; 

                        // if ($doctorWishDetail->location_id != null) {
                        //     $arr = explode(',', $doctorWishDetail->location_id);
                        //     $str = '';
                        //     foreach ($arr as $key1 => $value1) {
                        //         $State = State::select('id', 'name')->where('id', $value1)->first();
                        //         if ($State != null)
                        //             $str .= (($str != null ) ? ','.$State->name : $State->name);;
                        //     }
                        //     $doctorWishDetail->location = $str;
                        // } else {
                        //     $doctorWishDetail->location = '';
                        // }
                        $doctorWishDetail->edvolume_title = ((($doctorWishDetail->edvolume) && ($doctorWishDetail->edvolume->title != null )) ? $doctorWishDetail->edvolume->title : '');
                        
                        unset($doctorWishDetail->edvolume,$doctorWishDetail->created_at, $doctorWishDetail->updated_at, $doctorWishDetail->deleted_at);
                        $data = $doctorWishDetail;
                    }
                    else
                    {
                        $message = 'No Doctor Wish Found, Might Be Deleted';
                        $code = 400;
                        $status = 'false'; 
                    }
                }else{
                    $message = 'User not found';
                    $code = 400;
                    $status = 'false'; 
                }
            }
            else{
                $user = User::where('id',$request->user_id)->where('user_type',$request->user_type)->where('is_active',1)->first();
                
                if($user){
                    $doctorWishDetail = DoctorWish::select('doctor_wish.*','doctor_wish.id as doctor_wish_id',
                        DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "'.url('').'",CONCAT("/images/doctor/"),doctor_details.profile_image) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS doctorImage')
                        ,DB::raw('(CASE WHEN doctor_details.profile_name != "" THEN doctor_details.profile_name ELSE users.name END) AS doctorName')
                        )->with('edvolume')
                        // ->join('users', 'users.id', '=', 'doctor_wish.doctor_id')
                        // ->join('doctor_details', 'doctor_details.uid', '=', 'doctor_wish.doctor_id')
                          ->join('doctor_details', 'doctor_details.id', '=', 'doctor_wish.doctor_id')
                        ->join('users', 'users.id', '=', 'doctor_details.uid')
                        ->where('doctor_wish.status',1)
                        ->where('doctor_wish.id',$request->doctor_wish_id)
                        ->first();
                    if($doctorWishDetail != null)
                    {
                        $dateShiftArr = DateShift::select('from','to','shift_id',
                        DB::raw('(CASE WHEN shift.title != "" THEN shift.title ELSE "" END) AS shift_title')
                        )
                        ->join('shift', 'shift.id', '=', 'date_shift.shift_id')
                        ->where('doctor_wish_id',$request->doctor_wish_id)->get();
                        
                        
                        $doctorWishDetail->dateShiftArr = (($dateShiftArr != null) ? $dateShiftArr : []);
                        $doctorWishDetail->location = $doctorWishDetail->comma_sep_loc_names  ; 
                        
                        // if ($doctorWishDetail->location_id != null) {
                        //     $arr = explode(',', $doctorWishDetail->location_id);
                        //     $str = '';
                        //     foreach ($arr as $key1 => $value1) {
                        //         $State = State::select('id', 'name')->where('id', $value1)->first();
                        //         if ($State != null)
                        //             $str .= (($str != null ) ? ','.$State->name : $State->name);;
                        //     }
                        //     $doctorWishDetail->location = $str;
                        // } else {
                        //     $doctorWishDetail->location = '';
                        // }
                        $doctorWishDetail->edvolume_title = ((($doctorWishDetail->edvolume) && ($doctorWishDetail->edvolume->title != null )) ? $doctorWishDetail->edvolume->title : '');
                        unset($doctorWishDetail->edvolume,$doctorWishDetail->created_at, $doctorWishDetail->updated_at, $doctorWishDetail->deleted_at);
                        $data = $doctorWishDetail;
                    }
                    else
                    {
                        $message = 'No Doctor Wish Found, Might Be Deleted';
                        $code = 400;
                        $status = 'false'; 
                    }
                }else{
                    $message = 'User not found';
                    $code = 400;
                    $status = 'false'; 
                }
            }
        }
        
        
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
}
