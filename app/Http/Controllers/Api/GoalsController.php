<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Goal;
use DB;
class GoalsController extends Controller
{
    public function addgoal(Request $request){
       
        $data = array();
        $code = 200;
        $messages = 'Goal Added Successfully';
        $status =true;
        $error = '';
        $rules = array(
             'title' => 'required',
             'details' => 'required',
             'due_date' => 'required'
             //'goal_achieve_status' => 'required'
        );
        $user = User::find($request->user_id);
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = \Config::get('constants.responce_code.validation_failed');
            $messages = 'Not valid data';
        } else { 
            if($user){
                if($request->start_date != ''){
                    $start_date = explode('/', $request->start_date);
                    $month = $start_date[0];
                    $day = $start_date[1];
                    $year = $start_date[2];
                    $start_date = $year . '-' . $month . '-' . $day;    
                }else{
                    $start_date = date('Y-m-d',time());
                }
				
				$due_date = explode('/', $request->due_date);
				$emonth = $due_date[0];
				$eday   = $due_date[1];
				$eyear  = $due_date[2];
				$due_date=$eyear.'-'.$emonth.'-'.$eday;
                $goal=new Goal;     
                $goal['title'] = $request->title;
                $goal['details'] = $request->details;
                $goal['start_date'] = $start_date;
                $goal['due_date'] = $due_date;
                $goal['created_by'] = $request->user_id;
                //$goal['goal_achieve_status'] = $request->goal_achieve_status;
                $goal->save();
                $data=$goal;
            }else{
                 $status = false;
                 $code = 400;
                 $messages = 'User Does Not Exist';
            }
        }
        return response()->json(['data' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function editgoal(Request $request){
       
        $data = array();
        $code = 200;
        $messages = 'Goal Updated Successfully';
        $status =true;
        $error = '';
        $rules = array(
             'id'=>'required',
             'title' => 'required',
             'details' => 'required',
             'start_date' => 'required',
             'due_date' => 'required',
             'goal_status'=>'required'
            // 'goal_achieve_status' => 'required'
        );
        $user = User::find($request->user_id);
        $goal=Goal::find($request->id);
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = \Config::get('constants.responce_code.validation_failed');
            $messages = 'Not valid data';
        } else { 
            if($user){ 
                if($goal){  
					$start_date = explode('/', $request->start_date);
					$month = $start_date[0];
					$day   = $start_date[1];
					$year  = $start_date[2];   
					$start_date=$year.'-'.$month.'-'.$day;
					$due_date = explode('/', $request->due_date);
					$emonth = $due_date[0];
					$eday   = $due_date[1];
					$eyear  = $due_date[2];
					$due_date=$eyear.'-'.$emonth.'-'.$eday;
                    $goal['title'] = $request->title;
                    $goal['details'] = $request->details;
                    $goal['start_date'] = $start_date;
                    $goal['due_date'] = $due_date;
                    $goal['created_by'] = $request->user_id;
                    $goal['goal_status'] = $request->goal_status;
                   // $goal['goal_achieve_status'] = $request->goal_achieve_status;
                    $goal->update();
                    $data=$goal;
                }else{
                    $status = false;
                    $code = 400;
                    $messages = 'Goal Does Not Exist';
                }
            }else{
                    $status = false;
                    $code = 400;
                    $messages = 'User Does Not Exist';
                }
        }
        return response()->json(['data' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

   public function goallist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;
		$currentdate = date('Y-m-d');
        $id = $request->user_id;
		$goal_type = $request->goal_type;
        $users= User::where('users.id',$id)->first();
    
        $goaldata =array();

        if($users){
			if($goal_type==''){
            $goaldata = Goal::where('created_by',$id)->where('status','active')
            ->select('*',(DB::raw("CONCAT_WS('-',MONTH(due_date)) as months")))          
            ->get();
            }else if($goal_type=='0'){
				$goaldata = Goal::where('created_by',$id)->where('status','active')->where('goal_status',0)
				->select('*',(DB::raw("CONCAT_WS('-',MONTH(due_date)) as months")))          
				->get();
			}
			else if($goal_type=='1'){
				$goaldata = Goal::where('created_by',$id)->where('status','active')->whereIn('goal_status',array(1,3))         
				->select('*',(DB::raw("CONCAT_WS('-',MONTH(due_date)) as months")))    
				->get();
			}else{
				$goaldata = Goal::where('created_by',$id)->where('status','active')->where('goal_status',2)
				->select('*',(DB::raw("CONCAT_WS('-',MONTH(due_date)) as months")))          
				->get();
			}
            $goalsdata = array();
            foreach( $goaldata as $date ){
                 $goalsdata[date('Y-m-d', strtotime($date->due_date))][] = $date; 
            }
            if(count($goalsdata)>0){                
                         $data[]=$goalsdata;
                         $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No Goals Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }        
           
        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

    /* public function activategoallist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $users= User::where('users.id',$id)->first();
        if($users){
            $activategoaldata = Goal::where('created_by',$id)->where('status','active')->where('goal_status',0)->Where('goal_achieve_status','=',0)
            ->select('*',(DB::raw("CONCAT_WS('-',MONTH(due_date)) as months")))          
            ->get();
            
            $activatesgoaldata = array();
            foreach( $activategoaldata as $date ){
                 $activatesgoaldata[date('Y-m-d', strtotime($date->due_date))][] = $date; 
            }
            if(count($activatesgoaldata)>0){                
                $data[]=$activatesgoaldata;
                $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No Goals Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }        
           
        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }
   
    public function completegoallist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $users= User::where('users.id',$id)->first();
        if($users){
            $completegoaldata = Goal::where('created_by',$id)->where('status','active')->where('goal_status',1)
			->orWhere('goal_achieve_status','=',1)
            ->select('*',(DB::raw("CONCAT_WS('-',MONTH(due_date)) as months")))          
            ->get();
            
            $completesgoaldata = array();
            foreach( $completegoaldata as $date ){
                 $completesgoaldata[date('Y-m-d', strtotime($date->due_date))][] = $date; 
            }
            if(count($completegoaldata)>0){                
                $data[]=$completesgoaldata;
                $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No Goals Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }        
           
        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }
	
	public function expiregoallist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $users= User::where('users.id',$id)->first();
        $currentdate = date('Y-m-d');
        if($users){
            $expiregoaldata = Goal::where('created_by',$id)->where('status','active')->where('goal_status',0)->where('due_date','<',$currentdate)         
             ->select('*',(DB::raw("CONCAT_WS('-',MONTH(due_date)) as months")))    
            ->get();
          
            $expiresgoaldata = array();
            foreach( $expiregoaldata as $date ){
                // $expiresgoaldata[] = $date; 
                  $expiresgoaldata[date('Y-m-d', strtotime($date->due_date))][] = $date; 
            }
            if(count($expiresgoaldata)>0){                
                $data[]=$expiresgoaldata;
                $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No Goals Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }        
           
        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }
 */
    public function suggetion(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

            $suggetiondata = Goal::where('created_by',1)->where('status','active')->where('goal_status',0)  
            ->select('id as id','title as title','details as details')       
            ->get();
          
            $suggetionsdata = array();
            foreach( $suggetiondata as $date ){
                 $suggetionsdata[] = $date; 
            }
            if(count($suggetionsdata)>0){                
                $data[]=$suggetionsdata;
                $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No Suggetion Found';
                $code = 400;
            }
       
        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }
   
}
