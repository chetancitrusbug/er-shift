<?php

namespace App\Http\Controllers\Api;
use App\User;
use App\DoctorDetail;
use App\Education;
use App\Experience;
use App\EmployeeDetail;
use App\DoctorWish;
use App\State;
use App\Profession;
use App\Speciality;
use App\Degree;
use App\Shift;
use App\Edvolume;
use App\Priority;
use App\JobDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Locations ;

use DB;

class DoctorController extends Controller
{
    public function doctorUpdate(Request $request){   
        $data = [];
        $flag = 1;
        $message = "Doctor Updated";
        $status = true;
        $code = 200;  
        
        $requestData = $request->all();
        $rules = array(
            'user_id'=>'required|integer',
            'fname'=>'required',
            'lname'=>'required',
            'email'=>'required',
            'phone_no'=>'required',
            'profile_name'=>'required',
            'profession_id'=>'required|integer',
            'speciality_id'=>'required|integer',
            'experience_id'=>'required|integer',
            'degree_id'=>'required',
            'license_id'=>'required',
            'profile_image'=>'sometimes|mimes:jpg,jpeg,png|max:10000',
            //'resume_type'=>'sometimes|in:link,attachment',
            // 'location_id'=>'sometimes',

        );

        if (isset($requestData['resume_link']) && $requestData['resume_link'] != '' &&  $requestData['resume_link'] != null ) {
            $rules['resume_link'] = 'sometimes|regex:/^(https?:\/\/)?([\dA-Za-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        }
        if (isset($requestData['resume_attachment']) && $requestData['resume_attachment'] != '' && $requestData['resume_attachment'] != null ) {
            $rules['resume_attachment'] = 'sometimes|mimes:jpg,jpeg,pdf,doc,docx,xls,xlsx,ppt|max:20000';
        }


        if (isset($requestData['profession_id'])) {
            $rules['profession_id'] = 'sometimes|integer';
        }
        if (isset($requestData['speciality_id'])) {
            $rules['speciality_id'] = 'sometimes|integer';
        }
        if (isset($requestData['experience_id'])) {
            $rules['experience_id'] = 'sometimes|integer';
        }
        
        if(isset($requestData['degree_id'])){
            if(!is_array(json_decode($requestData['degree_id'])))
            {
                $flag = 0;
                $message = 'Degree id must be in array';
            }
            
        }

        if(isset($requestData['license_id'])){
            if(!is_array(json_decode($requestData['license_id'])))
            {
                $flag = 0;
                $message = 'License id must be in array';
            }
            if((count(json_decode($requestData['license_id']))) > 5)
            {
                $flag = 0;
                $message = 'License array must be less than 5';
            }
        }
    
        $validator = \Validator::make($request->all(), $rules, []);
        //dd($validator->passes(), $validator->messages()->toArray());

        if ($validator->fails()) {
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;
        }
        else if ($flag == 0) {
            $status = false;
            $code = 400;
        } else {
            $id = $request->user_id;
            $user = User::where('id',$id)->where('user_type','doctor')->first();
            if($user){
                if($user->is_active ==  '0')
                {
                    $message = 'User is inactive';
                    $code = 400;
                    $status = 'false';
                    return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
                }
                $userData = array();
                if(isset($requestData['fname']))
                $userData['fname'] = $requestData['fname'];
                if(isset($requestData['lname']))
                $userData['lname'] = $requestData['lname'];
                $userData['name'] = $requestData['fname'].' '.$requestData['lname'];
                if($user->login_type == 'facebook' && $user->email == ''){
                    $emailExist = User::where('id','!=',$request->user_id)->where('email',$request->email)->where('user_type','doctor')->first();
                    
                    if($emailExist != null)
                    {
                        $message = 'Email already exists, please use another emaill address';
                        $code = 400;
                        $status = 'false';
                        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
                    }
                    else{
                        $userData['email'] = $requestData['email'];
                    }
                }
                $userData['firebasetoken'] = ($request->firebasetoken != '') ? $request->firebasetoken : $user->firebasetoken ;
                if($userData != null)
                {
                    $user->update($userData);
                }                
                $DoctorDetail = DoctorDetail::where('uid',$id)->first();
                if($DoctorDetail == null){
                    $message = 'Doctor Not Found';
                    $status = false;
                    $code=400;
                }
                else {
                    $DoctorDetailData = array();
                    if ($request->file('profile_image')) {
                        $fimage = $request->file('profile_image');           
                        $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
                        $fimage->move(public_path('/images/doctor/'), $filename);
                        $DoctorDetail['profile_image'] = $filename;
                    }
                    // if (isset($requestData['resume_type']) && ($requestData['resume_type'] == 'link')) {
                    //     $DoctorDetail['resume_type'] = $requestData['resume_type'];
                    //     if (isset($requestData['resume_link']) && ($requestData['resume_link'] != null)) {
                    //         $DoctorDetail['resume'] = $requestData['resume_link'];
                    //     }
                    // }
                    // else if (isset($requestData['resume_type']) && ($requestData['resume_type'] == 'attachment')) {
                    //     $DoctorDetail['resume_type'] = $requestData['resume_type'];
                    //     $filename = '';
                    //     if ($request->file('resume_attachment')) {
                    //         $fimage = $request->file('resume_attachment');           
                    //         $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
                    //         $fimage->move(public_path('/images/doctor/pdf/'), $filename);
                    //         $DoctorDetail['resume'] = $filename;
                    //     }
                    // }
                    
                    
                    if (isset($requestData['resume_link']) && ($requestData['resume_link'] != null)) {
                        $DoctorDetail['resume_link'] = $requestData['resume_link'];
                    }
                    $filename = '';
                    if ($request->file('resume_attachment')) {
                        $fimage = $request->file('resume_attachment');           
                        $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
                        $fimage->move(public_path('/images/doctor/pdf/'), $filename);
                        $DoctorDetail['resume_attachment'] = $filename;
                    }
                    
                    
                    if(isset($requestData['profile_name']))
                    $DoctorDetailData['profile_name'] = $requestData['profile_name'];
                    
                    $DoctorDetailData['address'] = ( isset($requestData['address']) &&  ($requestData['address'] != null && $requestData['address'] != 'null') ? $requestData['address'] : '');
                    if(isset($requestData['phone_no']))
                    $DoctorDetailData['tel_number'] = $requestData['phone_no'];
                    if(isset($requestData['profession_id']))
                    $DoctorDetailData['profession_id'] = $requestData['profession_id'];
                    if(isset($requestData['speciality_id']))
                    $DoctorDetailData['speciality_id'] = $requestData['speciality_id'];
                    if(isset($requestData['experience_id']))
                    $DoctorDetailData['experience_id'] = $requestData['experience_id'];
                    // if(isset($requestData['location_id']))
                    // $DoctorDetailData['location_id'] = $requestData['location_id'];

                    if(isset($requestData['degree_id'])){
                        if(is_array(json_decode($requestData['degree_id'])))
                        $DoctorDetailData['degree_id'] = implode(",",json_decode($requestData['degree_id']));
                    }
            
                    if(isset($requestData['license_id'])){
                        if(is_array(json_decode($requestData['license_id'])))
                        $DoctorDetailData['license_id'] = implode(",",json_decode($requestData['license_id']));
                    }
                    $DoctorDetail->update($DoctorDetailData);
                    
                    $locations = [];
                    if(isset($requestData['location_id'])){
                        if(is_array(json_decode($requestData['location_id']))){
                            $locations = json_decode($requestData['location_id']) ;
                            $locations = array_filter($locations, function($l) { return $l != 0; }) ;
                        }
                    }
                    $DoctorDetail->locations()->forceDelete() ;
                    foreach($locations as $loc){
                        Locations::create([
                            'type' => 'doctor_details',
                            'field_id' => $DoctorDetail->id ,
                            'location_id' => $loc
                        ]);
                    }
                    
                    $DoctorDetail = DoctorDetail::with('speciality','profession','experience','location')->select('doctor_details.*',
                    DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "'.url('').'",CONCAT("/images/doctor/"),doctor_details.profile_image) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS profile_image')
                    )->where('uid',$id)->first();
                    $DoctorDetail->doctor_id = $id;
                    unset($DoctorDetail->uid,$DoctorDetail->created_at,$DoctorDetail->updated_at);
                    $data = $this->doctorResponseDetails($DoctorDetail);
                }
                
            }
            else{
                $message = 'User Not Found';
                $status = false;
                $code=400;
            }
        }
        
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
    
    public function doctorResponseDetails($DoctorDetail){
        if($DoctorDetail != null)
        {
            
            if($DoctorDetail->resume_attachment != '')
            {
                if(file_exists(public_path('images/doctor/pdf/'.$DoctorDetail->resume_attachment)) && ($DoctorDetail->resume_attachment != null))
                {
                    $DoctorDetail->resume_attachment = url('').'/images/doctor/pdf/'.$DoctorDetail->resume_attachment;
                }
                else
                {
                    $DoctorDetail->resume_attachment = '';
                }
            }
            $DoctorDetail->profession_id = (($DoctorDetail->profession != null) ? $DoctorDetail->profession->title : '');
            $DoctorDetail->speciality_id = (($DoctorDetail->speciality != null) ? $DoctorDetail->speciality->title : '');
            $DoctorDetail->experience_id = (($DoctorDetail->experience != null) ? $DoctorDetail->experience->title : '');
            // $DoctorDetail->location_id = (($DoctorDetail->location != null) ? $DoctorDetail->location->name : '');
             $DoctorDetail->location_id = $DoctorDetail->comma_sep_loc_names ;
            
            if($DoctorDetail->degree_id != null)
            {
                $arr = explode(',',$DoctorDetail->degree_id);
                $str = '';
                foreach ($arr as $key1 => $value1) {
                    $Degree = Degree::where('id',$value1)->first();
                    if($Degree != null)
                    $str .= (($str != null ) ? ','.$Degree->title : $Degree->title);
                }
                $DoctorDetail->degree_id = $str;
            }
            else
            {
                $DoctorDetail->degree_id = '';
            }

            if($DoctorDetail->license_id != null)
            {
                $arr = explode(',',$DoctorDetail->license_id);
                $str = '';
                foreach ($arr as $key1 => $value1) {
                    $State = State::where('id',$value1)->first();
                    if($State != null)
                    $str .= (($str != null ) ? ','.$State->name : $State->name);
                }
                $DoctorDetail->license_id = $str;
            }
            else
            {
                $DoctorDetail->license_id = '';
            }
            
        }
        return $DoctorDetail;
    }

    public function doctorDetail(Request $request){   
        $data = [];
        $message = "Success";
        $status = true;
        $code = 200;  
        
        $requestData = $request->all();
        $rules = array(
            'user_id'=>'required|integer'
        );
    
        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;
        }
        else {
            $id = $request->user_id;
            $user = User::where('id',$id)->where('user_type','doctor')->first();
            if($user){        
                $DoctorDetail = DoctorDetail::with('speciality','profession','experience','locations','user')->select('doctor_details.*',
                    DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "'.url('').'",CONCAT("/images/doctor/"),doctor_details.profile_image) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS profile_image')
                    )->where('uid',$id)->first();
                if($DoctorDetail == null){
                    $message = 'Doctor Not Found';
                    $status = false;
                    $code=400;
                }
                else {
                    $DoctorDetail->doctor_id = $id;
                    unset($DoctorDetail->uid,$DoctorDetail->created_at,$DoctorDetail->updated_at);
                    $data = $this->doctorResponseDetails($DoctorDetail);
                }
                  
            }
            else{
                $message = 'User Not Found';
                $status = false;
                $code=400;
            }
        }

        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
}
