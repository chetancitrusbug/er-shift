<?php

namespace App\Http\Controllers\Api;
use App\User;
use App\EmployeeDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class EmployeeController extends Controller
{
    public function employeeDetail(Request $request) {
        $data = [];
        $message = "success";
        $status = true;
        $code = 200;  
        $rules = array(
            'employee_id'=>'required|integer',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $id = $request->employee_id;
            $user = User::
                    select('id as user_id','name','fname','lname','email','is_active')
                    ->where('id',$id)
                    ->where('user_type','employee')
                    ->where('is_active','!=',0)->first();

            if($user){
                $user['employee_detail'] = EmployeeDetail::with('location')->
                select('employee_detail.emp_name','employee_detail.comp_name','employee_detail.comp_website','employee_detail.comp_information','employee_detail.tel_number','employee_detail.address','employee_detail.profile_pic','employee_detail.location_id',
                DB::raw('(CASE WHEN employee_detail.profile_pic != "" THEN CONCAT( "'.url('/images/employee/').'","/",employee_detail.profile_pic) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS profile_pic')
                )->where('uid',$id)->first();
                
                $user['employee_detail']->location_id = ((($user['employee_detail']->location != null) && ($user['employee_detail']->location->name != null)) ? $user['employee_detail']->location->name : '' );
                unset($user['employee_detail']->location);
                $data = $user;
                $message = 'success';
            }
            else{
                $message = 'No have Employee Detail';
                $code = 400;
                $status = 'false';
            }
        }
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
    public function employee_profile_update(Request $request){   
        $data = [];
        $flag = 1;
        $message = "Employee Updated Successfully!";
        $status = true;
        $code = 200;  
        
        $requestData = $request->all();
        //dd($requestData);
        $rules = array(
            'user_id'=>'required|integer',
            'fname'=>'required',
            'lname'=>'required',
            'emp_name'=>'required',
            'comp_name'=>'required',
			'location_id'=>'required|digits_between:1,3',
            'tel_number'=>'required|digits_between:10,12',
            'profile_pic'=>'sometimes|mimes:jpg,jpeg,png|max:10000',
            
        );
        
        $validator = \Validator::make($request->all(), $rules, []);
       
        if ($validator->fails()) {
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;
        }
       else {
            $id = $request->user_id;
            $user = User::find($id);
            if($user){
                $userData = array();
                if(isset($requestData['fname']))
                {
                    $userData['fname'] = $requestData['fname'];
                }
                if(isset($requestData['lname']))
                {
                    $userData['lname'] = $requestData['lname'];
                }
                if($userData != null)
                {
                    $user->update($userData);
                }                
                $EmployeeDetail = EmployeeDetail::where('uid',$id)->first();
                if($EmployeeDetail == null){
                    $message = 'Employee Not Found!!';
                    $status = false;
                    $code=400;
                }
                else {
                    $EmployeeDetailData = array();
                    if ($request->file('profile_pic')) {
                        $fimage = $request->file('profile_pic');           
                        $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
                        $fimage->move(public_path('images/employee'), $filename);
                        $EmployeeDetail['profile_pic'] = $filename;
                    }
                     
                    
                    if(isset($requestData['emp_name'])){
                        $EmployeeDetailData['emp_name'] = $requestData['emp_name'];
                    }
                    if(isset($requestData['comp_name']))
                    {
                        $EmployeeDetailData['comp_name'] = $requestData['comp_name'];
                    }
                    if(isset($requestData['comp_website']))
                    {
                        $EmployeeDetailData['comp_website'] = $requestData['comp_website'];
                    }
                    if(isset($requestData['tel_number'])){
                        $EmployeeDetailData['tel_number'] = $requestData['tel_number'];
                    }
                    if(isset($requestData['comp_information']))
                    {
                        $EmployeeDetailData['comp_information'] = $requestData['comp_information'];
                    }
                    
                    $EmployeeDetailData['address'] = (($requestData['address'] != null && $requestData['address'] != 'null') ? $requestData['address'] : '');
                    if(isset($requestData['location_id'])){
						$EmployeeDetailData['location_id'] = $requestData['location_id'];
					}
                    $EmployeeDetail->update($EmployeeDetailData);
                }
                $EmployeeDetail = EmployeeDetail::select('employee_detail.*', 'employee_detail.uid as user_id',
                DB::raw('(CASE WHEN employee_detail.profile_pic != "" THEN CONCAT( "'.url('/images/employee/').'","/",employee_detail.profile_pic) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS profile_pic')
                )->where('uid',$id)->first();
				//($EmployeeDetail);
                $EmployeeDetail->employee_id = $EmployeeDetail->id;
                unset($EmployeeDetail->id,$EmployeeDetail->uid,$EmployeeDetail->created_at,$EmployeeDetail->updated_at,$EmployeeDetail->employee_id);
                $data = $EmployeeDetail;  
            }
            else{
                $message = 'Employee Not Found!!';
                $status = false;
                $code=400;
            }
        }

        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
}
