<?php

namespace App\Http\Controllers\Api;
use App\Package;
use App\Order;
use App\Subscription;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;

class PackageController extends Controller
{
    public function list_package(Request $request){

        $result = [];
        $message = "success";
        $status = true;
        $code = 200;  
        //$current_pack = Order::where('emp_id',$request->user_id)->latest()->first();
        $current_pack = Subscription::with('packageDetail')->where('user_id',$request->user_id)->where('status',1)->latest()->first();
        
           // show active stripe packages
          $stripe = Stripe::make(config('services.stripe.secret'));  
          $plans = $stripe->plans()->all();
          $produts=[];
          foreach($plans['data'] as $plan){
              $produts[] = $plan['product'] ;
  
          }
        
        if($current_pack){
            if (isset($current_pack->packageDetail->isFree) && $current_pack->packageDetail->isFree == 1) {
                $Package = Package::select('id as package_id','title','description','amount','day','isFree')->where('status','1')->get();
            } else {
                $Package = Package::select('id as package_id','title','description','amount','day','isFree')->where('isFree',0)->where('status','1')->get();
            }
        }
        else{
            $Package = Package::select('id as package_id','title','description','amount','day','isFree')->whereIn("stripe_product_id",$produts)->where('isFree',0)->where('status','1')->get();
        }
        if($Package){
            if(count($Package) > 0){
                foreach($Package as $_pack) {
                    if($current_pack){
                        if ($_pack->package_id == $current_pack->package ) {
                            $_pack->package_active = "YES";
                        } else {
                            $_pack->package_active = "NO";
                        }
                    }
                    else{
                        $_pack->package_active = "NO";
                    }
                }
				$result = $Package;
                $message = 'success';
            }
            else{
                
                $message = 'No Priority Found';
                $code = 400;
                $status = 'false';
            }
        }
        else{
            $message = 'Not Found Any Package';
            $code = 400;
            $status = 'false';
        }   
        
        return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
    
    public function Package_detail(Request $request){
        $result = [];
        $msg = "success";
        $status = true;
        $code = 200;

        $rules=array(
            'package_id'=>'integer|required'
        );
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $id = $request->package_id;
            $package = Package::select('id as package_id','package.*')->where('id',$id)->where('status',1)->first();
            if($package){
                unset($package->created_at,$package->updated_at,$package->id);
                $result = $package;
                $message = 'success';
            }
            else{
                $message = 'No Record Found!';
                $code = 400;
                $status = 'false';
            }
        }
        return response()->json(['result'=>$result,'status'=>$status,'message'=>$message,'code'=>$code]);
    }
}
