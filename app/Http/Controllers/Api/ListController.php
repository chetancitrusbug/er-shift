<?php

namespace App\Http\Controllers\Api;
use App\Education;
use App\Experience;
use App\EmployeeDetail;
use App\DoctorWish;
use App\State;
use App\Profession;
use App\Speciality;
use App\Degree;
use App\Shift;
use App\Edvolume;
use App\Priority;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ListController extends Controller
{
    public function listAll(Request $request) {   
        $data = [];
        $message = "success";
        $status = true;
        $code = 200;  
        
        $requestData = $request->all();
        $id = $request->profession_id;
        $data['priority'] = Priority::where('status','1')->get();
        if(count($data['priority']) > 0){
            foreach ($data['priority'] as $key => $value) {
                $data['priority'][$key]->priority_id =$value->id;
                unset($value->id,$value->created_at,$value->updated_at,$value->status);
            }
        }
        $data['shift'] = Shift::where('status','1')->get();
        if(count($data['shift']) > 0){
            foreach ($data['shift'] as $key => $value) {
                $data['shift'][$key]->shift_id =$value->id;
                unset($value->id,$value->created_at,$value->updated_at,$value->status);
            }
        }
        $data['edvolume'] = Edvolume::where('status','1')->get();
        if(count($data['edvolume']) > 0){
            foreach ($data['edvolume'] as $key => $value) {
                $data['edvolume'][$key]->edvolume_id =$value->id;
                unset($value->id,$value->created_at,$value->updated_at,$value->status);
            }
        }
        $data['experience'] = Experience::where('status','1')->get();
        if(count($data['experience']) > 0){
            foreach ($data['experience'] as $key => $value) {
                $data['experience'][$key]->experience_id =$value->id;
                unset($value->id,$value->created_at,$value->updated_at,$value->status);
            }
        }
        $data['state'] = State::where('status','1')->get();
        if(count($data['state']) > 0){
            foreach ($data['state'] as $key => $value) {
                $data['state'][$key]->state_id =$value->id;
                unset($value->id,$value->created_at,$value->updated_at,$value->status);
            }
        }
       $data['profession'] = Profession::with(['speciality' => function($q){$q->where('status', '1');},
        'degree' => function($q1){  $q1->where('status', '1'); }])->where('status','1')->get();
        if(count($data['profession']) > 0){
            foreach ($data['profession'] as $key => $value) {
                $data['profession'][$key]->profession_id =$value->id;
                unset($value->id,$value->created_at,$value->updated_at,$value->status);
            }
        }
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function listPriority(Request $request) {   
        $data = [];
        $message = "success";
        $status = true;
        $code = 200;  
        $Priority = Priority::where('status','1')->get();
        if($Priority){
            if(count($Priority) > 0){
                foreach ($Priority as $key => $value) {
                    $Priority[$key]->priority_id =$value->id;
                    unset($value->id,$value->created_at,$value->updated_at,$value->status);
                }
                $data = $Priority;
                $message = 'success';
            }
            else{
                
                $message = 'No Priority Found';
                $code = 400;
                $status = 'false';
            }
        }
        else{
            $message = 'No have Ed volume';
            $code = 400;
            $status = 'false';
        }        
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
    
    public function listShift(Request $request) {   
        $data = [];
        $message = "success";
        $status = true;
        $code = 200;  
        $Shift = Shift::where('status','1')->get();
        if($Shift){
            if(count($Shift) > 0){
                foreach ($Shift as $key => $value) {
                    $Shift[$key]->Shift_id =$value->id;
                    unset($value->id,$value->created_at,$value->updated_at,$value->status);
                }
                $data = $Shift;
                $message = 'success';
            }
            else{
                
                $message = 'No Shift Found';
                $code = 400;
                $status = 'false';
            }

            
        }
        else{
            $message = 'No have Ed volume';
            $code = 400;
            $status = 'false';
        }        
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function listEDVolume(Request $request) {   
        $data = [];
        $message = "success";
        $status = true;
        $code = 200;  
        $Edvolume = Edvolume::where('status','1')->get();
        if($Edvolume){
            
            if(count($Edvolume) > 0){
                foreach ($Edvolume as $key => $value) {
                    $Edvolume[$key]->edvolume_id =$value->id;
                    unset($value->id,$value->created_at,$value->updated_at,$value->status);
                }
                $data = $Edvolume;
                $message = 'success';
            }
            else{
                
                $message = 'No Edvolume Found';
                $code = 400;
                $status = 'false';
            }
        }
        else{
            $message = 'No have Ed volume';
            $code = 400;
            $status = 'false';
        }        
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function listExperience(Request $request) {   
        $data = [];
        $message = "success";
        $status = true;
        $code = 200;  
        $Experience = Experience::where('status','1')->get();
        if($Experience){
            
            if(count($Experience) > 0){
                foreach ($Experience as $key => $value) {
                    $Experience[$key]->experience_id =$value->id;
                    unset($value->id,$value->created_at,$value->updated_at,$value->status);
                }
                $data = $Experience;
                $message = 'success';
            }
            else{
                
                $message = 'No Experience Found';
                $code = 400;
                $status = 'false';
            }
        }
        else{
            $message = 'No have Experience';
            $code = 400;
            $status = 'false';
        }        
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
    
    public function listLicense(Request $request) {   
        $data = [];
        $message = "success";
        $status = true;
        $code = 200;  
        $State = State::where('status','1')->get();
        if($State){
            
            if(count($State) > 0){
                foreach ($State as $key => $value) {
                    $State[$key]->license_id =$value->id;
                    unset($value->id,$value->code,$value->created_at,$value->updated_at,$value->status);
                }
                $data = $State;
                $message = 'success';
            }
            else{
                $message = 'No License Found';
                $code = 400;
                $status = 'false';
            }
        }
        else{
            $message = 'No have License';
            $code = 400;
            $status = 'false';
        }        
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function listLocation(Request $request) {   
        $data = [];
        $message = "success";
        $status = true;
        $code = 200;  
        $State = State::where('status','1')->get();
        if($State){
            
            if(count($State) > 0){
                foreach ($State as $key => $value) {
                    $State[$key]->location_id =$value->id;
                    unset($value->id,$value->code,$value->created_at,$value->updated_at,$value->status);
                }
                $data = $State;
                $message = 'success';
            }
            else{
                $message = 'No Location Found';
                $code = 400;
                $status = 'false';
            }
        }
        else{
            $message = 'No have Locatiuon';
            $code = 400;
            $status = 'false';
        }        
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function listDegree(Request $request) {   
        $data = [];
        $message = "success";
        $status = true;
        $code = 200;  
        if ($request->profession_id) {
            $id = $request->profession_id;
            $Degree = Degree::where('profession_id',$id)->where('status','1')->get();
            if($Degree){
                
                if(count($Degree) > 0){
                    foreach ($Degree as $key => $value) {
                        $Degree[$key]->degree_id =$value->id;
                        unset($value->id,$value->profession_id,$value->created_at,$value->updated_at,$value->status);
                    }
                    $data = $Degree;
                    $message = 'success';
                }
                else{
                    $message = 'No Degree Found';
                    $code = 400;
                    $status = 'false';
                }
            }
            else{
                $message = 'No have Degree';
                $code = 400;
                $status = 'false';
            }
        } else {
            $status = false;
            $message = 'Not valid data';
            $code = 400;
        }

        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function listSpeciality(Request $request) {   
        $data = [];
        $message = "success";
        $status = true;
        $code = 200;  
        if ($request->profession_id) {
            $id = $request->profession_id;
            $Speciality = Speciality::where('profession_id',$id)->where('status','1')->get();
            if($Speciality){
                if(count($Speciality) > 0){
                    foreach ($Speciality as $key => $value) {
                        $Speciality[$key]->speciality_id =$value->id;
                        unset($value->id,$value->profession_id,$value->created_at,$value->updated_at,$value->status);
                    }
                    $data = $Speciality;
                    $message = 'success';
                }
                else{
                    $message = 'No Speciality Found';
                    $code = 400;
                    $status = 'false';
                }
            }
            else{
                $message = 'No have Speciality';
                $code = 400;
                $status = 'false';
            }
        } else {
            $status = false;
            $message = 'Not valid data';
            $code = 400;
        }

        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function listState(Request $request) {   
        $data = [];
        $message = "success";
        $status = true;
        $code = 200;  
        $State = State::where('status','1')->get();
        if($State){
            
            if(count($State) > 0){
                foreach ($State as $key => $value) {
                    $State[$key]->state_id =$value->id;
                    unset($value->id,$value->code,$value->created_at,$value->updated_at,$value->status);
                }
                $data = $State;
                $message = 'success';
            }
            else{
                $message = 'No State Found';
                $code = 400;
                $status = 'false';
            }
        }
        else{
            $message = 'No have States';
            $code = 400;
            $status = 'false';
        }        
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function listProfesion(Request $request) {   
        $data = [];
        $message = "success";
        $status = true;
        $code = 200;  
        $Profession = Profession::where('status','1')->get();
        if($Profession){
            
            if(count($Profession) > 0){
                foreach ($Profession as $key => $value) {
                    $Profession[$key]->profession_id =$value->id;
                    unset($value->id,$value->created_at,$value->updated_at,$value->status);
                }
                $data = $Profession;
                $message = 'success';
            }
            else{
                $message = 'No Profession Found';
                $code = 400;
                $status = 'false';
            }
        }
        else{
            $message = 'No have Profession';
            $code = 400;
            $status = 'false';
        }        
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
}
