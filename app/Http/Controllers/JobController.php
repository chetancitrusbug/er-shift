<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Subscription;
use Yajra\Datatables\Datatables;
use App\JobDetail;
use App\Profession;
use App\Speciality;
use App\Degree;
use App\State;
use App\Experience;
use App\Edvolume;
use App\Shift;
use App\Setting;
use App\JobDates;
use App\DoctorWish;

class JobController extends Controller
{
    public function __construct()
    {
        $this->middleware('plandata');
    }

    public function index(Request $request)
    {
        $shifts = Auth::user()->employeeDetail ? Auth::user()->employeeDetail->jobs()->where('status','!=','2')->latest()->paginate(10) : [] ;
        return view('frontend-new.job.index', compact('shifts'));
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $job = JobDetail::with('profession','speciality','degree')->where('status','!=','2')
        ->where('employee_id',Auth::user()->id);
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(job_detail.job_title LIKE  '%$value%' OR job_detail.job_responsibility LIKE  '%$value%' )";
                $job = $job->whereRaw($where_filter);
            }
        }
        $job = $job->orderBy('job_detail.id','DESC');
        return Datatables::of($job)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        $plan_detail = Subscription::where('status',1)->where('user_id',Auth::user()->id)->first();
        if($plan_detail != null){
         $plan = json_decode($plan_detail->package_detail);
        }
        else
        {
         $plan=0;
        }

        $Setting = Setting::where('key', 'job_max_rate')->first();

       
        $jobMaxRate = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 100);

     
        // $jobMaxRateArray = array();
        // for ($i=1; $i <= $jobMaxRate ; $i++) {
        //     $jobMaxRateArray[$i]=$i;
        // }


        $profession = Profession::where('status','1')->pluck('title','id')->prepend('Select Profession','');
        //$speciality = Speciality::where('status','1')->pluck('title','id')->prepend('Select Speciality','');
        $degree = Degree::where('status','1')->pluck('title','id')->prepend('Select Degree','');
        $location = State::where('status','1')->pluck('name','id')->prepend('Select Location','');
        $experience = Experience::where('status','1')->pluck('title','id')->prepend('Select Experience','');
        $ed_volume = Edvolume::where('status','1')->pluck('title','id')->prepend('Select Ed volume','');
        $shift = Shift::where('status','1')->pluck('title','id')->prepend('Select Shift','');
        return view('frontend-new.job.create',compact('profession','speciality','degree','location','experience','ed_volume','shift','plan','jobMaxRate'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {

        $valmessages = [
            'job_title.required' => 'Shift title field is required.',
            'job_responsibility.required' => 'Shift responsibility field is required.'
        ];
        $this->validate($request, [
            'job_title'=>'required',
            'job_responsibility'=>'required',
            'profession_id'=>'required|integer',
            'speciality_id'=>'sometimes|integer',
            'degree_id'=>'required|integer',
            'experience_id'=>'required|integer',
            'location_id'=>'required|integer',
            'ed_volume_id'=>'required|integer',
            'dates'=>'required',
            'shift_id'=>'required|integer',
            'rate'=>'required|numeric',
        ],$valmessages);


        $data =$request->all();
        $data['employee_id'] = Auth::user()->id;
        $data['address'] = (($request->address != null) ? $request->address : '');
        $data['speciality_id'] = (($request->speciality_id != null) ? $request->speciality_id : 0);
       
        $job = JobDetail::create($data);
        $datesArray = explode(',', $request->dates);
        foreach ($datesArray as $key => $value) {
            $datesData['job_id'] = $job->id;
            $datesData['job_date'] = $value;
            JobDates::create($datesData);
        }

        Session::flash('flash_message', __('message.job.shift_added') );
        return redirect()->route('my-shifts')->withStatus( __('message.job.shift_added') );
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
         $plan_detail = Subscription::where('status',1)->where('user_id',Auth::user()->id)->first();
        if($plan_detail != null){
         $plan = json_decode($plan_detail->package_detail);
        }
        else
        {
         $plan=0;
        }
        $profession = Profession::where('status','1')->pluck('title','id')->prepend('Select Profession','');

        $degree = Degree::where('status','1')->pluck('title','id')->prepend('Select Degree','');
        $location = State::where('status','1')->pluck('name','id')->prepend('Select Location','');
        $experience = Experience::where('status','1')->pluck('title','id')->prepend('Select Experience','');
        $ed_volume = Edvolume::where('status','1')->pluck('title','id')->prepend('Select Ed volume','');
        $shift = Shift::where('status','1')->pluck('title','id')->prepend('Select Shift','');
        $job = JobDetail::where('id', $id)->first();
       
        if ($job) {
            $jobDates = JobDates::where('job_id',$id)->get();
          
            $dates = array();
           
            if(isset($jobDates) && $jobDates != null)
            {

                    foreach ($jobDates as $key => $item)
                    {
                        $date = explode('-',$item->job_date);
                        $dates[] = $date[0].','.$date[1].','.$date[2];
                    }
            }
            $job->dates = $dates;
            $Setting = Setting::where('key', 'job_max_rate')->first();
            $jobMaxRate = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 100);
            // $jobMaxRateArray = array();
            // for ($i=1; $i <= $jobMaxRate ; $i++) {
            //     $jobMaxRateArray[$i]=$i;
            // }

            $speciality = Speciality::where('profession_id', $job->profession_id)->where('status','1')->pluck('title','id')->prepend('Select Speciality','');
            return view('frontend-new.job.edit', compact('job','profession','speciality','degree','location','experience','ed_volume','shift','plan','jobMaxRate'));
        } else {
            return redirect()->route('my-shifts')->withErrors( __('message.job.shift_not_found') );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $valmessages = [
            'job_title.required' => 'Shift title field is required.',
            'job_responsibility.required' => 'Shift responsibility field is required.'
        ];
        $this->validate($request, [
            'job_title'=>'required',
            'job_responsibility'=>'required',
            'profession_id'=>'required|integer',
            'speciality_id'=>'sometimes|integer',
            'degree_id'=>'required|integer',
            'experience_id'=>'required|integer',
            'location_id'=>'required|integer',
            'ed_volume_id'=>'required|integer',
            'dates'=>'required',
            // 'from_date'=>'sometimes|date|after:tomorrow',
            // 'to_date'=>'sometimes|date|after:from_date',
            'shift_id'=>'required|integer',
            'rate'=>'required|numeric',
        ],$valmessages);

        $datesArray = explode(',', $request->dates);
        $requestData =$request->all();
        $requestData['address'] = (($request->address != null) ? $request->address : '');
        $requestData['speciality_id'] = (($request->speciality_id != null) ? $request->speciality_id : 0);
        //$requestData['dates'] = (($request->dates != null) ? $request->dates : '');

        // $requestData['from_date'] = (($request->from_date != null) ? $request->from_date : '');
        // $requestData['to_date'] = (($request->to_date != null) ? $request->to_date : '');
        unset($requestData['dates'],$requestData['_method'],$requestData['_token'],$requestData['expire_in_days']);
        $job = JobDetail::where('id', $id);
        $job->update($requestData);


        $jobDates = JobDates::where('job_id',$id)->delete();

        foreach ($datesArray as $key => $value) {
            $datesData['job_id'] = $id;
            $datesData['job_date'] = $value;
            JobDates::create($datesData);
        }


        Session::flash('flash_message',  __('message.job.shift_updated') );
        return redirect()->route('my-shifts')->withStatus( __('message.job.shift_updated') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */

    public function destroy(Request $request, $id)
    {
        
        if (Auth::user()->employeeDetail) {
            $shift = JobDetail::where('id',$id)->where('employee_id',Auth::user()->id)->first();
            if ($shift) {
                $shift->update(['status' => 2 ]);
                return redirect()->route('my-shifts')->withStatus( __('message.job.shift_deleted') );
            }
        }

        return redirect()->back()->withErrors( __('message.job.shift_not_found') );
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {
        $recent_shifts = DoctorWish::whereHas('doctor_user')->with('doctor_user','edvolume')->latest()->limit(3)->get();

        $job = Auth::user()->employeeDetail->jobs()->where('status','!=','2')->where('id', $id)->first();
        if(!$job) {
            Session::flash('flash_message', __('message.job.shift_not_found') );
            return redirect()->route('my-shifts')->withErrors(__('message.job.shift_not_found'));
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $job->status= '0';
                $job->update();
                Session::flash('flash_message', 'Shift Status Updated Successfully!');
            }else{
                $job->status= '1';
                $job->update();
                Session::flash('flash_message', 'Shift Status Updated Successfully!');
            }

        }

        return view('frontend-new.job.view', compact('job', 'recent_shifts'));
    }


    public function addBlukJobs(Request $request)
    {
        $job = JobDetail::with('experience','edvolume','profession','speciality','degree','location','shift')->where('status', 1)->first();
        //dd($job);
        $profession = Profession::with('speciality', 'degree')->where('status', '1')->get();
        $shiftData = array();
        foreach($profession as $key => $value) {
            $shiftData[$key]['profession'] = $value->title;
            if(count($value->speciality) > 0)
            {
                foreach ($value->speciality as $speciality) {
                    $shiftData[$key]['speciality'][] = $speciality->title;
                }
            }
            else
            {
                $shiftData[$key]['speciality'][] = 'No have a speciality';
            }

            if(count($value->degree) > 0)
            {
                foreach ($value->degree as $degree) {
                    $shiftData[$key]['degree'][] = $degree->title;
                }
            }
            else
            {
                $shiftData[$key]['degree'][] = 'No have a degree';
            }
        }
        $Setting = Setting::where('key', 'job_max_rate')->first();
            $jobMaxRate = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 100);
        $sample = '';
        if(file_exists(public_path('sample/sample-jobs.csv')))
        {
            $sample = url('sample/sample-jobs.csv');
        }
        
        return view('frontend-new.job.addBluk',compact('sample','job','shiftData','jobMaxRate'));
    }

    public function storeJobs(Request $request)
    {
        $file = $request->file('file');

        if(!empty($file)){
            if($file->getClientOriginalExtension() == 'csv')
            {
                $uploadedFile['name'] = str_replace(' ','',$file->getClientOriginalName());
                $path = public_path('/uploads');
                $file->move($path,$uploadedFile['name']);
                $filePath = public_path('/uploads/').'/'.$uploadedFile['name'];
                $csvfile = fopen($filePath,"r");

                while(!feof($csvfile)){
                    $rows[]=fgetcsv($csvfile);
                }
                $data = array();
                $jobs = array();
                $c = 0;
                $Setting = Setting::where('key', 'job_max_rate')->first();
                $jobMaxRate = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 100);
               
                for($i= 1; $i < count($rows); $i++){
                    $k = 0;

                    if(is_array($rows[$i]))
                    {
                        if(count($rows[$i]) > 1){
                            foreach($rows[0] as $key=>$value){
                                $value = strtolower(str_replace(' ','_',$value));
                                $data[$c][$value] = $rows[$i][$k];
                                $k++;
                            }
                            $c++;
                        }
                    }
                }

                
                foreach($data as $key => $value){

                    $postData = array();
                    $valmessages = [
                        'job_title.required' => 'Shift title field is required.',
                        'job_responsibility.required' => 'Shift responsibility field is required.'
                    ];
                    $rules = array(
                        'job_title'=>'required',
                        'job_responsibility'=>'required',
                        'profession'=>'required',
                        'speciality'=>'required',
                        'degree'=>'required',
                        'experience'=>'required',
                        'location'=>'required',
                        'ed_volume'=>'required',
                        'dates'=>'required',
                        // 'from_date'=>'required|date|after:'.date('Y-m-d'),
                        // 'to_date'=>'required|date|after:from_date',
                        'shift'=>'required',
                        'rate'=>'required|numeric|min:0|max:'.$jobMaxRate,
                    );
                    $validator = \Validator::make($value, $rules, $valmessages);
                   
                    if ($validator->fails()) {
                        
                        Session::flash('flash_message', __('message.job.csv_data_missing_shift_not_added') );
                        return redirect()->back()->withErrors( __('message.job.csv_data_missing_shift_not_added') );
                    }
                    else
                    {
                        if($value['location'] != '')
                        {
                            $state = State::where('name','like',$value['location']. '%')->first();
                            if($state != null)
                            $postData['location_id'] = $state->id;
                        }
                        if($value['profession'] != '')
                        {
                            $Profession = Profession::where('title','like',$value['profession']. '%')->first();
                            if($Profession != null)
                            {
                                $postData['profession_id'] = $Profession->id;

                                if($value['speciality'] != '' && $postData['profession_id'] != '')
                                {
                                    $Speciality = Speciality::where('profession_id',$postData['profession_id'])->where('title','like',$value['speciality']. '%')->first();
                                    if($Speciality != null)
                                    $postData['speciality_id'] = $Speciality->id;
                                }
                            }
                        }

                        if($value['degree'] != '')
                        {
                            $Degree = Degree::where('title','like',$value['degree']. '%')->first();
                            if($Degree != null)
                            $postData['degree_id'] = $Degree->id;
                        }

                        if($value['ed_volume'] != '')
                        {

                            $Edvolume = Edvolume::where('title','like',$value['ed_volume'].'%')->first();
                            if($Edvolume != null)
                            $postData['ed_volume_id'] = $Edvolume->id;
                            else{
                                $ed_volume = explode('-',$value['ed_volume']);
                                $Edvolume = null;
                                if(is_array($experience))
                                {
                                    if(count($ed_volume) > 1)
                                    {
                                        $ed_volume[0] =  preg_replace("/[^0-9,.]/", "", $ed_volume[0]);
                                        $ed_volume[1] = preg_replace("/[^0-9,.]/", "", $ed_volume[1]);
                                        $where_filter = "(min LIKE '%$ed_volume[0]%' OR max LIKE  '%$ed_volume[1]%' )";
                                        $Edvolume = Edvolume::whereRaw($where_filter)->first();
                                    }
                                    elseif((count($ed_volume)) == 1)
                                    {
                                        $ed_volume[0] =  preg_replace("/[^0-9,.]/", "", $ed_volume[0]);
                                        $Edvolume = Edvolume::where('min','<=',$ed_volume[0])->where('max','>=',$ed_volume[0])->first();
                                        //dd($Edvolume);
                                    }
                                    if($Edvolume != null)
                                    $postData['ed_volume_id'] = $Edvolume->id;
                                }
                            }
                        }

                        if($value['experience'] != '')
                        {
                            $Experience = Experience::where('title','like',$value['experience']. '%')->first();
                            if($Experience != null)
                            $postData['experience_id'] = $Experience->id;
                            else{
                                $experience = explode('-',$value['experience']);
                                $Experience = null;
                                if(is_array($experience))
                                {
                                    if(count($experience) > 1)
                                    {
                                        $experience[0] =  preg_replace("/[^0-9,.]/", "", $experience[0]);
                                        $experience[1] = preg_replace("/[^0-9,.]/", "", $experience[1]);
                                        $where_filter = "(min LIKE '%$experience[0]%' OR max LIKE  '%$experience[1]%' )";
                                        $Experience = Experience::whereRaw($where_filter)->first();
                                    }
                                    elseif((count($experience)) == 1)
                                    {
                                        $experience[0] =  preg_replace("/[^0-9,.]/", "", $experience[0]);
                                        $Experience = Experience::where('min','<=',$experience[0])->where('max','>=',$experience[0])->first();
                                    }
                                    if($Experience != null)
                                    $postData['experience_id'] = $Experience->id;
                                }

                            }
                        }


                        if($value['shift'] != '')
                        {
                            $Shift = Shift::where('title','like',$value['shift']. '%')->first();
                            if($Shift != null)
                            $postData['shift_id'] = $Shift->id;
                        }
                        if($value['job_title'] != '')
                        {
                            $postData['job_title'] = $value['job_title'];
                        }
                        if($value['job_responsibility'] != '')
                        {
                            $postData['job_responsibility'] = $value['job_responsibility'];
                        }
                        if($value['rate'] != '')
                        {
                            $postData['rate'] = $value['rate'];
                        }

                        $postData['status'] = 1;
                        $postData['address'] = (($value['address'] != null) ? $value['address'] : '');
                        $postData['employee_id'] = Auth::user()->id;
                        
                        if(count($postData) >= 13)
                        {
                            $job = JobDetail::create($postData);
                            $jobs[] = $job;
                            $datesArray = explode('|', $value['dates']);
                            $datesData = array();
                            $now = date('Y-m-d');
                            foreach ($datesArray as $key => $value) {
                                $date_arr  = explode('/', $value);
                                if ($date_arr != null && is_numeric($date_arr[0]) && is_numeric($date_arr[1]) && is_numeric($date_arr[2]) && checkdate($date_arr[0], $date_arr[1], $date_arr[2])) 
                                { 
                                    if(strtotime($value) > strtotime($now)) {
                                        $datesData[] = array('job_id'=> $job->id,'job_date' => date('Y-m-d',strtotime($value)));
                                    }                                
                                }
                            }
                            if($datesData != null)
                            {

                                JobDates::insert($datesData); 
                            }

                        }
                    }
                }
                
                fclose($csvfile);
               
                if((count($data)) == (count($jobs)))
                {
                    Session::flash('flash_message',  __('message.job.bulk_shifts_added') );
                    return redirect('my-shifts')->withStatus(  __('message.job.bulk_shifts_added') );
                }
                else {
                    Session::flash('flash_message',  __('message.job.csv_data_missing_shift_not_added') );
                    return redirect()->back()->withErrors( __('message.job.csv_data_missing_shift_not_added') );
                }

                unlink($filePath);
            }
            else
            {
                Session::flash('flash_message', __('message.job.select_csv_file') );
                return redirect('addBlukJobs')->withErrors( __('message.job.select_csv_file') );
            }
        }
        else
        {
            Session::flash('flash_message', __('message.job.select_file') );
            return redirect('addBlukJobs')->withErrors( __('message.job.select_file') );
        }
        return redirect('addBlukJobs');

    }

    public function getSpeciality(Request $request,$id)
    {
        $speciality = Speciality::where('profession_id', $id)->where('status','1')->pluck('title','id');
        $speciality = json_encode($speciality);
        return $speciality;
    }

    public function download(Request $request)
    {
        $profession = Profession::with('speciality','degree')->where('status','1')->get();

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=professionData.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $callback = function() use ($profession)
        {
            $columnsSpeciality = array('--- Profession ---','--- Speciality ---');
            $columnsDegree = array('--- Profession ---','--- Degree ---');
            $file = fopen('php://output', 'w');


            foreach($profession as $export) {
                fputcsv($file, $columnsSpeciality);
                if(count($export->speciality) > 0)
                {
                    foreach ($export->speciality as $speciality) {
                        fputcsv($file, array($export->title,$speciality->title));
                    }
                }
                else
                {
                    fputcsv($file, array($export->title,'No have a speciality'));
                }
            }
            foreach($profession as $export) {
                fputcsv($file, $columnsDegree);
                if(count($export->degree) > 0)
                {
                    foreach ($export->degree as $degree) {
                        fputcsv($file, array($export->title,$degree->title));
                    }
                }
                else
                {
                    fputcsv($file, array($export->title,'No have a degree'));
                }
            }
            fclose($file);
        };

       return \Response::stream($callback, 200, $headers);
    }
}
