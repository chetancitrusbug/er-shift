<?php

namespace App\Http\Controllers;
use App\Package;
use App\Order;
use App\Subscription;
use App\Profession ;
use Auth;
use Illuminate\Http\Request;
use App\DoctorDetail;
use App\User;
use App\Contact;
use App\DoctorWish;
use App\Degree;
use Session;
use Mail;
use DB;
use App\State;
use Yajra\Datatables\Datatables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->mail_function = new EmailController();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        $professions = Profession::where('status', 1)->pluck('title','id');

        $recent_shifts = DoctorWish::whereHas('doctor_user')->with('doctor_user','edvolume')->latest()->limit(3)->get();

        return view('frontend-new.home', compact('recent_shifts','professions'));
    }

    public function contactus(){
        return view('frontend-new.cms.contact');
    }

    public function contactAdd(Request $request ){
        $contact = new Contact;

        $contact->email = $request->input('email');
        $contact->name = $request->input('name');
        $contact->comment = $request->input('comment');

        if ($contact->save()) {

            $subject = "Contact us Inquiry";
            $to = config('setting.ADMIN_MAIL');
            \Mail::send('frontend-new.emails.contact', compact( 'to', 'contact'), function ($message) use ( $to,$subject) {
                $message->to($to)->subject($subject);
            });

            return redirect()->back()->withStatus( __('message.contact.request_submitted_thankyou_for_contact') );

        } else {

            return redirect()->back()->withErrors( __('message.contact.something_went_wrong') );
        }
        return redirect()->back();
    }

    public function pricing (){
        $packages = Package::where("stripe_product_id", "!=", null)->where('status', 1)->where('isFree', '!=', 1)->orderBy("amount", "ASC")->get();
        return view('frontend.pricing', compact('packages'));
    }

    public function terms(){
        return view('frontend-new.cms.terms');
    }

    public function privacy(){

        return view('frontend-new.cms.privacy');
    }

    public function dashboard(){
       return view('frontend.dashboard.home',compact('plan'));
    }

    public function datatable(request $request)
    {
        $doctorWishId =  $this->recommendedGetDoctorWishId(Auth::user()->id);

        $SearchDetail = DoctorWish::select('doctor_wish.*','profession.title as profession_title'
        ,DB::raw('(CASE WHEN doctor_details.profile_name != "" THEN doctor_details.profile_name ELSE users.name END) AS doctorName')
        )->with('edvolume')
        ->join('users', 'users.id', '=', 'doctor_wish.doctor_id')
        ->join('doctor_details', 'doctor_details.uid', '=', 'doctor_wish.doctor_id')
        ->join('profession', 'profession.id', '=', 'doctor_details.profession_id')
        ->whereIn('doctor_wish.id', $doctorWishId);
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(profession.title LIKE  '%$value%' OR doctor_details.profile_name LIKE  '%$value%' OR doctor_wish.wish_title LIKE  '%$value%' OR doctor_wish.rate LIKE  '%$value%' )";
                $SearchDetail = $SearchDetail->whereRaw($where_filter);
            }
        }
        $SearchDetail = $SearchDetail->get();
        foreach ($SearchDetail as $key => $value) {
            if ($value->location_id != null) {
                $arr = explode(',', $value->location_id);
                $str = '';
                foreach ($arr as $key1 => $value1) {
                    $State = State::select('id', 'name')->where('id', $value1)->first();
                    if ($State != null)
                        $str .= (($str != null ) ? ','.$State->name : $State->name);;
                }
                $SearchDetail[$key]->location = $str;
            } else {
                $SearchDetail[$key]->location = [];
            }

            if($SearchDetail[$key]->edvolume != null)
            {
                $SearchDetail[$key]->edvolume_title = (($SearchDetail[$key]->edvolume->title != null ) ? $SearchDetail[$key]->edvolume->title : '');
            }
            else
            {
                $SearchDetail[$key]->edvolume_title = '';
            }

            unset($value->edvolume,$value->created_at, $value->updated_at, $value->deleted_at);

        }
        return Datatables::of($SearchDetail)
            ->make(true);
        exit;
    }

    public function show(Request $request,$id)
    {
        $SearchDetail = DoctorWish::select('doctor_wish.*',
        DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "'.url('').'",CONCAT("/images/doctor/"),doctor_details.profile_image) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS doctorImage')
        ,DB::raw('(CASE WHEN doctor_details.profile_name != "" THEN doctor_details.profile_name ELSE users.name END) AS doctorName')
        )->with('edvolume')
        ->join('users', 'users.id', '=', 'doctor_wish.doctor_id')
        ->join('doctor_details', 'doctor_details.uid', '=', 'doctor_wish.doctor_id')
        ->where('doctor_wish.id', $id)->first();

        $SearchDetail->doctorDetail = DoctorDetail::
        where('doctor_details.uid', $SearchDetail->doctor_id)
        ->with('speciality','experience','location','profession')
        ->first();


        $srchLocStr = '';
        if ($SearchDetail->location_id != null) {
            $arr = explode(',', $SearchDetail->location_id);
            foreach ($arr as $key1 => $value1) {
                $State = State::select('id', 'name')->where('id', $value1)->first();
                if ($State != null)
                    $srchLocStr .= (($srchLocStr != null ) ? ','.$State->name : $State->name);;
            }
        }
        $SearchDetail->location = $srchLocStr;

        $srchDocDegreeStr = '';
        if($SearchDetail->doctorDetail->degree_id != null)
        {
            $arr = explode(',',$SearchDetail->doctorDetail->degree_id);
            foreach ($arr as $key1 => $value1) {
                $Degree = Degree::where('id',$value1)->first();
                if($Degree != null)
                $srchDocDegreeStr .= (($srchDocDegreeStr != null ) ? ','.$Degree->title : $Degree->title);
            }
        }
        $SearchDetail->doctorDetail->degree = $srchDocDegreeStr;

        $srchDocLicenseStr = '';
        if($SearchDetail->doctorDetail->license_id != null)
        {
            $arr = explode(',',$SearchDetail->doctorDetail->license_id);
            foreach ($arr as $key1 => $value1) {
                $State = State::where('id',$value1)->first();
                    if($State != null)
                    $srchDocLicenseStr .= (($srchDocLicenseStr != null ) ? ','.$State->name : $State->name);;
            }
        }

        if($SearchDetail->doctorDetail->resume_type == 'attachment')
        {
            if(file_exists(public_path('images/doctor/pdf/'.$SearchDetail->doctorDetail->resume)) && ($SearchDetail->doctorDetail->resume != null))
            {
                $SearchDetail->doctorDetail->resume = url('/images/doctor/pdf/').'/'.$SearchDetail->doctorDetail->resume;
            }
            else
            {
                $SearchDetail->doctorDetail->resume = '';
            }
        }

        $SearchDetail->doctorDetail->license = $srchDocLicenseStr;
        $SearchDetail->edvolume_title = ((($SearchDetail->edvolume) && ($SearchDetail->edvolume->title != null )) ? $SearchDetail->edvolume->title : '');

        return view('frontend.dashboard.show',compact('SearchDetail'));
    }

}
