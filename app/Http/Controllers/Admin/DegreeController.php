<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\JobDetail;
use App\Profession;
use App\Degree;
use Session;
use Yajra\Datatables\Datatables;

class DegreeController extends Controller
{
    public function index(Request $request)
    {

        return view('admin.degree.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $degree = Degree::with('professions')->get();
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(degree.title LIKE  '%$value%' OR degree.status LIKE  '%$value%'  )";
                $degree = Degree::with('professions')->whereRaw($where_filter);
            }
        }

        if ($request->get('status') != '') {
            $status = $request->get('status');
            $degree = $degree->where('status', $status);

        }
        return Datatables::of($degree)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        $profession = Profession::where('status','1')->pluck('title','id')->prepend('Select Profession','');

        return view('admin.degree.create',compact('profession'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'title' => 'required', //['required', \Illuminate\Validation\Rule::unique('degree', 'title')->whereNull('deleted_at')],
            'profession_id' => 'required',
            'status' => 'required',
        ]);
        $data = array(
            'title' => $request->input('title'),
            'profession_id' => $request->input('profession_id'),
            'status' => $request->input('status'));
        $degree = Degree::create($data);

        Session::flash('flash_message', 'Degree added!');
        return redirect('admin/degree');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $degree = Degree::where('id', $id)->first();
        $profession = Profession::where('status','1')->pluck('title','id')->prepend('Select Profession','');

        if ($degree) {
            return view('admin.degree.edit', compact('degree','profession'));
        } else {
            Session::flash('flash_message', 'Degree is not exist!');
            return redirect('admin/degree');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required', //'required|unique:degree,title,'.$id,
            'profession_id' => 'required',
            'status' => 'required',
        ]);
        $requestData = array(
            'title' => $request->input('title'),
            'profession_id' => $request->input('profession_id'),
            'status' => $request->input('status'));

        $degree = Degree::where('id', $id);
        $degree->update($requestData);
        Session::flash('flash_message', 'Degree Updated Successfully!');
        return redirect('admin/degree');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */

    public function destroy(Request $request, $id)
    {
        $jobDetailList = JobDetail::where('degree_id', $id)->where('status', 1)->get();
        if(count($jobDetailList) > 0)
        {
            $message="Degree can't deleted.This degree have a Shift.";
            return response()->json(['message'=>$message],400);
        }
        $degree = Degree::where('id', $id);
        $degree->delete();
        $message='degree Deleted';
        return response()->json(['message'=>$message],200);

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {
        $degree = Degree::where('id', $id)->first();
        if($degree == NULL) {
            Session::flash('flash_message', 'Degree is not exist!');
            return redirect('admin/degree');
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $degree->status= '0';
                $degree->update();
                Session::flash('flash_message', 'Degree Status change to inactive');
            }else{
                $degree->status= '1';
                $degree->update();
                Session::flash('flash_message', 'Degree Status change to active');
            }

        }
        return redirect('admin/degree');
    }
}
