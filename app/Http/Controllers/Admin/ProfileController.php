<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Hash;
use Session;

class ProfileController extends Controller
{
    public function index(){
        $user = User::where('id',Auth::user()->id)->first();
        return view('admin.profile.index', compact('user'));
    }

    public function edit()
    {
        $user = User::where('id',Auth::user()->id)->first();
        return view('admin.profile.edit', compact('user'));
    }

    public function updateProfile(Request $request){
  
        $this->validate($request,
        ['name' => 'required', 
        'fname' => 'required',
         'lname' => 'required']);
        
        $data = $request->except('_token');
        $user = User::where('id',Auth::user()->id)->first();
        $user->update($data);
        Session::flash('flash_message', 'Profile Update Successfully');
        return view('admin.profile.edit',compact('user'));

    }

    public function changePassword()
    {
        return view('admin.profile.changePassword');
    }
    public function updatePassword(Request $request)
    {
        $messages = [
            'current_password.required' => __('Please enter current password'),
            'password_confirmation.same' => __('The confirm password and new password must match.'),
        ];

        $this->validate($request,
            [
                'current_password' => 'required',
                'password' => 'required|min:6|max:255',
                'password_confirmation' => 'required|same:password',
            ], $messages);


        $cur_password = $request->input('current_password');


        $user = Auth::user();

        if (Hash::check($cur_password, $user->password)) {
            if (Hash::check($request->input('password'),$user->password)) {
                Session::flash('flash_success', 'Current password and new password are the same.');
                return redirect('/admin/profile/changePassword');
            }
            else
            {
                $user->password = Hash::make($request->input('password'));
                $user->save();
                Session::flash('flash_success', 'Password changed successfully.');
                return redirect('/admin/profile');
            }

        } else {
            $error = array('current-password' => __('Please enter correct current password'));
            return redirect()->back()->withErrors($error);
        }
        Session::flash('flash_error', 'Something wrong. Please try again latter.');
        return redirect()->back();


    }
}
