<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\Subscription;
use Yajra\Datatables\Datatables;
use App\JobDetail;
use App\Profession;
use App\Speciality;
use App\Degree;
use App\State;
use App\User;
use App\Experience;
use App\Edvolume;
use App\Shift;
use App\Setting;
use App\JobDates;

class JobController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.job.index');
    }

    public function datatable(request $request)
    {
        $job = JobDetail::with('profession','speciality','degree', 'employee')->whereHas('employee')->where('status','!=','2');
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(job_detail.job_title LIKE  '%$value%' OR job_detail.job_responsibility LIKE  '%$value%' )";
                $job = $job->whereRaw($where_filter);
            }
        }
        $job = $job->orderBy('job_detail.id','DESC')->get();

        return Datatables::of($job)
            ->make(true);
        exit;
    }

    public function destroy(Request $request, $id)
    {

        $job = JobDetail::where('id', $id)->first();
        if($job->isFree == 1)
        {
            $message="Do not allow to delete this Shift";
            return response()->json(['message'=>$message,'code'=>'400'],200);
        }
        else
        {
            $requestData = array(
                'status' => 2);
            $job->update($requestData);
            $message='Shift Deleted';
            return response()->json(['message'=>$message,'code'=>'200'],200);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {

        $job = JobDetail::where('id', $id)->first();
        if($job == NULL) {
            Session::flash('flash_message', 'Shift is not exist!');
            return redirect('admin/shift');
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $job->status= '0';
                $job->update();
                Session::flash('flash_message', 'Shift Status Updated Successfully!');
            }else{
                $job->status= '1';
                $job->update();
                Session::flash('flash_message', 'Shift Status Updated Successfully!');
            }

        }

        return redirect('admin/shift');
    }

    public function addBlukJobs(Request $request)
    {
        $job = JobDetail::with('experience','edvolume','profession','speciality','degree','location','shift')->where('status', 1)->first();
        //dd($job);
        $profession = Profession::with('speciality', 'degree')->where('status', '1')->get();
        $shiftData = array();
        foreach($profession as $key => $value) {
            $shiftData[$key]['profession'] = $value->title;
            if(count($value->speciality) > 0)
            {
                foreach ($value->speciality as $speciality) {
                    $shiftData[$key]['speciality'][] = $speciality->title;
                }
            }
            else
            {
                $shiftData[$key]['speciality'][] = 'No have a speciality';
            }

            if(count($value->degree) > 0)
            {
                foreach ($value->degree as $degree) {
                    $shiftData[$key]['degree'][] = $degree->title;
                }
            }
            else
            {
                $shiftData[$key]['degree'][] = 'No have a degree';
            }
        }
        $employees = User::with('EmployeeDetail')->where('user_type', 'employee')->pluck('name','id')->prepend('Select Employer','');
        $Setting = Setting::where('key', 'job_max_rate')->first();
            $jobMaxRate = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 100);
        //echo '<pre>'; print_r($employees); exit;
        $sample = '';
        if(file_exists(public_path('sample/sample-jobs.csv')))
        {
            $sample = url('sample/sample-jobs.csv');
        }
        //'profession','location','experience','ed_volume','shift'
        return view('admin.job.addBluk',compact('employees','sample','job','shiftData','jobMaxRate'));
    }

    public function storeJobs(Request $request)
    {
        $file = $request->file('file');
        if(!empty($file)){
            if(!empty($file)){
            if($file->getClientOriginalExtension() == 'csv')
            {
                $uploadedFile['name'] = str_replace(' ','',$file->getClientOriginalName());
                $path = public_path('/uploads');
                $file->move($path,$uploadedFile['name']);
                $filePath = public_path('/uploads/').'/'.$uploadedFile['name'];
                $csvfile = fopen($filePath,"r");

                while(!feof($csvfile)){
                    $rows[]=fgetcsv($csvfile);
                }
                $data = array();
                $jobs = array();
                $c = 0;
                $Setting = Setting::where('key', 'job_max_rate')->first();
                $jobMaxRate = ((($Setting) && ($Setting->value != null)) ? $Setting->value : 100);
                //dd($rows);
                for($i= 1; $i < count($rows); $i++){
                    $k = 0;

                    if(is_array($rows[$i]))
                    {
                        if(count($rows[$i]) > 1){
                            foreach($rows[0] as $key=>$value){
                                $value = strtolower(str_replace(' ','_',$value));
                                $data[$c][$value] = $rows[$i][$k];
                                $k++;
                            }
                            $c++;
                        }
                    }
                }

                //echo '<pre>'; print_r($data);exit;
                foreach($data as $key => $value){

                    $postData = array();
                    $valmessages = [
                        'job_title.required' => 'Shift title field is required.',
                        'job_responsibility.required' => 'Shift responsibility field is required.'
                    ];
                    $rules = array(
                        'job_title'=>'required',
                        'job_responsibility'=>'required',
                        'profession'=>'required',
                        'speciality'=>'required',
                        'degree'=>'required',
                        'experience'=>'required',
                        'location'=>'required',
                        'ed_volume'=>'required',
                        'dates'=>'required',
                        // 'from_date'=>'required|date|after:'.date('Y-m-d'),
                        // 'to_date'=>'required|date|after:from_date',
                        'shift'=>'required',
                        'rate'=>'required|numeric|min:0|max:'.$jobMaxRate,
                    );
                    $validator = \Validator::make($value, $rules, $valmessages);
                    //dd($validator->messages()->toArray());
                    if ($validator->fails()) {
                        //unset($data[$key]);
                        Session::flash('flash_message', "Data missing, Shifts could not be added");
                    }
                    else
                    {
                        if($value['location'] != '')
                        {
                            $state = State::where('name','like',$value['location']. '%')->first();
                            if($state != null)
                            $postData['location_id'] = $state->id;
                        }
                        if($value['profession'] != '')
                        {
                            $Profession = Profession::where('title','like',$value['profession']. '%')->first();
                            if($Profession != null)
                            {
                                $postData['profession_id'] = $Profession->id;

                                if($value['speciality'] != '' && $postData['profession_id'] != '')
                                {
                                    $Speciality = Speciality::where('profession_id',$postData['profession_id'])->where('title','like',$value['speciality']. '%')->first();
                                    if($Speciality != null)
                                    $postData['speciality_id'] = $Speciality->id;
                                }
                            }
                        }

                        if($value['degree'] != '')
                        {
                            $Degree = Degree::where('title','like',$value['degree']. '%')->first();
                            if($Degree != null)
                            $postData['degree_id'] = $Degree->id;
                        }

                        if($value['ed_volume'] != '')
                        {

                            $Edvolume = Edvolume::where('title','like',$value['ed_volume'].'%')->first();
                            if($Edvolume != null)
                            $postData['ed_volume_id'] = $Edvolume->id;
                            else{
                                $ed_volume = explode('-',$value['ed_volume']);
                                $Edvolume = null;
                                if(is_array($experience))
                                {
                                    if(count($ed_volume) > 1)
                                    {
                                        $ed_volume[0] =  preg_replace("/[^0-9,.]/", "", $ed_volume[0]);
                                        $ed_volume[1] = preg_replace("/[^0-9,.]/", "", $ed_volume[1]);
                                        $where_filter = "(min LIKE '%$ed_volume[0]%' OR max LIKE  '%$ed_volume[1]%' )";
                                        $Edvolume = Edvolume::whereRaw($where_filter)->first();
                                    }
                                    elseif((count($ed_volume)) == 1)
                                    {
                                        $ed_volume[0] =  preg_replace("/[^0-9,.]/", "", $ed_volume[0]);
                                        $Edvolume = Edvolume::where('min','<=',$ed_volume[0])->where('max','>=',$ed_volume[0])->first();
                                        //dd($Edvolume);
                                    }
                                    if($Edvolume != null)
                                    $postData['ed_volume_id'] = $Edvolume->id;
                                }
                            }
                        }

                        if($value['experience'] != '')
                        {
                            $Experience = Experience::where('title','like',$value['experience']. '%')->first();
                            if($Experience != null)
                            $postData['experience_id'] = $Experience->id;
                            else{
                                $experience = explode('-',$value['experience']);
                                $Experience = null;
                                if(is_array($experience))
                                {
                                    if(count($experience) > 1)
                                    {
                                        $experience[0] =  preg_replace("/[^0-9,.]/", "", $experience[0]);
                                        $experience[1] = preg_replace("/[^0-9,.]/", "", $experience[1]);
                                        $where_filter = "(min LIKE '%$experience[0]%' OR max LIKE  '%$experience[1]%' )";
                                        $Experience = Experience::whereRaw($where_filter)->first();
                                    }
                                    elseif((count($experience)) == 1)
                                    {
                                        $experience[0] =  preg_replace("/[^0-9,.]/", "", $experience[0]);
                                        $Experience = Experience::where('min','<=',$experience[0])->where('max','>=',$experience[0])->first();
                                    }
                                    if($Experience != null)
                                    $postData['experience_id'] = $Experience->id;
                                }

                            }
                        }


                        if($value['shift'] != '')
                        {
                            $Shift = Shift::where('title','like',$value['shift']. '%')->first();
                            if($Shift != null)
                            $postData['shift_id'] = $Shift->id;
                        }
                        if($value['job_title'] != '')
                        {
                            $postData['job_title'] = $value['job_title'];
                        }
                        if($value['job_responsibility'] != '')
                        {
                            $postData['job_responsibility'] = $value['job_responsibility'];
                        }
                        if($value['rate'] != '')
                        {
                            $postData['rate'] = $value['rate'];
                        }

                        $postData['status'] = 1;
                        $postData['address'] = (($value['address'] != null) ? $value['address'] : '');
                        $postData['employee_id'] = Auth::user()->id;
                        
                        if(count($postData) >= 13)
                        {
                            $job = JobDetail::create($postData);
                            $jobs[] = $job;
                            $datesArray = explode('|', $value['dates']);
                            $datesData = array();
                            $now = date('Y-m-d');
                            foreach ($datesArray as $key => $value) {
                                $date_arr  = explode('/', $value);
                                if ($date_arr != null && is_numeric($date_arr[0]) && is_numeric($date_arr[1]) && is_numeric($date_arr[2]) && checkdate($date_arr[0], $date_arr[1], $date_arr[2])) 
                                { 
                                    if(strtotime($value) > strtotime($now)) {
                                        $datesData[] = array('job_id'=> $job->id,'job_date' => date('Y-m-d',strtotime($value)));
                                    }                                
                                }
                            }
                            if($datesData != null)
                            {

                                JobDates::insert($datesData); 
                            }

                        }
                    }
                }
                //dd($data);
                fclose($csvfile);
                //dd(count($jobs));
                if((count($data)) == (count($jobs)))
                {
                    Session::flash('flash_message', 'Shifts added Successfully!');
                }
                else {
                    Session::flash('flash_message', "Data missing, Shifts could not be added");
                }

                unlink($filePath);
            }
            else
            {
                Session::flash('flash_message', "Please select only csv file!");
                return redirect('addBlukJobs');
            }
        }
            else
            {
                Session::flash('flash_message', "Please select only csv file!");
                return redirect('admin/addBlukJobs');
            }
        }
        else
        {
            Session::flash('flash_message', "Please select file!");
            return redirect('admin/addBlukJobs');
        }
        return redirect('admin/shift');

    }

    public function download(Request $request)
    {
        $profession = Profession::with('speciality','degree')->where('status','1')->get();

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=professionData.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $callback = function() use ($profession)
        {
            $columnsSpeciality = array('--- Profession ---','--- Speciality ---');
            $columnsDegree = array('--- Profession ---','--- Degree ---');
            $file = fopen('php://output', 'w');


            foreach($profession as $export) {
                fputcsv($file, $columnsSpeciality);
                if(count($export->speciality) > 0)
                {
                    foreach ($export->speciality as $speciality) {
                        fputcsv($file, array($export->title,$speciality->title));
                    }
                }
                else
                {
                    fputcsv($file, array($export->title,'No have a speciality'));
                }
            }
            foreach($profession as $export) {
                fputcsv($file, $columnsDegree);
                if(count($export->degree) > 0)
                {
                    foreach ($export->degree as $degree) {
                        fputcsv($file, array($export->title,$degree->title));
                    }
                }
                else
                {
                    fputcsv($file, array($export->title,'No have a degree'));
                }
            }
            fclose($file);
        };

       return \Response::stream($callback, 200, $headers);
    }
}
