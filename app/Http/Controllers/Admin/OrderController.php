<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Order;
use Session;
use Yajra\Datatables\Datatables;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.order.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $order = Order::leftjoin('users', 'transaction.emp_id', '=', 'users.id')
            ->leftjoin('package', 'transaction.pack_id', '=', 'package.id')
            ->select(['transaction.*','users.name as userName','package.title as packages','package.amount as amount']);

        if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(users.name LIKE '%$value%' OR package.title LIKE  '%$value%'  )";
                $order = $order->whereRaw($where_filter);
            }
        }  
        if ($request->get('status') != '') {
            $status = $request->get('status');
            $order = $order->where('transaction.status', $status);
        }
        $order = $order->orderBy('transaction.id', 'desc')->get();

        return Datatables::of($order)
            ->make(true);
        exit;
    }
   
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        $order = Order::with('employee','user','package')->where('id', $id)->first();
        if($order == NULL) {
            Session::flash('flash_message', 'Order is not exist!');
            return redirect('admin/order');
        }
        return view('admin.order.show',compact('order'));
    }
}
