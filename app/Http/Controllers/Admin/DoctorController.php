<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DoctorDetail;
use App\Profession;
use App\Speciality;
use App\Experience;
use App\Degree;
use App\State;
use App\Edvolume;
use App\Priority;
use App\Shift;
use App\Role;
use App\User;
use Session;
use Yajra\Datatables\Datatables;
use DB;
use App\Locations;

class DoctorController extends Controller
{
    public function index(Request $request)
    {
        // $keyword = $request->get('search');
        // $perPage = 15;

        // if (!empty($keyword)) {
        //     $doctors = DoctorDetail::where('profile_name', 'LIKE', "%$keyword%")
        //     ->orWhere('email', 'LIKE', "%$keyword%")
        //         ->paginate($perPage);
        // } else {
        //     $doctors = DoctorDetail::paginate($perPage);

        // }

        return view('admin.doctors.index', compact('doctors'));
    }

    public function datatable(Request $request){
        $doctors = User::with('doctorDetail')->where('user_type', 'doctor')->orderBy('id','desc');
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(users.name LIKE  '%$value%' OR users.email LIKE  '%$value%'  )";
                $doctors->whereRaw($where_filter);
            }
        }

        return Datatables::of($doctors)
             ->make(true);
        exit;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {

        $professions = Profession::where('status', 1)->pluck('title','id')->prepend('Select Profession','');

        $specialities = Speciality::select('id','title')->where('status',1)->pluck('title','id')->prepend('Select Speciality','');

        $experiences = Experience::select('id','title')->pluck('title','id')->prepend('Select Experience','');

        $degrees = Degree::select('id','title')->where('status',1)->pluck('title','id');

        $states = State::select('id','name')->where('status',1)->pluck('name','id')->prepend('Select State','');

        $licenses = State::select('id','name')->where('status',1)->pluck('name','id');

        $locations = State::select('id','name')->where('status',1)->pluck('name','id')->prepend('Select Location','');

        $ed_volumes = Edvolume::select('id','title')->where('status',1)->pluck('title','id')->prepend('Select Edvolume','');

		$shifts = Shift::select('id','title')->where('status',1) ->pluck('title','id')->prepend('Select Shift','');

        $priorities = Priority::select('id','title')->where('status',1)->pluck('title','id')->prepend('Select Priority','');
        return view('admin.doctors.create',compact('userdefaulrole','professions','specialities','experiences',
        'degrees','states','licenses','locations','ed_volumes','shifts','priorities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules = array(
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'profile_name'=>'required',
            'professions'=>'required|integer',
            'specialities'=>'required|integer',
            'experiences'=>'required|integer',
            'degrees'=>'required',
            'licenses'=>'required',
            'profile_image'=>'sometimes|mimes:jpg,jpeg,png|max:10000',
        );
        if (isset($requestData['tel_number']) && ($requestData['tel_number'] != '')) {
            $rules['tel_number'] ='required|digits_between:10,12';
        }

        if (isset($requestData['resume_type']) && ($requestData['resume_type'] == 'link')) {
            $rules['resume'] = 'required|regex:/^(https?:\/\/)?([\dA-Za-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        }
        elseif (isset($requestData['resume_type']) && ($requestData['resume_type'] == 'attachment')) {
            $rules['resume'] = 'required|mimes:jpg,jpeg,pdf,doc,docx|max:20000';
        }
        //dd($requestData);
        $this->validate($request, $rules);
        $data = $request->except('password');
        $data['name'] = $request->fname.' '.$request->lname;
        $useremail = User::where('email',$request->email)->where('user_type','doctor')->first();
        if($useremail != null){
            $this->validate($request, ['email' => 'required|unique:users']);
        }
        $data['email'] = $request->email;
        $data['type'] = 'doctor';
        $data['user_type'] = 'doctor';
        $data['login_type'] = 'simple';
        $data['fname'] = $request->fname;
        $data['lname'] = $request->lname;
        $data['password'] = bcrypt($request->password);
        $data['is_active'] = $request->is_active;
        $data['activation_token'] = sha1(time() . uniqid() . $data['email']);
        $data['api_token'] = '';
        $data['device_token'] = '';
        $data['device_type'] = '';
        $data['token_id'] = '0';
        $data['stripe_id'] = '0';
        $data['firebasetoken'] = '';
        $user = User::create($data);
        $this->subscribeMailChimp($data['email'],'doctor');
        $userdefaulrole = "Doctor";
        $user->assignRole($userdefaulrole);


        $input = array();
        $input['uid'] = $user->id;
        $input['address'] = $request->address;
        $input['profile_name']=$request->profile_name;
        $input['tel_number'] = $request->tel_number;
        $input['resume_type'] = (($request->resume_type != null) ? $request->resume_type : '');
        if($request->resume_type != 'link'){
            if ($request->file('resume')) {
                $pdf = $request->file('resume');
                $filename = uniqid(time()) . '.' . $pdf->getClientOriginalExtension();
                $pdf->move(public_path('images/doctor/pdf'), $filename);
                $input['resume'] = $filename;
            }
        }else{
            if($request->resume != null)
            {
                $disallowed = array('http://', 'https://');
                foreach($disallowed as $d) {
                    if(strpos($requestData['resume'], $d) === 0) {
                        $input['resume'] =  str_replace($d, '', $requestData['resume']);
                    }
                }
            }
            else
            {
                $input['resume'] = '';
            }

        }

        if ($request->file('profile_image')) {
           $image = $request->file('profile_image');
            $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
			$image->move(public_path('images/doctor'), $filename);
            $input['profile_image'] = $filename;
        }
        else{
            $input['profile_image'] = '';
        }
        $professions = $request->input('professions');
        $specialities = $request->input('specialities');
        $experiences = $request->input('experiences');
        $degrees = $request->input('degrees');
        $licenses = $request->input('licenses');
        $locations = $request->input('locations');

      

        $input['profession_id'] = $professions;
        $input['speciality_id'] = $specialities;
        $input['experience_id'] = $experiences;
        $input['degree_id'] = implode(",", $degrees);
        $input['license_id'] = implode(",", $licenses);
        // $input['location_id'] = (($locations != null) ? $locations : "");
       
        //dd($input);
        $doc_detail = DoctorDetail::create($input);

        if($doc_detail){
            foreach($locations as $loc){
                Locations::create([
                    'type' => 'doctor_details',
                    'field_id' => $doc_detail->id ,
                    'location_id' => $loc
                ]);
            }
        }
        if($user->is_active == 1)
        {
            $this->welcomeMail($user);
            //$user->notify(new \App\Notifications\Welcome($user));

            $user_id =$user->id;
            $user = array();
            $user = $data;
            $user['id'] = $user_id;
            //Welcome Mail Send

            // \Mail::send('email.welcome', compact('user'), function ($message) use ($user) {
            //     $message
            //     ->to($user['email'])
            //     ->subject('Welcome to Er-Shift');
            // });
        }
        else
        {
            $user->notify(new  \App\Notifications\ActivationLink($user));
        }
        return redirect('admin/doctors')->with('flash_message', 'Doctor added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {
        $user = User::where('id',$id)->first();
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $user->is_active= '0';
                $user->update();
            }else{
                $user->is_active= '1';
                $user->update();
            }
            Session::flash('flash_message', 'Ed-Staff Status update Successfully');
            return redirect('admin/doctors');
        }
        else{
            $doctor = DoctorDetail::with('user','speciality','profession','experience','location')->where('uid',$id)->first();
            if($doctor != null)
            {
                if($doctor->degree_id != null)
                {
                    $arr = explode(',',$doctor->degree_id);
                    $str = '';
                    foreach ($arr as $key1 => $value1) {
                        $Degree = Degree::where('id',$value1)->first();
                        if($Degree != null)
                        $str .= (($str != null ) ? ','.$Degree->title : $Degree->title);
                    }
                    $doctor->degree_id = $str;
                }
                else
                {
                    $doctor->degree_id = '';
                }

                if($doctor->license_id != null)
                {
                    $arr = explode(',',$doctor->license_id);
                    $str = '';
                    foreach ($arr as $key1 => $value1) {
                        $State = State::where('id',$value1)->first();
                        if($State != null)
                        $str .= (($str != null ) ? ','.$State->name : $State->name);
                    }
                    $doctor->license_id = $str;
                }
                else
                {
                    $doctor->license_id = '';
                }
                if(file_exists(public_path('images/doctor/'.$doctor->profile_image)) && ($doctor->profile_image != null))
                {
                    $doctor->profile_image = url('').'/images/doctor/'.$doctor->profile_image;
                }
                else
                {
                    $doctor->profile_image = '';
                }
                if($doctor->resume_type == 'attachment')
                {
                    if(file_exists(public_path('images/doctor/pdf/'.$doctor->resume)) && ($doctor->resume != null))
                    {
                        $doctor->resume = url('').'/images/doctor/pdf/'.$doctor->resume;
                    }
                    else
                    {
                        $doctor->resume = '';
                    }
                }
                /*else if ($doctor->resume_type == 'link')
                {
                    $doctor->resume = ((stripos($doctor->resume ,'https') || stripos($doctor->resume ,'http') ) ? $doctor->resume : 'https://'.$doctor->resume);
                }*/

                //dd($doctor);
                return view('admin.doctors.show', compact('doctor'));
            }
            else
            {
                return redirect('admin/doctors')->with('flash_error', 'Data Is Not Valid!');
            }
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {

        $professions = Profession::where('status', 1)->pluck('title','id')->prepend('Select Profession','');

        $specialities = Speciality::select('id','title')->where('status',1)->pluck('title','id')->prepend('Select Speciality','');

        $experiences = Experience::select('id','title')->pluck('title','id')->prepend('Select Experience','');

        $degrees = Degree::select('id','title')->where('status',1)->pluck('title','id');

        $states = State::select('id','name')->where('status',1)->pluck('name','id')->prepend('Select State','');

        $licenses = State::select('id','name')->where('status',1)->pluck('name','id');

        $locations = State::select('id','name')->where('status',1)->pluck('name','id')->prepend('Select Location','');

        $ed_volumes = Edvolume::select('id','title')->where('status',1)->pluck('title','id')->prepend('Select Edvolume','');

		$shifts = Shift::select('id','title')->where('status',1) ->pluck('title','id')->prepend('Select Shift','');

        $priorities = Priority::select('id','title')->where('status',1)->pluck('title','id')->prepend('Select Priority','');


        $doctor = DoctorDetail::where('uid',$id)->first();
        $user = User::find($id);

        //dd($doctor);
        if($user !=null && $doctor !=null)
        {
            $arr = explode(',',$doctor->degree_id);
            $doctor->degree_id = $arr;
            $arr = explode(',',$doctor->license_id);
            $doctor->license_id = $arr;

            if(file_exists(public_path('images/doctor/'.$doctor->profile_image)) && ($doctor->profile_image != null))
            {
                $doctor->profile_image = url('').'/images/doctor/'.$doctor->profile_image;
            }
            else
            {
                $doctor->profile_image = '';
            }
            if($doctor->resume_type == 'attachment')
            {
                if(file_exists(public_path('images/doctor/pdf/'.$doctor->resume)) && ($doctor->resume != null))
                {
                    $doctor->resume = url('').'/images/doctor/pdf/'.$doctor->resume;
                }
                else
                {
                    $doctor->resume = '';
                }
            }
            //$user = User::findOrFail($doctor->uid);
            //dd($doctor);
            return view('admin.doctors.edit', compact('doctor', 'user','professions','specialities','experiences','degrees','states','licenses','locations','ed_volumes','shifts','priorities'));
        }
        else
        {
            return redirect('admin/doctors')->with('flash_error', 'Data Is Not Valid!');
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int      $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {   
      
        $requestData = $request->all();

        $rules = array(
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required',
            'profile_name'=>'required',
            'professions'=>'required|integer',
            'specialities'=>'required|integer',
            'experiences'=>'required|integer',
            'degrees'=>'required',
            'licenses'=>'required',
            'profile_image'=>'sometimes|mimes:jpg,jpeg,png|max:10000',
        );
        if (isset($requestData['resume_type']) && ($requestData['resume'] != null) && ($requestData['resume_type'] == 'link')) {
            $rules['resume'] = 'required|regex:/^(https?:\/\/)?([\dA-Za-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        }
        elseif (isset($requestData['resume_type']) && ($request->file('resume')) && ($requestData['resume_type'] == 'attachment')) {
            $rules['resume'] = 'required|mimes:jpg,jpeg,pdf,doc,docx|max:20000';
        }
        //dd($requestData);
        $this->validate($request, $rules);
        $data['fname'] = $request->fname;
        $data['lname'] = $request->lname;
        $data['name'] = $request->fname.' '.$request->lname;
        $data['is_active'] = $request->is_active;
        $input = array();
        //$input['email'] = $request->email;
        $input['address'] = $request->address;
        $input['profile_name']=$request->profile_name;
        $input['tel_number'] = $request->tel_number;
        $input['resume_type'] = $request->resume_type;
        $input['resume_type'] = $request->resume_type;
        if ($request->resume_type != 'link') {
            if ($request->file('resume')) {
                $pdf = $request->file('resume');
                $filename = uniqid(time()) . '.' . $pdf->getClientOriginalExtension();
                $pdf->move(public_path('images/doctor/pdf'), $filename);
                $input['resume'] = $filename;
            }
        } else {
            $disallowed = array('http://', 'https://');
            foreach($disallowed as $d) {
                if(strpos($requestData['resume'], $d) === 0) {
                    $input['resume'] =  str_replace($d, '', $requestData['resume']);
                }
                else{
                    $input['resume'] =$requestData['resume'];
                }
            }
        }
        if ($request->file('profile_image')) {
            $image = $request->file('profile_image');
            $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/doctor/'), $filename);
            $input['profile_image'] = $filename;
        }

        $professions = $request->input('professions');
        $specialities = $request->input('specialities');
        $experiences = $request->input('experiences');
        $degrees = $request->input('degrees');
        $states = $request->input('states');
        $licenses = $request->input('licenses');
        $locations = $request->input('locations');
        $ed_volumes = $request->input('ed_volumes');
        $shifts = $request->input('shifts');
        $priorities = $request->input('priorities');
        $input['profession_id'] = $professions;
        $input['speciality_id'] = $specialities;
        $input['experience_id'] = $experiences;
        $input['degree_id'] = implode(",", $degrees);
        $input['state_id'] = $states;
        $input['license_id'] = implode(",", $licenses);
        // $input['location_id'] = (($locations != null) ? $locations :"");

        $doctorOld = DoctorDetail::where('uid',$id)->first();
        $doctor = DoctorDetail::findOrFail($doctorOld->id);
        $doctor->update($input);

        $doctor->locations()->forceDelete() ;
        foreach($locations as $loc){
            Locations::create([
                'type' => 'doctor_details',
                'field_id' => $doctor->id ,
                'location_id' => $loc
            ]);
        }


        $user = User::findOrFail($id);
        $user->update($data);

        return redirect('admin/doctors')->with('flash_message', 'Doctor updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $doctor = DoctorDetail::where('uid',$id)->first();

        $doctor->locations()->forceDelete() ;
        foreach ($doctor->doctor_wishes as $wish) {
            $wish->date_shifts()->delete();
            $wish->delete();
        }
       
        $doctor->delete();
        $user->delete();


        return response()->json(['message'=>'Doctor deleted!','code'=>'200'],200);
    }
}
