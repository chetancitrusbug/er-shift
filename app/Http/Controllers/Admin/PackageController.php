<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;
use App\Subscription;
use Session;
use Yajra\Datatables\Datatables;
use Carbon;

use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;

class PackageController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.package.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $package = Package::where('status','!=','2');
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(package.title LIKE  '%$value%' OR package.amount LIKE  '%$value%' OR package.day LIKE  '%$value%'  )";
                $package = $package->whereRaw($where_filter);
            }
        }
        $package = $package->orderBy('package.id', 'desc')->get();

        return Datatables::of($package)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        return view('admin.package.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $requestdata = $request->all();
        $this->validate($request, [
            'title' => 'required',//|unique:package',
            'amount' => 'required|numeric|min:1',
            'day' => 'required|integer|min:1',
        ]);
        
        $data = array(
            'title' => $request->input('title'),
            'amount' => $request->input('amount'),
            'day' => $request->input('day'),
            'isFree' => 0 ,
            'description' => (($request->input('description')) ? $request->input('description') : ''),
            'status' => $request->input('status')
        );
        $data['status']  = (($data['status'] != '') ? $data['status'] : 1);
        
        $package = Package::create($data);

        Session::flash('flash_message', 'Package added!');
        return redirect('admin/package');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $package = Package::where('id', $id)->first();
        if ($package) {
            return view('admin.package.edit', compact('package'));
        } else {
            Session::flash('flash_message', 'Package is not exist!');
            return redirect('admin/package');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'status' => 'required',
        ]);
        $requestData = array(
            'title' => $request->input('title'),
            'status' => $request->input('status'));
        
        $package = Package::where('id', $id);
        $package->update($requestData);
        Session::flash('flash_message', 'Package Updated Successfully!');
        return redirect('admin/package');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    
    public function destroy(Request $request, $id)
    {
 
        $package = Package::where('id', $id)->first();
        if($package->isFree == 1)
        {
            $message="Do not allow to delete this package";
            return response()->json(['message'=>$message,'code'=>'400'],200);
        }
        else 
        { 
            $subscription = Subscription::where('package',$id)->where('status',1)->get();
            if(count($subscription) > 0)
            {
                $message="Do not allow to delete this package.This package have Subscription.";
                return response()->json(['message'=>$message,'code'=>'400'],200);
            }
            $requestData = array(
                'status' => 2);
            $package->update($requestData);
    
            $message='Package Deleted';
            return response()->json(['message'=>$message,'code'=>'200'],200);
        }
    }

   
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        $package = Package::where('id', $id)->first();
        if($package == NULL) {
            Session::flash('flash_message', 'Package is not exist!');
            return redirect('admin/package');
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $package->status= '0';
                $package->update();    
                Session::flash('flash_message', 'Package Status Updated Successfully!');        
            }else{
                $package->status= '1';
                $package->update();    
                Session::flash('flash_message', 'Package Status Updated Successfully!');           
            }

        }
        
        return redirect('admin/package');
    }
    public function generateStripePlan($id,Request $request)
    {
        $item = Package::where("id",$id)->first();
        if(!$item){
            Session::flash('flash_error',"No Package Found");
            return redirect('admin/package');
        }
        
        if($item->stripe_product_id && $item->stripe_product_id !=""){
            Session::flash('flash_error',"Strip Package Already Exist");
            return redirect('admin/package');
        }
        
        $stripe = Stripe::make(config('services.stripe.secret'));
        $plan = $stripe->plans()->create([
            'id' => $item->id,
            'name' => $item->title,
            'amount' => $item->amount,
            'currency' => 'USD',
            'interval' => 'day',
            'interval_count'=>  $item->day,
            'statement_descriptor' => $item->amount." per ".$item->day .' day',
        ]);

        $item->stripe_product_id = $plan['product'];
        $item->save();
        
        Session::flash('flash_success',"Strip Package Created !!");
        return redirect('admin/package');
    }
}
