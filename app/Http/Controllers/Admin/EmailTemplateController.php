<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use Yajra\Datatables\Datatables;
use App\EmailTemplate;
use App\User;

class EmailTemplateController extends Controller
{
    public function index(Request $request)
    {
        // $user = User::where('id', 2)->first();
        // $user->notify(new  \App\Notifications\Welcome($user));
        return view('admin.emailtemplate.index');
    }

    public function datatable(request $request)
    {

        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(email_template.title LIKE  '%$value%' OR email_template.desc LIKE  '%$value%' )";
                $emailtemplate = EmailTemplate::whereRaw($where_filter)->get();
            }
            else {
                $emailtemplate = EmailTemplate::all();
            }
        }
        else {
            $emailtemplate = EmailTemplate::all();
        }

        return Datatables::of($emailtemplate)
            ->make(true);
        exit;
    }



    public function edit(Request $request, $id)
    {
        $emailtemplate = EmailTemplate::where('id', $id)->first();

        if ($emailtemplate) {
            return view('admin.emailtemplate.edit', compact('emailtemplate'));
        } else {
            Session::flash('flash_message', 'Email template is not exist!');
            return redirect('admin/emailtemplate');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required', //'required|unique:emailtemplate,title,'.$id,
            'content' => 'required',
        ]);
        $requestData = array(
            'title' => $request->input('title'),
            'content' => $request->input('content'));
        $emailtemplate = EmailTemplate::where('id', $id);
        $emailtemplate->update($requestData);
        Session::flash('flash_message', 'Email template Updated Successfully!');
        return redirect('admin/emailtemplate');
    }

}
