<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Profession;
use App\Speciality;
use App\JobDetail;
use Session;
use Yajra\Datatables\Datatables;

class SpecialityController extends Controller
{
    public function index(Request $request)
    {

        return view('admin.speciality.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $speciality = Speciality::with('professions')->get();
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(speciality.title LIKE  '%$value%' OR speciality.status LIKE  '%$value%'  )";
                $speciality = Speciality::with('professions')->whereRaw($where_filter);
            }
        }

        if ($request->get('status') != '') {
            $status = $request->get('status');
            $speciality = $speciality->where('status', $status);

        }
        return Datatables::of($speciality)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        $profession = Profession::where('status','1')->pluck('title','id')->prepend('Select Profession','');

        return view('admin.speciality.create',compact('profession'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',//['required', \Illuminate\Validation\Rule::unique('speciality', 'title')->whereNull('deleted_at')],

            'profession_id' => 'required',
            'status' => 'required',
        ]);
        $data = array(
            'title' => $request->input('title'),
            'profession_id' => $request->input('profession_id'),
            'status' => $request->input('status'));
        $speciality = Speciality::create($data);

        Session::flash('flash_message', 'Speciality added!');
        return redirect('admin/speciality');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $speciality = Speciality::where('id', $id)->first();
        $profession = Profession::where('status','1')->pluck('title','id')->prepend('Select Profession','');

        if ($speciality) {
            return view('admin.speciality.edit', compact('speciality','profession'));
        } else {
            Session::flash('flash_message', 'Speciality is not exist!');
            return redirect('admin/speciality');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required', //'required|unique:speciality,title,'.$id,
            'profession_id' => 'required',
            'status' => 'required',
        ]);
        $requestData = array(
            'title' => $request->input('title'),
            'profession_id' => $request->input('profession_id'),
            'status' => $request->input('status'));

        $speciality = Speciality::where('id', $id);
        $speciality->update($requestData);
        Session::flash('flash_message', 'Speciality Updated Successfully!');
        return redirect('admin/speciality');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */

    public function destroy(Request $request, $id)
    {

        $jobDetailList = JobDetail::where('speciality_id', $id)->where('status', 1)->get();
        if(count($jobDetailList) > 0)
        {
            $message="Speciality can't deleted.This speciality have a Shift.";
            return response()->json(['message'=>$message],400);
        }

        $speciality = Speciality::where('id', $id);
        $speciality->delete();
        $message='Speciality Deleted';
        return response()->json(['message'=>$message],200);

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {

        $speciality = Speciality::where('id', $id)->first();
        if($speciality == NULL) {
            Session::flash('flash_message', 'Speciality is not exist!');
            return redirect('admin/speciality');
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $speciality->status= '0';
                $speciality->update();
                Session::flash('flash_message', 'Speciality Status change to inactive');
            }else{
                $speciality->status= '1';
                $speciality->update();
                Session::flash('flash_message', 'Speciality Status change to active');
            }

        }
        return redirect('admin/speciality');
    }
}
