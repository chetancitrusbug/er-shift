<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\EmployeeDetail;
use App\profession;
use App\Speciality;
use App\Experience;
use App\Degrees;
use App\State;
use App\Edvolume;
use App\Priorities;
use App\Shift;
use App\Role;
use App\User;
use App\Subscription;
use App\Package;
use App\JobDetail;
use App\Locations;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class EmployeeController extends Controller
{
    public function listJobs(Request $request,$id)
    {
        $employee = EmployeeDetail::where('uid',$id)->first();
        if($employee!=null)
        {
            if(file_exists(public_path('images/employee/'.$employee->profile_pic)) && ($employee->profile_pic != null))
            {
                $employee->profile_pic = url('').'/images/employee/'.$employee->profile_pic;
            }
            else
            {
                $employee->profile_pic = '';
            }
            return view('admin.employees.listjob', compact('employee'));
        }
        else
        {
            return redirect('admin/employer')->with('flash_error', 'Data Is Not Valid!');
        }
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function jobDatatable(request $request,$id)
    {
        $job = JobDetail::with('profession','speciality','degree')->where('status','!=','2')
        ->where('employee_id',$id);
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(job_detail.job_title LIKE  '%$value%' OR job_detail.job_responsibility LIKE  '%$value%' )";
                $job = $job->whereRaw($where_filter);
            }
        }
        $job = $job->orderBy('job_detail.id','DESC');
        return Datatables::of($job)
            ->make(true);
        exit;
    }


    public function showJob(Request $request,$emp_id,$id)
    {
        $employee = EmployeeDetail::where('uid',$emp_id)->first();

        if($employee == null)
        {
            return redirect('admin/employer')->with('flash_error', 'Data Is Not Valid!');
        }

        $job = JobDetail::with('experience','edvolume','profession','speciality','degree','location','shift')->where('status','!=','2')->where('employee_id',$emp_id)->where('id',$id)->first();

        if($job!=null)
        {
            return view('admin.employees.jobshow', compact('job','emp_id'));
        }
        else
        {
            return redirect('admin/employer/jobs/'.$emp_id)->with('flash_error', 'Data Is Not Valid!');
        }


    }

    public function index(Request $request)
    {
        /*$keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            $employees = EmployeeDetail::where('emp_name', 'LIKE', "%$keyword%")
            ->orWhere('comp_name', 'LIKE', "%$keyword%")
            ->orWhere('tel_number', 'LIKE', "%$keyword%")
            ->orderBy('id','desc')
            ->paginate($perPage);
        } else {
            $employees = EmployeeDetail::orderBy('id','desc')->paginate($perPage);

        }*/

        return view('admin.employees.index', compact('employees'));
    }

    public function datatable(Request $request){
        $employees = User::whereHas('EmployeeDetail')->with('EmployeeDetail')->where('user_type', 'employee')->orderBy('id','desc');
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(name LIKE  '%$value%' OR email LIKE  '%$value%'  )";
                $employees->whereRaw($where_filter);
            }
        }
        return Datatables::of($employees)
             ->make(true);
        exit;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $location = State::select('id','name')->where('status',1)->pluck('name','id')->prepend('Select Location','');

        return view('admin.employees.create',compact('location'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules = array(
            'fname'=>'required',
            'lname'=>'required',
            'email' => 'required|email',
            'password' => 'required',
            'emp_name'=>'required',
            'comp_name'=>'required',
			'locations'=>'required',
            'profile_pic'=>'sometimes|mimes:jpg,jpeg,png|max:10000',
        );
        if (isset($requestData['tel_number']) && ($requestData['tel_number'] != '')) {
            $rules['tel_number'] =  'required|digits_between:10,12';
        }
        if (isset($requestData['comp_website']) && ($requestData['comp_website'] != '')) {
            $rules['comp_website'] =  'required|regex:/^(https?:\/\/)?([\dA-Za-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        }
        $this->validate($request, $rules);

        $data = $request->except('password');
        $data['name'] = $request->fname.' '.$request->lname;
        $useremail = User::where('email',$request->email)->where('user_type','employee')->first();
        if($useremail != null){
            $this->validate($request, ['email' => 'required|unique:users']);
        }
        $data['email'] = $request->email;
        $data['login_type'] = 'simple';
        $data['user_type'] = 'employee';
        $data['fname'] = $request->fname;
        $data['lname'] = $request->lname;
        $data['password'] = bcrypt($request->password);
        $data['is_active'] = $request->is_active;
        $data['activation_token'] = sha1(time() . uniqid() . $data['email']);
        $data['api_token'] = '';
        $data['device_token'] = '';
        $data['device_type'] = '';
        $data['token_id'] = '0';
        $data['stripe_id'] = '0';
        $data['firebasetoken'] = '';
        $user = User::create($data);
        $this->subscribeMailChimp($data['email'],'employee');
        $userdefaulrole = "Emp";
        $user->assignRole($userdefaulrole);

        $input = array();
        if ($request->file('profile_pic')) {
            $image = $request->file('profile_pic');
            $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/employee/'), $filename);
            $input['profile_pic'] = $filename;
        }
        else
        {
            $input['profile_pic'] = '';
        }

        $input['uid'] = $user->id;
        $input['emp_name']=$request->emp_name;
        $input['comp_name']=$request->comp_name;
        $input['comp_website']= (($request->comp_website) ? $request->comp_website : '');
        $input['comp_information']= (($request->comp_information) ? $request->comp_information : '');
        $input['tel_number'] = (($request->tel_number) ? $request->tel_number : '');
        $input['address'] = $request->address;
        // $input['location_id'] = (($request->location != null) ? $request->location : "");
        $employee = EmployeeDetail::create($input);

        if($employee){
            $locations = $request->input('locations');
            foreach($locations as $loc){
                Locations::create([
                    'type' => 'employee_detail',
                    'field_id' => $employee->id ,
                    'location_id' => $loc
                ]);
            }
        }


        // Free package set
        $package = Package::where('isFree', 1)->first();
        if($package != null)
        {
            $subScriptionDeatils['user_id'] = $user->id;
            $subScriptionDeatils['package'] = $package->id;
            $subScriptionDeatils['amount'] = 0;
            $subScriptionDeatils['start_date'] = date('Y-m-d');
            $subScriptionDeatils['end_date'] = date('Y-m-d', strtotime(date('Y-m-d'). ' + '.$package->day.' days'));
            $subScriptionDeatils['subscription_id'] = 0;
            $subScriptionDeatils['status'] = 1;
            Subscription::create($subScriptionDeatils);
        }
        if($user->is_active == 1)
        {
            $this->welcomeMail($user);
            $user_id =$user->id;
            $user = array();
            $user = $data;
            $user['id'] = $user_id;
            //Welcome Mail Send

            // \Mail::send('email.welcome', compact('user'), function ($message) use ($user) {
            //     $message
            //     ->to($user['email'])
            //     ->subject('Welcome to Er-Shift');
            // });
        }
        else
        {
            $user->notify(new  \App\Notifications\ActivationLink($user));
        }
        return redirect('admin/employer')->with('flash_message', 'Employee added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {
        $user = User::where('id',$id)->first();

        if($request->has('status')){
            $status = $request->get('status');
            if($status == 'active' ){
                $user->is_active= '0';
                $user->update();
            }else{
                $user->is_active= '1';
                $user->update();
            }
            Session::flash('flash_message', 'Employee Status update Successfully');
            return redirect('admin/employer');
        }
        $employee = EmployeeDetail::with('location')->where('uid',$id)->first();
        //dd($employee);
        if($employee!=null)
        {
            if(file_exists(public_path('images/employee/'.$employee->profile_pic)) && ($employee->profile_pic != null))
            {
                $employee->profile_pic = url('').'/images/employee/'.$employee->profile_pic;
            }
            else
            {
                $employee->profile_pic = '';
            }
            return view('admin.employees.show', compact('employee','user'));
        }
        else
        {
            return redirect('admin/employer')->with('flash_error', 'Data Is Not Valid!');
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $location = State::select('id','name')->where('status',1)->pluck('name','id')->prepend('Select Location','');

        $employee = EmployeeDetail::where('uid',$id)->first();
        if($employee!=null)
        {
            if(file_exists(public_path('images/employee/'.$employee->profile_pic)) && ($employee->profile_pic != null))
            {
                $employee->profile_pic = url('').'/images/employee/'.$employee->profile_pic;
            }
            else
            {
                $employee->profile_pic = '';
            }
            $user = User::findOrFail($id);

            return view('admin.employees.edit', compact('employee', 'user','location'));
        }
        else
        {
            return redirect('admin/employer')->with('flash_error', 'Data Is Not Valid!');
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int      $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        //dd($requestData);
        $rules = array(
            'fname'=>'required',
            'lname'=>'required',
            'emp_name'=>'required',
            'comp_name'=>'required',
			'locations'=>'required',
            'profile_pic'=>'sometimes|mimes:jpg,jpeg,png|max:10000',
        );
        if (isset($requestData['tel_number']) && ($requestData['tel_number'] != '')) {
            $rules['tel_number'] ='required|digits_between:10,12';
        }
        if (isset($requestData['comp_website']) && ($requestData['comp_website'] != '')) {
            $rules['comp_website'] ='required|regex:/^(https?:\/\/)?([\dA-Za-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        }
        $this->validate($request, $rules);

       $data['fname'] = $request->fname;
       $data['lname'] = $request->lname;
       $data['name'] = $request->fname.' '.$request->lname;
       $data['is_active'] = $request->is_active;
       $user = User::where('id',$id)->first();
       $user->update($data);
       $input = array();
       $input['emp_name']=$request->emp_name;
       $input['comp_name']=$request->comp_name;
       $input['comp_website']=$request->comp_website;
       $input['comp_information']=$request->comp_information;
       $input['tel_number'] = $request->tel_number;
       $input['address'] = $request->address;

       if ($request->file('profile_pic')) {
            $image = $request->file('profile_pic');
            $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/employee/'), $filename);
            $input['profile_pic'] = $filename;
        }

        $employee = EmployeeDetail::where('uid',$id)->first();
        $employee->update($input);

        $locations = $request->input('locations');
        $employee->locations()->forceDelete() ;
        foreach($locations as $loc){
            Locations::create([
                'type' => 'employee_detail',
                'field_id' => $employee->id ,
                'location_id' => $loc
            ]);
        }




        return redirect('admin/employer')->with('flash_message', 'Employee updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $EmployeeDetail = EmployeeDetail::where('uid',$id)->first();
        $EmployeeDetail->locations()->forceDelete() ;
        foreach($EmployeeDetail->jobs as $job){
            $job->delete();
        }

        $EmployeeDetail->delete();
        $user->delete();
        return response()->json(['message'=>'Employee deleted!','code'=>'200'],200);
    }

}
