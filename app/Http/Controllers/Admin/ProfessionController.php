<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Profession;
use App\Speciality;
use App\JobDetail;
use App\Degree;
use Session;
use Yajra\Datatables\Datatables;

class ProfessionController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.profession.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $profession = Profession::All();
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(profession.title LIKE  '%$value%' OR profession.status LIKE  '%$value%'  )";
                $profession = Profession::whereRaw($where_filter);
            }
        }

        if ($request->get('status') != '') {
            $status = $request->get('status');
            $profession = $profession->where('status', $status);

        }
        return Datatables::of($profession)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        return view('admin.profession.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' =>  'required',//['required', \Illuminate\Validation\Rule::unique('profession', 'title')->whereNull('deleted_at')],
            'status' => 'required',
        ]);
        $data = array(
            'title' => $request->input('title'),
            'status' => $request->input('status'));
        $profession = Profession::create($data);

        Session::flash('flash_message', 'Profession added!');
        return redirect('admin/profession');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $profession = profession::where('id', $id)->first();
        if ($profession) {
            return view('admin.profession.edit', compact('profession'));
        } else {
            Session::flash('flash_message', 'Profession is not exist!');
            return redirect('admin/profession');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required', //|unique:profession,title,'.$id,
            'status' => 'required',
        ]);
        $requestData = array(
            'title' => $request->input('title'),
            'status' => $request->input('status'));

        $profession = Profession::where('id', $id);
        $profession->update($requestData);
        Session::flash('flash_message', 'Profession Updated Successfully!');
        return redirect('admin/profession');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */

    public function destroy(Request $request, $id)
    {
        $specialityList = Speciality::where('profession_id', $id)->where('status', 1)->get();
        if(count($specialityList) > 0)
        {
            $message="Profession can't deleted.This profession have a speciality.";
            return response()->json(['message'=>$message],400);
        }
        $degreeList = Degree::where('profession_id', $id)->where('status', 1)->get();
        if(count($degreeList) > 0)
        {
            $message="Degree can't deleted.This degree have a speciality.";
            return response()->json(['message'=>$message],400);
        }
        $jobDetailList = JobDetail::where('profession_id', $id)->where('status', 1)->get();
        if(count($jobDetailList) > 0)
        {
            $message="Profession can't deleted.This profession have a Shift.";
            return response()->json(['message'=>$message],400);
        }
        $profession = profession::where('id', $id);
        $profession->delete();

        $message='Profession Deleted';
        return response()->json(['message'=>$message],200);

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {

        $profession = Profession::where('id', $id)->first();
        if($profession == NULL) {
            Session::flash('flash_message', 'Profession is not exist!');
            return redirect('admin/profession');
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $profession->status= '0';
                $profession->update();
                Session::flash('flash_message', 'Profession Status change to inactive');
            }else{
                $profession->status= '1';
                $profession->update();
                Session::flash('flash_message', 'Profession Status change to active');
            }

        }

        return redirect('admin/profession');
    }
}
