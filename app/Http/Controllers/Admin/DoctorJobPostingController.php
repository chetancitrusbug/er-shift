<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DoctorWish;
use App\DoctorDetail;
use App\State;
use App\Edvolume;
use App\Shift;
use App\Priorities;
use App\Priority;
use App\Locations;


class DoctorJobPostingController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
           
             $doctorwishes = DoctorWish::select('doctor_wish.*','doctor_wish.id as mainid','doctor_details.id as ddid','doctor_details.uid as dduid','users.*', 'users.id as userid')
             ->leftjoin('doctor_details','doctor_wish.doctor_id', 'doctor_details.id')
            ->leftjoin('users','doctor_details.uid', 'users.id')
            ->where('doctor_wish.status',1)
            ->where(function($q)use($keyword){
                $q->where('doctor_wish.wish_title', 'LIKE', "%$keyword%")
                    ->orWhere('users.fname', 'LIKE', "%$keyword%")
                    ->orWhere('users.lname', 'LIKE', "%$keyword%")
                    ->orWhere('users.name', 'LIKE', "%$keyword%");
            })->paginate($perPage);
            
           
        } else {
            $doctorwishes = DoctorWish::select('doctor_wish.*','doctor_wish.id as mainid','doctor_details.id as ddid','doctor_details.uid as dduid','users.*', 'users.id as userid')
             ->leftjoin('doctor_details','doctor_wish.doctor_id', 'doctor_details.id')
            ->leftjoin('users','doctor_details.uid', 'users.id')
            ->where('doctor_wish.status',1)
            ->paginate($perPage);
           
            
        }
   
        return view('admin.doctor-wish.index', compact('doctorwishes'));
    }

    
     

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {   
        $doctorwish = DoctorWish::with('doctor')->findOrFail($id);
        

        return view('admin.doctor-wish.show', compact('doctorwish'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $doctor = DoctorDetail::with('user')->get();
        $doctorArray=[];
        foreach($doctor as $doctors){
            $doctorArray[$doctors->id] = $doctors->user->name;
        }
        
        $locations = State::select('id','name')->where('status',1)->pluck('name','id');
        $ed_volumes = Edvolume::select('id','title')->where('status',1)->pluck('title','id');
        $shifts = Shift::select('id','title')->where('status',1)->pluck('title','id');
        $priorities = Priority::select('id','title')->where('status',1)->pluck('title','id');

        $doctorwish = DoctorWish::where('id',$id)->first();
        
         $date_shifts =  $doctorwish->date_shifts ;
         
        if($doctorwish!=null)
        {
            return view('admin.doctor-wish.edit', compact('doctorwish','doctorArray','locations','ed_volumes','shifts','priorities','date_shifts'));
        }
        else
        {
            return redirect('admin/doctor-wish')->with('flash_error', 'Data Is Not Valid!');
        }
       
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int      $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {   
        $this->validate($request, ['doctor_id' => 'required', 'wish_title' => 'required']);
        
        $data['doctor_id'] = $request->doctor_id;
        $data['wish_title'] = $request->wish_title;
        $data['wish_desc'] = $request->wish_desc;
        // $data['available_date'] = $request->available_date;
        $data['rate'] = $request->rate;
        // $data['status'] = 1;
        // $data['location_id'] = $request->location_id;
        // $data['shift_id'] = $request->shift_id;
        $data['ed_volum_id'] = $request->ed_volum_id;
        $data['priority_id'] = $request->priority_id;
        $doctorwish = DoctorWish::findOrFail($id);
        $doctorwish->update($data);

        $doctorwish->locations()->forceDelete() ;
        if($request->has('location_id') && $request->get('location_id') != "" ){
            // $data['location_id'] = implode(',', $request->location_id);
            $locations = $request->get('location_id') ;
            foreach($locations as $loc){
                Locations::create([
                    'type' => 'doctor_wish',
                    'field_id' => $doctorwish->id ,
                    'location_id' => $loc
                ]);
            }
        }


       if($request->has('from') &&  $request->get('from')  != "" && $request->has('to') &&  $request->get('to')  != "" && $request->has('shift_id') &&  $request->get('shift_id')  != "" ){
            $doctorwish->date_shifts()->get()->count() > 0 ? $doctorwish->date_shifts()->delete() : '' ;
            $froms  =  $request->get('from');  $to  =  $request->get('to'); $shift_id  =  $request->get('shift_id');
            foreach($froms as $key => $from ){
                if($from  != ''){
                       $doctorwish->date_shifts()->create([
                       
                        'doctor_id' => $doctorwish->doctor_user->uid , 
                        'from' => $from ,
                        'to' => $to[$key] ,
                        'shift_id' => $shift_id[$key] ,
                        
                    ]) ; 
                }
            }

       }

     


        
        return redirect('admin/doctorwish')->with('flash_message', 'ED staff wish Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        $doctorwish = DoctorWish::destroy($id);
        
        return redirect('admin/doctorwish')->with('flash_message', 'ED staff Wish Deleted!');
    }
}
