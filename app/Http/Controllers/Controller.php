<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use App\User;
use App\EmailTemplate;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function recommendedGetDoctorWishId($id = 0){
        if($id > 0){
        $query = collect(DB::select('
        SELECT DISTINCT
            (`doctor_wish`.`id`)
            FROM
            `doctor_wish`
            JOIN `job_detail`
                ON (
                doctor_wish.`priority_id` = 1
                AND job_detail.`location_id` IN (doctor_wish.`location_id`)
                )
                OR (
                doctor_wish.`priority_id` = 2
                AND job_detail.`ed_volume_id` = doctor_wish.`ed_volum_id`
                )
                OR (
                doctor_wish.`priority_id` = 5
                AND job_detail.`rate` = doctor_wish.`rate`
                )
                OR (
                doctor_wish.`priority_id` = 3
                AND
                (SELECT
                    date_shift.`from`
                FROM
                    date_shift
                WHERE date_shift.`doctor_wish_id` = doctor_wish.`id`
                    AND job_detail.`from_date` <= date_shift.`from`)
                AND
                (SELECT
                    date_shift.`to`
                FROM
                    date_shift
                WHERE date_shift.`doctor_wish_id` = doctor_wish.`id`
                    AND job_detail.`to_date` >= date_shift.`to`)
                )
                OR (
                doctor_wish.`priority_id` = 4
                AND
                (SELECT DISTINCT
                    (date_shift.`doctor_wish_id`)
                FROM
                    date_shift
                WHERE date_shift.`doctor_wish_id` = doctor_wish.`id`
                    AND job_detail.`shift_id` = date_shift.`shift_id`)
                )
            JOIN doctor_details ON doctor_details.`uid` = doctor_wish.`doctor_id`
            WHERE job_detail.`status` = 1 AND doctor_details.`profession_id` = job_detail.`profession_id` AND doctor_details.`speciality_id` = job_detail.`speciality_id`and doctor_wish.`status` = 1 and job_detail.employee_id = "'.$id.'"
            '))->pluck('id');
        //return $ids = implode(',', $query->toArray());
            return $ids = $query;
        }
        else{
            return $ids = array();
        }
    }

    public function recommendedGetEmployeeWishId($id = 0)
    {
        if ($id > 0) {
            $query = collect(DB::select('
        SELECT DISTINCT
            (`job_detail`.`id`)
            FROM
            `doctor_wish`
            JOIN `job_detail`
                ON (
                doctor_wish.`priority_id` = 1
                AND job_detail.`location_id` IN (doctor_wish.`location_id`)
                )
                OR (
                doctor_wish.`priority_id` = 2
                AND job_detail.`ed_volume_id` = doctor_wish.`ed_volum_id`
                )
                OR (
                doctor_wish.`priority_id` = 5
                AND job_detail.`rate` = doctor_wish.`rate`
                )
                OR (
                doctor_wish.`priority_id` = 3
                AND
                (SELECT
                    date_shift.`from`
                FROM
                    date_shift
                WHERE date_shift.`doctor_wish_id` = doctor_wish.`id`
                    AND job_detail.`from_date` <= date_shift.`from`)
                AND
                (SELECT
                    date_shift.`to`
                FROM
                    date_shift
                WHERE date_shift.`doctor_wish_id` = doctor_wish.`id`
                    AND job_detail.`to_date` >= date_shift.`to`)
                )
                OR (
                doctor_wish.`priority_id` = 4
                AND
                (SELECT DISTINCT
                    (date_shift.`doctor_wish_id`)
                FROM
                    date_shift
                WHERE date_shift.`doctor_wish_id` = doctor_wish.`id`
                    AND job_detail.`shift_id` = date_shift.`shift_id`)
                )
            JOIN doctor_details ON doctor_details.`uid` = doctor_wish.`doctor_id`
            WHERE job_detail.`status` = 1 AND doctor_details.`profession_id` = job_detail.`profession_id` AND doctor_details.`speciality_id` = job_detail.`speciality_id`and doctor_wish.`status` = 1 and doctor_details.`uid` = "' . $id . '"
            '))->pluck('id');
        //return $ids = implode(',', $query->toArray());
            return $ids = $query;
        } else {
            return $ids = array();
        }
    }

    public function sendPushNotification($fields)
    {
        if($fields['device_type'] == 'Android' || $fields['device_type'] == 'iOS' || $fields['device_type'] == 2)
        {
            
            $url = 'https://fcm.googleapis.com/fcm/send';
           
            $FIREBASE_KEY = \Config::get('setting.firebase_key'); 

            $notification =  $fields['data'];
            $message = array();

            $message['notification'] = $notification;

            $field = array(
                'to' => $fields['to'],
                'priority' => 'high',
                'notification' => $notification,
                'content_available'=> true,
                'data' => $notification          // for android notification when App is open
            );

            $headers = array(
                'Authorization: key=' . $FIREBASE_KEY,
                'Content-Type: application/json'
            );
            //Initializing curl to open a connection
            $ch = curl_init();

            //Setting the curl url
            curl_setopt($ch, CURLOPT_URL, $url);

            //setting the method as post
            curl_setopt($ch, CURLOPT_POST, true);

            //adding headers
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            //disabling ssl support
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            //adding the fields in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($field));

            //finally executing the curl request
            $result = curl_exec($ch);
            if ($result === false) {
                die('Curl failed: ' . curl_error($ch));
            }

            //Now close the connection
            curl_close($ch);

            //and return the result
            return $result;
        }
        elseif($fields['device_type'] == 'iOS')
        {
            /*Notification Code*/
            $notification =  $fields['data'];
            $message = array();
            $message['notification'] = $notification;

            //$deviceid = '41e0877e5d946d3c9ad0bacc168b983c2f68e7398ec0c442c7c8f25caeb5b9f7';
            $deviceid = $fields['device_token'];

            if($deviceid != ""){
                $pushData['aps'] = array(
                    'alert' => $notification['message'],
                    'badge' => 0,
                    'sound' => 'default',
                    'data' => $message
                );
                $pushData['show_id'] = 0; //$val['show_id']
                $pushData['type'] = 'show';
                $certificate_path = $_SERVER['DOCUMENT_ROOT'].'/dev/er-shift/public/pushnotificationios/gigableDistr.pem';
                //$gtlCommon = new GTL_Common();
                //echo "<pre>";print_r($pushData);exit;
                $notificationResp = send_notification_ios($deviceid, $pushData, "",$certificate_path);
                //echo "<pre>";print_r($notificationResp);
            }
            /*End Notification Code*/
        }
        else{
            //
        }

    }
    function send_notification_ios($userdeviceToken, $body = array(), $app_is_live = '0',$filename) {
        error_reporting(E_ALL);
      //  ini_set("display_errors", "-1");

        $app_is_live = '0';

        //$filename = $_SERVER['DOCUMENT_ROOT'] . "/app/gigableDistr.pem";;

        // Construct the notification payload
        if (!isset($body['aps']['sound'])) {
            $body['aps']['sound'] = "default";
        }

        // End of Configurable Items
        $payload = json_encode($body);
        $ctx = stream_context_create();


        if (!file_exists($filename)) {
            return true;
        }

           //echo "filename :".$filename; exit;
        if (is_array($userdeviceToken) && count($userdeviceToken) > 0) {

            foreach ($userdeviceToken as $key => $value) {

                stream_context_set_option($ctx, 'ssl', 'local_cert', $filename);
                if ($app_is_live == '0') {
                    $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
                    echo "<br/>url : ssl://gateway.push.apple.com:2195";
                } else {
                    $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
                    echo "<br/>url : ssl://sandbox.gateway.push.apple.com:2195";
                }


                if (!$fp) {
                    return "Failed to connect $err $errstr";
                    continue;
                } else {
                    $token_rec_id = '';
                    $token_rec_id = $value;
                        echo "<br/>Token Device : ".    $token_rec_id;
                    if ($token_rec_id != '') {
                        $msg = chr(0) . pack("n", 32) . pack('H*', str_replace(' ', '', $token_rec_id)) . pack("n", strlen($payload)) . $payload;
                        fwrite($fp, $msg);
                    }
                }
                fclose($fp);
            }
        } else {

            stream_context_set_option($ctx, 'ssl', 'local_cert', $filename);

            if ($app_is_live == '0'){
                $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
            }
            else{
                $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
            }
            if (!$fp) {
                return false;
            } else {
                $token_rec_id = $userdeviceToken;
                if ($token_rec_id != '') {
                    $msg = chr(0) . pack("n", 32) . pack('H*', str_replace(' ', '', $token_rec_id)) . pack("n", strlen($payload)) . $payload;
                    fwrite($fp, $msg);
                }
            }
            fclose($fp);
        }
        return $msg;
        // END CODE FOR PUSH NOTIFICATIONS TO ALL USERS
    }

    public function subscribeMailChimp($email,$usertype){

		header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");

        $apiKey = config('setting.mailchimp.key');

        ($usertype == 'doctor') ? $list_id =  config('setting.mailchimp.doctor_list_id') :  $list_id =  config('setting.mailchimp.emp_list_id') ;

        $json = '{"email_address": "'.$email.'","status": "subscribed"}';

        $url = "https://us18.api.mailchimp.com/3.0/lists/".$list_id."/members/";

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_USERPWD, 'ishan:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $result;
        $jsonArray = json_decode($result, true);

        $status = '';
        $call_status = "FALSE";
        $message = "Error in getting data";
        if (array_key_exists('status',$jsonArray)) {
            $status = $jsonArray['status'];

            if ($status == "subscribed") {
                $call_status = "TRUE";
                $message = "Subscribed successfully";

            } else {
                $message = $jsonArray['title'];
            }

        }

        $finalArray['STATUS'] = $call_status;
        $finalArray['MESSAGE'] = $message;
        // echo json_encode($finalArray);

        return true;
       
    }

    public function welcomeMail($user)
    {
        if(!is_array($user))
        {
            $user['user_type'] = $user->user_type;
            $user['name'] = $user->name;
            $user['email'] = $user->email;
        }

        $bodyArr = array('#NAME#');
        $postArr = array($user['name']);
        if($user['user_type'] == 'employee')
        {
            $data = EmailTemplate::where('template_key', 'employee')->first();
        }
        else {
            $data = EmailTemplate::where('template_key', 'doctor')->first();
        }
        $body = $data->content;
        $body = str_replace($bodyArr, $postArr, $body);
        $body = preg_replace('/\s\s+/', ' ', $body);
        //echo '<pre>'; print_r($body); exit;
        // //Welcome Mail Send
        // \Mail::send('email.welcomestaffemployee', compact('user','body'), function ($message) use ($user,$body) {
        //     $message->to($user['email'])->subject('Welcome to Er-Shift');
        // });

        //Welcome Mail Send
        try {
            \Mail::send('frontend-new.emails.welcome', compact('user','body'), function ($message) use ($user,$body) {
                $message->to($user['email'])->subject('Welcome to Er-Shift');
            });
        } catch (\Throwable $th) {
            \Log::info($th);
        }

        return true;
    }

    public function sendInvitationMail($user,$userFrom)
    {
        if(!is_array($user))
        {
            $user['user_type'] = $user->user_type;
            $user['name'] = $user->name;
            $user['email'] = $user->email;
        }

        //Invitation Mail
        try {
            \Mail::send('frontend-new.emails.chatinvitation', compact('user','userFrom'), function ($message) use ($user) {
                $message->to($user['email'])->subject('Chat invitation');
            });
        } catch (\Throwable $th) {
            //throw $th;
            \Log::info($th);
        }
        return true;
    }

   
}
