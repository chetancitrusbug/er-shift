<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\JobDetail;
use App\Subscription;
use App\Favourite;
use App\Setting;
use Auth;
use Session;
use Yajra\Datatables\Datatables;
class FavouriteController extends Controller
{
   public function index($id){
    $job = JobDetail::where('id', $id)->first();
        if($job == NULL)
        {
            Session::flash('flash_error', 'No Shift found');
            return redirect('jobs');
        }
        else
        {
            return view('frontend.favourit.index',compact('id'));
        }
   }
   public function datatable(request $request)
   {

        $SearchDetail = Favourite::select('favourite.*','users.name as name','job_detail.job_title as job_title')
        ->join('users', 'users.id', '=', 'favourite.user_id')
        ->join('job_detail', 'job_detail.id', '=', 'favourite.job_id')
        ->where('favourite.job_id',$request->VarID);


       if ($request->has('search') && $request->get('search') != '') {
           $search = $request->get('search');
           if ($search['value'] != '') {
               $value = $search['value'];
               $where_filter = "(job_detail.job_title LIKE  '%$value%' OR users.name LIKE  '%$value%' OR  favourite.user_type LIKE '%$value%')";
               $SearchDetail = $SearchDetail->whereRaw($where_filter);
           }
       }

        return Datatables::of($SearchDetail)->make(true);
        exit;
   }
   public function show(Request $request,$job_id,$id)
    {
        $job = JobDetail::where('id', $job_id)->first();
        $SearchDetail = Favourite::select('favourite.*','users.name as name','job_detail.job_title as job_title')
        ->join('users', 'users.id', '=', 'favourite.user_id')
        ->join('job_detail', 'job_detail.id', '=', 'favourite.job_id')
        ->where('favourite.id',$id)
        ->first();
        if($SearchDetail == NULL && $job == NULL) {
            Session::flash('flash_error', 'No Shift found');
            return redirect('jobs');
        }
        else if($job == NULL)
        {
            Session::flash('flash_error', 'No Shift found');
            return redirect('jobs');
        }
        else if($SearchDetail == NULL)
        {
            Session::flash('flash_error', 'Saved Shift not found');
            return redirect('favourite/'.$job_id);
        }
        else
        {
            return view('frontend.favourit.show',compact('SearchDetail'));
        }
    }
}
