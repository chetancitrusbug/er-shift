<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;

use App\User;
use App\Setting;
use App\Subscription;
use App\Order;
use Auth;
use Session;
use Mail;
use Lang;
use View;

class EmailController extends Controller {
    
    public function sendMailOnBillingStart($billing_id) {
        
        $admin_to_mail = env("MAIL_ADMIN_TO_ADDRESS","sadhana.citrusbug@gmail.com");
        $admin_cc_email = [];
        $Order = null;

        $setting = Setting::where('key','emails_as_cc')->first();
        if($setting && $setting->value != ''){
            $admin_cc_email = explode(',',$setting->value);
        }
        $admin_cc_email[] = $admin_to_mail;
       

        $billing = Subscription::with('user')->where("id",$billing_id)->first();


        if($billing && $billing->subscription_id && $billing->subscription_id!="" ){
            $package = json_decode($billing->package_detail);
           $user = User::where('id',Auth::user()->id)->first();
           $buyer_email = $user->email;
            $user = $billing->user;
            
            if($user){
                $buyer_email = $user->email;
            }
           
            $subject = trans('subscription.billing.mail_subject_user');
            $subject_admin = trans('subscription.billing.mail_subject_admin');
                        
            $lang = "en";
            Mail::send('email.billing.buyer.create-subscription', compact('billing', 'user','lang','package','Order'), function ($message) use ($buyer_email, $subject) {
                $message->to($buyer_email)->subject($subject);
            });
           
            Mail::send('email.billing.admin.create-subscription', compact('billing', 'user','lang','package','Order'), function ($message) use ($subject_admin, $admin_to_mail,$admin_cc_email) {
                $message->to($admin_cc_email)->subject($subject_admin);
            });

        }
    }
    public function sendMailOnInvoiceGenerate($Order_id) {
        
        $admin_to_mail = env("MAIL_ADMIN_TO_ADDRESS","sadhana.citrusbug@gmail.com");
        $admin_cc_email = [];

        $setting = Setting::where('key','emails_as_cc')->first();
        if($setting && $setting->value != ''){
            $admin_cc_email = explode(',',$setting->value);
        }
        $admin_cc_email[] = $admin_to_mail;
       

        $Order = Order::with('user')->where("id",$Order_id)->first();
       
        if($Order && $Order->billing){
            $billing = $Order->billing;
            $package = json_decode($billing->package);
           
            $buyer_email =$billing->buyer_email;
            $user = $billing->user;
            
            if($user){
                $buyer_email = $user->email;
            }
           
            $subject = trans('subscription.billing.mail_subject_subscription_user');
            $subject_admin = trans('subscription.billing.mail_subject_subscription_admin');
                        
            $lang = "en";
            Mail::send('email.billing.buyer.create-subscription-invoice', compact('billing', 'user','lang','package','Order'), function ($message) use ($buyer_email, $subject) {
                $message->to($buyer_email)->subject($subject);
            });
           
            Mail::send('email.billing.admin.create-subscription-invoice', compact('billing', 'user','lang','package','Order'), function ($message) use ($subject_admin, $admin_to_mail,$admin_cc_email) {
                $message->to($admin_cc_email)->subject($subject_admin);
            });

        }
    }

    
}
