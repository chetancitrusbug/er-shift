<?php

namespace App\Http\Controllers;
use App\User;
use App\Invitation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use DB;
use Auth;

class InvitationController extends Controller
{
    public function sendInvite(Request $request) {

        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $rules = array(
            'to_id'=>'required|integer',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400 ;
        } else {

            if(!Auth::check()){
                $request->session()->put('redirect_url', url()->previous());
                $request->session()->put('open_login_modal', true);
                $status = false ;
                $code = 501 ;
                $message = "Please login first to send the request";

            }else{

                $userFrom = Auth::user();
                $userTo = User::find($request->to_id);

                if(!$userFrom || !$userTo ){
                    $message = "User record not found";
                }else{
                    if($userFrom->user_type == $userTo->user_type ){
                        $message = "Both users have same type. You cannot send invite";
                    }else{
                        // find existing record
                        $invite = Invitation::Where('from_id',$userFrom->id)->Where('to_id',$userTo->id)->first();
                        if($invite){
                            if($invite->status == 0){
                                $message = 'Already invited';
                            }else if($invite->status == 1){
                                $message = 'Your invitation is already accepted ';
                            }else if($invite->status == 2){
                                $message = 'Your invitation is declined ';
                            }
                        }else{

                            $to_invite = Invitation::Where('to_id',$userFrom->id)->Where('from_id',$userTo->id)->first();

                            if($to_invite){
                                if($to_invite->status == 0){
                                    $message = 'You are already invited';
                                }else if($to_invite->status == 1){
                                    $message = 'You have already accepted invitation';
                                }else if($to_invite->status == 2){
                                    $message = 'you have declined the invitation';
                                }
                            }else{

                                if(!$invite && !$to_invite){
                                    $this->sendInvitationMail($userTo,$userFrom);
                                    $requestData['from_id'] = Auth::id();
                                    $requestData['to_id'] = $userTo->id;
                                    $requestData['init'] = $userFrom->user_type;
                                    $Invitation = Invitation::create($requestData);
                                    $data = $Invitation;
                                    $data->invitation_id = $data->id;
                                    $fields['data'] = array(
                                        'title' => 'New chat request arrived',
                                        'message' => 'Request from '. $userTo->name,
                                        'type' => 'chatinvation',
                                        'user_type' => $request->init,
                                        'request_user_name' => $userTo->name,
                                        'user_id' => $userTo->id,
                                        'from_id' => $userFrom->id ,
                                        'to_id' => $userTo->id
                                    );

                                    $fields['to'] = $userTo->firebasetoken;
                                    $fields['device_token'] = $userTo->device_token;
                                    $fields['device_type'] = $userTo->device_type;
                                    $this->sendPushNotification($fields);

                                    $message = 'Invitation has been sent succesfully';


                                }
                            }
                        }
                    }
                }

            }
        }
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }


    public function sendMessage(Request $request) {
        $data = [];
        $message = "Message sent successfully";
        $status = true;
        $code = 200;

        $requestData = $request->all();
        $rules = array(
            'invitation_id'=>'required|integer',
            'to_id'=>'required|integer',
            'message' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $UesrFrom = Auth::user();
            $userTo = User::find($request->to_id);
            $UesrInvitation = Invitation::where('id',$request->invitation_id)->where('status',1)->first();
            if($UesrFrom && $userTo  && $UesrInvitation)
            {
                $fields['data'] = array(
                    'title' => 'New Message arrived',
                    'body' => 'Message from '. $UesrFrom->name,
                    'message' => 'Message from '. $UesrFrom->name,
                    'type' => 'chatmessage',
                    'user_type' => $UesrFrom->user_type,
                    'user_id' => $request->to_id,
                    'from_id' => $UesrFrom->id ,
                    'to_id' => $request->to_id
                );
                $fields['to'] = $userTo->firebasetoken;
                $fields['device_token'] = $userTo->device_token;
                $fields['device_type'] = $userTo->device_type;
                $this->sendPushNotification($fields);
            }
        }
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function getInviteStatus(Request $request){
        $data = Invitation::find($request->id) ? Invitation::find($request->id) : [] ;
        return $data ;
    }

    public function updateInvite(Request $request) {
        $data = [];
        $message = "Invitation Updated";
        $status = true;
        $code = 200;
        $rules = array(
            'invitation_id'=>'required|integer',
            'read_status'=>'required|in:0,1',
            'status'=>'required|in:0,1,2',
        );
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;
        } else {
            $userTo = Auth::user();
            if($userTo != null)
            {
                $InvitationDetail = Invitation::Where('to_id', $userTo->id)->Where('id',$request->invitation_id)->first();
                if($InvitationDetail && $InvitationDetail['status'] == 0) {
                    $updateArr['status'] = $request->status;
                    $updateArr['read_status'] = $request->read_status;
                    $InvitationDetail = $InvitationDetail->update($updateArr);
                    if($request->status == 1)
                        $message = "Invitation Accepted, now you can start messaging";
                    if($request->status == 2)
                        $message = "Invitation Declined";
                }
                else if($InvitationDetail && $InvitationDetail['status'] == 1) {
                    $InvitationDetail->invitation_id = $InvitationDetail->id;
                    $message = 'Already Accepted';
                }
                else if($InvitationDetail && $InvitationDetail['status'] == 2) {
                    $code = 400;
                    $message = 'Already Decline';
                }
                else {
                    $code = 400;
                    $message = 'Not Found Invitation';
                }
            }
            else
            {
                $status = false;
                $code = 400;
                $message = 'Uesr not match';
            }
        }
        return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }



}
