<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invitation;
use Auth;
use App\DoctorDetail;
use App\user;

class MessageController extends Controller
{
    public function index(Request $request){
        $chat_users = null;
        if (Auth::user()->invites) {
            $chat_users = Invitation::whereIn('id',Auth::user()->invites)->get();
        }
        return view ('frontend-new.message.index',compact('chat_users'));
    }

    public function getDoctors(Request $request){

       $users = User::join('doctor_details','doctor_details.uid','=','users.id');
        if($request->has('name') &&  $request->get('name') != "" ){
            $users->where(function($q)use($request){
                $q->where('name', 'LIKE', '%'.$request->name.'%')
                ->orwhere('fname', 'LIKE', '%'.$request->name.'%')
                ->orwhere('lname', 'LIKE', '%'.$request->name.'%')
                ->orwhere('email', 'LIKE', '%'.$request->name.'%')
                ->orwhere('doctor_details.profile_name', 'LIKE', '%'.$request->name.'%');
            });
        }
        return $users->get();
    }
}
