<?php

namespace App\Http\Controllers\Auth;
use App\Providers\RouteServiceProvider;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Cartalyst\Stripe\Laravel\Facades\Stripe;

use Auth;
use Validator;
use Socialite;
use Session;
use Exception;
use File;

use App\User;
use App\EmployeeDetail;
use App\Package;
use App\Subscription;
use Stripe\Error\Card;


class SocialLoginController extends Controller
{
    protected $redirectTo = '/';

    public function redirectToLinkedin()
    {
        return Socialite::driver('linkedin')->redirect();
    }



    public function handleLinkedinCallback()
    {
        try {
            $user = Socialite::driver('linkedin')->user();

            $social_media_id = $user->id ;

            // check user to login
            $login_user = User::where("email", $user->email)->where("login_type",'linkedIn')->where("social_media_id", $social_media_id )->first();

            $nameArr = explode(' ',$user->name);
            $data = [
                'name' => $user->name ,
                'fname' => (($nameArr[0]) ? $nameArr[0] : ''),
                'lname' => (($nameArr[1]) ? $nameArr[1] : ''),
                'email' => $user->email ,
                'social_media_id' => $social_media_id ,
                'login_type' => "linkedIn" ,
                'user_type' => "employee" ,
                'is_active' => 1
            ];

            if($login_user){

                $login_user->update($data) ;

                Auth::loginUsingId($login_user->id);


            }else{


                $new_user  = User::create($data);
                $userDetails['uid'] = $new_user->id;
                $userDetails['tel_number'] = '';
                $userDetails['profile_pic'] = '';
                $userDetails['emp_name'] = $user->name;
                $userDetails['comp_name'] = '';
                $userDetails['comp_website'] = '';
                $userDetails['comp_information'] = '';
                $userDetails['address'] = '';
                $userDetails['location_id'] = 0;
                EmployeeDetail::create($userDetails);

                // Free package set
                $package = Package::where('isFree', 1)->first();
                if($package != null)
                {
                    $subScriptionDeatils['user_id'] = $new_user->id;
                    $subScriptionDeatils['package'] = $package->id;
                    $subScriptionDeatils['amount'] = 0;
                    $subScriptionDeatils['start_date'] = date('Y-m-d');
                    $subScriptionDeatils['end_date'] = date('Y-m-d', strtotime(date('Y-m-d'). ' + '.$package->day.' days'));
                    $subScriptionDeatils['subscription_id'] = 0;
                    $subScriptionDeatils['status'] = 1;
                    Subscription::create($subScriptionDeatils);
                }


                $stripe = Stripe::make(config('services.stripe.secret'));
                if ($new_user->stripe_id == "" || !$new_user->stripe_id) {

                    try {
                        $new_user_stripe = $stripe->customers()->create([
                            'email' => $user->email,
                        ]);

                        if ($new_user_stripe && isset($new_user_stripe['id'])) {
                            $new_user->stripe_id = $new_user_stripe['id'];
                            $new_user->save();
                        }
                    } catch (\Throwable $th) {
                        //throw $th;
                    }

                }

                try {
                    $this->welcomeMail($new_user);
                } catch (\Throwable $th) {
                    //throw $th;
                    \Log::info($th);
                }
                $this->welcomeMail($new_user);

                Auth::loginUsingId($new_user->id);


            }

            // if(Auth::user()->doctorDetail && Auth::user()->doctorDetail->profession_id > 0 ){

            //     $redirect_url = \Session::get('redirect_url') ;
            //     if($redirect_url){
            //         \Session::put('redirect_url', '');
            //         \Session::put('open_login_modal', false);
            //         $addToFavourite  = \Session::get('addToFavourite') ;
            //         if($addToFavourite){
            //             \Session::put('addToFavourite','');
            //             app('App\Http\Controllers\ShiftsController')->addToFavourite($request,$addToFavourite);
            //         }
            //         return redirect($redirect_url);
            //     }
                
                
            //     return redirect('/shifts');
            // }else{
            //     return redirect()->route('profile.edit');
            // }

            $redirect_url = \Session::get('redirect_url') ;
            if($redirect_url){
                \Session::put('redirect_url', '');
                \Session::put('open_login_modal', false);
                return redirect($redirect_url);
            }

            return redirect('/shifts');

        } catch (Exception $e) {
            return redirect()->route('home')->withErrors($e->getMessage());
        }
    }

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }


    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();

            // save image
            $filename = "" ;
            if($user->avatar_original){
                $fileContents = file_get_contents($user->avatar_original);
                try {
                    $filepath = public_path() . '/images/employee/' ;
                    $filename = $user->getId() . ".jpg" ;
                    File::put( $filepath.$filename, $fileContents);
                } catch (\Throwable $th) {
                    //throw $th;
                    \Log::info($th);
                }
            }

            $social_media_id = $user->id ;

            // check user to login
            $login_user = User::where("email", $user->email)->where("login_type",'facebook')->where("social_media_id", $social_media_id )->first();

            $nameArr = explode(' ',$user->name);
            $data = [
                'name' => $user->name ,
                'fname' => (($nameArr[0]) ? $nameArr[0] : ''),
                'lname' => (($nameArr[1]) ? $nameArr[1] : ''),
                'email' => $user->email ,
                'social_media_id' => $social_media_id ,
                'login_type' => "facebook" ,
                'user_type' => "employee" ,
                'is_active' => 1
            ];

            if($login_user){

                $login_user->update($data) ;

                if($login_user->employeeDetail){
                    $login_user->employeeDetail->update([
                        'profile_pic' => $filename ,
                        'emp_name' => $user->name
                    ]) ;
                }

                Auth::loginUsingId($login_user->id);



            }else{

                $new_user  = User::create($data);
                $userDetails['uid'] = $new_user->id;
                $userDetails['tel_number'] = '';
                $userDetails['profile_pic'] = $filename;
                $userDetails['emp_name'] = $user->name;
                $userDetails['comp_name'] = '';
                $userDetails['comp_website'] = '';
                $userDetails['comp_information'] = '';
                $userDetails['address'] = '';
                $userDetails['location_id'] = 0;
                EmployeeDetail::create($userDetails);

                // Free package set
                $package = Package::where('isFree', 1)->first();
                if($package != null)
                {
                    $subScriptionDeatils['user_id'] = $new_user->id;
                    $subScriptionDeatils['package'] = $package->id;
                    $subScriptionDeatils['amount'] = 0;
                    $subScriptionDeatils['start_date'] = date('Y-m-d');
                    $subScriptionDeatils['end_date'] = date('Y-m-d', strtotime(date('Y-m-d'). ' + '.$package->day.' days'));
                    $subScriptionDeatils['subscription_id'] = 0;
                    $subScriptionDeatils['status'] = 1;
                    Subscription::create($subScriptionDeatils);
                }

                $stripe = Stripe::make(config('services.stripe.secret'));
                if ($new_user->stripe_id == "" || !$new_user->stripe_id) {

                    try {
                        $new_user_stripe = $stripe->customers()->create([
                            'email' => $user->email,
                        ]);

                        if ($new_user_stripe && isset($new_user_stripe['id'])) {
                            $new_user->stripe_id = $new_user_stripe['id'];
                            $new_user->save();
                        }
                    } catch (\Throwable $th) {
                        //throw $th;
                        \Log::info($th);
                    }

                }
                try {
                    $this->welcomeMail($new_user);
                } catch (\Throwable $th) {
                    //throw $th;
                    \Log::info($th);
                }
                $this->welcomeMail($new_user);

                Auth::loginUsingId($new_user->id);


            }

            $redirect_url = \Session::get('redirect_url') ;
            if($redirect_url){
                \Session::put('redirect_url', '');
                \Session::put('open_login_modal', false);
                return redirect($redirect_url);
            }

            return redirect('/shifts');

        } catch (Exception $e) {
            return redirect()->route('home')->withErrors($e->getMessage());
        }
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }


    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->user();

            // save image
            $filename = "" ;
            if($user->avatar_original){
                $fileContents = file_get_contents($user->avatar_original);
                try {
                    $filepath = public_path() . '/images/employee/' ;
                    $filename = $user->getId() . ".jpg" ;
                    File::put( $filepath.$filename, $fileContents);
                } catch (\Throwable $th) {
                    //throw $th;
                    \Log::info($th);
                }
            }

            $social_media_id = $user->id ;

            // check user to login
            $login_user = User::where("email", $user->email)->where("login_type",'google')->where("social_media_id", $social_media_id )->first();

            $nameArr = explode(' ',$user->name);
            $data = [
                'name' => $user->name ,
                'fname' => (($nameArr[0]) ? $nameArr[0] : ''),
                'lname' => (($nameArr[1]) ? $nameArr[1] : ''),
                'email' => $user->email ,
                'social_media_id' => $social_media_id ,
                'login_type' => "google" ,
                'user_type' => "employee" ,
                'is_active' => 1
            ];

            if($login_user){

                $login_user->update($data) ;

                if($login_user->employeeDetail){
                    $login_user->employeeDetail->update([
                        'profile_pic' => $filename ,
                        'emp_name' => $user->name
                    ]) ;
                }

                Auth::loginUsingId($login_user->id);



            }else{

                $new_user  = User::create($data);
                $userDetails['uid'] = $new_user->id;
                $userDetails['tel_number'] = '';
                $userDetails['profile_pic'] = $filename;
                $userDetails['emp_name'] = $user->name;
                $userDetails['comp_name'] = '';
                $userDetails['comp_website'] = '';
                $userDetails['comp_information'] = '';
                $userDetails['address'] = '';
                $userDetails['location_id'] = 0;
                EmployeeDetail::create($userDetails);

                // Free package set
                $package = Package::where('isFree', 1)->first();
                if($package != null)
                {
                    $subScriptionDeatils['user_id'] = $new_user->id;
                    $subScriptionDeatils['package'] = $package->id;
                    $subScriptionDeatils['amount'] = 0;
                    $subScriptionDeatils['start_date'] = date('Y-m-d');
                    $subScriptionDeatils['end_date'] = date('Y-m-d', strtotime(date('Y-m-d'). ' + '.$package->day.' days'));
                    $subScriptionDeatils['subscription_id'] = 0;
                    $subScriptionDeatils['status'] = 1;
                    Subscription::create($subScriptionDeatils);
                }

                

                $stripe = Stripe::make(config('services.stripe.secret'));
                if ($new_user->stripe_id == "" || !$new_user->stripe_id) {

                    try {
                        $new_user_stripe = $stripe->customers()->create([
                            'email' => $user->email,
                        ]);

                        if ($new_user_stripe && isset($new_user_stripe['id'])) {
                            $new_user->stripe_id = $new_user_stripe['id'];
                            $new_user->save();
                        }
                    } catch (\Throwable $th) {
                        //throw $th;
                    }

                }
                try {
                    $this->welcomeMail($new_user);
                } catch (\Throwable $th) {
                    //throw $th;
                    \Log::info($th);
                }
                $this->welcomeMail($new_user);

                Auth::loginUsingId($new_user->id);


            }


            $redirect_url = \Session::get('redirect_url') ;
            if($redirect_url){
                \Session::put('redirect_url', '');
                \Session::put('open_login_modal', false);
                return redirect($redirect_url);
            }

            return redirect('/shifts');

        } catch (Exception $e) {
            return redirect()->route('home')->withErrors($e->getMessage());
        }
    }


}
