<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Hash;
use App\ActionLog;
use Illuminate\Http\Request;
use Auth;
use Session;
use App\Package;
use App\Subscription;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected function authenticate(Request $request)
    {
        $credentials = array(
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'is_active' => 1,
            'user_type' => 'admin'
        );
        $credentialsEmployee = array(
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'is_active' => 1,
            'user_type' => 'employee'
        );

        $user = User::where("email", $request->email)->where("user_type","!=","doctor")->first();
    
        if ($user) {        
            
            if($user->password == ""){
                
                return redirect()->back()->withInfo( __('message.login.email_registered_with_social_login',['login_type' => ucfirst($user->login_type)]) );   
            }
           
            if (Hash::check($request->password, $user->password)) {

                if ($user->is_active == 1 && (Auth::attempt($credentials) || Auth::attempt($credentialsEmployee))) {
                         
					if(Auth::user()->hasRole('SU')){
                        return redirect('/admin'); 
                    }
                    else if(Auth::user()->user_type =="employee"){
                        $stripe = Stripe::make(config('services.stripe.secret'));
                        if($user->stripe_id=="" || !$user->stripe_id){
            
                            $customer = $stripe->customers()->create([
                                'email' => $user->email,
                            ]);
                
                              if($customer && isset($customer['id'])){
                                $user->stripe_id = $customer['id'];
                                $user->save();
                            }
                        }
                        // Free package set 
                        $package = Package::where('isFree', 1)->first();
                        $current_subscriptions = Subscription::where('user_id',Auth::user()->id)->where('status',1)->latest()->first();
                        $subscription = Subscription::where('user_id',Auth::user()->id)
                                                    ->where('package',$package->id)->first();
                                                  //  dd($subscription);
                        if($current_subscriptions == null && $subscription == null){
                            if($package != null)
                            {
                                $subScriptionDeatils['user_id'] = Auth::user()->id;
                                $subScriptionDeatils['package'] = $package->id;
                                $subScriptionDeatils['amount'] = 0;
                                $subScriptionDeatils['start_date'] = date('Y-m-d');
                                $day = $package->day;
                                $subScriptionDeatils['end_date'] = date('Y-m-d', strtotime(date('Y-m-d'). ' + '.$day.' days'));
                                $subScriptionDeatils['subscription_id'] = 0;
                                $subScriptionDeatils['status'] = 1;
                                Subscription::create($subScriptionDeatils);
                            }
                        }

                        $redirect_url = \Session::get('redirect_url') ;
                        if($redirect_url){
                            \Session::put('redirect_url', '');
                            \Session::put('open_login_modal', false);
                            return redirect($redirect_url);
                        }
        
                        return redirect('/shifts');
					}
                    
                    Session::put('email',$request->email);
                    Session::put('password',$request->password);

                } else {
                    Auth::logout();
                    if($user->is_active != 1){
                        Session::flash('flash_error',  __('message.login.inactive_account')  );
                        return back()->withInfo(  __('message.login.inactive_account')  );
                    }else{
                        Session::flash('flash_error',  __('message.login.invalid_credential') );
                        return back()->withErrors(  __('message.login.invalid_credential') );
                    }
                    Session::flash('is_active_error', 'Yes');
                
                }
            } else {
                
                Session::flash('flash_error',  __('message.login.incorrect_password') );
                return back()->withErrors(  __('message.login.incorrect_password') );
            }
        } else {
            Session::flash('flash_error',  __('message.login.email_not_exist')  );
            return back()->withErrors(  __('message.login.email_not_exist')  );
        }
          return redirect()->back()->withInput();   
    } 


    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function login(Request $request){

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
            ]);

        $admin = \Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
            'user_type' => 'admin',
            'login_type' => 'simple',
            'is_active' => 1
            ]) ;

        $employee = \Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
            'user_type' => 'employee',
            'login_type' => 'simple',
            'is_active' => 1
            ]) ;

        if ($admin || $employee){

            if(Auth::user()->hasRole('SU')){
                return redirect('/admin'); 
            }

            if(Auth::user()->user_type == "employee"){

                $stripe = Stripe::make( config('services.stripe.secret') );
                if(Auth::user()->stripe_id=="" || !Auth::user()->stripe_id){
                    try {
                        $customer = $stripe->customers()->create([
                            'email' => Auth::user()->email,
                        ]);
                        if($customer && isset($customer['id'])){
                            Auth::user()->stripe_id = $customer['id'];
                            Auth::user()->save();
                        }
                    } catch (\Throwable $th) {
                        return redirect()->route('home')->withErrors($th->getMessage());
                    }
                }

                // Free package set 
                $package = Package::where('isFree', 1)->first();
                $current_subscriptions = Subscription::where('user_id',Auth::user()->id)->where('status',1)->latest()->first();
                $subscription = Subscription::where('user_id',Auth::user()->id)
                                            ->where('package',$package->id)->first();
                                          //  dd($subscription);
                if($current_subscriptions == null && $subscription == null){
                    if($package != null)
                    {
                        $subScriptionDeatils['user_id'] = Auth::user()->id;
                        $subScriptionDeatils['package'] = $package->id;
                        $subScriptionDeatils['amount'] = 0;
                        $subScriptionDeatils['start_date'] = date('Y-m-d');
                        $day = $package->day;
                        $subScriptionDeatils['end_date'] = date('Y-m-d', strtotime(date('Y-m-d'). ' + '.$day.' days'));
                        $subScriptionDeatils['subscription_id'] = 0;
                        $subScriptionDeatils['status'] = 1;
                        Subscription::create($subScriptionDeatils);
                    }
                }

                $redirect_url = \Session::get('redirect_url') ;
                if($redirect_url){
                    \Session::put('redirect_url', '');
                    \Session::put('open_login_modal', false);
                    return redirect($redirect_url);
                }

                return redirect('/shifts');

            }
            
        }else{

            $user = User::where("email", $request->email)->whereIn("user_type",["employee", "admin"])->first();
            $error = "" ;
            if(!$user){
                $error = "This email does not exist. Please register" ;
                return redirect()->back()->withInput()->withErrors($error);
            }else{

                if($user->login_type != "simple"){
                    $error = "This email is registered with ".ucfirst($user->login_type).", Please try to login via ".ucfirst($user->login_type) ;
                    return redirect()->back()->withInput()->withErrors($error);
                }
                if ($user->is_active != 1) {
                    $error = "Your account is not activate , Please activate your account from mail" ;
                    return redirect()->back()->withInput()->withErrors($error);
                }
                if(!\Hash::check($request->password, $user->password)) {
                    $error = "You have entered wrong password" ;
                    return redirect()->back()->withInput()->withErrors($error);
                }
            }
            return redirect()->back()->withErrors(trans('auth.failed'))->withInput();
        }
        return redirect()->back()->withErrors(trans('auth.failed'))->withInput();
    }



    public function logout(Request $request)
    {
		 
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/login');
    }
}
