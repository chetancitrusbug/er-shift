<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Notifications\AdminMail;
use Auth;
use Session;
use App\EmployeeDetail;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:50',
            'last_name' => 'required|string|max:50',
            'email' => 'required|string|email|max:80',
            'password' => 'required|string|min:8|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        //dd($data);


        $user = User::create([
            'name' => $data['first_name']." ".$data['last_name'],
            'fname' => $data['first_name'],
            'lname' => $data['last_name'],
            'email' => $data['email'],
			'login_type' => 'simple',
            'password' => bcrypt($data['password']),
            'user_type' => "employee",
            'activation_token' => sha1(time() . uniqid() . $data['email']),
            'activation_time' => \Carbon\Carbon::now(),
            'is_active' => 0,
            'social_media_id' => '',
            'api_token' => '',
            'device_token' => '',
            'device_type' => '',
            'token_id' => '0',
            'stripe_id' => '0',
            'firebasetoken' => '',
        ]);

        $userDetails['uid'] = $user->id;
        $userDetails['tel_number'] = '';
        $userDetails['profile_pic'] = '';
        $userDetails['emp_name'] = $user->name;
        $userDetails['comp_name'] = '';
        $userDetails['comp_website'] = '';
        $userDetails['comp_information'] = '';
        $userDetails['address'] = '';
        $userDetails['location_id'] = '';
        EmployeeDetail::create($userDetails);
        $this->subscribeMailChimp($data['email'],'employee');
        return $user;
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $useremail = User::where('email',$request->email)->where('user_type','employee')->where("login_type",'simple')->first();
       
        if($useremail != null){
            $this->validate($request, ['email' => 'required|unique:users']);
        }
        $user = $this->create($request->all());
        $stripe = Stripe::make(config('services.stripe.secret'));
        if($user->stripe_id=="" || !$user->stripe_id){

            try {
                $customer = $stripe->customers()->create([
                    'email' => $user->email,
                ]);
                if($customer && isset($customer['id'])){
                    $user->stripe_id = $customer['id'];
                    $user->save();
                }
            } catch (\Throwable $th) {
                //throw $th;
                \Log::info($th);
            }
        }
        event(new Registered($user));

        $this->afterRegister($user);


        return $this->registered($request, $user)
            ?: redirect($this->redirectPath())->withStatus( __('message.register.register_success') );

    
    }
    public function afterRegister($user)
    {
        $this->sendEmail($user);
    }

     /**
     * Send Email Helper
     *
     * @param User $user
     */
    public function sendEmail(User $user)
    {
        // $user->notify(new  \App\Notifications\Welcome($user));
        try {
            $this->welcomeMail($user);
        } catch (\Throwable $th) {
            \Log::info($th);
        }
        return $user->notify(new  \App\Notifications\ActivationLink($user));
    }

    public function activateAccount($token)
    {

        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return redirect()->route('home')->withErrors( __('message.register.activation_link_expired') );
        }
        elseif ($user->is_active == 1) {
            return redirect()->route('home')->withStatus( __('message.register.account_already_activated') );
        }
        $user->activation_token = null;
        $user->activation_time = null;
        $user->is_active = 1;
        $user->save();
        // \Auth::loginUsingId($user->id);
        return redirect()->route('home')->withStatus( __('message.register.account_activated') );

    }
}
