<?php

namespace App\Http\Controllers\Auth;


use App\User;
use App\Package;
use App\Subscription;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\EmployeeDetail;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Socialite;
use Session;
use Auth;
use Exception;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;


class AuthController extends Controller
{


    use  ThrottlesLogins;


    protected $redirectTo = '/';


    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }


    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
			'login_type' => 'linkedIn',
        ]);
    }


    public function redirectToLinkedin()
    {
        return Socialite::driver('linkedin')->redirect();
    }


    public function handleLinkedinCallback()
    {
        try {
            $user = Socialite::driver('linkedin')->user();

            $social_media_id = $user->id ; 

            // check user to login 
            $login_user = User::where("email", $user->email)->where("login_type",'linkedIn')->where("social_media_id", $social_media_id )->first();

            $nameArr = explode(' ',$user->name);
            $data = [
                'name' => $user->name ,
                'fname' => (($nameArr[0]) ? $nameArr[0] : ''),
                'lname' => (($nameArr[1]) ? $nameArr[1] : ''),
                'email' => $user->email ,
                'social_media_id' => $social_media_id ,
                'login_type' => "linkedIn" ,
                'user_type' => "employee" ,
                'is_active' => 1 
            ];

            if($login_user){

                $login_user->update($data) ;

                Auth::loginUsingId($login_user->id);

                return redirect('/shifts');

            }else{

                $new_user  = User::create($data);
                $userDetails['uid'] = $new_user->id;
                $userDetails['tel_number'] = '';
                $userDetails['profile_pic'] = '';
                $userDetails['emp_name'] = '';
                $userDetails['comp_name'] = '';
                $userDetails['comp_website'] = '';
                $userDetails['comp_information'] = '';
                $userDetails['address'] = '';
                $userDetails['location_id'] = '';
                EmployeeDetail::create($userDetails);

                // Free package set
                $package = Package::where('isFree', 1)->first();
                if($package != null)
                {
                    $subScriptionDeatils['user_id'] = $new_user->id;
                    $subScriptionDeatils['package'] = $package->id;
                    $subScriptionDeatils['amount'] = 0;
                    $subScriptionDeatils['start_date'] = date('Y-m-d');
                    $subScriptionDeatils['end_date'] = date('Y-m-d', strtotime(date('Y-m-d'). ' + '.$package->day.' days'));
                    $subScriptionDeatils['subscription_id'] = 0;
                    $subScriptionDeatils['status'] = 1;
                    Subscription::create($subScriptionDeatils);
                }

                $stripe = Stripe::make(config('services.stripe.secret'));
                if ($new_user->stripe_id == "" || !$new_user->stripe_id) {

                    $new_user_stripe = $stripe->customers()->create([
                        'email' => $user->email,
                    ]);

                    if ($new_user_stripe && isset($new_user_stripe['id'])) {
                        $new_user->stripe_id = $new_user_stripe['id'];
                        $new_user->save();
                    }
                }
                $this->welcomeMail($new_user);

                Auth::loginUsingId($new_user->id);

                return redirect('/shifts');
              
            }

            return redirect('/login');
    
        } catch (Exception $e) {
            if($e->getCode() == 400)
            {
                return redirect('/login');
            }
            Session::flash('flash_error',$e->getMessage());
            return redirect('/register');
        }
    }
}