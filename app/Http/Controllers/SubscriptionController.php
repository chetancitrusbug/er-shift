<?php

namespace App\Http\Controllers;
use App\Subscription;
use Illuminate\Http\Request;
use Session;
use Carbon;
use App\Package;
use Auth;
use App\User;
use App\Order;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;
use App\Http\Controllers\EmailController;
use App\Billingcycle;
class SubscriptionController extends Controller
{
    private $mail_function;
    public function __construct()
    {
       $this->mail_function = new EmailController();
    }
   
    public function plans(Request $request)
    { 
    
        $billingcyle = new Billingcycle();
        $billingcyle->checkBillingCycle();
        $current_subscription = Subscription::
                             with('packageDetail')
                             ->where("status","1")
                             ->where("user_id",Auth::user()->id)
                             ->first();
        
        if($current_subscription != null)
        {
            $orderChangeMsg = 'Are you sure you want to Cancel Previous Subscription and Purchase new Plan ?';
        }
        else{
            $orderChangeMsg = 'Are you sure you want to Purchase new Plan ?';
        }
        // show active stripe packages
        $stripe = Stripe::make(config('services.stripe.secret'));  
        $plans = $stripe->plans()->all();
        $produts=[];
        foreach($plans['data'] as $plan){
            $produts[] = $plan['product'] ;

        }
        $count_subscription = Subscription::where("user_id",Auth::user()->id)->count();
        $packages = Package::whereIn("stripe_product_id",$produts)->where('status',1)->where('isFree','!=',1)->orderBy("amount","DESC")->get();

        $orders = Order::with('package')->where("emp_id","=",Auth::user()->id)->where('status',1)->orderBy("id","desc")->get();

        return view('frontend-new.subscription.plans',compact('packages','orderChangeMsg','count_subscription','current_subscription', 'orders'));
    }
    
    public function subscription(Request $request)
    {        
       $current_subscription = Subscription::
                             with('packageDetail')
                             ->where("status","1")
                             ->where("user_id",Auth::user()->id)
                             ->latest()
                             ->first();
                           
       $count_subscription = Subscription::where("user_id",Auth::user()->id)->count();
       return view('frontend.subscription.subscription',compact('current_subscription','count_subscription'));
    }
    
    
    public function doSubscriptionTrial(Request $request)
    {

        $package    = Package::where('isFree', 1)->first();
        if($package != null)
        {
            $subScriptionDeatils['user_id'] = Auth::user()->id;
            $subScriptionDeatils['package'] = $package->id;
            $subScriptionDeatils['amount'] = 0;
            $subScriptionDeatils['start_date'] = date('Y-m-d');
            $subScriptionDeatils['end_date'] = date('Y-m-d', strtotime(date('Y-m-d'). ' + '.$package->day.' days'));
            $subScriptionDeatils['subscription_id'] = 0;
            $subScriptionDeatils['status'] = 1;
            Subscription::create($subScriptionDeatils);

            return redirect()->back()->withStatus( __('message.subscription.free_trial_plan_activated' ,  [ 'days' =>  $package->day ] )  );
        }
       
       return redirect()->back()->withErrors( __('message.subscription.package_not_found') );
    }
    
    
    public function history(Request $request)
    {
        $user_id = Auth::user()->id;
     
       $Order = Order::with('package')->where("emp_id","=",$user_id)->where('status',1)->orderBy("id","desc")->get();
    
       return view('frontend.subscription.history',compact('Order'));
    }
   
    public function confirmOrder($package_id)
    {
        
       $package = Package::whereId($package_id)->where('status',1)->orderBy("status","1")->first();
       
       if(!$package){
            return redirect('billing/plans')->withErrors(__('message.subscription.package_not_found'));
       }
      
       $current_subscription = Subscription::
                             where("status","1")
                             ->where("package",$package_id)
                             ->where("user_id",Auth::user()->id)
                             ->first();

        if($current_subscription != null){
            return redirect('billing/plans')->withStatus( __('message.subscription.already_purchased_plan_choose_another') );
        }
       return view('frontend-new.subscription.confirm-order',compact('package'));

    }
    public function confirmOrderPayment($order_id)
    {
        
       $billing = Subscription::whereId($order_id)->where("status",3)->first();
       
       if(!$billing){
            return redirect('billing/plans')->withErrors( __('message.subscription.something_went_wrong') );
       }
       return view('frontend-new.subscription.confirm-order-payment',compact('billing'));

    }
    public function doOrderStep1(Request $request)
    {
      
        $this->validate($request, [
                'name' => 'required',
                'email' => 'required',
                'package_id' => 'required',
        ]);
         
        $package = Package::where("id",$request->package_id)->where('status',1)->first();
        if(!$package){
            return redirect()->back()->withErrors( __('message.subscription.package_not_found') );
        }
        
        $requestData = [];
        $requestData['user_id']=Auth::user()->id;
        $requestData['package']=$request->package_id;
        $requestData['amount']=$package->amount;
        $requestData['status']=3;
        $requestData['start_date']= Carbon\Carbon::now(); 
        $requestData['end_date']=Carbon\Carbon::now();
        $requestData['package_detail']=  json_encode($package);
        $requestData['plan_id']= $package->id;

        $billing = Subscription::create($requestData);
        return redirect('order/payment/'.$billing->id);
    }
    public function doOrderPayment(Request $request)
    {
        $year = date('Y', strtotime(date('Y'). ' - 1 year'));
        
        $this->validate($request, [
            // 'card_no' => 'required',
            // 'ccExpiryMonth' => 'required|integer|between:1,12',
            // 'ccExpiryYear' => 'required|date_format:"Y"|after:'.$year,
            // 'cvvNumber' => 'required',
            'token' => 'required',
            'billing_id' => 'required',
        ]);
       
        $billing = Subscription::with('packageDetail')->where("id",$request->billing_id)->first();
        if(!$billing){
            Session::flash('flash_message', __('message.subscription.something_went_wrong') );
            return redirect()->back()->withErrors(__('message.subscription.something_went_wrong'));
        }
        //$package = json_decode($billing->package);
     //  dd(getenv("STRIPE_SECRET"));
        $stripe = Stripe::make(config('services.stripe.secret'));
        
        $user = User::where('id',Auth::user()->id)->first();
     
        if($user->stripe_id=="" || !$user->stripe_id){
            
            $customer = $stripe->customers()->create([
                'email' => $user->email,
            ]);

            if($customer && isset($customer['id'])){
                $user->stripe_id = $customer['id'];
                $user->save();
            }
        }
     
        if($user->stripe_id==""){
            Session::flash('flash_message',__('message.subscription.stripe_user_not_created'));
            return redirect()->back()->withInfo(__('message.subscription.stripe_user_not_created'));
        }
        try {
            
            // $card = $stripe->tokens()->create([
            //     'card' => [
            //         'number' => $request->get('card_no'),
            //         'exp_month' => $request->get('ccExpiryMonth'),
            //         'exp_year' => $request->get('ccExpiryYear'),
            //         'cvc' => $request->get('cvvNumber')
            //     ],
            // ]); 

            // Assign card to customer
            // if($card && isset($card['id'])){
            //     $stripe->cards()->create($user->stripe_id,$card['id']); 
            // }

            $card = $stripe->cards()->create($user->stripe_id, $request->get('token') );

            $plans = $stripe->plans()->all();
            $subscription = $stripe->subscriptions()->create($user->stripe_id, [
                'plan' => $billing->packageDetail->id,
            ]);
            
            if($subscription && isset($subscription['id'])){
                $billingcyle = new Billingcycle();
                $billingcyle->stopAllSubscriptionByUserId(Auth::user()->id,$user->stripe_id);
                $billing->subscription_id =  $subscription['id'];
                $billing->customer_stripe_id =  $user->stripe_id;
                $billing->status =  1;
                $billing->save();
            }else{
                Session::flash('flash_message',__('message.subscription.subscription_not_started'));
                return redirect()->back()->withErrors(__('message.subscription.subscription_not_started'));
            }
            $this->mail_function->sendMailOnBillingStart($billing->id);
            $billingcyle = new Billingcycle();
            $billingcyle->checkBillingCycle();
            Session::flash('flash_message', __('message.subscription.subscription_started' ) );
            return redirect('billing/plans')->withStatus( __('message.subscription.subscription_started' ) );
        }
        catch (\Exception $e) {
            Session::flash('flash_message',$e->getMessage());
            return redirect('order/payment/'.$request->billing_id)->withErrors($e->getMessage());
        }
    }

    
    public function cancelSubscription(Request $request,$id){
        
        $requestData = $request->all();
        $user = User::where('id',Auth::user()->id)->first();
        $Subscription = Subscription::where('id',$id)->where('status',1)->first();
        
        if($Subscription != null ){
            $Package = Package::where('id',$Subscription->package)->first();
            if($Package != null && $Package->isFree != 1)
            {
                $stripe = Stripe::make(config('services.stripe.secret'));
                try {
                    $canSub = $stripe->subscriptions()->cancel($user->stripe_id, $Subscription->subscription_id);
                    $updateRequestData = array(
                        'status' => 2);
                    $Subscription->update($updateRequestData);
                    Session::flash('flash_message',__('message.subscription.subscription_package_cancelled') );
                }
                catch (\Exception $e) {
                    Session::flash('flash_message',$e->getMessage());
                }
            }
            else
            {
                Session::flash('flash_message', __('message.subscription.cannot_cancel_free_plan') );
            }
        }
        else{
            Session::flash('flash_message', __('message.subscription.subscription_not_found') );
        }
        return redirect('billing/plans');
    }

}
