<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Sadmin extends Model
{
    use SoftDeletes;
    //only for user model
    public $is_user = true;

    use Notifiable, HasRoles;


    use  BaseModelTrait;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','cpnj','mobile','phone','comments','deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'created_by', 'updated_by'
    ];

    public function role()
    {
        return $this->belongsToMany('App\Role','role_user','user_id','role_id');
    }


    public function session(){
        return $this->hasOne('App\Session','user_id','id');
    }

    
}
