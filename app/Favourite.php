<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Favourite extends Model
{
    protected $table = 'favourite';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'user_type','user_id','job_id' ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = []; 

    public function job()
    {
        return $this->belongsTo('App\JobDetail','job_id','id')->select('id', 'job_title', 'job_responsibility', 'profession_id', 'speciality_id', 'degree_id', 'experience_id', 'address', 'location_id', 'ed_volume_id', 'from_date' , 'to_date' , 'shift_id', 'rate', 'status', 'created_at','employee_id')->where('status','=', 1);
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id')->select('id', 'name');
    }

    public function doctor()
    {
        return $this->belongsTo('App\DoctorDetail','user_id','uid')->select('doctor_details.id', 'doctor_details.uid', 'doctor_details.profile_image', 'doctor_details.address', 'doctor_details.location_id',
        DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "'.url('').'",CONCAT("/images/doctor/"),doctor_details.profile_image) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS profile_image')
        )->with('location');

        /*return $this->belongsToOne('App\State','doctor_details','id','location_id')->select('doctor_details.id', 'doctor_details.uid', 'doctor_details.profile_image', 'doctor_details.address', 'doctor_details.location_id','state.name as location_title',
        DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "'.url('').'",CONCAT("/images/doctor/"),doctor_details.profile_image) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS profile_image')
        );*/
    }
}
