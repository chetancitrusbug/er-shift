<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'transaction';
    
    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pack_id', 'emp_id', 'amount', 'start_date', 'expire_date' ,'auth_response',
    'trans_status','status' ];


    public function employee(){
        return $this->hasOne('App\EmployeeDetail','id','emp_id');
    }

    public function user(){
        return $this->hasOne('App\User','id','emp_id');
    }

    public function package(){
        return $this->hasOne('App\Package','id','pack_id');
    }
	
	public function biiling(){
        return $this->hasOne('App\Subscription','id','billing_id');
    }

    /*public function user(){
        return $this->hasOne('App\User','id','emp_id')->select('id','name','fname','lname','email','api_token','token_id','stripe_id');
    }

    public function package(){
        return $this->hasOne('App\Package','id','pack_id')->select('id','title','description','amount','day','isFree');
    }
	 if($Order){
                if(count($Order) > 0){
                    
                    
                   
                    $result = $Order;
                    $message = 'success';
                }
                else{
                    
                    $message = 'No Record Found';
                    $code = 400;
                    $status = 'false';
                }
            }
            else{
                $message = 'Not Found Any Order';
                $code = 400;
                $status = 'false';
            }   
            
	*/
}
