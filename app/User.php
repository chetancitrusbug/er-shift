<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\HasRoles;
class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password','name','fname','lname','activation_token','api_token','activation_time','user_type','login_type','social_media_id','device_token','device_type','is_active','stripe_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function doctorDetail(){
        return $this->hasOne('App\DoctorDetail','uid','id');
    }
	public function EmployeeDetail(){
        return $this->hasOne('App\EmployeeDetail','uid','id');
    }

    public function invites_from(){
        return $this->belongsTo('App\Invitation','id','from_id')->whereIn('status',[0,1]);
    }
    public function invites_to(){
        return $this->belongsTo('App\Invitation','id','to_id')->whereIn('status',[0,1]);
    }

    public function getInvitesAttribute(){
       return array_unique(array_merge($this->invites_from()->pluck('id')->toArray(),$this->invites_to()->pluck('id')->toArray())) ;
    }

    public function getMyInviteCountAttribute(){
        return $this->invites_to()->where('status',0)->count() ;
     }
     
}
