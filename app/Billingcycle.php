<?php

namespace App;
use App\Subscription;
use Illuminate\Database\Eloquent\Model;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;
use App\Order;
use App\Http\Controllers\EmailController;

class Billingcycle extends Model
{
    private $mail_function;
    public function __construct()
    {
       $this->mail_function = new EmailController();
    }
    
    public function checkBillingCycle()
    {
        //dd(44);
        $stripe = Stripe::make(config('services.stripe.secret'));
        
        Subscription::where("status",1)->where("end_date","<=",date('y-m-d'))->update(["status"=>"4"]);
        
        $billings = Subscription::where("status","!=",3)->where("status","!=",2)->where("end_date","<=",date('y-m-d'))->get();
       // dd($billings);
        foreach($billings as $billing){
            $package = json_decode($billing->package_detail);
            if($package != null)
            {
                try { 
                    $invoices = $stripe->invoices()->all(["subscription"=>$billing->subscription_id]);            
                    if(isset($invoices['data'])){
                        foreach($invoices['data'] as $invoice){
                            $p_start_time =  \Carbon\Carbon::now()->startOfDay();
                            
                            if($package->day){
                                $p_end_time =  date('Y-m-d', strtotime(date('Y-m-d'). ' + '.($package->day + 1).' days'));
                            }
                            else{
                                $p_end_time = date('Y-m-d');
                            }
                            
                            $Order = Order::where("invoice_id",$invoice['id'])->first();
                            if(!$Order && $invoice['amount_remaining']==0){
                            //  dd(123);
                                $Order = new Order();
                                $Order->emp_id = $billing->user_id;
                                $Order->pack_id = $billing->package;
                                $Order->billing_id = $billing->id;
                                $Order->invoice_id = $invoice['id'];
                                $Order->trans_status = $billing->status;
                                $Order->subscription_id = $billing->subscription_id;
                                $Order->next_pay = $billing->end_date;
                                $Order->bulk_json = json_encode($invoices['data']);
                                
                                $Order->start_date = $p_start_time;
                                $Order->expire_date = $p_end_time;
                                //dd($Order);
                                $Order->save();
                                
                                if(date('Y-m-d') < $p_end_time){
                                    // dd(4444);
                                    $billing->status = "1";
                                    $billing->start_date = $p_start_time;
                                    $billing->end_date = $p_end_time;
                                    $billing->save();
                                }
                                
                                $this->mail_function->sendMailOnInvoiceGenerate($Order->id);
                            }
                        }
                    }
                }
                catch (\Exception $e) {
                    //dd($e);
                }
            }
        }
        
    }
    public function stopAllSubscriptionByUserId($user_id)
    {
        $billings = Subscription::where("user_id",$user_id)->where("status",1)->get();
        //dd($billings);
        
        foreach($billings as $billing){
            if($billing->subscription_id && $billing->subscription_id!=""){
                $stripe = Stripe::make(config('services.stripe.secret'));
                $subscription = $stripe->subscriptions()->cancel($billing->customer_stripe_id,$billing->subscription_id);
            }
        }
        Subscription::where("user_id",$user_id)->update(["status"=>3]);
    }
}
