<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    //


    protected $table = 'education';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'school', 'degree','field_of_study','grade','activities_societies','from','to','description','media'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
