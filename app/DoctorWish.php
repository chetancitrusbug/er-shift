<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class DoctorWish extends Model
{
    protected $table = 'doctor_wish';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','doctor_id', 'wish_title', 'wish_desc', 'location_id', 'ed_volum_id', 'rate', 'status','priority_id'];
    

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function doctor()
    {
        return $this->hasOne('App\DoctorDetail','id','doctor_id')->select('doctor_details.*',
            DB::raw('(CASE WHEN doctor_details.profile_image != "" THEN CONCAT( "'.url('').'",CONCAT("/images/doctor/"),doctor_details.profile_image) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS profile_image')
        )->with('user');
    }


    public function edvolume(){
        return $this->belongsTo('App\Edvolume','ed_volum_id','id')->select( 'id', 'title');
    }
    
    public function priority(){
        return $this->belongsTo('App\Priority');
    }
    
     public function location(){
        return $this->belongsTo('App\State', 'location_id');
    }
    
      public function doctor_user()
    {
        return $this->belongsTo('App\DoctorDetail','doctor_id')->with('user');
    }
    
     public function date_shifts(){
        return $this->hasMany('App\DateShift');
    }
    
      public function locations(){
        return $this->hasMany('App\Locations','field_id','id')->where( 'type', 'doctor_wish')->where('location_id', '!=', 0);
    }

    public function getCommaSepLocNamesAttribute(){
        
        $loc_arr = [] ;
        foreach($this->locations as  $loc){
            $loc_arr[] =  $loc->name ;
        }
        return  implode(', ',$loc_arr) ; 
    }


    
}
