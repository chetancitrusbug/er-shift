<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'package';
    
    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [  'title', 'description', 'amount', 'day', 'isFree','status','stripe_product_id' ];

    public function trans(){
        return $this->hasMany('App\Order','pack_id','id');
    }

    public function pack_trans(){
        return $this->hasMany('App\Order','pack_id','id')->where('emp_id', Auth::user()->id);
    }
    
}
