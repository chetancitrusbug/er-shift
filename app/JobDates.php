<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobDates extends Model
{
    protected $table = 'job_dates';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['job_id', 'job_date'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

}
