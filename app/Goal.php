<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Goal extends Model
{
     use SoftDeletes;
     public $table="goal";
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','title','details','start_date','due_date','goal_status','status','created_by'
    ];
}
