<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chat';  

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['from_id', 'to_id', 'message' , 'invitation_id' , 'read_status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public function fromuser(){
        return $this->belongsTo('App\User','from_id','id')->select( 'id', 'name');
    }
    public function touser(){
        return $this->belongsTo('App\User','to_id','id')->select( 'id', 'name');
    }
    
}
