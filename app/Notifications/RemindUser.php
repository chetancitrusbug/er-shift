<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RemindUser extends Notification
{
    use Queueable;
    public $userDetail;
	/**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->userDetail = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
 /*       return (new MailMessage)
                    ->line($this->userDetail->name)
					->line('your package expires after 7 days.please purchase new package.')
					->line('Thank you');*/
        return \Mail::send('email.reminderUser', compact('userDetail'), function ($message) use ($userDetail) {
            $message
            ->to($this->userDetail->email)
            //->to('snehal.citrusbug@gmail.com')
            ->subject('Reminder For Expire Plan');
        });
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }
}
