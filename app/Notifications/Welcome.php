<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use Lang;

class Welcome extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // echo '<pre>'; print_r($notifiable); exit;

        if($notifiable->user_type == 'employee')
        {
            return (new MailMessage)
                ->subject("Account Activation Mail")
                ->greeting('Hello, ' . $notifiable->name)
                ->line("Welcome to ErShifts.com and ErShifts mobile App.")
                ->line("We are a physician owned company that connects Emergency department shifts with Providers.")
                ->line("Having trouble getting ED shifts covered?")
                ->line("Wouldn’t it be nice to have provider availability already listed?")
                ->line("Wouldn’t you like to connect with providers who have the time to fill a shift?")
                ->line("Work efficiently and make your contact count.")
                ->line("Connect with Emergency Department Doctors, Physician assistants, Nurse Practitioner and Nurses")
                ->line("\nFast start:")
                ->line("Enter your company profile")
                ->line("List your available ED jobs/shifts")
                ->line("The mobile App matches your jobs with Providers")
                ->line("Make unlimited contact with interested providers.")
                ->line("Unlimited searches for providers")
                ->line("\nFrom all of us,")
                ->line("May you find the best shifts.")
                ->line("Contact: Staff@ErShifts.com");
        }
        else {
             return (new MailMessage)
                ->subject("Account Activation Mail")
                ->greeting('Hello, ' . $notifiable->name)
                ->line("Welcome to ErShifts.com and ErShifts mobile App.")
                ->line("We are a physician owned company that connects Emergency department shifts with Providers")
                ->line("Wouldn’t it be nice to list your available dates to work?")
                ->line("Wouldn’t it be nice to fill ED shifts at your own time and your own pay rate?")
                ->line("\nConnect with high paying shifts")
                ->line("\nFast start:")
                ->line("\nFill out your profile")
                ->line("Click on “shifts” then “Add Shift Request”")
                ->line("Add a shift request based on your available dates")
                ->line("Select priority matching criteria- location or ED volume or pay rate")
                ->line("Enter the pay rate that you are willing to accept for the shift")
                ->line("SAVE and that is it")
                ->line("The mobile app matches your results")
                ->line("You will have unlimited contact with Employers/ Recruiters")
                ->line("From all of us,")
                ->line("May you find the best shifts.")
                ->line("Contact: staff@ErShifts.com");
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
