<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Invitation extends Model
{
    protected $table = 'invitation';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['from_id', 'to_id', 'init' , 'read_status' , 'status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */


    public function user()
    {   
        return $this->belongsTo('App\User','from_id','id');
    }

    public function doctor()
    {
        if($this->init == 'doctor'){
            return $this->belongsTo('App\DoctorDetail','from_id','uid')->whereHas('user');
        }else{
            return $this->belongsTo('App\DoctorDetail','to_id','uid')->whereHas('user'); 
        }
    }

    public function employee()
    {   
        if ($this->init == 'employee') {
            return $this->belongsTo('App\EmployeeDetail', 'from_id', 'uid')->whereHas('user');
        }else{
            return $this->belongsTo('App\EmployeeDetail', 'to_id', 'uid')->whereHas('user'); 
        }
    }

    public function chat(){
        return $this->belongsTo('App\Chat','invitation_id','invitation_id')->select('chat.*',DB::raw('(select max(`created_at`) from chat) AS chatDate'));
        
    }

    public function  other_user($id)
    {   
       if($this->from_id == $id){
          if($this->init == 'doctor'){
            return  $this->employee ;
          }
          if($this->init == 'employee'){
            return  $this->doctor ;
          }
       }
       if($this->to_id == $id ){
        if($this->init == 'doctor'){
          return  $this->doctor ;
        }
        if($this->init == 'employee'){
          return  $this->employee ;
        }
     }

    }
    
}
