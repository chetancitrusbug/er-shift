<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facebookresponse extends Model
{
    public $table="facebook_response";
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','fr_lgn_id','fr_response','status'
    ];
}
