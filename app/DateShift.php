<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DateShift extends Model
{
    protected $table = 'date_shift';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['doctor_wish_id', 'doctor_id', 'from', 'to', 'shift_id', 'status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function shift()
    {
        return $this->hasOne('App\Shift', 'id', 'shift_id');
    }

    
}
