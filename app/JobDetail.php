<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class JobDetail extends Model
{
    //

    protected $table = 'job_detail';

        /**
        * The database primary key value.
        *
        * @var string
        */
        protected $primaryKey = 'id';
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [  'job_title', 'job_responsibility', 'profession_id', 'speciality_id', 'degree_id', 'experience_id', 'address', 'location_id', 'ed_volume_id', 'from_date' , 'to_date' , 'shift_id', 'rate', 'status','employee_id' ];

        public function experience(){
            return $this->belongsTo('App\Experience')->select( 'id', 'title');
        }
        public function employee()
        {
            return $this->belongsTo('App\EmployeeDetail','employee_id','uid')->whereHas('user')->select('id', 'uid', 'emp_name', 'comp_name', 'comp_website','comp_information','tel_number','location_id','address',
            DB::raw('(CASE WHEN employee_detail.profile_pic != "" THEN CONCAT( "'.url('').'",CONCAT("/images/employee/"),employee_detail.profile_pic) ELSE "'.url('/assets/images/avatar.jpg').'" END) AS profile_pic'));
        }

        public function shift(){
            return $this->belongsTo('App\Shift')->select( 'id', 'title');
        }

        public function ed_volume(){
            return $this->belongsTo('App\Edvolume','ed_volume_id','id')->select( 'id', 'title');
        }

        public function edvolume(){
            return $this->belongsTo('App\Edvolume','ed_volume_id','id')->select( 'id', 'title');
        }

        public function profession(){
            return $this->belongsTo('App\Profession','profession_id','id')->select( 'id', 'title');
        }

        public function speciality(){
            return $this->belongsTo('App\Speciality')->select( 'id', 'title');
        }

        public function degree(){
            return $this->belongsTo('App\Degree')->select( 'id', 'title');
        }

        public function location(){
            return $this->belongsTo('App\State')->select( 'id', 'name');
        }

        public function favourite(){
            return $this->hasMany('App\Favourite','job_id','id')->select( 'id', 'user_type','user_id','job_id');
        }

        public function employee_user()
        {
            return $this->belongsTo('App\EmployeeDetail','employee_id', 'uid')->whereHas('user')->with('user');
        }

        public function job_dates(){
            return $this->hasMany('App\JobDates','job_id','id');
        }

}
