<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Goal;

class Goalstatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Goalstatus:goalstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Expired Goal Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currentdate = date('Y-m-d');
        $goaldata = Goal::where('status','active')->where('goal_status',0)->where('due_date','<',$currentdate)         
				->get();
        foreach($goaldata as $goalsdata){
            $goalsdata->goal_status='2';
            $goalsdata->save();
        }
        echo"Goal Status Updated Successfully";
        exit;
    }
}
