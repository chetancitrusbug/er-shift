<?php

namespace App\Console\Commands;
use App\User;
use App\Subscription;
use App\Order;
use Illuminate\Console\Command;
use Cartalyst\Stripe\Laravel\Facades\Stripe;

class renewalSubscriptionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'RenewalSubscriptionCommand:renewalSubscriptionCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stripe = Stripe::make(config('services.stripe.secret'));
        Subscription::where("status",1)->where("end_date","<=",\Carbon\Carbon::now())->update(["status"=>"4"]);
        
        $billings = Subscription::where("status","!=",3)->where("status","!=",2)->where("end_date","<=",\Carbon\Carbon::now()->addHour(12))->get();
        //dd($billings);
        foreach($billings as $billing){
            $package = json_decode($billing->package_detail);
            if($package != null)
            {
                try { 
                    $invoices = $stripe->invoices()->all(["subscription"=>$billing->subscription_id]);

                    if(isset($invoices['data'])){
                        foreach($invoices['data'] as $invoice){
                            $p_start_time =  date('Y-m-d');
                            
                            if($package->day){
                                $p_end_time =  date('Y-m-d', strtotime(date('Y-m-d'). ' + '.($package->day + 1).' days'));
                            }
                            else{
                                $p_end_time = date('Y-m-d');
                            }
                            $Order = Order::where("invoice_id",$invoice['id'])->first();
                            if(!$Order && $invoice['amount_remaining'] == 0){
                                $Order = new Order();
                                $Order->emp_id = $billing->user_id;
                                $Order->pack_id = $billing->package;
                                $Order->billing_id = $billing->id;
                                $Order->invoice_id = $invoice['id'];
                                $Order->trans_status = $billing->status;
                                $Order->subscription_id = $billing->subscription_id;
                                $Order->next_pay = $billing->end_date;
                                $Order->bulk_json = json_encode($invoices['data']);
                                
                                $Order->start_date = $p_start_time;
                                $Order->expire_date = $p_end_time;
                                $Order->save();
                                
                                $billing->status = "1";
                                $billing->start_date = $p_start_time;
                                $billing->end_date = $p_end_time;
                                $billing->save();
                                
                                
                                $this->mail_function->sendMailOnInvoiceGenerate($Order->id);
                            }
                        }
                    }
                }
                catch (\Exception $e) {
                    //dd($e);
                }
            }
        }
    }
}
