<?php

namespace App\Console\Commands;
use App\User;
use App\Subscription;
use App\Setting;
use Illuminate\Console\Command;
use App\Notifications\RemindUser;
use App\Notifications\RemindAdmin;
use App\Http\Controllers\Controller;

class NotificationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    private $notification;
    
    protected $signature = 'NotificationCommand:notificationCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recommended Jobs/Wish';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->notification = new Controller();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userDetails  = User::where('is_active','1')->get();
        foreach($userDetails as $user){
            $fields['data'] = array(
                'title' => 'Recommended Wish',
                'message' => 'New Wishes found',
                'type' => 'home',
                'user_type' => $user->user_type,
                'user_id' => $user->id,
                
            );
            $fields['to'] = $user->firebasetoken;
            $fields['device_token'] = $user->device_token;
            $fields['device_type'] = $user->device_type;
            $this->notification->sendPushNotification($fields);
        }
		
    }
}
