<?php

namespace App\Console\Commands;
use App\User;
use App\Subscription;
use App\Setting;
use Illuminate\Console\Command;
use App\Notifications\RemindUser;
use App\Notifications\RemindAdmin;


class reminderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ReminderCommand:reminderCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Plan Expire reminder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Enddate = date('Y-m-d', strtotime(date('Y-m-d'). ' + 7 days'));

        $subscriptions = Subscription::where('end_date',$Enddate)->where('status',1)->get();
		//dd($subscriptions);
		foreach($subscriptions as $key => $value){
			$user_id = $value->user_id;
			$userDetails  = User::where('id',$user_id)->first();
            $Setting = Setting::where('key','admin_email')->first();
            $adminEmailId['email'] = ((($Setting) && ($Setting->value != null)) ? $Setting->value : '');
            //dd($adminEmailId);
            if($userDetails != null){
                $adminEmailId['userDetails'] = $userDetails;

                // Admin Email Notification
                \Mail::send('email.reminderAdmin', compact('adminEmailId'), function ($message) use ($adminEmailId) {
                    $message
                    //->to($adminEmailId['email'])
                    ->to('snehal.citrusbug@gmail.com')
                    ->subject('Reminder For Expire Plan');
                });
                //$userDetails->notify(new RemindUser($userDetails));

                // USer Email Notification
                return \Mail::send('email.reminderUser', compact('userDetails'), function ($message) use ($userDetails) {
                    $message
                    ->to($userDetails->email)
                    //->to('snehal.citrusbug@gmail.com')
                    ->subject('Reminder For Expire Plan');
                });

                // push notification array
                if($userDetails->device_token != null && $userDetails->device_type != null){
                    $notificationArray = array();
                    $notificationArray['device_token'] = $userDetails->device_token;
                    $notificationArray['device_type'] = $userDetails->device_type;
                    $notificationArray['msg'] = 'Your package expires after 7 days.please purchase new package.';

                }

			}
		}
    }
}
