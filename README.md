# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


Query
==========
ALTER TABLE `doctor_wish` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

In doctor_wish set primery_key.
In doctor-wish table delete priority field.
In doctor-wish table add priority_id field.
In doctor-wish table add rate field make float.


ALTER TABLE `employee_detail` CHANGE `address` `address` VARCHAR(191) NULL DEFAULT NULL;

ALTER TABLE `subscription` CHANGE `subscription_id` `subscription_id` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `subscription` CHANGE `customer_stripe_id` `customer_stripe_id` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `subscription` CHANGE `package_detail` `package_detail` LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `transaction` CHANGE `invoice_id` `invoice_id` VARCHAR(255) NULL DEFAULT NULL;
ALTER TABLE `transaction` CHANGE `bulk_json` `bulk_json` LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE package AUTO_INCREMENT = 900;