<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],
    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY', 'pk_test_7Barys4nP4dexRYSLTjN3gnI'),
        'secret' => env('STRIPE_SECRET', 'sk_test_99gZgjwIk0zD8Feiuv8hFFPN'),
        // 'version' => '2020-04-27',
    ],
    'linkedin' => [
        'client_id' =>  env('LI_CLIENT_ID', '81t02ojzcdth54') ,
        'client_secret' => env('LI_CLIENT_SECRET', 'lhPJE8TUALQHIsN8') ,
        'redirect' => env('LI_REDIRECT_URL', 'http://localhost/er-shift/public/auth/linkedin/callback')
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID', '861763924332324'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET', '741e566ba17299492d8b4c35a2d2e196'),
        'redirect' => env('FACEBOOK_CALLBACK_URL', 'http://localhost/er-shift/public/auth/facebook/callback'),
    ],
    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID', '976488637935-7rm10uullhn2nr3rb1n4rnf2hkun0vil.apps.googleusercontent.com'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET', 'I8UBApnmTsAmR-MJ9KkciB54'),
        'redirect' => env('GOOGLE_CALLBACK_URL', 'http://localhost/er-shift/public/auth/google/callback'),
    ],

]; 
