<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected function insertAndReturnId($string, $parent_id = 0)
    {
        $a = explode('|', $string);

        $ins = [
            'name' => trim($a[0]),
            'label' => trim($a[1]),
        ];

        $permission = Permission::create($ins);

        return $permission->id;
    }

    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Permission::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');



        $arr = [
            'access.users|Can Access Users' =>
                [
                    'access.user.create|Can Create User',
                    'access.user.edit|Can Edit User',
                    'access.user.delete| Can Delete User'
                ],


            'access.roles|Can Access Roles' =>
                [
                    'access.role.create|Can Create Role',
                    'access.role.edit|Can Edit Role',
                    'access.role.delete| Can Delete Role'
                ],

            'access.permissions|Can Access Permission' =>
                [
                    'access.permission.create|Can Create Permission',
                    'access.permission.edit|Can Edit Permission',
                    'access.permission.delete| Can Delete Permission'
                ],
            
             
		];





        foreach ($arr as $k => $v) {


            $id = $this->insertAndReturnId($k);


            foreach ($v as $string) {
                $this->insertAndReturnId($string, $id);

            }


        }


		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Role::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $role = Role::create(//1
            [
                'name' => 'SU',
                'label' => 'Super User'
            ]);


        $role->permissions()->sync(\App\Permission::pluck('id'));


        $arr = [

            [
                'name' => 'Emp',
                'label' => 'Employee'
            ],
            
        ];

        foreach ($arr as $a) {
            Role::create($a);
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $user = User::create(
            [
                'name' => 'Wikitu',
                'email' => 'wikitudedev@gmail.com',
                'user_type' => '0',
                'login_type' => '0',
                'password' => bcrypt(123456),
            ]
        );


        if (!$user->hasRole('SU')) {
            $user->assignRole('SU');

        }
        

    }

}
