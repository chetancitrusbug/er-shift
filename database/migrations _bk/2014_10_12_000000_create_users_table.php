<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('user_type');
            $table->string('login_type');
            $table->string('email');
            $table->string('password')->nullable();
            $table->string('social_media_id')->nullable();
            $table->string('api_token')->nullable();
            $table->string('activation_token')->nullable();
            $table->dateTime('activation_time')->nullable();
            $table->string('device_token')->nullable();
            $table->string('device_type')->nullable();
            $table->integer('is_active')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
