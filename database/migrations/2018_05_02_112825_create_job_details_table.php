<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('job_title')->nullable();
            $table->string('job_responsibility')->nullable();
            $table->integer('profession_id')->nullable();
            $table->integer('speciality_id')->nullable();
            $table->integer('degree_id')->nullable();
            $table->integer('experience_id')->nullable();
            $table->text('address')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('ed_volume_id')->nullable();
            $table->string('date_available')->nullable();
            $table->integer('shift_id')->nullable();
            $table->integer('rate')->nullable();
            $table->string('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_detail');
    }
}
