<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldInSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscription',function(Blueprint $table){
            $table->integer('customer_stripe_id')->default(1);
            $table->string('package_detail')->nullable();
            $table->string('plan_id')->nullable();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumn('customer_stripe_id');
        Schema::dropColumn('package_detail');
        Schema::dropColumn('plan_id');
    }
}
