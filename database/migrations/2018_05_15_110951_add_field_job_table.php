<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_detail',function(Blueprint $table){
            $table->dropColumn('date_available');
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_detail',function($table){
            $table->date('date_available')->nullable();
            $table->dropColumn('from_date');
            $table->dropColumn('to_date');
       });
    }
}
