<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction',function(Blueprint $table){
            $table->integer('subscription_id')->default(1);
            $table->string('next_pay')->nullable();
            $table->string('bulk_json')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumn('subscription_id');
        Schema::dropColumn('next_pay');
        Schema::dropColumn('bulk_json');
    }
}
