<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->nullable();;
            $table->string('profile_image')->nullable();
            $table->string('profile_name')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('tel_number')->nullable();
            $table->enum('resume_type',['link', 'attachment'])->nullable();   
            $table->string('resume')->nullable();
            $table->integer('profession_id')->nullable();
            $table->integer('speciality_id')->nullable();
            $table->integer('experience_id')->nullable();
            $table->string('degree_id')->nullable();
            $table->string('state_id')->nullable();
            $table->string('license_id')->nullable();
            $table->string('location_id')->nullable();
            $table->string('ed_volume_id')->nullable();
            $table->string('shift_id')->nullable();
            $table->string('priority_id')->nullable();
            $table->double('rate', 10, 2)->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_details');
    }
}
