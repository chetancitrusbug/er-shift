<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDoctorDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('doctor_details',function($table){
            $table->dropColumn('state_id');
            $table->dropColumn('location_id');
            $table->dropColumn('ed_volume_id');
            $table->dropColumn('shift_id');
            $table->dropColumn('priority_id');
            $table->dropColumn('rate');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctor_details',function($table){
            $table->string('state_id')->nullable();
            $table->string('location_id')->nullable();
            $table->string('ed_volume_id')->nullable();
            $table->string('shift_id')->nullable();
            $table->string('priority_id')->nullable();
            $table->double('rate', 10, 2)->nullable();
        });
    }
}
