<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDateShiftTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('date_shift', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doctor_wish_id')->nullable();
            $table->integer('doctor_id')->nullable();
            $table->date('from')->nullable();
            $table->date('to')->nullable();
            $table->integer('shift_id')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('date_shift');
    }
}
