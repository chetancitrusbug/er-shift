<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorWishTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_wish', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doctor_id')->nullable();
            $table->string('wish_title')->nullable();
            $table->text('wish_desc')->nullable();
            $table->string('location_id')->nullable();
            $table->string('ed_volum_id')->nullable();
            $table->date('available_date')->nullable();
            $table->string('shift_id')->nullable();
            $table->double('rate', 10, 2)->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_wish');
    }
}
