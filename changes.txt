[10:26 AM, 10/22/2018] Ishan Vyas: ER Shift changes

This page: ershifts.com/price should be the following. Take out the FAQ portion for now

1.	How much does it cost to use?
The site is free for all Employers during beta testing. Any further changes will be announced.
The site is always free for all medical providers

2.	What do I download?
Employers can download the mobile app ErShifts from the app store or google play
Employers can input multiple shifts on the web login but not the mobile apps
Medical Staff have access to the mobile app only

[10:27 AM, 10/22/2018] Ishan Vyas: Hope this helps
ERRORS:
Web:
-	Contact us: broken link
-	Privacy policy not working
https://termsfeed.com/privacy-policy/0510c84b31f37123f02da21fe7aa97d2

-	Terms and conditions link not working
https://termsfeed.com/terms-conditions/b3c190c83e42ebd8eee6a6a9aed70294


Admin log in
-	Under Employer, add new employee should be add new employer

WEB Employer log in:
1.	Typos (I did not catch this in time)
-	“Your plan expires in “ ….. not “expired”
Every location with “Expired” should be “expires”
“Plan expired on” should be “Plan expires in”
-	Change Add job change to “Add 1 Shift”
-	“Add bulk job” change to “Add Multiple shifts”
-	Anywhere with “jobs” should be changed to “Shift" or "Shifts" base on plural tense
-	"Job title" and "job responsibility" change to "Shift title", "Shift responsibility"

-	Under create job link
o	Rate should be "Rate ($/hr) and should have easy rate selection with mouse... any that is good for web and does not have to be like mobile. I am trying to make it easier for employer. Click on pay bar or have a number selection like currency (---.00/ HR)

o	From and to dates should have easy calender with mouse- 1 day or multiple days


2.	ON this page http://ershifts.com/addBlukJobs

-	Dowanload CSV Proper Data
-	You need to use this profession, speciality and degree for bulk upload job

SHOULD be- “Download sample csv format for multiple jobs”
I think we should just have these info on the page without download so all can see….like this-

To upload multiple shifts please use the format below. We highly encourage you to have the pay rate. You will get more inquiries.

CSV format
Shift Title, Shift Responsibility, Profession,Speciality, Degree, Experience, Address, Location, Ed volume,From Date,To Date, Shift, Rate

Data points
--- Profession ---	--- Speciality ---
Nurse	Registered Nurse
--- Profession ---	--- Speciality ---
Physician Assistant	Emergency Medicine
--- Profession ---	--- Speciality ---
Nurse Practitioner	Family Nurse Practitioner
Nurse Practitioner	Pediatrics
--- Profession ---	--- Speciality ---
Physician	Med-Peds
Physician	Internal Medicine
Physician	General Surgery
Physician	Family Practice
Physician	Pediatrics
Physician	Emergency medicine
--- Profession ---	--- Degree ---
Nurse	RN
Nurse	BSN
--- Profession ---	--- Degree ---
Physician Assistant	MPAS, MHS, MMSc
--- Profession ---	--- Degree ---
Nurse Practitioner	Doctor of Nursing Practice
Nurse Practitioner	Masters
--- Profession ---	--- Degree ---
Physician	M.D
Physician	DO
Physician	MBBS

OR you can send the file to Staff@ershifts.com and we will gladly upload it for you



Mobile APP:
-	Linkedin log in not working
-	Under resume add the text (web address) after “link”
So it looks like Link (http://MyResumePage.com) but "http://" is permanent

Employer log in
In profile: “Website link” put permanent "http://" in text field to avoid errors



UPDATE to help me launch the app:
admin:
-	Ability to load multiple jobs for employers by Admin
-	Is there a way to have that home page video stay in that space permanently with a pause so users can see a snipet before it plays like videos appear in FB.
-	I need access to dynamic welcome emails
-	How to connect mailchimp to capture new users